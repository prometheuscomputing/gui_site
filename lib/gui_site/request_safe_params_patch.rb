class Ramaze::Request
  # Similar to #params but returned parameters have been sanitized
  def safe_params
    safe_params = {}
    self.params.each do |name, value|
      safe_params[name] = clean_param(value)
    end
    safe_params
  end
  
  # Sanitizes the passed parameter value
  def clean_param(value)
    case value
    when Array # Not sure if this ever happens
      value.map { |e| clean_param(e) }
    when Hash # For multiple pieces of form-data associated with the same name (e.g. user_roles[1], user_roles[2], etc.)
      h = {}
      # Key is typically a numeric value, but cleaning anyway since I don't know if this is always the case. -SD
      value.each { |k,v| h[clean_param(k)] = clean_param(v) }
      h
    when Tempfile # should already be ok.
      value
    when Symbol
      value
    when NilClass
      value
    when String
      # Check for XSS attack. Do not attempt to clean if found, just error out.
      # These regex's are copied from HP's webinspect suggestions
      # raise "XSS attack detected!" if value =~ /((\%3C) <)((\%2F) \/)*[a-z0-9\%]+((\%3E) >)/ix
      raise "XSS attack detected (paranoid)!" if value =~ /((\%3C) <)[^\n]+((\%3E) >)/i
      value #.sanitize # Sanitize is disabled until we can find a solution that doesn't destroy Latex
    else
      # For now, just raise if unexpected type is found.
      raise "Unexpected parameter type: #{value.class}"
      #value.to_s.sanitize
    end
  end    
  
  # Returns a single sanitized parameter
  def safe_param(name)
    clean_param(self[name])
  end
  
  # Sanitization method for URLs.
  # Currently this does nothing.
  # There isn't a security problem provided that the URL isn't stored
  # and the requested link is allowed to be absolute (go to another site besides the server).
  def safe_url_param(name)
    value = self[name]
    value
  end
  
  # Sanitization method for relative URLs.
  # Ensures that the requested link is relative.
  def safe_relative_url_param(name)
    value = self[name]
    unless URI.parse(value).host.nil?
      raise "Invalid relative URL: #{value}"
    end
    value
  end
  
  def to_safe_instance_variables(*args)
    instance = Current.action.instance
    args.each do |arg|
      next unless safe_value = safe_param(arg)
      instance.instance_variable_set("@#{arg}", safe_value)
    end
  end
end