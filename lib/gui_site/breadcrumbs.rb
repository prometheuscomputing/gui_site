require_relative 'url_encoder'

module Gui
  class Controller < WebFrameworkController
    def add_breadcrumb(url)
      return if url == Gui.option(:home_page)[:url] # Do not add Home breadcrumb
      flash[:current_breadcrumbs] ||= Gui::NavigationPath.new 
      flash[:current_breadcrumbs].add_url(url)
    end
  
    def add_breadcrumb_node(node)
      flash[:current_breadcrumbs] ||= Gui::NavigationPath.new 
      flash[:current_breadcrumbs].add_node(node)
    end
  
    def remove_last_breadcrumb
      flash[:current_breadcrumbs].remove_current_node if flash[:current_breadcrumbs]
      flash[:current_breadcrumbs] ||= Gui::NavigationPath.new 
    end
  
    def reset_breadcrumbs
      # Passing ['/'] results in the Home breadcrumb
      # flash[:current_breadcrumbs] = Gui::NavigationPath.new(['/'])
      flash[:current_breadcrumbs] = Gui::NavigationPath.new
    end
  
    def current_breadcrumbs
      bcs = Ramaze::Current.session.flash[:current_breadcrumbs]
      bcs ||= Gui::NavigationPath.new
      bcs.path.reverse
    end
  
    def breadcrumbs_previous_page
      flash[:current_breadcrumbs] ||= Gui::NavigationPath.new
      flash[:current_breadcrumbs].previous_page
    end
  end
end

module Gui
  class NavigationPath
    attr_reader :path

    def initialize(breadcrumb_urls_or_objs = [])
      @path = []
      breadcrumb_urls_or_objs.each do |url_or_obj|
        #domain_object, params = Gui.create_object_from_url(url)
        node = url_or_obj.is_a?(String) ? NavigationNode.from_url(url_or_obj) : NavigationNode.from_domain_obj(url_or_obj)
        @path.unshift(node)
      end
    end
    
    # Return the current node (or nil if one is not present in path)
    def current_node
      @path.first
    end

    # Return the previous node (or nil if one is not present in path)
    def previous_node
      @path[1]
    end

    def add_node page_node
      if page_node.is_a?(UrlNavigationNode) || page_node.domain_obj_class && page_node.domain_obj_id
        # Look for node in path history, starting with oldest entry.
        index_in_path = @path.rindex(page_node)
        if index_in_path # Node is already in path history. Shift path to just before that node.
          @path.shift(index_in_path + 1)
        end
        @path.unshift(page_node)
      end # If no domain object for passed node, ignore add_page request
    end
    
    def add_url(url)
      add_node(NavigationNode.from_url(url))
    end
    
    def remove_current_node
      @path.shift
    end
    
    def length
      @path.length
    end
    
    # Deprecated methods
    
    # TODO: remove use of this method in favor of #current_node
    def current_page
      current_node ? current_node.to_url : nil
    end
    
    # TODO: remove use of this method in favor of #previous_node
    def previous_page
      previous_node ? previous_node.to_url : nil
    end
    
    def remove_current_page
      remove_current_node
    end
  end # NavigationPath
  
  class NavigationNode
    # A hash containing view-name, view-classifier, and view-type information
    attr_accessor :view_hash
    attr_accessor :package
    attr_accessor :domain_obj_class
    attr_accessor :domain_obj_id
    attr_accessor :domain_obj
    
    def initialize(package, domain_obj_class, domain_obj_id, domain_obj, view_hash = {})
      @package          = package.to_s
      @domain_obj_class = domain_obj_class.to_s
      @domain_obj_id    = domain_obj_id.to_s
      @view_hash        = view_hash
      @domain_obj       = domain_obj || @domain_obj_class.to_const[@domain_obj_id.to_i]
    end
    
    # Not truly equality; If this node has a nil pk, then it will match any other_node of the same domain_object klass.
    # The nil-id matching is one-way (if other_node has a nil id, it will not match). This makes adding pages more convenient.
    def ==(other_node)
      self.class == other_node.class && \
        self.domain_obj_class == other_node.domain_obj_class && \
        ((self.domain_obj_id.nil? || self.domain_obj_id == 'new') || (self.domain_obj_id == other_node.domain_obj_id))
    end

    def to_title
      return domain_obj_title if domain_obj
      self.domain_obj_class.to_s.split('::').last.to_title
    end
    
    def domain_obj_title
      # remove HTML
      domain_obj.to_title.gsub(/<.*?>/, '')
    end
    
    def to_url
      Gui.create_url_from_breadcrumb(self)
    end
    
    def to_history_url
      Gui.create_history_url_from_breadcrumb(self)
    end
    
    def to_clone_url
      Gui.create_clone_url_from_breadcrumb(self)
    end
    
    def self.from_url(url)
      Gui.create_breadcrumb_from_url(url)
    end
    
    def self.from_domain_obj(obj)
      Gui.create_breadcrumb_from_url(Gui.create_url_from_object(obj), obj)
    end
  end
  
  class UrlNavigationNode < NavigationNode
    attr_accessor :url
    attr_accessor :label
    
    def initialize(url, label = nil)
      @url = url
      @label = label
    end
    
    def ==(other_node)
      self.class == other_node.class && @url == other_node.url
    end
    
    def to_title
      @label || @url.split('/').last.to_capitalized_title
    end
  end
end
