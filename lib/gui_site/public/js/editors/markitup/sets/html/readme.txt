Markup language: 
	HTML

Description:
	A basic HTML markup set with Headings, Paragraph, Bold, Italic, Stroke through, Picture, Link, List, Clean button, Preview button.

Install:
	- Download the zip file
	- Unzip it in your markItUp! sets folder
	- Modify your JS link to point at this set.js