// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Html tags
// http://en.wikipedia.org/wiki/html
// ----------------------------------------------------------------------------
// Basic set. Feel free to add more tags
// ----------------------------------------------------------------------------
latexSettings = {	
  nameSpace:		"latex", // Useful to prevent multi-instances CSS conflict
  previewParserPath:	'/latex_preview/',
  previewParserVar: 'data', 
  onShiftEnter:   {keepDefault:false, replaceWith:'<br />\n'},
  onShiftEnter:   {keepDefault:false, openWith:'\n'},
  onEnter:        {keepDefault:false, openWith:'\n'},
  onCtrlEnter:    {keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
  onTab:          {keepDefault:false, replaceWith:'    '},
  markupSet:  [
    {name:'Bold', className: 'strongButton', openWith:'\\textbf{', closeWith:'}' },
    {name:'Italic', className: 'italicButton', openWith:'\\textit{', closeWith:'}'  },
    {name:'Underline', className: 'underlineButton', openWith:'\\underline{', closeWith:'}' },
    // {name:'Stroke through', className: 'strikethroughButton', key:'S', openWith:'\\sout{', closeWith:'}' },
    // {name: 'Small Caps', className: 'smallCapsButton', openWith:'\\textsc{', closeWith:'}'},
    // {separator:'---------------' },
    // {name:'Align Left', className:'alignLeftButton', openWith:'\\begin{flushleft}\n', closeWith:'\n\\end{flushleft}'},
    // {name:'Align Center', className:'alignCenterButton', openWith:'\\begin{center}\n', closeWith:'\n\\end{center}'},
    // {name:'Align Right', className:'alignRightButton', openWith:'\\begin{flushright}\n', closeWith:'\n\\end{flushright}'},
    // {separator:'---------------' },
    {name:'Small Skip', className:'smallSkipButton', openWith:'\\smallskip\n'},
    {name:'Medium Skip', className:'mediumSkipButton', openWith:'\\medskip\n'},
    {separator:'---------------' },
    {name:'Superscript', className:'superscriptButton', openWith:'$^{', closeWith:'}$'},
    {name:'Subscript', className:'subscriptButton', openWith:'$_{', closeWith:'}$'},
    {name:'Chemical Equation', className:'chemicalEquationButton', openWith:'\\ce{', closeWith:'}'},
    {name:'Partial Pressure of Oxygen', className:'partialPressureOxygen', openWith:'\\textit{P}$_{\\ce{O2}}$'},
    {name:'Degrees Celsius', className:'degree', openWith:'$^{\\circ}$C'},
    {separator:'---------------', name:'Math' },
    {name:'Greek Letters',    className:'greekCharacters', dropMenu: [
      {name:'α - alpha',    className:'alphaLower',   replaceWith:'$\\alpha$' },
      {name:'Α - Alpha',    className:'alphaUpper',   replaceWith:'$\\Alpha$' },
      {name:'β - beta',     className:'betaLower',    replaceWith:'$\\beta$' },
      {name:'Β - Beta',     className:'betaUpper',    replaceWith:'$\\Beta$' },
      {name:'γ - gamma',    className:'gammaLower',   replaceWith:'$\\gamma$' },
      {name:'Γ - Gamma',    className:'gammaUpper',   replaceWith:'$\\Gamma$' },
      {name:'δ - delta',    className:'omegaUpper',   replaceWith:'$\\delta$' },
      {name:'Δ - Delta',    className:'deltaUpper',   replaceWith:'$\\Delta$' },
      {name:'ε - epsilon',  className:'omegaUpper',   replaceWith:'$\\epsilon$' },
      {name:'Ε - Epsilon',  className:'epsilonUpper', replaceWith:'$\\Epsilon$' },
      {name:'ζ - zeta',     className:'omegaUpper',   replaceWith:'$\\zeta$' },
      {name:'Ζ - Zeta',     className:'zetaUpper',    replaceWith:'$\\Zeta$' },
      {name:'η - eta',      className:'omegaUpper',   replaceWith:'$\\eta$' },
      {name:'Η - Eta',      className:'etaUpper',     replaceWith:'$\\Eta$' },
      {name:'θ - theta',    className:'omegaUpper',   replaceWith:'$\\theta$' },
      {name:'Θ - Theta',    className:'theatUpper',   replaceWith:'$\\Theta$' },
      {name:'ι - iota',     className:'omegaUpper',   replaceWith:'$\\iota$' },
      {name:'Ι - Iota',     className:'iotaUpper',    replaceWith:'$\\Iota$' },
      {name:'κ - kappa',    className:'omegaUpper',   replaceWith:'$\\kappa$' },
      {name:'Κ - Kappa',    className:'kappaUpper',   replaceWith:'$\\Kappa$' },
      {name:'λ - lambda',   className:'omegaUpper',   replaceWith:'$\\lambda$' },
      {name:'Λ - Lambda',   className:'lambdaUpper',  replaceWith:'$\\Lambda$' },
      {name:'μ - mu',       className:'omegaUpper',   replaceWith:'$\\mu$' },
      {name:'Μ - Mu',       className:'muUpper',      replaceWith:'$\\Mu$' },
      {name:'ν - nu',       className:'omegaUpper',   replaceWith:'$\\nu$' },
      {name:'Ν - Nu',       className:'nuUpper',      replaceWith:'$\\Nu$' },
      {name:'ξ - xi',       className:'omegaUpper',   replaceWith:'$\\xi$' },
      {name:'Ξ - Xi',       className:'xiUpper',      replaceWith:'$\\Xi$' },
      {name:'ο - omicron',  className:'omegaUpper',   replaceWith:'o' },
      {name:'Ο - Omicron',  className:'omicronUpper', replaceWith:'O' },
      {name:'π - pi',       className:'omegaUpper',   replaceWith:'$\\pi$' },
      {name:'Π - Pi',       className:'piUpper',      replaceWith:'$\\Pi$' },
      {name:'ρ - rho',      className:'omegaUpper',   replaceWith:'$\\rho$' },
      {name:'Ρ - Rho',      className:'rhoUpper',     replaceWith:'$\\Rho$' },
      {name:'σ - sigma',    className:'omegaUpper',   replaceWith:'$\\sigma$' },
      {name:'Σ - Sigma',    className:'sigmaUpper',   replaceWith:'$\\Sigma$' },
      {name:'τ - tau',      className:'omegaUpper',   replaceWith:'$\\tau$' },
      {name:'Τ - Tau',      className:'tauUpper',     replaceWith:'$\\Tau$' },
      {name:'υ - upsilon',  className:'omegaUpper',   replaceWith:'$\\upsilon$' },
      {name:'Υ - Upsilon',  className:'omegaUpper',   replaceWith:'$\\Upsilon$' },
      {name:'φ - phi',      className:'omegaUpper',   replaceWith:'$\\phi$' },
      {name:'Φ - Phi',      className:'omegaUpper',   replaceWith:'$\\Phi$' },
      {name:'χ - chi',      className:'omegaUpper',   replaceWith:'$\\chi$' },
      {name:'Χ - Chi',      className:'omegaUpper',   replaceWith:'$\\Chi$' },
      {name:'ψ - psi',      className:'omegaUpper',   replaceWith:'$\\psi$' },
      {name:'Ψ - Psi',      className:'omegaUpper',   replaceWith:'$\\Psi$' },
      {name:'ω - omega',    className:'omegaUpper',   replaceWith:'$\\omega$' },
      {name:'Ω - Omega',    className:'omegaUpper',   replaceWith:'$\\Omega$' }
    ]},    
    {separator:'---------------' },
    {name:'Caption Section', className:'captions', openWith:'\\begin{diagram_captions}\n', closeWith:'\n\\end{diagram_captions}'},
    {name:'Diagram Caption', className:'phaseDiagram', openWith:'\\diagram{', closeWith:'}'},
    {separator:'---------------' },
    {name:'Numbered Equation', className:'numberedEquationButton', openWith:'\\begin{equation}\n', closeWith:'\n\\end{equation}\n'},
    {name:'Unnumbered Equation', className:'unnumberedEquationButton', openWith:'\\begin{equation*}\n', closeWith:'\n\\end{equation*}\n'},
    
    {name:'Math Symbols',    className:'mathSymbols', dropMenu: [
      {name:' \u2113 &nbsp&nbsp script "l"',    className:'scriptEll',   replaceWith:'$\\ell$' },
      {name:' ln &nbsp&nbsp natural log',    className:'naturalLog',   replaceWith:'$\\ln$'},
      {name:' ± &nbsp&nbsp plus minus',    className:'plusMinus',   replaceWith:'$\\pm$'},
      {name:' \' &nbsp&nbsp prime',    className:'prime',   replaceWith:"$'$"},
      {name:' \'\' &nbsp&nbsp double prime',    className:'doublePrime',   replaceWith:'$\'\'$'},
      {name:' ↔ &nbsp&nbsp left/right arrow',    className:'lrArrow',   replaceWith:'$\\leftrightarrow$'},
      {name:' ← &nbsp&nbsp left arrow',    className:'leftArrow',   replaceWith:'$\\leftarrow$'},
      {name:' → &nbsp&nbsp right arrow',    className:'rightArrow',   replaceWith:'$\\rightarrow$'},
      {name:' < &nbsp&nbsp less than',    className:'lessThan',   replaceWith:'$<$'},
      {name:' > &nbsp&nbsp greater than',    className:'greaterThan',   replaceWith:'$>$'},
      {name:' ≤ &nbsp&nbsp less than or equal',    className:'lessThanOrEqual',   replaceWith:'$\\leq$'},
      {name:' ≥ &nbsp&nbsp greater than or equal',    className:'greaterThanOrEqual',   replaceWith:'$\\geq$'},
      {name:' · &nbsp&nbsp center dot',    className:'centerDot',   replaceWith:'$\\cdot$'},
      {name:' Å  &nbsp&nbsp Angstrom',    className:'angstrom',   replaceWith:'\\AA'},
    ]},
    
    {separator:'---------------' },
    {name: 'Insert Table', className:'insertTable', replaceWith:function(h) {return startLatexTable(h);}},
    {name:'Normal Text\n(for text in S column)', className:'curlyBracesButton', openWith:'{', closeWith:'}'},
    
    {separator:'---------------' },
    {name:'Preview', call:'preview', className:'preview'},
    
    // Using 'beforeInsert' for the callback even though nothing is inserted. 'call' does not pass on the context.
    {name:'Download PDF', beforeInsert:function(h) {latexPdfPreview($(h.textarea).val());}, className:'download'},
    
    {separator:'---------------', name:'language_selector' }
  ]
}

function latexPdfPreview(data) {
  // form_element = $('<form id="pdf_preview_form" action="/latex_preview/pdf" method="POST">' + 
  //   '<input type="hidden" name="' + latexSettings.previewParserVar + '" value="' + data + '">' +
  //   '</form>');

    form_element = $('<form id="pdf_preview_form" action="/latex_preview/pdf" method="POST"> </form>')
    // console.log(form_element);
    
      input_element = $('<input type=hidden name="' + latexSettings.previewParserVar + '" />');
      input_element.val($('<div/>').text(data).html());
      form_element.appendTo("body");
      input_element.appendTo("#pdf_preview_form");
      form_element.append(input_element);
      console.log($('<div>').append($('#pdf_preview_form').clone()).html());
      // console.log(form_element);
 
 
  // Firefox will not submit the form unless it has been added to the page. So, we will add it
  // and then immediately remove it.  Chrome will work with "form_element.submit();" without the
  // append and remove.
  form_element.appendTo("body");
  $("#pdf_preview_form").submit();
  console.log($('#pdf_preview_form input').val());
  $("#pdf_preview_form").remove();
}

function startLatexTable(data) {
  data.selection = "newselection";
  num_cols = prompt("Number of data columns", "3");
  table_outline = "\\begin{table}[H] \\large \\centering \\begin{tabular}{"
  for (i = 0; i < num_cols; i++) {
    table_outline += " c"
  }
  table_outline += " }\n"
  table_outline += "\\toprule\n"
  
  for (i = 0; i < num_cols - 1; i++) {
    table_outline += "header & "
  }
  table_outline += "header \\\\\n\\midrule\n"
  
  for (i = 0; i < num_cols - 1; i++) {
    table_outline += "val & "
  }
  
  table_outline += "val \\\\\n\\bottomrule\n"
  
  table_outline += "\\end{tabular} \\end{table}\n"
  return table_outline;
}
