Markup language: 
	Dotclear

URL:
	http://dotclear.org/documentation/2.0/usage/syntaxes

Description:
	A basic Dotclear markup set with Headings, Bold, Italic, Stroke through, Picture, Link, List, Quotes, Code, Preview button.

Install:
	- Download the zip file
	- Unzip it in your markItUp! sets folder
	- Modify your JS link to point at this set.js