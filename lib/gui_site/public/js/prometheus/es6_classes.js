//Babel transpile this to ES5 compatible Javascript
//npx babel lib/gui_site/public/js/prometheus/es6_classes.js --out-file lib/gui_site/public/js/prometheus/compiled-es6.js

//TODO this is currently reliant on html from gui_site object wrapper being present on the page.  I would like to get away from that.
//This also needs requires Mark.js for highlighting and adding text matching.
class DocumentSearchWidget {
  //This uses Mark.js for the searching and highlighting
  //It also uses jquery currently since everything else on the site currently does
  constructor() {
    this.widgetSelector = '#documentSearchContainer';
    this.foundWidgets = [];
    this.searchInput = $('#documentSearchInput');
    this.replaceInput = $('#documentReplaceInput');
    this.searchText = '';
    this.currentFoundIndex = 0;
    this.previousFoundIndex = 0;
    this.numResults = 0;
    this.markSelector = '.pretty_view';
  }
  
  searchAndHighlightMatches() {
    this.resetSearchVariables();
    this.searchText = this.searchInput.val().trim();
    let options = {
      'element' : 'span',
      'className' : 'search-highlight',
      'separateWordSearch' : false,
      'each': (node,range) => {
          this.foundWidgets.push(node);
        },
       'done' : (counter) => {
         this.numResults = counter;
         this.displaySearchResults();
         this.currentFoundIndex = 0;
       }
    };
    
    //This will need to be adjusted if there is no jquery support
    $(this.markSelector).mark(this.searchText, options);
  }
  
  clearMatches() {
    this.resetSearchVariables();
    this.searchInput.val('');
    this.replaceInput.val('');
    let options = {
      'element' : 'span', 
      'className' : 'search-highlight', 
      'done' : (counter) => { 
        this.displaySearchResults();
      }
    };
    
    
    
    //This will need to be adjusted if there is no jquery support
    $(this.markSelector).unmark(options);
  }
  
  displaySearchResults() {
    let searchResults = $('#documentSearchResults');
    let totalFoundSpan = $('#documentSearchContainer .totalFound');
    let matchString = `${this.numResults} matches found`;
    let currentIndexSpan = $('#documentSearchContainer .currentFound');
    
    //Go Next spans
    currentIndexSpan.text('0');
    totalFoundSpan.text(this.numResults);  
    
    //Total matches found span
    searchResults.text(matchString);
  }
  
  setupClickEvents() {
    $('#submitDocumentSearch').click( () => {
      this.searchAndHighlightMatches();
    });
    
    $('#submitDocumentReplace').click( () => {
      this.replaceAllWidgetText();
    });
    
    $('#clearDocumentSearchResults').click( () => {
      this.clearMatches();
    });
    
    $('#goToNextFoundButton').click( () => {
      this.goToNextFound();
    });
    
    $('#goToPreviousButton').click( () => {
      this.goToPreviousFound();
    });
    
    this.searchInput.keypress( (event) => {
      if ( event.which == 13 ) {
        event.preventDefault();
        this.searchAndHighlightMatches();
      }
    });
    
    this.replaceInput.keypress( (event) => {
      if ( event.which == 13 ) {
        event.preventDefault();
        this.replaceAllWidgetText();
      }
    });
    
  }
  
  resetSearchVariables() {
    this.numResults = 0;
    this.searchText = '';
    this.foundWidgets = [];
    
    //If this replace info is present remove it from the page as well
    // $(`${this.widgetSelector} .replaceResultsInfo`).remove();
    $(`${this.widgetSelector} .replaceResultsInfo`).text('');
  }
  
  goToNextFound() {
    if (this.foundWidgets.length > 0) {
      let nextIndex = (this.currentFoundIndex + 1) % this.foundWidgets.length;
      let nextWidget = this.foundWidgets[nextIndex];

      //TODO possibly make this part of the currentFound Index setter
      //TODO could also just build that entire string instead of using multiple spans - probably better to just have one currentfoundresults span using a rebuilt string
      let currentIndexSpan = $('#documentSearchContainer .currentFound');
      
      currentIndexSpan.text((nextIndex + 1).toString());
      this.scrollToElement(nextWidget);
      this.currentFoundIndex = nextIndex;

    }
  }
  
  goToPreviousFound() {
    if (this.foundWidgets.length > 0) {
      let prevIndex = (this.currentFoundIndex + this.foundWidgets.length - 1) % this.foundWidgets.length;
      let prevWidget = this.foundWidgets[prevIndex];
      
      //TODO possibly make this part of the currentFound Index setter
      //TODO could also just build that entire string instead of using multiple spans - probably better to just have one currentfoundresults span using a rebuilt string
      let currentIndexSpan = $('#documentSearchContainer .currentFound');
      
      currentIndexSpan.text((prevIndex + 1).toString());
      this.scrollToElement(prevWidget);
      this.currentFoundIndex = prevIndex;
    }
  }

  scrollToElement(element) {
    //Size of the topbar
    // let topBarSize = 56;
    let topbar = $('.topbar').first();
    let topBarSize = 0;
    //If there is a topbar calculate the size else set it to 0
    if (topbar.length > 0) {
      topBarSize = topbar.height();
    } else {
      topBarSize = 0;
    }
    $('html, body').animate({scrollTop: $(element).offset().top - topBarSize}, 200);
  }
  
  //TODO This currently won't work for individual replacements.  It will need another function that gets used inside of this one based on the positioning of the index in the for loop for that functionality
  replaceAllWidgetText() {
    let replaceText = this.replaceInput.val().trim();
    let replacementInfo = $('#documentSearchContainer .replaceResultsInfo');
    
    if (replaceText) {
      let replacementCount = 0;
    
      for (let w of this.foundWidgets) {        
        //find the abomination that is our data stuff and replace that if it is mutable
        let widgetData = $(w).closest('.pretty_view').siblings('.editable_view').find('.widget_data');
        
        //This means it is mutable since it has an editable view and it is ok to replace the text
        if (widgetData.length > 0) {
          replacementCount++;
          
          //Replace the pretty view text
          $(w).replaceWith(replaceText);
          
          //Replace the editable widget data values
          //TODO This is a pretty strong method of replace.  It is an exact match and will stomp anything in that exact match.
          let replacementData = $(widgetData).val().replace(this.searchText, replaceText);
          widgetData.val(replacementData);       
          
        }   
      }
      
      let numReplacedString;
      if (replacementCount > 1) {
        numReplacedString = `${replacementCount} occurrences`
      } else if (replacementCount == 1) {
        numReplacedString = `one occurrence`;
      } else {
        numReplacedString = `no occurrences`;
      }
      let replacementString = `Replaced ${numReplacedString} of ${this.searchText} with ${replaceText}`;
      if (replacementInfo.length > 0) {
        replacementInfo.text(replacementString);
      } else {
        // this.replaceInput.parent().append(`<p class='replaceResultsInfo'>${replacementString}</p>`);
        
        alert(replacementString);
      }
    }

  }
  
}
//End of DocumentSearchWidget

class DocumentCommentWidget {
  
  constructor(widgetData, parentWidget) {
    this.widgetPrettyName = widgetData.commentGetter;
    this.widgetTitle = this.widgetPrettyName.split('--')[1].replace(/_/g, ' ').match(/([A-Z]?[^A-Z]*)/g).slice(0,-1).join(' ');
    this.documentComments = [];
    this.widgetID = '#document-widget-comment-container';
    this.addNewCommentID = '#add-new-widget-comment';
    this.parentWidget = parentWidget;
  }
  
  buildCommentWidgetHTML() {
    //TODO break up each segment into individual components e.g. close button, the container.
    let html = `<div id='document-widget-comment-container' class='widget-comment-container'>
                <button type='button' aria-label='Close' id='document-widget-comment-close' class='widget-comment-close close'><span aria-hidden='true'>&times;</span></button>
                <div class='widget-comment-title text-capitalize text-center h4'>${this.widgetTitle} Comments</div>
                ${this.buildComments()}
                <div class='new-comment-container text-center'>
                <textarea rows='3' cols='5' placeholder='Enter Comment Text...' class='new-comment-content form-control mb-2'></textarea><button type='button' id='add-new-widget-comment' class='add-new-document-comment btn btn-primary'>Add a New Comment</button>
                </div>
                </div>
                `;
    return html;
  }
  
  //This method is for displaying comments outside of the popup widget
  buidCommentsDisplayHTML() {
    let html = `<div class='widget-comment-display-container'>
                <div class='widget-comment-title h4 text-center'>All Comments for the above information</div>
                ${this.buildComments()}
                </div>
               `;
    return html;
  }
  
  buildComments() {
    let html = `<section class='widget-comments'>
                ${this.documentComments.map(comment => this.buildComment(comment)).join('')}
                </section>
                `;
    return html;
  }
  
  buildComment(comment) {
    let html = `<article class='widget-comment'>
                <p class='comment-content p-1'>${comment.comment}</p>
                <p class='comment-attribution p-1'>by ${comment.author} at ${new Date(comment.created_time)}</p>
                </article> 
               `;
    return html;
  }
  
  addNewComment(comment) {
    let html = this.buildComment(comment);
    this.documentComments.push(comment);
    this.parentWidget.find('.widget-comments').append(html);
    
    //Add the has-widgets-class to the parent to mark it is having comments.  If it is already there it doesn't do anything.
    this.parentWidget.addClass('has-widget-comments');
  }
  
  attachWidgetToDOM() {
    this.detachWidget();
    this.parentWidget.append(this.buildCommentWidgetHTML());   
    $(this.widgetID).draggable(); 
    this.setupClickEvents();
  }
  
  attachCommentsToDOM() {
    //TODO Make a method called buildCommentsDisplayHTML() and use that here instead
    this.parentWidget.append(this.buidCommentsDisplayHTML());
  }
  
  //TODO Maybe clean this up to better make use of the widget class with instance variables
  setupClickEvents() {
    //Setup Docuemnt Widget Comment Container close button
    let container = this.parentWidget;
    let commentCloseButton = container.find('#document-widget-comment-close');
    commentCloseButton.click( () => {
      this.detachWidget();
    });
  
    //Setup Add new Comment
    //TODO Make this work better with document widget class
    container.find('.add-new-document-comment').click( () => {
      let newCommentInput = container.find('.new-comment-content');
      let commentContent = newCommentInput.val().trim();
      let newCommentData = { 'newCommentContent' : commentContent, 'widgetPrettyName' : this.widgetPrettyName };

      //TODO Maybe change the input to a form for required validation
      //Don't submit if there is no comment
      if (commentContent) {
        let request = $.post('/add_new_document_comment/', newCommentData)
          .done(data => {
            newCommentInput.val('');
            this.addNewComment(JSON.parse(data));
          });
      }
    });
  }
  
  fetchComments(container) {
    let commentData = {commentGetter: this.widgetPrettyName};
    let widget = this;
    let ajaxPromise = new Promise((resolve, reject) => {
      let request = $.get('/fetch_document_comments/', commentData)
        .done(data => {
          this.documentComments = JSON.parse(data);
          // self.attachWidgetToDOM(container);
          resolve('resolved');
      });
    });
    // p.then(val => {this.attachWidgetToDOM()});
    return ajaxPromise; 
  }
  
  detachWidget() {
    //Remove the widget comment container if it already exists to prevent multiple copies existing simultaneously
    let documentWidgetContainer = $(this.widgetID);
    if ( documentWidgetContainer.length > 0 ) {
      documentWidgetContainer.remove();
    }
  }
  
  setupDocumentWidget() {
    let ajaxPromise = this.fetchComments();
    // prom.then(comments => this.attachWidgetToDOM(container, comments));
    ajaxPromise.then(val => {this.attachWidgetToDOM()});
    // this.attachWidgetToDOM(container);
  }
  
  setupDocumentComments() {
    let ajaxPromise = this.fetchComments();
    // prom.then(comments => this.attachWidgetToDOM(container, comments));
    // ajaxPromise.then(val => {this.attachWidgetToDOM()});
    ajaxPromise.then(val => {this.attachCommentsToDOM()});
    // this.attachWidgetToDOM(container);
  }
  
}

//Add the click handler and associated creation of the document comment widget
//This is used in document association
function setupDocumentCommentWidgets(container) {
  let commentButton = container.find('.comment_button');
  commentButton.click( e => {
    //TODO Check if creating a new object uses memory or something.
    // I'm not really used to using classes in javascript yet
    let currentButton = $(e.currentTarget);
    let commentWidget = new DocumentCommentWidget(currentButton.data(), currentButton.parent());
    commentWidget.setupDocumentWidget();    
    // get_document_widget_comments(data, $(this).parent());
  });
  
}

function showContainerDocumentComments(container) {
  let all_widgets = container.find('.has-widget-comments').toArray();
  for ( let widget of all_widgets ) {
    let jqueryWidget = $(widget);
    let commentButton = jqueryWidget.find('.comment_button');
    let commentWidget = new DocumentCommentWidget(commentButton.data(), jqueryWidget);
    commentWidget.setupDocumentComments();
  }
  return false;
}

function showAllDocumentComments() {
  
  let all_widgets = $('.has-widget-comments').toArray();
  
  for ( let widget of all_widgets ){
    let jqueryWidget = $(widget);
    let existingCommentWidget = jqueryWidget.find('.widget-comment-display-container');
    if (existingCommentWidget.length > 0) {
      existingCommentWidget.show();
    } else {
      let commentButton = jqueryWidget.find('.comment_button');
      let commentWidget = new DocumentCommentWidget(commentButton.data(), jqueryWidget);
      commentWidget.setupDocumentComments();
    }
  }
  
  return false;
}

function hideAllDocumentComments() {
  //TODO this is gross it should be done through the Widget objects somehow
  //They should be persisted somehow and back data behind each of them somehow.
  $('.widget-comment-display-container').hide();
  return false;
}

function setupShowAllDocumentCommentsButton() {
  
  $('#show-all-comments-button').click(function(){
    if ($(this).data('show') == 'show') {
      $(this).data('show', 'hide');
      $(this).text('Hide All Document Comments');
      showAllDocumentComments();
    } else {
      $(this).data('show', 'show');
      $(this).text('Show All Document Comments');
      hideAllDocumentComments();
    }
    return false
  });
  
  return false;
}

//Setup the search Widget.  If the haml doesn't exist it will just exist with no inputs to trigger it.
const searchWidget = new DocumentSearchWidget();

searchWidget.setupClickEvents();