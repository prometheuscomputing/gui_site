function get_filter_data(content_div, include_association_attributes) {
  // load the filter information
  var filter_set = {};
  var filter_subset = function(ee) {
    var filter_type = ee.next().attr('value'); // depending on next returning an element indicates filter_type.  Brittle....
    if (filter_set[filter_type] === undefined){  // does this filter subset already exist?
      filter_set[filter_type] = {}; // initialize it if it doesn't
    }
    return filter_set[filter_type];
  }

  var filter_cell_class = '.filter_cell';
  if (!include_association_attributes === true) {
    filter_cell_class = filter_cell_class + ':not(.association_class_attribute)';
  }

  // gets the filter info from text fields
  content_div.find(filter_cell_class + ' input').each(function (i, e) {
      var eAnchor = $(e);
      var filter_val = eAnchor.val();
      if (typeof filter_val == 'string' && filter_val.length > 0) {
          filter_subset(eAnchor)[eAnchor.attr('name')] = eAnchor.val();
      }
  });
  // this gets the filter info from a choice widget
  content_div.find(filter_cell_class + ' select option:selected').each(function (i, e) {
    var select_anchor = $(e).closest('select');
    var filter_name = select_anchor.attr('name');
    var filter_val = $(e).val();
    if (typeof filter_val == 'string' && filter_val.length > 0) {
      filter_subset(select_anchor)[filter_name] = filter_val;
      }
    });
  return filter_set;
}

function get_search_all_filter_data(content_div, include_association_attributes) {
  // var filter_cell_class = '.filter_cell';
  if (!include_association_attributes === true) {
    // filter_cell_class = filter_cell_class + ':not(.association_class_attribute)';
    var filter_cells = content_div.find('.filter_cell:not(.association_class_attribute) .filter');
  }
  else {
    var filter_cells = content_div.find('.filter_cell .filter');
  }
  var filter_input = content_div.find('.search_all_filter');
  var search_value = filter_input.val();
  // var filter_cells = content_div.find('.filter_cell .filter:not(.association_class_attribute)');
  
  var filter_types = content_div.find('.filter_cell .type_select option');
  
  
  // console.log(filter_cells);
  
  var column_names = [];
  filter_cells.each(function(key,value) {
    // console.log(value.name);
    var filter_value = $(value);
    var column_name = filter_value.prop('name');
    // console.log(filter_value.next());
    var column_type = filter_value.next().attr('value');
    column_names.push([column_name, column_type]);
    // return true;
  });
  
  var type_array = [];
  filter_types.each(function(i, option) {
    type_array.push(option.value);
  });
  type_array = type_array.slice(1);
  
  search_all_hash = {}
  search_all_hash["columns"] = column_names
  // console.log(search_all_hash);
  search_all_hash["search_all_value"] = search_value
  search_all_hash["possible_types"] = type_array;
  // console.log(search_all_hash);
  return search_all_hash;
}

function get_previous_filter(content_div) {
  var previous_exists = content_div.parents('.widget').first().find('input[name="previous_filter"]').length > 0;
  if (previous_exists) {
    // return JSON.stringify(content_div.parents('.widget').first().find('input[name="previous_filter"]').val());
    return content_div.parents('.widget').first().find('input[name="previous_filter"]').val();
  } else {
    return null;
  }
}

function get_previous_search_all_filter(content_div) {
  var previous_exists = content_div.parents('.widget').first().find('input[name="previous_search_all_filter"]').length > 0;
  if (previous_exists) {
    // return JSON.stringify(content_div.parents('.widget').first().find('input[name="previous_filter"]').val());
    return content_div.parents('.widget').first().find('input[name="previous_search_all_filter"]').val();
  } else {
    return null;
  }
}

function setup_filter_cells_to_submit_on_enter(content_div) {
  // Pressing enter in a filter input field clicks Go
  content_div.find('.filter_inputs input').keypress(function(e) {
    if (e.which == 13) {
      // Save currently focused filter cell name
      var filter_cell_name = $(this).attr('name')
      var filter_cell_selector = '.filter_cell input[name='+filter_cell_name+']'
      populate_collection($(this).parents('.collection_content').first(), filter_cell_selector);
      return false;
    }
    return true;
  });
  
  content_div.find('.search_all_filter').keypress(function(e) {
    if (e.which == 13) {
      // Save currently focused filter cell name
      // var filter_cell_name = $(this).attr('name')
      var filter_cell_selector = '.search_all_filter';
      populate_collection($(this).parents('.collection_content').first(), filter_cell_selector);
      return false;
    }
    return true;
  });
}

function clear_filter_inputs(content_div) {
  var filter_inputs = content_div.find('.filter_cell input');
  filter_inputs.val('');
  return false;
}

function initialize_filter_appearance(content_div) {
  var search_all_filter_input = content_div.find('.search_all_filter');  
  var filter_row_visible = content_div.find('.table_filter_row').is(':visible') ? true : false;
  if(filter_row_visible) {
    search_all_filter_input.prop('disabled', true);
    search_all_filter_input.addClass('disabled_input');
  }
  
  
  return false;
}
