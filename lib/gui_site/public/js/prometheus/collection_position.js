var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$(this).width($(this).width());
	});
	return ui;
};

function reorder_positions_on_sortable_update(event, ui) {
  // Get the moved row from arguments
  var moved_row = $(ui.item[0]);
  // Trigger any listeners for the input positional field
  moved_row.find('input.position_field').trigger('keypress')  ;
  reorder_positions(moved_row);
}

function sortNumber(a,b) {
    return a - b;
}

function reorder_positions(moved_row) {
  // Get all position input elements
  var position_inputs = moved_row.parent().find('tr .position_field:not(:disabled)');
  
  // Get an array of positions
  var positions = position_inputs.map(function(i, el){ return parseInt($(el).val()) });
  // Sort the positions
  positions = positions.sort(sortNumber);
  // Apply the sorted positions to the position input elements
  position_inputs.each(function(index) {
    // $(this) now refers to the current position input element
    $(this).val(positions[index]);
  });
}

function setup_collection_positioning(container) {
  // Display popup on clicking 'Add Association' buttons
  container.find('.sortable tbody').sortable({
  	helper: fixHelper,
  	update: reorder_positions_on_sortable_update,
    placeholder: 'sortable-placeholder',
  	cancel: ':input,button, .table_header_row, .table_filter_row, .pending_assoc, .pending_deletion, .unassociated_object, .custom-control',
  	items: 'tr'
  })//.disableSelection();
  
  // Specific application of disableSelection prevents text input fields from being glitchy in FireFox
  container.find('.sortable tbody .domain_obj_row, .table_header_row').disableSelection();
}

function document_reorder_positions_on_sortable_update(event, ui) {
  // Get the moved row from arguments
  var moved_row = $(ui.item[0]);
  // Trigger any listeners for the input positional field
  moved_row.find('.document_position_field').trigger('keypress');
  document_reorder_positions(moved_row);
}

function document_reorder_positions(moved_row) {
  // Get all position input elements
  var position_inputs = moved_row.parent().find('li .document_position_field:not(:disabled), tr .document_position_field:not(:disabled)');
  // var position_inputs = moved_row.parent().find('li:not(.broken_association) .document_position_field:not(:disabled), tr:not(.broken_association) .document_position_field:not(:disabled)');
  
  // Get an array of positions
  // Using the val here instead of the position in the array seems backwards to me and doesn't make sense - Tyler
  // var positions = position_inputs.map(function(i, el){ return parseInt($(el).val()) });
  var positions = position_inputs.map(function(i, el){ return parseInt(i) });
  // Sort the positions
  positions = positions.sort(sortNumber);

  var uniq_position_getter = null;
  // Apply the sorted positions to the position input elements
  position_inputs.each(function(index) {
    // $(this) now refers to the current position input element
    $(this).val(positions[index] + 1);
    //Update all other values for the same positional getters in the document
    uniq_position_getter = $(this).data('singleGetter');
    uniq_position_getter = '.' + uniq_position_getter;
    $(uniq_position_getter).val(positions[index] + 1);
  });
  document_update_position_everywhere(uniq_position_getter);
}

function document_update_position_everywhere (selector) {
  addDocumentUnsavedPositioningFlag($(selector).closest('.document_association'));
  var association_lists = $(selector).closest('.document_association_content_data');
  association_lists.each(function(){
    var association_items = $('> li, > tbody tr.content_data', $(this));
    var button_rows = $('> div.document_association_add_item', $(this));
    var button_table_body = $('> tbody', $(this));
    association_items.sort(function (a, b) {
      return ($(a).find('.document_position_field').val() > $(b).find('.document_position_field').val());
    });
    
    //Add the rearranged items back the content item
    $(this).append(association_items);
    
    //Add the list and table add assocation sections back to the bottom of the content
    $(this).append(button_rows);
    $(this).append(button_table_body);
  });

  
}

function setup_document_collection_positioning(container) {
  //This is slow as heck -TB
  //This might have to be adjusted if there are multiple table bodies in one sortable widget
  container.find('.sortable, .sortable > tbody').sortable({
    helper: fixHelper,
    placeholder: 'clearview-placeholder',
    update: document_reorder_positions_on_sortable_update,
    cancel: ':input,button, .header_row, .table_filter_row, .pending_assoc, .pending_deletion, .document_association_add_item, .add_association, .unassociated_object, .editable_view, .custom-control',
    items: '> li, > tr'
  })// .disableSelection();
  
  // Specific application of disableSelection prevents text input fields from being glitchy in FireFox
  // container.find('.sortable tbody .domain_obj_row, .table_header_row').disableSelection();
  // container.find('.sortable tbody .domain_obj_row, .table_header_row').disableSelection();
}