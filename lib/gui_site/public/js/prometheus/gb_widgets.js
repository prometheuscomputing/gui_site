// Global constants yuck but definitely faster.
root_data_id_element = document.querySelector('.root.view_widget input.data_id');
if (root_data_id_element) {
  root_data_id = document.querySelector('.root.view_widget input.data_id').value;
  root_data_classifier =  document.querySelector('.root.view_widget input.data_classifier').value;
}
else {
  root_data_id = undefined;
  root_data_classifier = undefined;
}

// Get the root widget information
function get_root_data_classifier() {
  return $('.root.view_widget > input.data_classifier').val();
}

function get_root_data_id() {
  return $('.root.view_widget > input.data_id').val();
}

function get_root_data_getter() {
  return $('.root.view_widget > input.data_getter').val();
}

function get_root_view_type() {
  return $('.root.view_widget > input.view_type').val();
}

// Get widget information based on passed view_content div
function get_data_getter(widget) {
  return widget.children('.data_getter').val();
}

function get_view_type(widget) {
  return widget.children('.view_type').val();
}

function get_view_name(widget) {
  return widget.children('.view_name').val();
}

function get_pretty_id(widget) {
  return widget.children('.pretty_id').val();
}

function check_for_association_class_widget(widget){
  return widget.children('.association_class_widget').val();
}

function get_parent_data_classifier(widget) {
  var parent_view_widget = widget.parent().closest('.view_widget').first();
  // Ugly hack to deal with selection collections where there is no containing view widget
  if (parent_view_widget.length == 0) {
    var parent_data_classifier = widget.parent().parent().find('.parent_data_classifier').val();
  } else {
    // Prefer to use .classifier over .data_classifier if available
    // TODO: figure out why we don't always use .classifier -SD
    var parent_data_classifier = parent_view_widget.children('.classifier').val();
    if (parent_data_classifier == null) {
      parent_data_classifier = parent_view_widget.children('.data_classifier').val();
    }
  }
  if (parent_data_classifier == null) {
    // parent_data_classifier = get_root_data_classifier();
    parent_data_classifier = root_data_classifier;
  }
  return parent_data_classifier;
}

function get_parent_through_classifier(widget) { 
  var parent_view_widget = widget.parent().closest('.view_widget').first();
  // Ugly hack to deal with selection collections where there is no containing view widget
  if (parent_view_widget.length == 0) {
    var parent_through_classifier = widget.parent().parent().find('.parent_through_classifier').val();
  } else {
    var parent_through_classifier = parent_view_widget.children('.through_classifier').val();
    if (parent_through_classifier == null) {
      parent_through_classifier = widget.parent().find('.parent_though_classifier').val();
      if (parent_through_classifier == null) {
        parent_view_widget = widget.parent().closest('.document_association_item').first();
        parent_through_classifier = parent_view_widget.children('.through_classifier').val();
      }
    }
  }
  return parent_through_classifier;
}

function get_parent_classifier(widget) {
  if (check_for_association_class_widget(widget)) {
    var parent_classifier = get_parent_through_classifier(widget);
  } else {
    var parent_classifier = get_parent_data_classifier(widget);
  }
  return parent_classifier;
}

function get_parent_data_id(widget) {
  var parent_view_widget = widget.parent().closest('.view_widget').first();
  // Ugly hack to deal with selection collections where there is no containing view widget
  if (parent_view_widget.length == 0) {
    var parent_data_id = widget.parent().parent().find('.parent_data_id').val();
  } else {
    // Prefer to use .identifier over .data_id if available
    // TODO: figure out why we don't always use .identifier -SD
    var parent_data_id = parent_view_widget.children('.identifier').val();
    if (parent_data_id == null) {
      parent_data_id = parent_view_widget.children('.data_id').val();
    }
  }
  if (parent_data_id == null) {
    // parent_data_id = get_root_data_id();
    parent_data_id = root_data_id;
  }
  return parent_data_id;
}

function get_parent_through_id(widget) {
  var parent_view_widget = widget.parent().closest('.view_widget').first();
  // Ugly hack to deal with selection collections where there is no containing view widget
  if (parent_view_widget.length == 0) {
    var parent_through_id = widget.parent().parent().find('.parent_through_id').val();
  } else {
    // Prefer to use .identifier over .data_id if available
    // TODO: figure out why we don't always use .identifier -SD
    if (parent_through_id == null) {
      parent_through_id = parent_view_widget.children('.through_id').val();
      if (parent_through_id == null) {
        parent_view_widget = widget.parent().closest('.document_association_item').first();
        parent_through_id = parent_view_widget.children('.through_id').val();
      }
    }
  }
  return parent_through_id;
}

function get_parent_id(widget) {
  if (check_for_association_class_widget(widget)) {
    var parent_id = get_parent_through_id(widget);
  } else {
    var parent_id = get_parent_data_id(widget);
  }
  return parent_id;
}

function find_last_update(widget) {
  last_update = widget.find('input.last_update').val();
  return last_update;
}

function get_class_and_id_values_from_row(row, include_position) {
  var row = $(row);
  var inputs_hash = {
    identifier: row.find('.identifier').val(), classifier: row.find('.classifier').val(),
    through_identifier: row.find('.through_identifier').val(), through_classifier: row.find('.through_classifier').val()
  }
  if (include_position === true) {
     inputs_hash['position'] = row.find('.position_field').val();
  }
  return inputs_hash;
}

function document_get_class_and_id_values_from_row(row, include_position) {
  var row = $(row);
  var inputs_hash = {
    identifier: row.children('.data_id').first().val(), classifier: row.children('.data_classifier').first().val(),
    through_identifier: row.children('.through_id').first().val(), through_classifier: row.children('.through_classifier').first().val()
  };
  if (include_position === true) {
     inputs_hash['position'] = row.children('.position').find('.document_position_field').first().val();
  }
  return inputs_hash;
}

function setup_widgets_to_submit_on_enter(container) {
  container.find('.widget_data').keypress(function(e) {
    if (e.which == 13) {
      //if (!$(document.activeElement).is("textarea") && !$(document.activeElement).is(".filter_cell input")) {
      if (!$(document.activeElement).is("textarea")) {
        document.activeElement.blur();
        var submit_button = $('button#save_and_stay');
        submit_button.focus();
        submit_button.click();
        return false;
      } else {
        return true;
      }
    }
    return true;
  });
}

// Sets up 'edit' links that toggle between 'editable' and 'pretty' views
// This is currently only used for RichText widgets
// NOTE: toggling back to 'pretty_view' is currently disabled (since this is processed server-side)
function setup_widget_toggle_links() {
  $('a.widget_toggle_link').click(function() {
    var editable_view = $(this).parents('.simple_widget').find('.editable_view');
    var pretty_view = $(this).parents('.simple_widget').find('.pretty_view');
    if (editable_view.hasClass('d-none')) {
      editable_view.removeClass('d-none');
      pretty_view.addClass('d-none');
      // Hide the edit link
      $(this).addClass('d-none');
    }
    return false;
  });
}

// Given a widget, will return an array of widget information hashes
// Inner views will have a 'content' key that contains the view's structure
function get_widget_structure(container) {
  var structure = [];
  
  //If the tree view is active use the object container as the container
  var split_pane_container = container.children('.split_pane_container');
  if (split_pane_container.length > 0) {
    container = split_pane_container.first();
  }
  
  // If there is a view_content, use it as the container
  var view_content = container.children('.view_content');
  if (view_content.length > 0) {
    container = view_content.first();
  }
  // If there is an inner form, use it as the container
  var inner_form = container.children('.view_form');
  if (inner_form.length > 0) {
    container = inner_form.first();
  }
  // If there is a content_data element, use it as the container
  var content_data = container.children('.content_data');
  if (content_data.length > 0) {
    container = content_data.first();
  }
  
  // Select child widgets. Also looks for widgets inside table bodies (as rows) and table cells (as cell contents)
  var child_widgets = container.find('> .widget, > tbody > .widget, > td > .widget');
  child_widgets.each(function(i, widget) {
    var w = $(widget);
    var mutable = w.hasClass('mutable')
    
    // Skip widget templates
    if (w.hasClass('widget_template')) {return true;}
    // We would like to skip immutable widgets but we can't because they can have mutable children...
    // if (mutable == false) {return true;}
    
    var getter = w.find('.widget_getter').first().val(); // can be multiple fields per getter
    //NOTE Removed Single Quote from the widget label since it will cause a JSON parse error
    var label = $.trim(w.find('.widget_label').first().text()).replace("'", "");
    var widget_type = w.children('.widget_type').first().val();
    var association_class_widget = (w.children('.association_class_widget').length > 0);
    var type = null;
    if (w.hasClass('simple_widget')) {
      if (w.hasClass('enumeration')) {
        type = 'enumeration'
      } else {
        type = 'simple'
      }
    } else {
      type = 'view'
    }
     
    var widget_information = {label: label, widget: widget_type, type: type, getter: getter, association_class_widget: association_class_widget, mutable: mutable};
    
    widget_information['data']                   = {};
    widget_information['initial_data']           = {};
    widget_information['initial_data_in_params'] = {};
    // var last_update = find_last_update(w);
    // widget_information['last_update'] = last_update;
    widget_information['last_update'] = find_last_update(w);
    widget_information['id'] = w.attr('id');
    
    if ((type == 'simple') || (type == 'enumeration')) { // Text, strings
      // widget_info can be used to pass generic information (such as restore) commands to the form parser.
      // we will store the widget_info's data_name pointing to the name. We will then retrieve the value from the
      // parameters using the name from the widget structure. Will be persistent across continue edits
      w.find('.widget_info').each(function() {
        widget_information[$(this).attr('data_name')] = $(this).val();
      });
      widget_information['delete'] = w.find('.attribute_delete_checkbox').is(':checked');
      w.find('.widget_data').each(function() {
        var value = ($(this).is('[data_name]')) ? $(this).attr('data_name') : 'value';
        widget_information['data'][value] = $(this).prop('name');
        // console.log(value + " = " + $(this).prop('name'));
        // TODO We want to make this work for File Widgets w/o ChangeTracker but that will take a lot of work.  We have talked about adding a column to the database for each file that is a hash calculated from the associated BinaryData.data value.  We could do that but it will take quite a bit of work to get done  
        widget_information['initial_data'][value] = $(this).attr('initial_value'); 
      });
      // Necessary for data that can't be stored as an attribute (ex. text)
      w.find('.widget_initial_data').each(function() {
        var value = ($(this).is('[data_name]')) ? $(this).attr('data_name') : 'value';
        widget_information['initial_data_in_params'][value] = $(this).prop('name');
      });
      // These apply only to 'button' widgets currently.
      widget_information['display_result'] = w.find('.display_result').val();
      widget_information['action_button'] = w.find('.action_button').attr('name');
    } else if (type == 'view') {
      widget_information['data']['value'] = w.find('.pretty_id').val();
      widget_information['data_classifier'] = w.children('.data_classifier').val();
      widget_information['data_id'] = w.children('.data_id').val();
      widget_information['through_classifier'] = w.children('.through_classifier').val();
      widget_information['through_id'] = w.children('.through_id').val();
      if (w.hasClass('new_association')) {
        widget_information['add_association'] = true
      } else if (w.hasClass('broken_association')) {
        widget_information['remove_association'] = true
      } else if (w.hasClass('deleted_object')) {
        widget_information['delete_association'] = true
      }
        
      
      // For collections (and organizer pretending to be collections) we need to get the pending association operations
      widget_information['pending_association_additions'] = $.makeArray(w.find('.pending_association_addition').map(function(){
        return get_class_and_id_values_from_row(this, false)
      }));
      widget_information['pending_association_removals'] = $.makeArray(w.find('.pending_association_removal').map(function(){
        return get_class_and_id_values_from_row(this, false)
      }));
      widget_information['pending_association_deletions'] = $.makeArray(w.find('.pending_association_deletion').map(function(){
        return get_class_and_id_values_from_row(this, false)
      }));
      
      // Record ordering information
      if (w.find('.has_unsaved_positioning').length > 0) {
        widget_information['order'] = [];
        w.find('.domain_obj_row').each(function() { widget_information['order'].push(get_class_and_id_values_from_row(this, true)) });
      }
      
      // Record ordering information
      if (w.find('.document_has_unsaved_positioning').length > 0) {
        widget_information['order'] = [];
        // w.find('.domain_obj_row').each(function() { widget_information['order'].push(document_get_class_and_id_values_from_row(this, true)) });  
        
        //Gets all of the first level of domain_row_objects in each widget rather than all domain row objects beneath the current widget      
        w.find('.domain_obj_row:not(.broken_association)').first().each(function() { widget_information['order'].push(document_get_class_and_id_values_from_row(this, true)) });
        w.find('.domain_obj_row').first().siblings('.domain_obj_row:not(.broken_association)').each(function() { widget_information['order'].push(document_get_class_and_id_values_from_row(this, true)) });
      }
      // console.log(w);
      // console.log(widget_information['order']);
      

      // Recursively get content's structure
      widget_information['content'] = get_widget_structure(w);
    }
    structure.push(widget_information);
  });
  // return false
  return structure;
}
