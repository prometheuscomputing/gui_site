//Check to see if this is still true with newer jquery ui - TB
//jQuery.curCSS = jQuery.css; // jquery UI Layout uses a deprecated (i.e. removed) function
//This is nasty logic that I did. It sucks fix me sometime -TB
function setup_tree_view_show_hide_button() {
  $('#split_container_into_panes_button').click(function() {
    var jstree_div = $('#jsTreePaneDiv');
    if(jstree_div.length > 0) {
      var content_div = jstree_div.closest('#split_pane_container').find('.js_tree_content');
      if($(this).text() === 'Display Tree View') {
        $(this).text('Remove Tree View');
        content_div.removeClass('expanded_tree_content_div');
      }
      else {
        $(this).text('Display Tree View');
        content_div.addClass('expanded_tree_content_div');
      }
      jstree_div.toggle();
      jstree_div.parent().toggle();
    }
    else {
      $(this).text('Remove Tree View');
      split_panes_for_tree_view();
    }
    
  });
}

function tree_view_resizable_setup() {
  var tree_view_pane = $('.resizable_tree_div');
  var container_width = $('.split_pane_container').width();

  tree_view_pane
    .wrap('<div/>')
      .css({'overflow':'hidden'})
        .parent()
          .css({'display':'inline-block',
                'overflow':'hidden',
                'height':function(){return $('.resizable',this).height();},
                'width':  function(){return $('.resizable',this).width();},
                'paddingBottom':'12px',
                'paddingRight':'12px'
                
               }).resizable({
                        maxWidth: 1400,
                        minWidth: 430,
                        minHeight: 400,
                        resize: function(event, ui) {
                          var current_width = ui.size.width;
                          // var container_width = $('.split_pane_container').width();
      
                          //TODO If the window is resized reset the container width somehow
                          //TODO The limit could be a percentage instead of 350 pixels but if the window is that small nothing else works well anyhow
                          //This should give the content container a 350 pixel width limit that should force to stay on the page
                          if(container_width - current_width < 350) {
                            //Sets the width of the content div to 350
                            $(".js_tree_content").width(350);
                            //Sets the resizing jstree pane to it's container's width - the 350 min pixel limit of the content div
                            $(this).width(container_width - 350);
                            return false;
                          }
      
                          //Accounts for lag in jquery ui width calculate if this isn't here it can lead to instability  
                          $(this).width(current_width);
      
                          $(".js_tree_content").width(container_width - current_width - 10);
      
                        }
                      })
                  .find('.resizable_tree_div')
                    .css({overflow:'auto',
                          width:'100%',
                          height:'100%'});
}

//Function for the button that will split the view into two panes and initialize the jstree object
function split_panes_for_tree_view() {
  var root_div = $(".root");
  // var rootHTML = $(".root").html();
  // var jsTree_content_container = "<div id='jsTreeContentDiv'></div>";
  var jstree_container_html = "<div id='jsTreePaneDiv' class='tree_view_split_pane split_panes resizable_tree_div'></div>";

  var split_pane_container_html = "<div id='split_pane_container' class='split_pane_container'></div>";
  root_div.before(split_pane_container_html);
  
  var split_pane_container_div = $("#split_pane_container");
  
  split_pane_container_div.html(jstree_container_html);

  var jstree = $("#jsTreePaneDiv");
  jstree.after(root_div);
  root_div.addClass("split_panes");
  root_div.addClass("js_tree_content container-fluid");
  
  setup_jstree_html(jstree);
}

//Function for the inital setup of the jstree object using an ajax call to populate the div with html and then create the object
function setup_jstree_html(jstree_div) {
  var root_widget = $('.root');
  var data_id = $(root_widget).find('.data_id').val();
  var data_classifier = $(root_widget).find('.data_classifier').val();
  var host = window.location.host
  var protocol = window.location.protocol
  $.ajax({
    type: 'POST',
    url: '/populate_jstree/' + host + '/' + protocol,
    data: {
      'data_id': data_id,
      'data_classifier': data_classifier
    },
    success: function(json, status) {
      var data = JSON.parse(json)
      jstree_div.html(data.html);
      jstree_div.jstree({
        "core": {
          "themes":{
            "icons":false
          }
        },
        "plugins" : [ "wholerow" ]
      });
      if (data.options.no_open_all != true) {
        jstree_div.jstree("open_all");
      }
      jstree_div.jstree('select_node', '#current_node')

      tree_view_resizable_setup();

      setup_jstree_event_handlers(jstree_div);
    }
  });
}

function get_breadcrumb_urls() {
  var bread_crumbs = $('.main_form .breadcrumb_trail a');
  var urls = [];
  $.each(bread_crumbs, function( index, value ) {
    urls.push(value.href);
  });
  return urls;
}

function setup_jstree_event_handlers(js_tree_div) {
  //event handler for onclick left click
  js_tree_div.on("select_node.jstree", function(evt, data) {
    var link = data.node.a_attr.href;
    // var nodeText = data.node.text;
    window.history.replaceState(null, "Search Results", link + "/")
    // window.location.href="#"+link;
    var root_widget = $('.content');
    var content_div = $('.root');
    var data_classifier = data.node.a_attr["data-classifier"];
    var data_id = data.node.a_attr["data-id"];
    
    // var widget_structure = get_widget_structure(root_widget);
    // widget_structure = JSON.stringify(widget_structure);
    // console.log(widget_structure);
    $.ajax({
     type: 'POST',
     url: '/render_node_from_treeview/',
     data: {
       'link' : link,
       'widget_structure' : {},
       'data_id':data_id,
       'data_classifier': data_classifier,
       'breadcrumbs':get_breadcrumb_urls()
       // 'widget_structure' : widget_structure
     },
     dataType: 'html',
     success: function(data, status) {
       content_div.replaceWith(data);
       content_div = $('.root');
       content_div.addClass('split_panes');
       content_div.addClass('js_tree_content');
   
       //Redoing various setup functions since the content div has been recreated with an ajax call

       // Setup "Add Another" association button -- this function was defined in clearview specific js and is no longer available.  Should it have been defined in a more general place?  Do we need this after all?
       // setup_add_association_buttons(content_div);

       // Setup Richtext editors (MarkItUp)
       setup_richtext_editors(content_div);

       // Now that the data is in place, setup the toggle to show possible associations
       setup_show_possible_associations_toggle(content_div);

       // Setup dropdowns for creation links.  Why is this commented out?
       // setup_dropdown_boxes(content_div);

       // (Possibly) setup positioning
       setup_collection_positioning(content_div);

       // Setup checkboxes/radio buttons to activate/deactivate actions for associated domain objects
       setup_associated_object_selections(content_div);

       // Setup checkboxes/radio buttons to activate/deactivate actions for un-associated domain objects
       setup_unassociated_object_selections(content_div);

       // Setup ajax call to save the page when leaving to create a new association.
       setup_save_and_go_links(content_div);

       setup_discard_and_go_links(content_div);

       //Setup confirmation_prompt links
       setup_delete_confirmation_prompt_links(content_div);

       setup_organizer_headers(content_div);
       setup_collection_headers(content_div);

       // Pressing enter in a filter input field clicks Go
       setup_filter_cells_to_submit_on_enter(content_div);

       //This is a hack due to the ajax call to prevent double headings and two forms
       $('#split_pane_container').find('.page_title').remove();
       $('#split_pane_container').find('.split_container_into_panes').remove();


       //This entire process of trying to fix breadcrumbs is terrible and should probably be done a different way
       var split_bread_crumbs = $('#split_pane_container').find('.main_form .breadcrumb_trail');
       // var latest_bread_crumb = $('#split_pane_container').find('.main_form .breadcrumb_trail a').last().clone();
       $('#split_pane_container').find('.main_form').remove();
       var bread_crumbs = $('.main_form .breadcrumb_trail');
       bread_crumbs.replaceWith(split_bread_crumbs);
       // var last_position_in_breadcrumbs = $('.breadcrumb_trail').find('a').last();
       // last_position_in_breadcrumbs.after(latest_bread_crumb);
       // last_position_in_breadcrumbs.after(">");
       
       
       //Replace the Clone and History link
       // var new_clone_href = $('#split_pane_container').find('#clone_link').prop("href");
       var new_history_href = $('#split_pane_container').find("a:contains('History')").prop("href");
       var sub_position = new_history_href.indexOf("/change_tracker");
       new_history_href = new_history_href.slice(sub_position);
       // $('.content').find('#clone_link').prop("href", new_clone_href);
       $('.content').find("a:contains('History')").prop("href", new_history_href);
       
       //Remove extra History link and Clone Link
       $('#split_pane_container').find('#cloneDomainObjectButton').remove();
       $('#split_pane_container').find("a:contains('History')").remove();
       
       //Setup change listeners on the newly generated content div
       setup_change_listeners();
       
       setup_column_sorting();
       
       show_save_buttons_if_editable();
       
       $('#root_view_form').submit(function(){
         var form = $(this);
         var wrapper_content = $('#wrapper .content');
         var widget_structure = get_widget_structure(wrapper_content);
         var structure_json = JSON.stringify(widget_structure);
         form.append("<input name='widget_structure' type=hidden value='" + structure_json + "' />");
       });
       
     }
    });
  });
}
