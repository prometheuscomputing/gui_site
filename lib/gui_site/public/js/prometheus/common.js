// Container argument defines scope to setup dropdown boxes within
// function setup_dropdown_boxes(container) {
//   container.on('mouseenter', 'ul.dropdown_menu:not(.disabled) li', function () {
//     $('ul', this).slideDown(50); //show its submenu
//   });
//
//   container.on('mouseleave', 'ul.dropdown_menu:not(.disabled) li', function () {
//     $('ul', this).slideUp(50); //show its submenu
//   });
// }

function setup_tooltips(container) {
  // $('.tooltip').tooltipster({
  //   side: 'right',
  // });
  
  //TODO This is very slow - TB
  // container.find('[data-toggle="tooltip"]').tooltip();
  container.find('.bootstrap-tooltip').tooltip();
}

function setupPopovers(container) {
  container.find('[data-toggle="popover"]').popover();
}

//This function creates a action popup if it is needed.
function display_action_dialog() {
  var popup = $('#action_popup');
  if (popup.length > 0) {
    
    var buttons = {};
    buttons['Ok'] = function() {
      $(this).remove();
    };

    $('#action_popup').dialog({
      modal: true,
      resizable: true,
      width: 1000,
      buttons: buttons,
      position: ['middle', 20],
      open: function() {
        $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove(); 
      },
      close: function() {
        $(this).parents(".ui-dialog:first").remove();
      }
    });
    // Center the buttons -- TODO: figure out why CSS isn't working
    $(".ui-dialog-buttonpane").css('text-align', 'center');
    $(".ui-dialog-buttonpane button").css('float', 'none');
  }
}