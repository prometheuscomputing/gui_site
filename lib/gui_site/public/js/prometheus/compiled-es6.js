'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//Babel transpile this to ES5 compatible Javascript
//npx babel lib/gui_site/public/js/prometheus/es6_classes.js --out-file lib/gui_site/public/js/prometheus/compiled-es6.js

//TODO this is currently reliant on html from gui_site object wrapper being present on the page.  I would like to get away from that.
//This also needs requires Mark.js for highlighting and adding text matching.
var DocumentSearchWidget = function () {
  //This uses Mark.js for the searching and highlighting
  //It also uses jquery currently since everything else on the site currently does
  function DocumentSearchWidget() {
    _classCallCheck(this, DocumentSearchWidget);

    this.widgetSelector = '#documentSearchContainer';
    this.foundWidgets = [];
    this.searchInput = $('#documentSearchInput');
    this.replaceInput = $('#documentReplaceInput');
    this.searchText = '';
    this.currentFoundIndex = 0;
    this.previousFoundIndex = 0;
    this.numResults = 0;
    this.markSelector = '.pretty_view';
  }

  _createClass(DocumentSearchWidget, [{
    key: 'searchAndHighlightMatches',
    value: function searchAndHighlightMatches() {
      var _this = this;

      this.resetSearchVariables();
      this.searchText = this.searchInput.val().trim();
      var options = {
        'element': 'span',
        'className': 'search-highlight',
        'separateWordSearch': false,
        'each': function each(node, range) {
          _this.foundWidgets.push(node);
        },
        'done': function done(counter) {
          _this.numResults = counter;
          _this.displaySearchResults();
          _this.currentFoundIndex = 0;
        }
      };

      //This will need to be adjusted if there is no jquery support
      $(this.markSelector).mark(this.searchText, options);
    }
  }, {
    key: 'clearMatches',
    value: function clearMatches() {
      var _this2 = this;

      this.resetSearchVariables();
      this.searchInput.val('');
      this.replaceInput.val('');
      var options = {
        'element': 'span',
        'className': 'search-highlight',
        'done': function done(counter) {
          _this2.displaySearchResults();
        }
      };

      //This will need to be adjusted if there is no jquery support
      $(this.markSelector).unmark(options);
    }
  }, {
    key: 'displaySearchResults',
    value: function displaySearchResults() {
      var searchResults = $('#documentSearchResults');
      var totalFoundSpan = $('#documentSearchContainer .totalFound');
      var matchString = this.numResults + ' matches found';
      var currentIndexSpan = $('#documentSearchContainer .currentFound');

      //Go Next spans
      currentIndexSpan.text('0');
      totalFoundSpan.text(this.numResults);

      //Total matches found span
      searchResults.text(matchString);
    }
  }, {
    key: 'setupClickEvents',
    value: function setupClickEvents() {
      var _this3 = this;

      $('#submitDocumentSearch').click(function () {
        _this3.searchAndHighlightMatches();
      });

      $('#submitDocumentReplace').click(function () {
        _this3.replaceAllWidgetText();
      });

      $('#clearDocumentSearchResults').click(function () {
        _this3.clearMatches();
      });

      $('#goToNextFoundButton').click(function () {
        _this3.goToNextFound();
      });

      $('#goToPreviousButton').click(function () {
        _this3.goToPreviousFound();
      });

      this.searchInput.keypress(function (event) {
        if (event.which == 13) {
          event.preventDefault();
          _this3.searchAndHighlightMatches();
        }
      });

      this.replaceInput.keypress(function (event) {
        if (event.which == 13) {
          event.preventDefault();
          _this3.replaceAllWidgetText();
        }
      });
    }
  }, {
    key: 'resetSearchVariables',
    value: function resetSearchVariables() {
      this.numResults = 0;
      this.searchText = '';
      this.foundWidgets = [];

      //If this replace info is present remove it from the page as well
      // $(`${this.widgetSelector} .replaceResultsInfo`).remove();
      $(this.widgetSelector + ' .replaceResultsInfo').text('');
    }
  }, {
    key: 'goToNextFound',
    value: function goToNextFound() {
      if (this.foundWidgets.length > 0) {
        var nextIndex = (this.currentFoundIndex + 1) % this.foundWidgets.length;
        var nextWidget = this.foundWidgets[nextIndex];

        //TODO possibly make this part of the currentFound Index setter
        //TODO could also just build that entire string instead of using multiple spans - probably better to just have one currentfoundresults span using a rebuilt string
        var currentIndexSpan = $('#documentSearchContainer .currentFound');

        currentIndexSpan.text((nextIndex + 1).toString());
        this.scrollToElement(nextWidget);
        this.currentFoundIndex = nextIndex;
      }
    }
  }, {
    key: 'goToPreviousFound',
    value: function goToPreviousFound() {
      if (this.foundWidgets.length > 0) {
        var prevIndex = (this.currentFoundIndex + this.foundWidgets.length - 1) % this.foundWidgets.length;
        var prevWidget = this.foundWidgets[prevIndex];

        //TODO possibly make this part of the currentFound Index setter
        //TODO could also just build that entire string instead of using multiple spans - probably better to just have one currentfoundresults span using a rebuilt string
        var currentIndexSpan = $('#documentSearchContainer .currentFound');

        currentIndexSpan.text((prevIndex + 1).toString());
        this.scrollToElement(prevWidget);
        this.currentFoundIndex = prevIndex;
      }
    }
  }, {
    key: 'scrollToElement',
    value: function scrollToElement(element) {
      //Size of the topbar
      // let topBarSize = 56;
      var topbar = $('.topbar').first();
      var topBarSize = 0;
      //If there is a topbar calculate the size else set it to 0
      if (topbar.length > 0) {
        topBarSize = topbar.height();
      } else {
        topBarSize = 0;
      }
      $('html, body').animate({ scrollTop: $(element).offset().top - topBarSize }, 200);
    }

    //TODO This currently won't work for individual replacements.  It will need another function that gets used inside of this one based on the positioning of the index in the for loop for that functionality

  }, {
    key: 'replaceAllWidgetText',
    value: function replaceAllWidgetText() {
      var replaceText = this.replaceInput.val().trim();
      var replacementInfo = $('#documentSearchContainer .replaceResultsInfo');

      if (replaceText) {
        var replacementCount = 0;

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = this.foundWidgets[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var w = _step.value;

            //find the abomination that is our data stuff and replace that if it is mutable
            var widgetData = $(w).closest('.pretty_view').siblings('.editable_view').find('.widget_data');

            //This means it is mutable since it has an editable view and it is ok to replace the text
            if (widgetData.length > 0) {
              replacementCount++;

              //Replace the pretty view text
              $(w).replaceWith(replaceText);

              //Replace the editable widget data values
              //TODO This is a pretty strong method of replace.  It is an exact match and will stomp anything in that exact match.
              var replacementData = $(widgetData).val().replace(this.searchText, replaceText);
              widgetData.val(replacementData);
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        var numReplacedString = void 0;
        if (replacementCount > 1) {
          numReplacedString = replacementCount + ' occurrences';
        } else if (replacementCount == 1) {
          numReplacedString = 'one occurrence';
        } else {
          numReplacedString = 'no occurrences';
        }
        var replacementString = 'Replaced ' + numReplacedString + ' of ' + this.searchText + ' with ' + replaceText;
        if (replacementInfo.length > 0) {
          replacementInfo.text(replacementString);
        } else {
          // this.replaceInput.parent().append(`<p class='replaceResultsInfo'>${replacementString}</p>`);

          alert(replacementString);
        }
      }
    }
  }]);

  return DocumentSearchWidget;
}();
//End of DocumentSearchWidget

var DocumentCommentWidget = function () {
  function DocumentCommentWidget(widgetData, parentWidget) {
    _classCallCheck(this, DocumentCommentWidget);

    this.widgetPrettyName = widgetData.commentGetter;
    this.widgetTitle = this.widgetPrettyName.split('--')[1].replace(/_/g, ' ').match(/([A-Z]?[^A-Z]*)/g).slice(0, -1).join(' ');
    this.documentComments = [];
    this.widgetID = '#document-widget-comment-container';
    this.addNewCommentID = '#add-new-widget-comment';
    this.parentWidget = parentWidget;
  }

  _createClass(DocumentCommentWidget, [{
    key: 'buildCommentWidgetHTML',
    value: function buildCommentWidgetHTML() {
      //TODO break up each segment into individual components e.g. close button, the container.
      var html = '<div id=\'document-widget-comment-container\' class=\'widget-comment-container\'>\n                <button type=\'button\' aria-label=\'Close\' id=\'document-widget-comment-close\' class=\'widget-comment-close close\'><span aria-hidden=\'true\'>&times;</span></button>\n                <div class=\'widget-comment-title text-capitalize text-center h4\'>' + this.widgetTitle + ' Comments</div>\n                ' + this.buildComments() + '\n                <div class=\'new-comment-container text-center\'>\n                <textarea rows=\'3\' cols=\'5\' placeholder=\'Enter Comment Text...\' class=\'new-comment-content form-control mb-2\'></textarea><button type=\'button\' id=\'add-new-widget-comment\' class=\'add-new-document-comment btn btn-primary\'>Add a New Comment</button>\n                </div>\n                </div>\n                ';
      return html;
    }

    //This method is for displaying comments outside of the popup widget

  }, {
    key: 'buidCommentsDisplayHTML',
    value: function buidCommentsDisplayHTML() {
      var html = '<div class=\'widget-comment-display-container\'>\n                <div class=\'widget-comment-title h4 text-center\'>All Comments for the above information</div>\n                ' + this.buildComments() + '\n                </div>\n               ';
      return html;
    }
  }, {
    key: 'buildComments',
    value: function buildComments() {
      var _this4 = this;

      var html = '<section class=\'widget-comments\'>\n                ' + this.documentComments.map(function (comment) {
        return _this4.buildComment(comment);
      }).join('') + '\n                </section>\n                ';
      return html;
    }
  }, {
    key: 'buildComment',
    value: function buildComment(comment) {
      var html = '<article class=\'widget-comment\'>\n                <p class=\'comment-content p-1\'>' + comment.comment + '</p>\n                <p class=\'comment-attribution p-1\'>by ' + comment.author + ' at ' + new Date(comment.created_time) + '</p>\n                </article> \n               ';
      return html;
    }
  }, {
    key: 'addNewComment',
    value: function addNewComment(comment) {
      var html = this.buildComment(comment);
      this.documentComments.push(comment);
      this.parentWidget.find('.widget-comments').append(html);

      //Add the has-widgets-class to the parent to mark it is having comments.  If it is already there it doesn't do anything.
      this.parentWidget.addClass('has-widget-comments');
    }
  }, {
    key: 'attachWidgetToDOM',
    value: function attachWidgetToDOM() {
      this.detachWidget();
      this.parentWidget.append(this.buildCommentWidgetHTML());
      $(this.widgetID).draggable();
      this.setupClickEvents();
    }
  }, {
    key: 'attachCommentsToDOM',
    value: function attachCommentsToDOM() {
      //TODO Make a method called buildCommentsDisplayHTML() and use that here instead
      this.parentWidget.append(this.buidCommentsDisplayHTML());
    }

    //TODO Maybe clean this up to better make use of the widget class with instance variables

  }, {
    key: 'setupClickEvents',
    value: function setupClickEvents() {
      var _this5 = this;

      //Setup Docuemnt Widget Comment Container close button
      var container = this.parentWidget;
      var commentCloseButton = container.find('#document-widget-comment-close');
      commentCloseButton.click(function () {
        _this5.detachWidget();
      });

      //Setup Add new Comment
      //TODO Make this work better with document widget class
      container.find('.add-new-document-comment').click(function () {
        var newCommentInput = container.find('.new-comment-content');
        var commentContent = newCommentInput.val().trim();
        var newCommentData = { 'newCommentContent': commentContent, 'widgetPrettyName': _this5.widgetPrettyName };

        //TODO Maybe change the input to a form for required validation
        //Don't submit if there is no comment
        if (commentContent) {
          var request = $.post('/add_new_document_comment/', newCommentData).done(function (data) {
            newCommentInput.val('');
            _this5.addNewComment(JSON.parse(data));
          });
        }
      });
    }
  }, {
    key: 'fetchComments',
    value: function fetchComments(container) {
      var _this6 = this;

      var commentData = { commentGetter: this.widgetPrettyName };
      var widget = this;
      var ajaxPromise = new Promise(function (resolve, reject) {
        var request = $.get('/fetch_document_comments/', commentData).done(function (data) {
          _this6.documentComments = JSON.parse(data);
          // self.attachWidgetToDOM(container);
          resolve('resolved');
        });
      });
      // p.then(val => {this.attachWidgetToDOM()});
      return ajaxPromise;
    }
  }, {
    key: 'detachWidget',
    value: function detachWidget() {
      //Remove the widget comment container if it already exists to prevent multiple copies existing simultaneously
      var documentWidgetContainer = $(this.widgetID);
      if (documentWidgetContainer.length > 0) {
        documentWidgetContainer.remove();
      }
    }
  }, {
    key: 'setupDocumentWidget',
    value: function setupDocumentWidget() {
      var _this7 = this;

      var ajaxPromise = this.fetchComments();
      // prom.then(comments => this.attachWidgetToDOM(container, comments));
      ajaxPromise.then(function (val) {
        _this7.attachWidgetToDOM();
      });
      // this.attachWidgetToDOM(container);
    }
  }, {
    key: 'setupDocumentComments',
    value: function setupDocumentComments() {
      var _this8 = this;

      var ajaxPromise = this.fetchComments();
      // prom.then(comments => this.attachWidgetToDOM(container, comments));
      // ajaxPromise.then(val => {this.attachWidgetToDOM()});
      ajaxPromise.then(function (val) {
        _this8.attachCommentsToDOM();
      });
      // this.attachWidgetToDOM(container);
    }
  }]);

  return DocumentCommentWidget;
}();

//Add the click handler and associated creation of the document comment widget
//This is used in document association


function setupDocumentCommentWidgets(container) {
  var commentButton = container.find('.comment_button');
  commentButton.click(function (e) {
    //TODO Check if creating a new object uses memory or something.
    // I'm not really used to using classes in javascript yet
    var currentButton = $(e.currentTarget);
    var commentWidget = new DocumentCommentWidget(currentButton.data(), currentButton.parent());
    commentWidget.setupDocumentWidget();
    // get_document_widget_comments(data, $(this).parent());
  });
}

function showContainerDocumentComments(container) {
  var all_widgets = container.find('.has-widget-comments').toArray();
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = all_widgets[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var widget = _step2.value;

      var jqueryWidget = $(widget);
      var commentButton = jqueryWidget.find('.comment_button');
      var commentWidget = new DocumentCommentWidget(commentButton.data(), jqueryWidget);
      commentWidget.setupDocumentComments();
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return false;
}

function showAllDocumentComments() {

  var all_widgets = $('.has-widget-comments').toArray();

  var _iteratorNormalCompletion3 = true;
  var _didIteratorError3 = false;
  var _iteratorError3 = undefined;

  try {
    for (var _iterator3 = all_widgets[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
      var widget = _step3.value;

      var jqueryWidget = $(widget);
      var existingCommentWidget = jqueryWidget.find('.widget-comment-display-container');
      if (existingCommentWidget.length > 0) {
        existingCommentWidget.show();
      } else {
        var commentButton = jqueryWidget.find('.comment_button');
        var commentWidget = new DocumentCommentWidget(commentButton.data(), jqueryWidget);
        commentWidget.setupDocumentComments();
      }
    }
  } catch (err) {
    _didIteratorError3 = true;
    _iteratorError3 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion3 && _iterator3.return) {
        _iterator3.return();
      }
    } finally {
      if (_didIteratorError3) {
        throw _iteratorError3;
      }
    }
  }

  return false;
}

function hideAllDocumentComments() {
  //TODO this is gross it should be done through the Widget objects somehow
  //They should be persisted somehow and back data behind each of them somehow.
  $('.widget-comment-display-container').hide();
  return false;
}

function setupShowAllDocumentCommentsButton() {

  $('#show-all-comments-button').click(function () {
    if ($(this).data('show') == 'show') {
      $(this).data('show', 'hide');
      $(this).text('Hide All Document Comments');
      showAllDocumentComments();
    } else {
      $(this).data('show', 'show');
      $(this).text('Show All Document Comments');
      hideAllDocumentComments();
    }
    return false;
  });

  return false;
}

//Setup the search Widget.  If the haml doesn't exist it will just exist with no inputs to trigger it.
var searchWidget = new DocumentSearchWidget();

searchWidget.setupClickEvents();
