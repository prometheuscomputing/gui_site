function organizer_break_associations(widget) {
  var collection_content = widget.find('.collection_content');
  var organizer_content = widget.find('.organizer_content');
  removal_association_container = widget.find('.pending_association_removals');
  addition_association_container = widget.find('.pending_association_additions');
  deletion_association_container = widget.find('.pending_association_deletions');
  // if find organizer_content, then it's contents are being displayed? Need to remove the contents
  // inputs will be the same whether looking at organizer contents or collection contents
  // Will only be one tr checked, they are radio
  var object_id;
  var object_class;

  // If organizer content, then we're looking at the original organizer being displayed
  if (organizer_content.length > 0 && !organizer_content.hasClass('hidden')) {
    //Store original name for possible later use in un-breaking it
    object_id = widget.find('input.identifier').val();
    object_class = widget.find('input.classifier').val();
    widget.find('.original_object_class_and_id').append(
      create_pending_inputs('original_object', object_id, object_class, null, null)
    );
    // widget.find('.organizer_content').remove();
    // organizer_content.addClass('hidden');
    // organizer_content.hide();
    hide_origanizer_content(widget);
  } else {
    widget.find('.collection_content_data').find('tr').each(function () {
      if ($(this).find('input:checked').length > 0) {
        var selected_row = $(this);
        object_id = selected_row.find('input.identifier').val();
        object_class = selected_row.find('input.classifier').val();
      }
    });
  }
  // Remove all objects from pending deletes
  deletion_association_container.find('.pending_association_deletion').each(function () {
    $(this).remove();
  });
  // Cancel out opposing inputs in 'add association' pending
  found_intersection = false;
  // Should only ever be one item in a time in the additions. Will Assume so.
  addition_association_container.find('.pending_association_addition').each(function(){
    var row = $(this);
    if (row.find('.identifier').val() === object_id && row.find('.classifier').val() === object_class) {
      found_intersection = true;
      row.remove();
      // widget.find('.organizer_content.hidden').show();
    }
  });
  // If no interestion with pending additions, then add to the pending removals
  if (!found_intersection) {
    removal_association_container.append(
      create_pending_inputs('pending_association_removal', object_id, object_class, null, null)
    );
  }

  no_breaks_pending = removal_association_container.find('.pending_association_removal').length === 0

  // If no breaks, then have to add back the break the original object name if it exists (In the case that the orignal object was broken, then added back, then broken again)
  if (no_breaks_pending && widget.find('.original_object').length > 0) {
    var original_object = widget.find('.original_object')
    var object_id = original_object.find('input.identifier').val();
    var object_class = original_object.find('input.classifier').val();
    existing_object = widget.find('input.original_existing_obj').attr('value');
    removal_association_container.append(
      create_pending_inputs('pending_association_removal', object_id, object_class, null, null)
    );
  }

  // Enable widget's create new object menu. Could have been disabled in a previous action.
  enable_object_create_dropdown_menu(widget);

  populate_collection($(collection_content));
  return false;
}


function organizer_delete_associations(widget) {
  var data_classifier = widget.find('.data_classifier').val();
  var data_id = widget.find('.data_id').val();
  var organizer_content = widget.find('.organizer_content');
  if (organizer_content.length > 0 && !organizer_content.hasClass('hidden')) {
    // organizer_content.remove();
    // organizer_content.addClass('hidden');
    // organizer_content.hide();
    hide_origanizer_content(widget)
  }
  var collection_content = widget.find('.collection_content');
  deletion_association_container = widget.find(".pending_association_deletions");

  deletion_association_container.append(
    create_pending_inputs('pending_association_deletion', data_id, data_classifier)
  );
  // Enable widget's create new object menu. Could have been disabled in a previous action.
  enable_object_create_dropdown_menu(widget);
  // No need to cancel out opposing inputs. Can't delete a pending 'add' or 'break' assoc
  populate_collection($(collection_content));
  return false;
}

function organizer_add_associations(widget) {
  var collection_content = widget.find('.collection_content');
  // var organizer_content = widget.find('.organizer_content');
  addition_association_container = widget.find('.pending_association_additions');
  removal_association_container = widget.find('.pending_association_removals');
  deletion_association_container = widget.find('.pending_association_deletions');
  var selected_row;
  widget.find('.collection_content_data').find('tr').each(function () {
    if ($(this).find('input:checked').length > 0) {
      selected_row = $(this);
    }
  });
  var selected_object_id = selected_row.find('input.identifier').val();
  var selected_object_class = selected_row.find('input.classifier').val();

  // Remove any other pending changes
  deletion_association_container.find('.pending_association_deletion').each(function () {$(this).remove(); });
  addition_association_container.find('.pending_association_addition').each(function () { $(this).remove(); });

  // Cancel out opposing inputs in 'add association' pending
  found_intersection = false;
  // Should only ever be one item in a time in the additions
  removal_association_container.find('.pending_association_removal').each(function(){
    var row = $(this);
    if (row.find('.identifier').val() === selected_object_id && row.find('.classifier').val() === selected_object_class) {
      found_intersection = true;
      row.remove();
    }
  });
  // If no interestion with pending removals, then add to the pending additions
  if (!found_intersection) {
    addition_association_container.append(
      create_pending_inputs('pending_association_addition', selected_object_id, selected_object_class, null, null)
    );
  }

  no_breaks_pending = removal_association_container.find('.pending_association_removal').length === 0

  // Grab values from original object
  var original_object = widget.find('.original_object');
  var original_object_id = null;
  var original_object_class = null;
  if (original_object) {
    original_object_id = original_object.find('input.identifier').val();
    original_object_class = original_object.find('input.classifier').val();
  }

  // If no breaks, then have to add back the break the original object name if it exists (In the case that the orignal object was broken, then added back, then a second object was added)
  if (no_breaks_pending && !found_intersection && original_object.length > 0) {
    existing_object = widget.find('input.original_existing_obj').attr('value');
    removal_association_container.append(
      create_pending_inputs('pending_association_removal', original_object_id, original_object_class, null, null)
    );
  }


  if (selected_object_class === original_object_class && selected_object_id === original_object_id) {
    show_origanizer_content(widget);
    // organizer_content.removeClass('hidden');
    // organizer_content.show();
    // collection_content.remove();
    // widget.find('.collection_content_data').remove();
    remove_collection_data(widget);
    // widget.find('table.bottom_bar').remove();
    // alert('test2');
  } else {

    collection_content.find('.toggle_view_all_listings').attr('checked', false);
    populate_collection($(collection_content), null, {'disable_create_obj_menu': true});

    // Disable widget to prevent trying to add multiple objects in a to-one relationship
    // disable_object_create_dropdown_menu(widget);
    // console.log('DISABLING DROPDOWN');
  }
  return false;
}

function hide_origanizer_content(widget) {
  var organizer_content = widget.find('.organizer_content');
  organizer_content.addClass('hidden');
  organizer_content.hide();
}

function show_origanizer_content(widget) {
  var organizer_content = widget.find('.organizer_content');
  organizer_content.removeClass('hidden');
  organizer_content.show();
}

// Organizer acts like a collection when this function is run
function organizer_undelete_associations(widget) {
  // var collection_content = widget.find('.collection_content');
  remove_collection_data(widget);
  show_origanizer_content(widget);
  // deletion_association_container = widget.find(".pending_association_deletions");
  // widget.find('.collection_content_data').find('tr').each(function () {
  //   var row = $(this);
  //   if (row.find('td.checkbox input.undeletable:checked').length > 0) {
  //     var object_id = row.find('input.identifier').val();
  //     var object_class = row.find('input.classifier').val();

  //     deletion_association_container.find('.pending_association_deletion').each(function(){
  //         var row = $(this);
  //         if (row.find('.identifier').val() === object_id && row.find('.classifier').val() === object_class) {
  //           row.remove();
  //         }
  //     });
  //   }
  // });
    // // Disable widget to prevent trying to add multiple objects in a to-one relationship
    // disable_object_create_dropdown_menu(widget);
  // populate_collection($(widget.find('.collection_content')));

  return false;
}

function setup_organizer_headers(container) {
  // container.find('.organizer_header').click(function() {
  //   var widget = $(this).parent()
  //   var content_div = widget.find('.organizer_content');
  //   // If the organizer content is hidden with this class, it means that we need to display or hide the collection content instead
  //   // - The org content is hidden so we can redisplay it in its org summary format if the user re-adds the pending broken association (one-to-many)
  //   if ($(content_div).hasClass('hidden')) {
  //     content_div = widget.find('.collection_content');
  //   }
  //   var is_visible  = $(content_div).is(':visible');
  //   if (is_visible) {
  //     $(content_div).hide();
  //     widget.addClass('collapsed');
  //   } else {
  //     $(content_div).show();
  //     widget.removeClass('collapsed');
  //   }
  // });
}

function setup_file_delete_checkbox(container) {
  container.find('.delete_label').click(function(e){
    if ( !$(this).hasClass('disabled') ) {
      $(this).closest('.stored_file').find('span').toggleClass('red-strikethrough');
    } else {
      e.preventDefault();
      return false;
    }
  });
}
