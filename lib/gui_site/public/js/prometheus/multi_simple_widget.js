$(function() {
  $(".multi_widget").each(function() {
    var template = $(this).prevAll('.multi_template');
    var mutable = (!template.attr('disabled'));
    if (mutable) {
      $(this).after(createRemoveButton($(this)));;
    }
  });

  $(".add_multi").click(function() {
    templateId = $(this).attr('id').substr(4) + "_template";
   
    addMultiWidget($('#' + templateId), $(this));
  });
});

function removeMultiWidget(ele) {
    // Trim the prefix
    thingToRemoveId = ele.attr('id').substr(13);
    $('#'+thingToRemoveId).remove();
    ele.remove();
}

function addMultiWidget(templateElement, beforeElement) {
  var mutable = (templateElement.attr('disabled') != 'disabled');
  newElement = templateElement.clone();
  newElement.removeClass('hidden');
  newElement.addClass('multi_widget');
  newElement.attr('name', newElement.attr('name').replace('template', 'edit'));

  baseId = newElement.attr('id').replace('_template', ''); 
  idIndex = findNextIndex(baseId, 'multi_widget');
  newElement.attr('id', baseId + idIndex);

  beforeElement.after(newElement);
  if (mutable) {
    rb = createRemoveButton(newElement); 
    newElement.after(rb);
    rb.after('<br />');
  }
}

function sortNumberDescending(a,b) {
  return b - a;
}


function findNextIndex(baseId, cssClass) {
  allWithBaseId = $('.' + cssClass + '[id*="' + baseId + '"]').map(function(i, val) {
    return parseInt(val.id.replace(baseId, ''));
  });

  allWithBaseId = allWithBaseId.get();

  allWithBaseId.sort(sortNumberDescending);

  return allWithBaseId[0] + 1;

}

function createRemoveButton(linkedElement) {
  removeButton = $("<input class='remove_multi' id='remove_multi_" + linkedElement.attr('id') + "' type='button' value='Delete' />");
  removeButton.click(function() {
    removeMultiWidget($(this));
  });
  return removeButton;
}
