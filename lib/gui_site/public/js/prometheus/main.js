// $.getScript('/js/tree_view.js');
// $.getScript('/js/test.js');
$(function() {
  // Set up clone link to trigger clone popup
  setup_clone_link();
  
  setup_syntax_highlighting($(this));
  
  var mainContainer = $(this);
  
  // Setup Richtext editors (MarkItUp)
  setup_richtext_editors(mainContainer);
  
  setup_widgets_to_submit_on_enter(mainContainer);
  
  setup_date_picker_widgets(mainContainer);
  
  // Convert the 'Save and Leave' button into a 'Save and close' button if conditions are met
  var close_on_save_and_leave = ($('input#close_on_save_and_leave').val() == 'true');
  if (close_on_save_and_leave && window.name == 'perform_task') {
    var save_and_leave = $('#save_and_leave');
    save_and_leave.val('Save and Close');
    save_and_leave.text('Save and Close');
    save_and_leave.click(function() {
      var form = $('form.organizer_form');
      form.append("<input name='save_and_close' type=hidden />");
      form.submit();
      return false;
    });
  }

  //temp setup for column_sorting stuff
  setup_column_sorting();

  setup_change_listeners();

  display_concurrency_dialog();

  display_uniqueness_dialog();
  
  display_action_dialog();
  
  setup_widget_toggle_links();
  
  setup_collection_positioning(mainContainer);
  // setup_dropdown_boxes(mainContainer);
  
  setup_collection_headers(mainContainer);
  
  setup_organizer_headers(mainContainer);
  
  setup_checkbox_inputs(mainContainer);
  
  setup_file_delete_checkbox(mainContainer);

  setup_collection_content_actions();
  
  var submittedForm = null;
  
  populate_expanded_content(mainContainer);
  
  setup_save_and_go_links(mainContainer);

  show_save_buttons_if_editable(mainContainer);
  
  setup_discard_and_go_links(mainContainer);
  
  setup_delete_confirmation_prompt_links(mainContainer);
  
  setup_tree_view_show_hide_button();
   
  // Instead of submitting form, gather information via JQuery and submit as JSON hash
  // This allows data to be collection about the structure of the collected widgets which
  // would otherwise be flattened into a parameters array.
  $('#root_view_form').submit(function(e){
    var form = $(this);
    var wrapper_content = $('#wrapper .content');
    var widget_structure = get_widget_structure(wrapper_content);
    var structure_json = JSON.stringify(widget_structure);
    form.append("<input name='widget_structure' type=hidden value='" + structure_json + "' />");
    // Do not show the spinner if we are going to finish very quickly.
    setTimeout(big_spinning_blocker_on, 500);
  });
  
  // Attaching listener to files. If uploading new, clearing out and disabling the delete checkbox
  $("input[type='file']").change(function(){
    var file_upload = $(this);
    // alert("input.widget_info[type='" + file_upload.attr('id') + "_delete_attrib']");
    file_delete_checkbox = $("input.attribute_delete_checkbox[name='" + file_upload.prop('id') + "_delete_attrib']");
    file_delete_label = $(".delete_label[for='" + file_upload.prop('id') + "_delete_attrib']");

    // If there's anything in the file upload path value
    if (file_upload.val().length > 0) {
      //Update the widget to display the new filename
      var filename = file_upload.val().split("\\").pop();
      file_upload.next('.custom-file-label').text(filename);
      
      if (file_delete_checkbox.length > 0) {
        file_delete_checkbox.removeAttr('checked').attr('disabled', 'true');
      }
      if (file_delete_label.length > 0) {
        file_delete_label.addClass('disabled not-allowed-cursor');
        file_delete_label.prop('disabled', 'true');
        file_delete_checkbox.prop('disabled', 'true');
      }
    } else { // Else the pathname is empty, no file selected.
      if (file_delete_checkbox.length > 0) {
        file_delete_checkbox.removeAttr('disabled');
      }
      if (file_delete_label.length > 0) {
        file_delete_label.removeClass('disabled');
      }
    }
  });
  
  setup_form_draft_saving();
  
  //Setup Validation on forms with the validation class and attribute
  setupFormValidation();
  
  //Testing new delete selected modal
  setupDeleteSelectedModal(mainContainer);
  
  setup_tooltips(mainContainer);
  
  setupPopovers(mainContainer);
  
  setup_miscellaneous_widgets(mainContainer);
  
  //These two mostly globals need to be wrapped in a closure or class like all of our other javascript.
  // root_data_id = document.querySelector('.root.view_widget input.data_id').value;
  // root_data_classifier = document.querySelector('.root.view_widget input.data_classifier').value;
  
  // autofocus on first input field...that is not a timestamp widget
  $('.simple_widget:not(.timestamp) :input:enabled:visible:first').focus();
  
  // MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
  
});

// $.ajaxSetup({cache: false})
