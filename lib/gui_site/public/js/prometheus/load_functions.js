markitup_root = "/js/editors/markitup";
wymeditor_root = "/js/editors/wymeditor";

// FIXME: this kind of thing should be replaced with something like require.js
function load_js(url, success_callback) {
  $.ajax({
    url:      url,
    async:    false,
    cache:    true,
    dataType: 'script'
  });
}
function load_markitup_style(style_name) {
  var head = $('head');
  // document.getElementById('markitup_style')
  if (!document.getElementById('markitup_style')) {
    head.append('<link id="markitup_style" rel="stylesheet" type="text/css" href="'+markitup_root+'/skins/markitup/style.css" />');
  }
  if (!document.getElementById(style_name + '_style')) {
    head.append('<link id="'+style_name+'_style" rel="stylesheet" type="text/css" href="'+markitup_root+'/sets/'+style_name+'/style.css" />');
  }
}

function load_markitup_js(js_name) {
  var head = $('head');
  if (!document.getElementById('markitup_js')) {
    head.append('<a id="markitup_js" style="display: none;" />');
    // load_js(markitup_root + '/jquery.markitup.pack.js');
    load_js(markitup_root + '/jquery.markitup.js');
  }
  if (!document.getElementById(js_name+'_js')) {
    head.append('<a id="'+js_name+'_js" style="display: none;" />');
    load_js(markitup_root + '/sets/'+js_name+'/set.js');
  }
}

//TODO all these functions need to be made async.  Having a blocking ajax call is disgusting - TB
function load_markdown(element) {
  $(function() {
    load_markitup_style('markdown');
    load_markitup_js('markdown');
    element.markItUp(markdownSettings);
    replace_placeholder_with_language_selector(element);
  });
}

//Probably ok using the same style as markdown for now
function load_kramdown(element) {
  $(function() {
    load_markitup_style('markdown');
    load_markitup_js('markdown');
    element.markItUp(markdownSettings);
    replace_placeholder_with_language_selector(element);
  });
}

function load_textile(element) {
  load_markitup_style('textile');
  $(function() {
    load_markitup_js('textile');
    element.markItUp(mySettings);
    replace_placeholder_with_language_selector(element);
  });
}

function load_latex(element) {
  load_markitup_style('latex');
  $(function() {
    load_markitup_js('latex');
    element.markItUp(latexSettings);
    replace_placeholder_with_language_selector(element);
  });
}

function load_html_wysiwyg(element) {
  // create_ckeditor_widget(element);
  create_summernote_widget(element);
  replace_placeholder_with_language_selector(element);
}

function load_ckeditor(element) {
  create_ckeditor_widget(element);
}

function remove_html_editor_widget(element) {
  // remove_ckeditor_widget(element);
  remove_summernote_widget(element);
}

function create_ckeditor_widget(element) {
  CKEDITOR.replace(element.prop("id"), {
    // width : '90%'
  });
}

function remove_ckeditor_widget(element) {
  var instances = CKEDITOR.instances[element.prop("id")];
  if (instances) { instances.destroy(true);}
}

function create_summernote_widget(element) {
  element.summernote();
}

function remove_summernote_widget(element) {
  element.summernote('destroy');
}

// Move the language selector from beneath the textarea to inside the MarkItUp header toolbar
function replace_placeholder_with_language_selector(markitup_element) {
  var containing_widget_row = markitup_element.closest('.widget').first();
  var language_selector = containing_widget_row.find('select.language_selector').first();
  
  //This doesn't work for the html editor.  It will jsut leave the select box floating on the right side.
  var header = containing_widget_row.find('.markItUpHeader');
  language_selector.insertAfter(header);
  setup_language_selector(language_selector, markitup_element);
}

// Move the language selector from the MarkItUp toolbar back to below the textarea
function unreplace_placeholder_with_language_selector(markitup_element) {
  var containing_widget_row = markitup_element.closest('.widget').first();
  var language_selector = containing_widget_row.find('select.language_selector');
  var editable_view = containing_widget_row.find('.editable_view');
  editable_view.prepend(language_selector);
}

function setup_language_selector(language_selector, markitup_element) {
  language_selector.change(function() {
    if (!$(this).is(':focus')) { return }
    var text = $.trim($(this).find('option:selected').text());
    var markitup_element = $(this).parents('.text.widget').find('textarea:not(.widget_initial_data)')
    unreplace_placeholder_with_language_selector(markitup_element);
    if ( markitup_element.hasClass('markItUpEditor') ) {
      markitup_element.markItUpRemove();
    }
    else {
      // remove_ckeditor_widget(markitup_element);
      remove_html_editor_widget(markitup_element);
    }
    // markitup_element.markItUpRemove();
    // markitup_element.markItUpRemove();
    // remove_ckeditor_widget(markitup_element);
    if (text == "Markdown") {
      load_markdown(markitup_element);
    } else if (text == "LaTeX") {
      load_latex(markitup_element);
    } else if (text == "Textile") {
      load_textile(markitup_element);
    } else if (text == "Kramdown") {
      load_kramdown(markitup_element);
    } else if (text == "HTML") {
      load_html_wysiwyg(markitup_element);
    }
      else {
      // Do nothing
    }
  });
}