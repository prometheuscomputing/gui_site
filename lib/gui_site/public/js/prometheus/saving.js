// Set up save_and_go links. Saves the page before redirecting to the desired link
function setup_save_and_go_links(content_div) {
  // content_div.find('.save_and_go_link').click(function() {
  //   return save_and_go($(this).attr('href'));
  // }); 
  
  //Function that sets up the click handler for the save and go links
  //This function will also allow for opening of new tabs if the control key or middle mouse button is used
  content_div.find('.save_and_go_link').on('click', function(e) {
    if ((e.which == 1 && (e.ctrlKey || e.altKey || e.metaKey)) || e.which == 2) {
        $(this).unbind('click.lc').bind('click.lc', function(event){
            event.preventDefault();
            window.open($(this).attr('href'), '_blank');
        });
    }
    else {
      return save_and_go($(this).attr('href'));
    }
  });
}

// Set up discard_and_go links. Still have to submit a form so that breadcrumbs will be updated.
function setup_discard_and_go_links(content_div) {
  // content_div.find('.discard_and_go_link').click(function() {
  //   return discard_and_go($(this).attr('href'));
  // });  
  content_div.find('.discard_and_go_link').on('click', function() {
    return discard_and_go($(this).attr('href'));
  });
}

// Javascript functions called when saving/discarding the current page and going to a new URL
function save_and_go(url) { return go_to_url(url, 'save_and_go'); }
function discard_and_go(url) { return go_to_url(url, 'discard_and_go'); }

// Take the given link class and make them submit the main form along with the link's information
function go_to_url(go_to_url, param_name) {
  // console.log("Inside go_to_url");
  // Get main organizer form. Note that it will not exist on the home page
  var form = $('form.organizer_form');
  
  if (form.length < 1) {
    form = $('form.document_form');
  }
  
  // If an organizer is not available, use alternate form.
  if (form.length < 1) {
    form = $('form.main_form');
  }
  
  // If alternate form is not available, create a new one.
  if (form.length < 1) {
    // Could not find organizer form. Create a new one instead
    $('#wrapper > .content').append('\
      <div id="submit_link_form" style="display:none;">\
        <form action="" method="POST">\
        </form>\
      </div>\
      ');
    form = $('#submit_link_form > form');
  }
  
  form.append("<input name='" + param_name + "' type=hidden />");
  // Destination URL. Still possibly needs to have assoc-id=& replaced with current domain obj id (after save occurs)      
  form.append("<input name='go_to_url' type=hidden value='" + go_to_url + "' />");
  form.append("<input name='from_url' type=hidden value='" + window.location.pathname + "' />");
  form.submit();
  
  return false; // Cancel the normal HREF navigation
}

$.blockUI.defaults.css = {
  padding:    0,
  margin:     0,
  width:      '30%',
  top:        '20%',
  left:       '35%',
  textAlign:  'center',
  color:      '#000',
  // border:     '3px solid #aaa',
  // backgroundColor:'#fff',
  cursor:     'wait'
};
function big_spinning_blocker_on() {
      $.blockUI({  overlayCSS: { backgroundColor: '#B0B0B0' }, message: '<img src="/images/128_spinner_23F5CB.gif" />' });
};

function show_save_buttons_if_editable(container) {
  // var saveContainer = document.getElementById('bottom-save-button-container');
  var saveContainer = $('#bottom-save-button-container');
  if (saveContainer && saveContainer.offsetHeight > 0) {
    if (container.find('.widget_data')) {
      // $('#bottom-save-button-container').show();
      saveContainer.show();
      // $('#save_and_stay').show();
      // $('#save_and_continue').show();
      // $('#save_and_leave').show();
      // $('#discard_and_stay').show();
    }
  }
  // if ($('#save_and_stay').is(':hidden') && container.find('.widget_data')) {
  //   $('#save_and_stay').show();
  //   $('#save_and_continue').show();
  //   $('#save_and_leave').show();
  //   $('#discard_and_stay').show();
  // }
};
