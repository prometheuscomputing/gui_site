// Set up richtext editors in the given container
function setup_richtext_editors(container) {
  // enable markdown for all .text-markdown class elements
  container.find('textarea.text-markdown').each(function(i, ele) { 
    if (ele.id) {
      load_markdown($('#' + ele.id));
    }  
  });
  
  // enable latex markitup editor for all .text-latex class elements
  container.find('textarea.text-latex').each(function(i, ele) { 
    if (ele.id) {
      load_latex($('#' + ele.id));
    }
  });
  
  // enable latex markitup editor for all .text-textile class elements
  container.find('textarea.text-textile').each(function(i, ele) { 
    if (ele.id) {
      load_textile($('#' + ele.id));
    }
  });
  
  container.find('textarea.text-kramdown').each(function(i, ele) {
    if (ele.id) {
      load_kramdown($('#' + ele.id));
    }
  });
  
  container.find('textarea.text-html').each(function(i, ele) {
    if (ele.id) {
      load_html_wysiwyg($('#' + ele.id));
    }
  });
  
  container.find('textarea.text-ckeditor').each(function(i, ele) {
    if (ele.id) {
      load_ckeditor($('#' + ele.id));
    }
  });
}

// Set up richtext editors in the given container
function setup_richtext_editors_new(container) {
  
  // var textAreaElement = container.find('textarea.widget_data').first()[0];
  // if ( textAreaElement ) {
  //   var textAreaClasses = textAreaElement.classList;
  //   switch (true) {
  //   case textAreaClasses.contains('text-markdown'):
  //     load_markdown($('#' + textAreaElement.id));
  //     break;
  //   case textAreaClasses.contains('text-latex'):
  //     load_latex($('#' + textAreaElement.id));
  //     break;
  //   case textAreaClasses.contains('text-textile'):
  //     load_textile($('#' + textAreaElement.id));
  //     break;
  //   case textAreaClasses.contains('text-kramdown'):
  //     load_kramdown($('#' + textAreaElement.id));
  //     break;
  //   case textAreaClasses.contains('text-html'):
  //     load_html($('#' + textAreaElement.id));
  //     break;
  //   case textAreaClasses.contains('text-ckeditor'):
  //     load_ckeditor($('#' + textAreaElement.id));
  //     break;
  //   }
  // }

  container.find('textarea.widget_data').each(function(i, ele){
    var textAreaClasses = ele.classList;
    switch (true) {
    case textAreaClasses.contains('text-markdown'):
      load_markdown($('#' + ele.id));
      break;
    case textAreaClasses.contains('text-latex'):
      load_latex($('#' + ele.id));
      break;
    case textAreaClasses.contains('text-textile'):
      load_textile($('#' + ele.id));
      break;
    case textAreaClasses.contains('text-kramdown'):
      load_kramdown($('#' + ele.id));
      break;
    case textAreaClasses.contains('text-html'):
      load_html($('#' + ele.id));
      break;
    case textAreaClasses.contains('text-ckeditor'):
      load_ckeditor($('#' + ele.id));
      break;
    }
  });
  
  // enable markdown for all .text-markdown class elements
  //This is inefficient
  // container.find('textarea.text-markdown').each(function(i, ele) {
 //    if (ele.id) {
 //      load_markdown($('#' + ele.id));
 //    }
 //  });
 //
 //  // enable latex markitup editor for all .text-latex class elements
 //  container.find('textarea.text-latex').each(function(i, ele) {
 //    if (ele.id) {
 //      load_latex($('#' + ele.id));
 //    }
 //  });
 //
 //  // enable latex markitup editor for all .text-textile class elements
 //  container.find('textarea.text-textile').each(function(i, ele) {
 //    if (ele.id) {
 //      load_textile($('#' + ele.id));
 //    }
 //  });
 //
 //  container.find('textarea.text-kramdown').each(function(i, ele) {
 //    if (ele.id) {
 //      load_kramdown($('#' + ele.id));
 //    }
 //  });
 //
 //  container.find('textarea.text-html').each(function(i, ele) {
 //    if (ele.id) {
 //      load_html_wysiwyg($('#' + ele.id));
 //    }
 //  });
 //
 //  container.find('textarea.text-ckeditor').each(function(i, ele) {
 //    if (ele.id) {
 //      load_ckeditor($('#' + ele.id));
 //    }
 //  });
  

}

// this is a lazy load function for image upload elements.  it only writes the
// HTML for the image uploader if we need it.  it should only write it once
// function displayImageUploadContainer(widget_div) {
//   if ($('#image_upload_popup').length == 0) {
//     // var root_data_classifier = get_root_data_classifier();
//     // var root_data_id = get_root_data_id();
//     var root_data_getter = get_root_data_getter();
//     var getter = widget_div.find('.widget_getter').val();
//     var pretty_id = widget_div.find('.pretty_id').val();
//     var parent_div_widget = widget_div.parents(".widget").first();
//     var root_data_classifier = parent_div_widget.find('.data_classifier').val();
//     var root_data_id = parent_div_widget.find('.data_id').val();
//     var through_classifier = parent_div_widget.find('.through_classifier').val();
//     var through_id = parent_div_widget.find('.through_id').val();
//
//     $.ajax({
//       type: 'POST',
//       url: '/image_upload_popup',
//       data: {
//         data_classifier: root_data_classifier,
//         data_id: root_data_id,
//         data_getter: root_data_getter,
//         getter: getter,
//         pretty_id: pretty_id,
//         through_classifier: through_classifier,
//         through_id: through_id
//       },
//       success: function(responseText) {
//         display_image_upload_popup_from_response(responseText, widget_div);
//       }
//     });
//   }
// }

function displayImageUploadContainer(widget_div) {
  // $('#richTextImageUploadModal').modal('toggle');  
  var root_data_getter = get_root_data_getter();
  var getter = widget_div.find('.widget_getter').val();
  var pretty_id = widget_div.find('.pretty_id').val();
  var parent_div_widget = widget_div.parents(".widget").first();
  var root_data_classifier = parent_div_widget.find('.data_classifier').val();
  var root_data_id = parent_div_widget.find('.data_id').val();
  var through_classifier = parent_div_widget.find('.through_classifier').val();
  var through_id = parent_div_widget.find('.through_id').val();
  
  var data = {
      data_classifier: root_data_classifier,
      data_id: root_data_id,
      data_getter: root_data_getter,
      getter: getter,
      pretty_id: pretty_id,
      through_classifier: through_classifier,
      through_id: through_id
  };
  
  var jqxhr = $.post('/image_upload_popup', data)
  .done(function(responseText) {
    $('body').append(responseText);
    var modal = $('#richTextImageUploadModal');
    
    // Make clicking an image dismiss the popup and paste the given image's Markup
    setup_richtext_image_selectors();
    setup_richtext_image_delete_links();
    
    var uploadButton = modal.find('#confirmRichTextImageUpload');
    
    // Re-enable the button when a file is selected
    
    modal.on('hidden.bs.modal', function (e) {
      $(this).modal('dispose');
      $(this).remove();
    });
    
    modal.find('.image_upload_file').change(function() {
      var file = $(this).val();
      if (file.length == 0) {
        uploadButton.prop('disabled', true).addClass('disabled');
      } else {
        uploadButton.prop('disabled', false).removeClass('disabled');
      }
    });
    
    // container.find('#confirmRichTextImageUpload').on('click', function(){
    modal.find('#confirmRichTextImageUpload').click(function() {
      var textWidget = widget_div;
    
      // Show loading message
      //$('.ui-dialog-buttonpane').append('<center id="content_loading_image">Cloning, please wait. &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/ajax-loader.gif" /></center>');
      var file_upload_input = modal.find('.image_upload_file');
      var image_upload_div = textWidget.find('.image_uploads');
      var getter = textWidget.find('.widget_getter').val();
    
      // Move the file upload into the image_upload div
      file_upload_input.appendTo(image_upload_div);
      file_upload_input.hide();
    
      $.markItUp({
        // replaceWith: "![Image Caption]("+absolute_image_url+getter+"/images/"+escape(file_upload_input.val().split(/(\\|\/)/g).pop())+" \"Image Title\")"
        replaceWith: "![Image Caption]("+getter+"/images/"+escape(file_upload_input.val().split(/(\\|\/)/g).pop())+" \"Image Title\")"
      });
    
      //Hides the modal
      modal.modal('hide');
      // modal.modal('dispose');
      // $('#image_upload_popup').remove();
    });
    
    // modal.modal('toggle');
    modal.modal('show');
  });
  
}

// Take the AJAX response and display as a popup
// function display_image_upload_popup_from_response(response, widget_div) {
//   // Attach response to body
//   $('body').append(response);
//
//   // Create buttons
//   var buttons = {};
//   buttons['Cancel'] = function() {
//     $('#image_upload_popup').remove();
//   };
//   // Note that this button doesn't actually upload, but instead marks the file to be uploaded
//   buttons['Upload'] = function() {
//     // Disable buttons right away
//     $(".ui-dialog-buttonpane button:contains('Upload')").button("disable");
//     // No way to cancel in-progress upload
//     $(".ui-dialog-buttonpane button:contains('Cancel')").button("disable");
//
//     // Show loading message
//     //$('.ui-dialog-buttonpane').append('<center id="content_loading_image">Cloning, please wait. &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/ajax-loader.gif" /></center>');
//     var file_upload_input = $('#image_upload_popup').find('.image_upload_file');
//     var image_upload_div = widget_div.find('.image_uploads');
//     var getter = widget_div.find('.widget_getter').val();
//
//     // Move the file upload into the image_upload div
//     file_upload_input.appendTo(image_upload_div);
//     file_upload_input.hide();
//
//     var parent_data_id = get_parent_data_id(widget_div);
//     var parent_data_classifier = get_parent_data_classifier(widget_div);
//
//     $.markItUp({
//       // replaceWith: "![Image Caption]("+absolute_image_url+getter+"/images/"+escape(file_upload_input.val().split(/(\\|\/)/g).pop())+" \"Image Title\")"
//       replaceWith: "![Image Caption]("+getter+"/images/"+escape(file_upload_input.val().split(/(\\|\/)/g).pop())+" \"Image Title\")"
//     });
//
//     $('#image_upload_popup').remove();
//   };
//   var image_upload_popup = $('#image_upload_popup')
//   image_upload_popup.dialog({
//     modal: true,
//     resizable: true,
//     width: 1000,
//     buttons: buttons,
//     position: ['middle', 'middle'],
//     close: function() {
//       // I think this occurs when 'esc' is pressed instead of pressing 'Cancel', but I'm not sure -SD
//       $('#image_upload_popup').remove();
//     }
//   });
//   // Initially disable the 'Upload' button (until a file is selected)
//   $(".ui-dialog-buttonpane button:contains('Upload')").button("disable");
//   // Re-enable the button when a file is selected
//   $('#image_upload_popup .image_upload_file').change(function() {
//     var file = $(this).val();
//     if (file.length == 0) {
//       $(".ui-dialog-buttonpane button:contains('Upload')").button("disable");
//     } else {
//       $(".ui-dialog-buttonpane button:contains('Upload')").button("enable");
//     }
//   });
//
//   // Make clicking an image dismiss the popup and paste the given image's Markup
//   setup_richtext_image_selectors();
//   setup_richtext_image_delete_links();
// }


function setup_richtext_image_delete_links() {
  setup_confirmation_prompt_links($('#richTextImageUploadModal'), '.delete_richtext_image', 'Delete', 'Are you sure you want to delete this image?', function(link){
    // Remove the image thumbnail
    $(link).parents('.image_thumb').remove();
  });
}


function setup_richtext_image_selectors() {
  $('.richtext_image_selector').click(function() {
    var url  = $(this).attr('href');
    $.markItUp({
      replaceWith: "![Image Caption](" + escape(url) + " \"Image Title\")"
    });
    $('#richTextImageUploadModal').modal('hide');
    return false;
  });
};