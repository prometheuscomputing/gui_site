function serialize_map(map) {
  var serialized_string = '';
  for (var key in map) {
    serialized_string = serialized_string + '&' + key + '=' + map[key];
  }
  return serialized_string;
}