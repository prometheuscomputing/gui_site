function setup_form_draft_saving() {
  //Setup the root view form to be a garlic form and have a conflict manager
  $( '.garlic_form' ).garlic({
      conflictManager: {
          enabled: true
          , garlicPriority: true
          , template: '<span class="garlic-swap"></span>'
          , message: "This button will restore the saved database data. Click here to swap the value back to the saved database value."
      }
  });
  
  //Alter the text on the button to swap the draft data and the database data
  $(".garlic-swap").on('click', function() {
    var garlic_span = $(this);
    var garlic_span_text = $(this).text();
    if (garlic_span_text == "This button will restore the saved database data. Click here to swap the value back to the saved database value.") {
      garlic_span.text('This button will restore your unsaved changes.  Click here to swap the value back to the changes you have made without saving.');
    }
    else {
      garlic_span.text("This button will restore the saved database data. Click here to swap the value back to the saved database value.");
    }
  });
  
  // Not the greatest way of doing this
  //Add a message near the pretty text view to show that it has a saved draft user data in the editable field
  var garlic_pretty_views = $('.garlic-swap').parent().parent().find('.pretty_view');
  if (garlic_pretty_views.length > 0) {
  
    garlic_pretty_views.each(function(){
      $(this).append('<span class="garlic_pretty_view_message">This textfield is a saved draft</span>');
      $(this).find('p').css('display', 'inline-block');
      $(this).find('p').text($(this).parent().find('.editable_view').children('div textarea').val());
    });
    
  }
  //Fix the pretty view text 
}