function setupFormValidation() {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('custom-validation-form');
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      checkChangePasswordInputs();
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
}

//This is just a basic example and the messages don't match what is on the page currently but it does block the submission and styles the form correctly.
function checkChangePasswordInputs() {
  //There should only ever be one of each of these on the page at once
  var oldPasswordInput = document.getElementById("old_password");
  var newPasswordInput = document.getElementById("new_password");
  var reenterNewPasswordInput = document.getElementById("reenter_new_password");
  
  if (newPasswordInput) {
    if ( newPasswordInput.value.length > 0 ) {
      if ( newPasswordInput.value != reenterNewPasswordInput.value ) {
        reenterNewPasswordInput.setCustomValidity('New Password and Reenter New Password do not match');
      } else {
        reenterNewPasswordInput.setCustomValidity('');
      }
    } else {
      reenterNewPasswordInput.setCustomValidity('');
    }
  }
   
  if (oldPasswordInput) {
    if ( oldPasswordInput.value.length > 0 && newPasswordInput.value.length < 1) {
      newPasswordInput.setCustomValidity('Please enter a new valid password with a length greater than 5 characters');
    } else {
      newPasswordInput.setCustomValidity('');
    }
  }

  // return false;
}

