// Set up save_and_go links. Saves the page before redirecting to the desired link
function load_spec() {
  big_spinning_blocker_on();
  $.ajax({
    type: 'POST',
    url: '/load_spec/',
    success: function() {
      location.reload();
    }
  });
}