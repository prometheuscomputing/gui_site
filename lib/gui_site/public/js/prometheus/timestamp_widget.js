function setup_date_picker_widgets(container) {
  container.find(".date .widget_data, .document_date .widget_data").datepicker({
    //dateFormat: "yy-mm-dd" // 2013-01-01
    dateFormat: "MM d, yy" // January 1, 2013
  });

  // NOTE: for below items, dateFormat and timeFormat must match
  //       output of Timestamp#pretty_time otherwise datetimepicker/timepicker refuses to
  //       initialize slider positions correctly and will pick a different date/time than
  //       the one originally present when opened
  container.find(".timestamp .widget_data, .document_timestamp .widget_data").datetimepicker({
    dateFormat: "MM d, yy", // January 1, 2013
    timeFormat: 'h:mm:sstt'
  });

  container.find(".time .widget_data, .document_time .widget_data").timepicker({
    timeFormat: 'h:mm:sstt'
  });
};