// Populate the collection from the database
// Called when user expands a collection header for the first time
// Focuses on the given selector within the populated content once finished
function populate_collection(content_div, focus_selector, options) {
  if (undefined === options) {
    options = {};
  }
  
  // console.log("In Populate collection: " + options['order_by_column']);
  var content = $(content_div);
  var widget  = content.parent();
  var disabled = widget.hasClass('disabled');
  var creation_disabled = widget.hasClass('creation_disabled');
  var deletion_disabled = widget.hasClass('deletion_disabled');
  var removal_disabled  = widget.hasClass('removal_disabled');
  var addition_disabled = widget.hasClass('addition_disabled');
  var traversal_disabled = widget.hasClass('traversal_disabled');
  
  // Temporary Kludge here to stop loss of unsaved positional data for collections
  var has_positioning_data = widget.find('.unsaved_changes_message').hasClass('has_unsaved_positioning');
  if (has_positioning_data && (options['confirm_data_loss'] === undefined || options['confirm_data_loss'] === false)) {
    return warn_about_positional_data_loss(widget, content_div, focus_selector);
  }

  var new_page_index  = get_page_number(content_div) - 1;
  var new_page_size   = get_page_size(content_div);
  var collection_size = get_collection_size(content_div);
  var order_by_column = options['order_by_column'];

  var is_populated = content.hasClass('populated');

  var filter_row_visible = content.find('.table_filter_row').is(':visible') ? true : false;
  var search_all_filter_row_visible = content.find('.table_search_all_filter_row').is(':visible') ? true : false;
  
  var toggle_view_all_listings = content.find('.toggle_view_all_listings');
  var show_possible_assoc = (toggle_view_all_listings.length > 0 && toggle_view_all_listings.is(':checked')) ? true : false;

  var previous_toggle_listings = (content.find('.previous_view_all_listings').val() === 'true' ? true : false);
  // Reset page number if toggling association listings
  if (previous_toggle_listings != show_possible_assoc) {
    new_page_index = 0;
  }

  var unassociated_only_input = content.find('input.unassociated_only')
  var unassociated_only = (unassociated_only_input.length > 0) ? unassociated_only_input.val() : 'n';
  var selection_input = content.find('input.selection');
  var selection = (selection_input.length > 0) ? selection_input.val() : 'n';
  var selection_param_name_input = content.find('input.selection_param_name')
  var selection_param_name = (selection_param_name_input.length > 0) ? selection_param_name_input.val() : null;
  var selection_create_input = content.find('input.selection_create');
  var selection_create = (selection_create_input.length > 0) ? selection_create_input.val() : 'n';
  var standalone_input = content.find('input.standalone');
  var standalone = (standalone_input.length > 0) ? standalone_input.val() : 'n';
  var object_view_name = content.find('input.object_view_name').val();
  var object_view_type = content.find('input.object_view_type').val();
  
  // For the pending associations, we could grab the assocations 'through' object information, but doesn't seem necessary right now for populate_collection
  var pending_association_additions = $(content_div).find('.pending_association_addition').map(function(i, el) {
    var addition = {};
    addition['identifier'] = $(el).find('input.identifier').val();
    addition['classifier'] = $(el).find('input.classifier').val();
    addition['skip_association'] = $(el).find('input.skip_association').val();
    return addition;
  });
  
  var pending_association_removals = $(content_div).find('.pending_association_removal').map(function(i, el) {
    var removal = {};
    removal['identifier'] = $(el).find('input.identifier').val();
    removal['classifier'] = $(el).find('input.classifier').val();
    return removal;
  });

  var pending_association_deletions = $(content_div).find('.pending_association_deletion').map(function(i, el) {
    var deletion = {};
    deletion['identifier'] = $(el).find('input.identifier').val();
    deletion['classifier'] = $(el).find('input.classifier').val();
    deletion['through_identifier'] = $(el).find('input.through_identifier').val();
    deletion['through_classifier'] = $(el).find('input.through_classifier').val();
    return deletion;
  });

  var previous_filter = get_previous_filter(content_div);
  var previous_search_all_filter = get_previous_search_all_filter(content_div);
  // var previous_page_size = get_previous_page_size(content_div);

  if (true === options['reset_filter']) {
    var filter_data = {};
    var search_all_filter = {};
  } else {
    var filter_data = get_filter_data(content_div, !show_possible_assoc);
    var search_all_filter = get_search_all_filter_data(content_div, !show_possible_assoc);
  }
  if (is_populated) { clear_collection(content_div); }

  // Display loading image
  content_div.find('.content_loading_image').html('<center id="content_loading_image"><img src="/images/ajax-loader.gif" /></center>');
  var data = {
    'pretty_id': get_pretty_id(widget),
    // NOTE: data_classifier and data_id should use _parent_ methods, not root.
    //       Currently, it would break selection collections to alter this.
    'data_classifier':               get_parent_data_classifier(widget),
    'data_id':                       get_parent_data_id(widget),
    'data_getter':                   get_data_getter(widget),
    'page_index':                    new_page_index,
    'page_size':                     new_page_size,
    'collection_size':               collection_size,
    'order_by_column':               get_table_sort_column(widget),
    'table_sort_direction':          get_table_sort_direction(widget),
    // 'max_page_index': max_page_index,
    // Get previous page size
    // 'previous_page_size': previous_page_size,
    'search_all_filter':             search_all_filter,
    'filter':                        filter_data,
    // Get previous filter data if any
    'previous_filter':               previous_filter,
    'previous_search_all_filter':    previous_search_all_filter,
    'show_filter_row':               (filter_row_visible ? 'y' : 'n'),
    'show_search_all_filter_row':    (search_all_filter_row_visible ? 'y' : 'n'),
    'show_possible_assoc':           (show_possible_assoc ? 'y' : 'n'),
    'unassociated_only':             unassociated_only,
    'selection':                     selection,
    'selection_param_name':          selection_param_name,
    'selection_create':              selection_create,
    'standalone':                    standalone,
    'view_type':                     get_view_type(widget),
    'view_name':                     get_view_name(widget),
    'object_view_name':              object_view_name,
    'object_view_type':              object_view_type,
    'pending_association_additions': $.makeArray(pending_association_additions),
    'pending_association_removals':  $.makeArray(pending_association_removals), 
    'pending_association_deletions': $.makeArray(pending_association_deletions),
    'disabled':                      disabled,
    'creation_disabled':             creation_disabled,
    'deletion_disabled':             deletion_disabled,
    'removal_disabled':              removal_disabled,
    'addition_disabled':             addition_disabled,
    'traversal_disabled':            traversal_disabled
  };
  var request = $.post('/populate_collection/', data)
                .done(function(responseHTML) {
                  content_div.append(responseHTML);
                  content_div.addClass('populated');   // Mark the collection as populated
                  setup_richtext_editors(content_div); // Setup Richtext editors (MarkItUp)
                  setup_show_possible_associations_toggle(content_div); // Now that the data is in place, setup the toggle to show possible associations
                  // setup_dropdown_boxes(content_div);   // Setup dropdowns for creation links
                  setup_collection_positioning(content_div); // (Possibly) setup positioning
                  setup_associated_object_selections(content_div); // Setup checkboxes/radio buttons to activate/deactivate actions for associated domain objects
                  setup_unassociated_object_selections(content_div); // Setup checkboxes/radio buttons to activate/deactivate actions for un-associated domain objects
                  setup_save_and_go_links(content_div); // Setup ajax call to save the page when leaving to create a new association.
                  setup_discard_and_go_links(content_div);
                  setup_delete_confirmation_prompt_links(content_div); //Setup confirmation_prompt links
                  setup_filter_cells_to_submit_on_enter(content_div); // Pressing enter in a filter input field clicks Go
                  setup_sort_arrow_direction(content_div); //Setup Search direction icons on table header
                  initialize_filter_appearance(content_div);
                  setup_filter_toggle_button(content_div);
                  setup_advanced_search_toggle(content_div);
                  content_div.find('.content_loading_image').empty(); // Remove loading image
                  // Disable create new object dropdown if option passed
                  if (options['disable_create_obj_menu'] === true) {
                    disable_object_create_dropdown_menu(widget);
                  }

                  if (options['show_unassoc_entry_count'] === true) {
                    disable_object_create_dropdown_menu(widget);
                  }
                  // Fire off ajax collection_populated event
                  content_div.trigger('content_populated');
                  content_div.trigger('collection_populated');
      
                  // MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
      
                  // Focus on the passed focus_selector (if available)
                  if (!(focus_selector === undefined || focus_selector === null)) { content_div.find(focus_selector).focus() }
                  
                })
                .fail(function(data, responseText){
                  content_div.find('.content_loading_image').empty(); // Remove loading image
                  content_div.append(data.responseText);
                });
  
  return request;
}

function setup_advanced_search_toggle(content_div) {
  content_div.find('.advanced_search_link').click(function () {
    var row = content_div.find('.table_filter_row');
    var search_all_input = content_div.find('.table_search_all_filter_row .search_all_filter');        
    if (row.hasClass('d-none')) {   
      search_all_input.val('');
      search_all_input.prop('disabled', true);
      row.find('.filter_cell input').first().focus();
      $(this).text("Hide Individual Columns Search");
    }
    else {
      clear_filter_inputs(content_div);
      search_all_input.prop('disabled', false);
      search_all_input.focus();
      $(this).text("Show Individual Columns Search");
    }   
    row.toggleClass('d-none');
    return false;
  });
}

function setup_filter_toggle_button(content_div) {
  // Setup filter toggle button  
  content_div.find('.filter_toggle_cell a').click(function () {
    var search_all_filter_row = content_div.find('.table_search_all_filter_row');
    var advanced_filter_row = content_div.find('.table_filter_row');
    var search_all_filter_input = search_all_filter_row.find('.search_all_filter');

    if (search_all_filter_input.length > 0) {
      search_all_filter_row.toggleClass('d-none');
      if (search_all_filter_input.prop('disabled') && !search_all_filter_row.hasClass('d-none')) {
        advanced_filter_row.removeClass('d-none');
        advanced_filter_row.find('.filter_cell input').first().focus();
      } else {
        advanced_filter_row.addClass('d-none');
        search_all_filter_input.focus();
      }
    } else {
      advanced_filter_row.toggleClass('d-none');
    }
    return false;
  });
}

// function populate_infinite_collection(content_div, focus_selector, options) {
//   if (undefined === options) {
//     options = {};
//   }
//
//   var content = $(content_div);
//   var widget =  content.parent();
//   // Temporary Kludge here to stop loss of unsaved positional data for collections
//   var has_positioning_data = widget.find('.unsaved_changes_message').hasClass('has_unsaved_positioning');
//   if (has_positioning_data && (options['confirm_data_loss'] === undefined || options['confirm_data_loss'] === false)) {
//     return warn_about_positional_data_loss(widget, content_div, focus_selector);
//   }
//   var disabled = widget.hasClass('disabled');
//   var creation_disabled  = widget.hasClass('creation_disabled');
//   var deletion_disabled  = widget.hasClass('deletion_disabled');
//   var removal_disabled   = widget.hasClass('removal_disabled');
//   var addition_disabled  = widget.hasClass('addition_disabled');
//   var traversal_disabled = widget.hasClass('traversal_disabled');
//
//   var new_page_index =        get_page_number(content_div) - 1;
//   var new_page_size =         get_page_size(content_div);
//   var collection_size =       get_collection_size(content_div);
//   var order_by_column =       options['order_by_column'];
//   var total_number_of_pages = get_collection_total_pages(content_div);
//
//   if (new_page_index >= total_number_of_pages) {
//     return 0;
//   }
//
//   var is_populated = content.hasClass('populated');
//
//   var filter_row_visible = content.find('.table_filter_row').is(':visible') ? true : false;
//
//   var toggle_view_all_listings = content.find('.toggle_view_all_listings');
//   var show_possible_assoc = (toggle_view_all_listings.length > 0 && toggle_view_all_listings.is(':checked')) ? true : false;
//
//   var previous_toggle_listings = (content.find('.previous_view_all_listings').val() === 'true' ? true : false);
//   // Reset page number if toggling association listings
//   if (previous_toggle_listings != show_possible_assoc) {
//     new_page_index = 0;
//   }
//
//   var unassociated_only_input = content.find('input.unassociated_only')
//   var unassociated_only = (unassociated_only_input.length > 0) ? unassociated_only_input.val() : 'n';
//   var selection_input = content.find('input.selection');
//   var selection = (selection_input.length > 0) ? selection_input.val() : 'n';
//   var selection_param_name_input = content.find('input.selection_param_name')
//   var selection_param_name = (selection_param_name_input.length > 0) ? selection_param_name_input.val() : null;
//   var selection_create_input = content.find('input.selection_create');
//   var selection_create = (selection_create_input.length > 0) ? selection_create_input.val() : 'n';
//   var standalone_input = content.find('input.standalone');
//   var standalone = (standalone_input.length > 0) ? standalone_input.val() : 'n';
//   var object_view_name = content.find('input.object_view_name').val();
//   var object_view_type = content.find('input.object_view_type').val();
//   // For the pending associations, we could grab the assocations 'through' object information, but doesn't seem necessary right now for populate_collection
//   var pending_association_additions = $(content_div).find('.pending_association_addition').map(function(i, el) {
//     var addition = {};
//     addition['identifier'] = $(el).find('input.identifier').val();
//     addition['classifier'] = $(el).find('input.classifier').val();
//     addition['skip_association'] = $(el).find('input.skip_association').val();
//     return addition;
//   });
//
//   var pending_association_removals = $(content_div).find('.pending_association_removal').map(function(i, el) {
//     var removal = {};
//     removal['identifier'] = $(el).find('input.identifier').val();
//     removal['classifier'] = $(el).find('input.classifier').val();
//     return removal;
//   });
//
//   var pending_association_deletions = $(content_div).find('.pending_association_deletion').map(function(i, el) {
//     var deletion = {};
//     deletion['identifier'] = $(el).find('input.identifier').val();
//     deletion['classifier'] = $(el).find('input.classifier').val();
//     return deletion;
//   });
//
//
//   var previous_filter = get_previous_filter(content_div);
//   // var previous_page_size = get_previous_page_size(content_div);
//
//   if (true === options['reset_filter']) {
//     var filter_data = {};
//   } else {
//     var filter_data = get_filter_data(content_div, !show_possible_assoc);
//   }
//
//   content_div.find('.content_loading_image').html('<center id="content_loading_image"><img src="/images/ajax-loader.gif" /></center>');
//   $.ajax({
//     type: 'POST',
//     url: '/populate_collection/',
//     data: {
//       'pretty_id': get_pretty_id(widget),
//       // NOTE: data_classifier and data_id should use _parent_ methods, not root.
//       //       Currently, it would break selection collections to alter this.
//       'data_classifier': get_parent_data_classifier(widget),
//       'data_id': get_parent_data_id(widget),
//       'data_getter': get_data_getter(widget),
//       'page_index': new_page_index,
//       'page_size': new_page_size,
//       'collection_size': collection_size,
//       'order_by_column': get_table_sort_column(widget),
//       'table_sort_direction': get_table_sort_direction(widget),
//       // 'max_page_index': max_page_index,
//       // Get previous page size
//       // 'previous_page_size': previous_page_size,
//       'filter': filter_data,
//       // Get previous filter data if any
//       'previous_filter': previous_filter,
//       'show_filter_row': (filter_row_visible ? 'y' : 'n'),
//       'show_possible_assoc': (show_possible_assoc ? 'y' : 'n'),
//       'unassociated_only': unassociated_only,
//       'selection': selection,
//       'selection_param_name': selection_param_name,
//       'selection_create': selection_create,
//       'standalone': standalone,
//       'view_type': get_view_type(widget),
//       'view_name': get_view_name(widget),
//       'object_view_name': object_view_name,
//       'object_view_type': object_view_type,
//       'pending_association_additions': $.makeArray(pending_association_additions),
//       'pending_association_removals': $.makeArray(pending_association_removals),
//       'pending_association_deletions': $.makeArray(pending_association_deletions),
//       'disabled':           disabled,
//       'creation_disabled':  creation_disabled,
//       'deletion_disabled':  deletion_disabled,
//       'removal_disabled':   removal_disabled,
//       'addition_disabled':  addition_disabled,
//       'traversal_disabled': traversal_disabled
//     },
//     dataType: 'html',
//     success: function(data, status) {
//       var html_data = $.parseHTML(data);
//       var table_rows = $(html_data).find('table tr.domain_obj');
//       // var page_size = get_page_size(content);
//       // var number_of_new_rows = table_rows.size();
//       // set_page_size( content, parseInt(page_size) + number_of_new_rows);
//
//       var infinite_table = content.find('table').first();
//       infinite_table.append(table_rows);
//
//       // infinite_table.fixedHeaderTable('show');
//
//       // Remove loading image
//       content_div.find('.content_loading_image').empty();
//       // Focus on the passed focus_selector (if available)
//       if (!(focus_selector === undefined || focus_selector === null)) { content_div.find(focus_selector).focus() }
//     }
//   });
//
// }
// 
// function setup_infinite_scrolling(content) {
//   var widget = content.parent();
//   var table_div = content.find('.infinite_scroll_div');
//   //TODO Investigate if the scroll functionality can be placed on the table itself
//   $(table_div).scroll(function() {
//     var paddT = parseInt(table_div.css('padding-top'),10);
//      if(table_div.scrollTop() + $(table_div).height() == table_div[0].scrollHeight - paddT) {
//          load_next_page_with_scroll(content);
//      }
//
//   });
// }
//
// function setup_floating_table_header(content) {
//   var $table = content.find('.infinite_scroll_div table').first();
//   $table.floatThead({
//       scrollContainer: function($table){
//           return $table.closest('.infinite_scroll_div');
//       }
//   });
// }
//
// function load_next_page_with_scroll(content) {
//   var current_page_index = parseInt(get_page_number(content), 10);
//   var total_number_of_pages = parseInt(get_collection_total_pages(content), 10);
//
//   if (current_page_index < total_number_of_pages) {
//     set_page_number(content, current_page_index + 1);
//     populate_infinite_collection(content);
//   }
// }

function populate_expanded_content(container) {
  var expanded_content_divs = container.find('.collection.expanded .view_content');
  expanded_content_divs.each(function(index, div) {
    populate_collection($(div));
  });
  
  // var expanded_document_association_divs = container.find('.document_association.expanded .view_content');
  // expanded_document_association_divs.each(function(index, div) {
  //   populate_document_association($(div));
  // });
  
  var expanded_document_association_divs = container.find('.document_association.expanded .view_content');
  var len = expanded_document_association_divs.length;
  var i = 0;
  for (i; i < len; i++) {
     populate_document_association($(expanded_document_association_divs[i]));
  }
  
  
  //This will delay the ajax call until the container's label is clicked.  It is triggered by a widget's expanded attribute. Expanded = false will be collapsed by default.
  // var collapsed_document_association_divs = container.find('.document_association.collapsed .view_content');
  // collapsed_document_association_divs.each(function(index, div) {
  //   delayed_populate_document_association($(div));
  // });
  
  //This will delay the ajax call until the container's label is clicked.  It is triggered by a widget's expanded attribute. Expanded = false will be collapsed by default.
  var collapsed_document_association_divs = container.find('.document_association.collapsed .view_content');
  i = 0;
  len = collapsed_document_association_divs.length;
  for (i; i < len; i++) {
    delayed_populate_document_association($(collapsed_document_association_divs[i]));
  }
  
  //Setup ajax calls for new section widget.  Both delayed and normal
  //Expanded auto ajax
  // Probably not neccessary now that the section should just yield on through without Ajax.
  // var expanded_document_sections = container.find('.section.widget.expanded .view_content');
  // expanded_document_sections.each(function(){
  //   populate_document_section($(this));
  // });
  
  //Collapsed delayed ajax
  // var expanded_document_sections = container.find('.section.widget.collapsed .view_content');
  // expanded_document_sections.each(function(){
  //   delayed_populate_document_section($(this));
  // });

  var expanded_document_sections = container.find('.section.widget.collapsed .view_content');
  i = 0;
  len = expanded_document_sections.length;
  for (i; i < len; i++) {
    delayed_populate_document_section($(expanded_document_sections[i]));
  }

  // Setup "enter" handling for widgets
  // setup_widgets_to_submit_on_enter(container);
  
  // MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
  
  // setup_date_picker_widgets(container);
}

function setup_collection_content_actions() {
  // Setup "Go" button
  $(document).on('click', '.collection button[name="go_collection_page"], .organizer button[name="go_collection_page"]', function() {
    var content_div = $(this).parents('.collection_content').first();
    populate_collection(content_div);
  });
  // Setup "Clear Filter" button
  $(document).on('click', '.collection .clear_filter_collection_page, .organizer .clear_filter_collection_page', function() {
    var content_div = $(this).parents('.collection_content').first();
    populate_collection(content_div, undefined, { 'reset_filter': true });
  });
  // Setup "Next" and "Previous" page buttons
  $(document).on('click', '.collection a.next_collection_page, .organizer a.next_collection_page', function() {
    var content_div = $(this).parents('.collection_content').first();
    set_page_number(content_div, (parseInt(get_page_number(content_div), 10) + 1));
    populate_collection(content_div);
    return false;
  });
  $(document).on('click', '.collection a.previous_collection_page, .organizer a.previous_collection_page', function() {
    var content_div = $(this).parents('.collection_content').first();
    set_page_number(content_div, (parseInt(get_page_number(content_div), 10) - 1));
    populate_collection(content_div);
    return false;
  });
  //Setup Individual Page links
  $(document).on('click', '.collection a.pagination-collection-link, .organizer a.pagination-collection-link', function() {
    var content_div = $(this).parents('.collection_content').first();
    var page_number = $(this).data("page-number");
    set_page_number(content_div, parseInt(page_number, 10));
    populate_collection(content_div);
    return false;
  });
  
}

function clear_collection(content_div) {
  content_div.children('.collection_content_data').remove();
  content_div.children('table').remove();
  content_div.removeClass('populated');
}

function create_pending_inputs(div_class, object_id, object_class, through_id, through_class) {
  var inputs = (
    "<div class='" + div_class + "'>" +
    "<input style='display:none;' class='identifier' value='" + object_id + "' />" +
    "<input style='display:none;' class='classifier' value='" + object_class + "' />"
  );
  if (through_id !== undefined && through_id !== null && through_class !== undefined && through_class !== null) {
    inputs = inputs.concat(
      "<input style='display:none;' class='through_identifier' value='" + through_id + "' />" +
      "<input style='display:none;' class='through_classifier' value='" + through_class + "' />" +
      "</div>"
    );
  }
  return inputs;
}

function collection_break_associations(widget, remove_or_delete) {
  var content = widget.find('.collection_content');
  addition_association_container = widget.find(".pending_association_additions");
  removal_association_container = widget.find(".pending_association_removals");
  deletion_association_container = widget.find(".pending_association_deletions");
  widget.find('.collection_content_data').find('tr').each(function () {
    var row = $(this);
    if (row.find('input:checked').length > 0) {
      var object_id = row.find('input.identifier').val();
      var object_class = row.find('input.classifier').val();
      // TODO: fix the variable names below
      // I don't know what these variables have to do with position
      // They seem to be the ID and class of the join entry. -SD
      var position_id = row.find('input.through_identifier').val();
      var position_class = row.find('input.through_classifier').val();
      // If has no 'to' object, then move to delete container instead of break to delete the join object.
      if ($(this).hasClass('no_to_object')) {
        deletion_association_container.append(
          create_pending_inputs('pending_association_deletion', object_id, object_class, position_id, position_class)
        );
      } else {
        // Find the object in the pending additions, and remove it if it exists
        found_intersection = false;
        addition_association_container.find('.pending_association_addition').each(function(){
          var row = $(this);
          if (row.find('.identifier').val() === object_id && row.find('.classifier').val() === object_class) {
            found_intersection = true;
            row.remove();
          }
        });

        // If no interestion with pending additions, then add to the pending removals
        if (!found_intersection) {
          removal_association_container.append(
            create_pending_inputs('pending_association_removal', object_id, object_class, position_id, position_class)
          );
        }
      }
    }
  });
  populate_collection($(content));
  return false;
}

// Need CSS changes as well - use disable\enable in to_one organizers.....
function disable_object_create_dropdown_menu(widget) {
  if (!is_object_create_dropdown_disabled(widget)) {
    widget.find('ul.dropdown_menu').addClass('disabled');
  }
}

function enable_object_create_dropdown_menu(widget) {
  if (is_object_create_dropdown_disabled(widget)) {
    widget.find('ul.dropdown_menu').removeClass('disabled');
  }
}

function is_object_create_dropdown_disabled(widget) {
  if (widget.find('ul.dropdown_menu').hasClass('disabled')) {
    return true;
  } else {
    return false;
  }
}

// The purpose of this function is to remove the visible parts of the collection,
// while preserving the divs like the ones that store pending association changes (but not the data contained within them).
function remove_collection_data(widget) {
  collection_content = widget.find('.collection_content')
  if (collection_content.hasClass('populated')) {
    collection_content.removeClass('populated');
  }
  widget.find('.original_object_class_and_id').empty();
  widget.find('.collection_content_data').remove();
  widget.find('table.bottom_bar').remove();
}

function collection_undelete_associations(widget) {
  deletion_association_container = widget.find(".pending_association_deletions");
  widget.find('.collection_content_data').find('tr').each(function () {
    var row = $(this);
    if (row.find('td.checkbox input.undeletable:checked').length > 0) {
      var object_id = row.find('input.identifier').val();
      var object_class = row.find('input.classifier').val();
      // unused variables
      // var position_id = row.find('input.through_identifier').val();
      // var position_class = row.find('input.through_classifier').val();

      deletion_association_container.find('.pending_association_deletion').each(function(){
          var row = $(this);
          if (row.find('.identifier').val() === object_id && row.find('.classifier').val() === object_class) {
            row.remove();
          }
      });
    }
  });
  populate_collection($(widget.find('.collection_content')));
  return false;
}

function collection_delete_associations(widget) {
  var content = widget.find('.collection_content');
  deletion_association_container = widget.find(".pending_association_deletions");
  widget.find('.collection_content_data').find('tr').each(function () {
    var selected_row = $(this);
    var object_id = selected_row.find('input.identifier').val();
    var object_class = selected_row.find('input.classifier').val();
    var position_id = selected_row.find('input.through_identifier').val();
    var position_class = selected_row.find('input.through_classifier').val();
    // skip any elements from deletion if they are a pending association change, but really, should never reach here
    if (selected_row.find('input:checked').length > 0 && !selected_row.hasClass('pending_assoc')) {
      deletion_association_container.append(
        create_pending_inputs('pending_association_deletion', object_id, object_class, position_id, position_class)
      );
    }
  });
  // No need to cancel out opposing inputs. Can't delete a pending 'add' or 'break' assoc
  populate_collection($(content));
  return false;
}

function collection_add_associations(widget) {
  var content = widget.find('.collection_content');
  addition_association_container = widget.find('.pending_association_additions');
  removal_association_container = widget.find(".pending_association_removals");
  // list_of_values = [];
  widget.find('.collection_content_data').find('tr.domain_obj_row').each(function () {
    var selected_row = $(this);
    var object_id = selected_row.find('input.identifier').val();
    var object_class = selected_row.find('input.classifier').val();
    var position_id = selected_row.find('input.through_identifier').val();
    var position_class = selected_row.find('input.through_classifier').val();
    if ($(this).find('input:checked').length > 0) {
      found_intersection = false;
      removal_association_container.find('.pending_association_removal').each(function(){
        var row = $(this);
        if (row.find('.identifier').val() === object_id && row.find('.classifier').val() === object_class) {
          found_intersection = true;
          row.remove();
        }
      });

      // If no interestion with pending removals, then add to the pending additions
      if (!found_intersection) {
        addition_association_container.append(
          create_pending_inputs('pending_association_addition', object_id, object_class, position_id, position_class)
        );
      }
    }
  });
  
  
  content.find('.toggle_view_all_listings').attr('checked', false);
  populate_collection($(content));
  return false;
}

function get_collection_total_pages(content_div) {
  var total_page_container = content_div.parents('.widget').first().find('.total_pages');
  var total_page_count = 0;
  if (total_page_container.length > 0) {
    total_page_count = total_page_container.text();
  }
  return total_page_count;
}

function get_collection_size(content_div) {
  var has_collection_size = content_div.parents('.widget').first().find('input[name="collection_size"]').length > 0;
  if (has_collection_size) {
    return content_div.parents('.widget').first().find('input[name="collection_size"]').val();
  }
  else {
    return 0;
  }
}

function setup_show_possible_associations_toggle(content_div) {
  var widget = content_div.parents('.widget').first();
  widget.find('.toggle_view_all_listings').click(function() {
    var checkbox = $(this);

    // Pass to populate collection, any options that should be persisted.
    var options = {};
    if (is_object_create_dropdown_disabled(widget)) {
      options['disable_create_obj_menu'] = true;
    }
    // Instead of hiding/unhiding rows, we will instead repopulate the collection
    populate_collection($(this).parents('.collection_content').first(), null, options);
    return false;
  });

}

// Activates the break_association and/or delete_selected button when an association(s) is selected
function setup_associated_object_selections(content_div) {
  // This could either be checkboxes or radio buttons
  var domain_obj_selections = content_div.find('tr.domain_obj td.checkbox input');
  
  var domain_obj_rows = content_div.find('tr.domain_obj');
  
  domain_obj_selections.click(function() {

    var parent_row = $(this).parents('tr').first();
    if ($(this).is(':checkbox')) {
      // A checkbox has been selected/deselected
      if ($(this).is(':checked')) {
        parent_row.removeClass('unselected');
        parent_row.addClass('selected table-primary');
      } else {
        parent_row.removeClass('selected table-primary');
        parent_row.addClass('unselected');
      }
      // Loop through all checkboxes to determine if at least one is checked
      var is_any_deletable_selected = domain_obj_selections.is('.deletable:checked');
      var is_any_selected = domain_obj_selections.is(':checked');
      var is_any_pending_selected = domain_obj_selections.is('.pending:checked');
      var is_any_undeletable_selected = domain_obj_selections.is('.undeletable:checked');

      // Convert button to delete and enable if deletable objects are selected without undeletables or pendings
      if (is_any_deletable_selected && !is_any_undeletable_selected && !is_any_pending_selected) {
        convert_undelete_button_to_delete(content_div.find('.delete_selected, .undelete_selected'));
        content_div.find('.delete_selected').removeAttr('disabled', 'disabled');
      }
      // Can't break pending deletes. Can break pendings though.
      if (is_any_selected && !is_any_undeletable_selected) {
        content_div.find('.break_submit').removeAttr('disabled', 'disabled');
      }
      // Convert button to undelete if any undeletables are selected and no deletable or pendings
      if (is_any_undeletable_selected && !is_any_deletable_selected && !is_any_pending_selected) {
        convert_delete_button_to_undelete(content_div.find('.delete_selected, .undelete_selected'));
        content_div.find('.undelete_selected').removeAttr('disabled', 'disabled');
      }
        // No checkboxes are checked or undeletes selected
      if (!is_any_selected || is_any_undeletable_selected) {
        content_div.find('.break_submit').attr('disabled', 'disabled');
      }
      // Neither deletable nor undeletable selected, or both are selected. Reset delete button. Handles all cases where should be disabled
      booleans_active = 0;
      $.each([is_any_deletable_selected, is_any_pending_selected, is_any_undeletable_selected], function (index, boolean) {
        if (boolean === true) {
          booleans_active = booleans_active + 1;
        }
      });

      if (booleans_active !== 1) {
        convert_undelete_button_to_delete(content_div.find('.delete_selected, .undelete_selected'));
        content_div.find('.delete_selected').attr('disabled', 'disabled');
      }
    } else {
      // A radio button has been clicked
      if ($(this).is('.deletable')) {
        content_div.find('.delete_selected').removeAttr('disabled', 'disabled');
      } else {
        content_div.find('.delete_selected').attr('disabled', 'disabled');
      }
      domain_obj_rows.removeClass('selected table-primary');
      domain_obj_rows.addClass('unselected');
      parent_row.removeClass('unselected');
      parent_row.addClass('selected table-primary');
      content_div.find('.break_submit').removeAttr('disabled', 'disabled');
    }
  });
}

function setup_unassociated_object_selections(content_div) {
  var unassociated_obj_rows = content_div.find('tr.unassociated_object');

  // This could either be checkboxes or radio buttons (although only radio buttons are used at the moment)
  var domain_obj_selections = unassociated_obj_rows.find('td.checkbox input');

  domain_obj_selections.click(function() {
    var parent_row = $(this).parents('tr').first();
    if ($(this).is(':checkbox')) {
      // A checkbox has been selected/deselected
      if ($(this).is(':checked')) {
        parent_row.removeClass('unselected');
        parent_row.addClass('selected table-primary');
      } else {
        parent_row.removeClass('selected table-primary');
        parent_row.addClass('unselected');
      }
      // Loop through all checkboxes to determine if at least one is checked
      var is_any_selected = domain_obj_selections.is(':checked');
      
      if (is_any_selected) {
        // A checkbox(s) is checked
        content_div.find('.assoc_submit').removeAttr('disabled', 'disabled');
      } else {
        // No checkboxes are checked
        content_div.find('.assoc_submit').attr('disabled', 'disabled');
      }
    } else {
      // A radio button has been clicked
      unassociated_obj_rows.removeClass('selected table-primary');
      unassociated_obj_rows.addClass('unselected');
      parent_row.removeClass('unselected');
      parent_row.addClass('selected table-primary');
      content_div.find('.assoc_submit').removeAttr('disabled', 'disabled');
    }
  });
}

// custom accodion-like widget that collapses collections when clicking the haeder
function setup_collection_headers(container) {
  container.find('.collection_header').click(function() {
    var header = $(this);
    var widget = header.parent();
    var content = widget.find('.collection_content');
    var is_visible = $(content).is(':visible');
    
    var collapsableWidget = widget.find('.collection_content');
    
    // This class is to mark the collection as activated
    // It is removed at the end of this function
    widget.addClass('activated');

    if (is_visible) {
      // $(content).hide();
      content.collapse('hide');
      widget.addClass('collapsed');

      // NOTE: possibly usefull if we moved the entry count to the collection_content, to make dynamic
      // Remove header entry count to be replaced by the content_data's dynamic content
      // if (widget.find('.header_entry_count').length > 0) {
      // header.find('.header_entry_count').css('visibility', 'd-none');
      // }

    } else {
      // $(content).show();

      var is_populated = $(content).hasClass('populated');
      if (!is_populated) {
        // Adds 'populated' to the content div's classes on success
        var ajaxRequest = populate_collection($(content));
        
        ajaxRequest.done(function(){
          content.collapse('show');
        });
        
      } else {
        content.collapse('show');
      }
      // The following code was intented to detect page index or page size differences.
      // If someone changes the page size or index, collapsed and then expanded the page, it would refresh the content
      // else {
      //   var is_same_page = $(content).find('input[name="loaded_page_number"]').val() == get_page_number($(content));
      //   var is_same_page_size = $(content).find('input[name="loaded_page_size"]').val() == get_page_size($(content));
      //   if (!is_same_page || !is_same_page_size) {
      //     populate_collection($(content));
      //   }
      // }
      widget.removeClass('collapsed');
    }

    // this will hide all other collection widgets (except those marked as expanded -- they are independent of accordion)
    if (!widget.hasClass('expanded')) {
      var all_collections = $('.collection').not('.activated').not('.expanded');
      all_collections.each(function(index, collection) {
        // $(collection).find('.view_content').hide();
        $(collection).find('.view_content').collapse('hide');
        $(collection).addClass('collapsed');
      });
    };
    widget.removeClass('activated');
  });
}


function setup_checkbox_inputs(container) {
  container.find("td.checkbox input").click(function() {
    // if this is a radio button, then remove the selected class for all radio
    // buttons in the table
    if ($(this).is(':radio')) {
      $(this).parent().parent().parent().find('.selected').removeClass('selected');
    }

    // we will now focus on the row of the current radio button
    var row = $(this).parent().parent();
    if ($(this).attr('checked')) {
      row.addClass('selected');
    } else {
      row.removeClass('selected');
    }
  });
}

function hideWidgetChangesMessage(widget) {
  if (!widget.hasClass('root') && widget.hasClass('unsaved_changes')) {
    widget.removeClass('unsaved_changes');
    widget.find('.unsaved_changes_message').css('visibility','hidden').addClass('d-none');
  }
}

function removeUnsavedPositioningFlag(widget) {
  widget.find('.unsaved_changes_message').removeClass('has_unsaved_positioning');
}
function addUnsavedPositioningFlag(widget) {
  widget.find('.unsaved_changes_message').addClass('has_unsaved_positioning');
}

function removeDocumentUnsavedPositioningFlag(widget) {
  widget.find('.document_unsaved_changes_message').removeClass('document_has_unsaved_positioning');
}
function addDocumentUnsavedPositioningFlag(widget) {
  widget.find('.document_unsaved_changes_message').addClass('document_has_unsaved_positioning');
}



// Kludge method here. Will go away once gui_director vertexes and SSA can handle pending positions
// function warn_about_positional_data_loss(widget, content_div, focus_selector) {
//   $('body').append(
//     '<div title="Unsaved Changes" id="unsaved_changes_popup" class="dialog unsaved_changes_warning">' +
//       '<div class="dialog_message" readonly="readonly" id="popupMessage"></div>' +
//     '</div>');
//   var popup = $('#unsaved_changes_popup');
//   var popup_message = popup.find('#popupMessage');
//   popup_message.html("Warning: This association has unsaved positional changes. Please click 'Discard' to discard and continue, or click cancel.");
//   var buttons = {};
//   buttons['Cancel'] = function() {
//     popup.remove();
//   };
//
//   buttons['Discard'] = function() {
//     popup.remove();
//     hideWidgetChangesMessage(widget);
//     removeUnsavedPositioningFlag(widget);
//     populate_collection(content_div, focus_selector, {'confirm_data_loss': true})
//   };
//
//   popup.dialog({
//     modal: true,
//     resizable: false,
//     width: 500,
//     buttons: buttons,
//     open: function() {
//       $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove();
//     }
//   });
//   return false;
// }

function warn_about_positional_data_loss(widget, content_div, focus_selector){
  
  //Build the popup div
  //This is disgusting and could be done much better with ES6 template strings but that causes issues with testing unless it is transpiled to ES5 -TB
  var modalID = 'unsavedChangesModal';
  var htmlArray = [];
  var message = 'Warning: This association has unsaved positional changes. Please click "Discard" to discard and continue, or click cancel.';
  htmlArray.push( '<div class = "modal warning" tabindex="-1" role="dialog" aria-labelledby="unsavedPositioningModalLabel" id="' + modalID + '">');
  htmlArray.push( '<div class = "modal-dialog" role="document">');
  htmlArray.push( '<div class = "modal-content">');
  htmlArray.push( '<div class="modal-header">');
  htmlArray.push( '<h5 class= "modal-title" id="unsavedPositioningModalLabel">Unsaved Changes</h5>');
  htmlArray.push( '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
  htmlArray.push( '</div>');
  htmlArray.push( '<div class="modal-body">' + message + '</div>');
  htmlArray.push( '<div class="modal-footer"><button id="unsavedPositioningCancelButton" class="btn btn-secondary">Cancel</button><button id="unsavedPositioningDiscardButton" class="btn btn-primary">Discard Changes</button></div>');
  htmlArray.push( '</div>');
  htmlArray.push( '</div>');
  htmlArray.push( '</div>');
  
  $('body').append(htmlArray.join(''));
  
  var modal = $('#'+modalID);
  
  //Setup close and cancel button click handlers
  modal.find('.close, #unsavedPositioningCancelButton').click(function(){
    modal.modal('hide');
    // modal.modal('dispose');
    return false;
  });
  
  //Setup confirm click handler
  modal.find("#unsavedPositioningDiscardButton").click(function(){    
    // $.ajax({
    //   type: 'POST',
    //   url: action_url,
    //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    //   success: success(link)
    // });
    
    // popup.remove();
    modal.modal('hide');
    // modal.modal('discard');
    hideWidgetChangesMessage(widget);
    removeUnsavedPositioningFlag(widget);
    populate_collection(content_div, focus_selector, {'confirm_data_loss': true})
    
    // modal.modal('hide');
    // modal.modal('dispose');
    return false;
  });
  
  modal.modal('show');
  
  modal.on('hidden.bs.modal', function(e){
    $(this).modal('dispose');
    $(this).remove();
    // modal.modal('dispose');
    // modal.remove();
  });
  
}
// Catches out of bounds page index. Currently being handled on the server side.
// function collection_pagination_safety_check(page_number, max_page_number) {
//   new_page_number = page_number;
//   if (page_number > max_page_number) {
//     new_page_number = max_page_number;
//   } else if (page_number < 1) {
//     new_page_number = 0
//   }
//   return new_page_number
// }
