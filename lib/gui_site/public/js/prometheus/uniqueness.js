//This function creates a uniqueness warning dialog if it is needed.
function display_uniqueness_dialog_old() {
  var popup = $('#uniqueness_popup');
  if (popup.length > 0) {
    var is_warning = popup.hasClass('uniqueness_warning');
    var is_creation = (popup.find('input#is_creation').val() == 'true');
    var has_assoc_obj = (popup.find('input#has_assoc_obj').val() == 'true');
    
    // Set up radio buttons to enable 'Select Existing' button
    $('input.selection_obj').click(function() {
      $(".ui-dialog-buttonpane button:contains(' Existing')").button("enable");
    });
    
    var buttons = {};
    buttons['Cancel'] = function() {
      $(this).remove();
    };
    // Only show 'Continue Anyway' option for warnings
    if (is_warning) {
      buttons['Continue Anyway'] = function() {
        form = $('form.organizer_form');
        form.append("<input type='hidden' value='true' name='continue_anyway' id='continue_anyway' />");
        form.append("<input type='hidden' name='save_and_stay' />");
        $(this).remove();
        form.submit();
      };
    }
    // Decide which '... Existing' button to add based on provided context
    if (is_creation) {
      if (has_assoc_obj) {
        buttons['Abandon Creation & Use Existing Item'] = function() {
          form = $('form.organizer_form');
          form.append("<input type='hidden' name='select_existing' value='discard_and_select' />");
          var selected_obj_string = $("input.selection_obj[type='radio']:checked").val();
          form.append("<input type='hidden' name='selected_obj' value='" + selected_obj_string + "' />");
          $(this).remove();
          form.submit(); }
      } else {
        buttons['Abandon Creation & View Existing Item'] = function() {
          form = $('form.organizer_form');
          form.append("<input type='hidden' name='select_existing' value='discard_and_view' />");
          var selected_obj_string = $("input.selection_obj[type='radio']:checked").val();
          form.append("<input type='hidden' name='selected_obj' value='" + selected_obj_string + "' />");
          $(this).remove();
          form.submit(); }
      }
    } // TODO: Allow for Merge with existing button. Needs merge capability in gui builder 
    //         since the user will have to choose what gets placed into the existing object from the duplicate object
    //  else {
    //   buttons['Merge with Existing'] = function() {
    //     form = $('form.organizer_form');
    //     form.append("<input type='hidden' name='select_existing' value='merge' />");
    //     var selected_obj_string = $("input.selection_obj[type='radio']:checked").val();
    //     form.append("<input type='hidden' name='selected_obj' value='" + selected_obj_string + "' />");
    //     $(this).remove();
    //     form.submit(); }
    // }

    $('#uniqueness_popup').dialog({
      modal: true,
      resizable: true,
      width: 700,
      position: ['middle', 20],
      buttons: buttons,
      open: function() {
        $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove(); 
      },
      close: function() {
        $(this).parents(".ui-dialog:first").remove();
      }
    });
    // Center the buttons -- TODO: figure out why CSS isn't working
    $(".ui-dialog-buttonpane").css('text-align', 'center');
    $(".ui-dialog-buttonpane button").css('float', 'none');
    
    // Initially disable the 'Select Existing' button (until a row is marked)
    $(".ui-dialog-buttonpane button:contains(' Existing')").button("disable");
  }
}

//This function creates a uniqueness warning dialog if it is needed.
function display_uniqueness_dialog() {
  var modal = $('#uniquenessModal');
  if (modal.length > 0) {
    var is_warning = modal.hasClass('uniqueness_warning');
    var is_creation = (modal.find('input#is_creation').val() == 'true');
    var has_assoc_obj = (modal.find('input#has_assoc_obj').val() == 'true');
    
    // Set up radio buttons to enable 'Select Existing' button
    //TODO make this work with the new modal
    
    modal.find('input.selection_obj').click(function() {
      modal.find("button:contains(' Existing')").prop("disabled", false);
    });
    
    modal.find('#closeUniquenessModal, .close').click(function() {
      // modal.modal('hide');
      modal.modal('hide');
    });
    
    modal.find('#uniquenessModalContinueAnyway').click(function() {
      form = $('form.organizer_form');
      form.append("<input type='hidden' value='true' name='continue_anyway' id='continue_anyway' />");
      form.append("<input type='hidden' name='save_and_stay' />");
      modal.modal('hide');
      form.submit();
    });
    
    modal.find('#uniquenessModalAbandonAndUse').click(function() {
      form = $('form.organizer_form');
      form.append("<input type='hidden' name='select_existing' value='discard_and_select' />");
      var selected_obj_string = $("input.selection_obj[type='radio']:checked").val();
      form.append("<input type='hidden' name='selected_obj' value='" + selected_obj_string + "' />");
      modal.modal('hide');
      form.submit();
    });
    
    modal.find('#uniquenessModalAbandonAndView').click(function() {
      form = $('form.organizer_form');
      form.append("<input type='hidden' name='select_existing' value='discard_and_view' />");
      var selected_obj_string = $("input.selection_obj[type='radio']:checked").val();
      form.append("<input type='hidden' name='selected_obj' value='" + selected_obj_string + "' />");
      modal.modal('hide');
      form.submit();
    });

    modal.on('hidden.bs.modal', function(e){
      $(this).modal('dispose');
      $(this).remove();
    });

    //TODO Put in the select existing disable
    // Initially disable the 'Select Existing' button (until a row is marked)
    modal.find("button:contains(' Existing')").prop("disabled", true);
    modal.modal('show');
  }
}