
function setup_table_header_click() {
  $('.table_widget').on('click', '.table_header_row .is_sortable', function() {
    var header = $(this);
    var header_position = header.index();
    if (header_position >= 2) {
      sort_by_column_header(header);
      // populate_collection(header.parents('.collection_content').first());
    }
    
  }); 
}

function sort_by_column_header (table_header) {
  var column_name = get_real_table_column_name(table_header);
  if ( column_name != "derived_column_value") {
    check_for_hidden_sort_inputs(table_header);
    populate_collection(table_header.parents('.collection_content').first());
  }
  return 0;
}

function check_for_hidden_sort_inputs(table_header) {
  // var header_text = table_header.text().replace(/ /g,"_").toLowerCase().trim();
  var fake_header_text = table_header.text().trim();
  var header_text = get_real_table_column_name(table_header);
  var sorted_html = "<input class='table_sort_direction' type='hidden' value=''>";
  var sorted_by_column = "<input class='table_sort_by_column' name='" + fake_header_text + "' type='hidden' value='" + header_text + "'>";
  var parent_container = table_header.parents('.table_widget').first();
  var table_sort_direction = parent_container.children('.table_sort_direction');
  
  var table_sort_by_column = parent_container.children('.table_sort_by_column');
  if( table_sort_by_column.length > 0) {
    //Update the table_sort_column to be the new column
    if(table_sort_by_column.val() === header_text) {
      toggle_sort_column_direction(table_sort_direction);
    }
    else {
      table_sort_by_column.val(header_text);
      table_sort_by_column.prop("name", fake_header_text);
      table_sort_direction.val('');
    }
  }
  else {
    parent_container.append(sorted_html);
    parent_container.append(sorted_by_column);
  }
}

function toggle_sort_column_direction(sort_direction_input) {
  if( sort_direction_input.val() === '') {
    sort_direction_input.val('descending');
  }
  else {
    sort_direction_input.val('');
  }
}

function get_real_table_column_name(header) {
  var column_position = header.index();
  var table_filter_cells = header.parents('.table_widget').first().find('.collection_content .table_filter_row .filter_cell');
  column_position = column_position - table_filter_cells.index();
  var filter_cell = table_filter_cells.find('.filter').eq(column_position); 
  var filter_type = filter_cell.parent().find('.filter_type');
  var column_name = filter_cell.prop("name");
  if ( filter_type.length <= 0) {
    column_name = "derived_column_value";
  }
  return column_name;
}


function get_table_sort_direction(widget) {
  return widget.children('.table_sort_direction').val();
}

function get_table_sort_column(widget) {
  return widget.children('.table_sort_by_column').val();
}

function setup_column_sorting() {
  setup_table_header_click();
}

function setup_sort_arrow_direction(widget) {
  var container = widget.parent();
  var sort_direction = container.find('.table_sort_direction').val();
  var header_name = container.find('.table_sort_by_column').prop("name");
  if ( widget.parent().find('.table_sort_by_column').length > 0 ) {
    
    var table_header = container.find("th:contains('" + header_name + "')");
    
    //Place the arrow on the floating header if infinite scrolling div is present
    var header_scrolling_div = table_header.find('.scrolling_header_cell');
    
    if (sort_direction === "descending") {
      if (header_scrolling_div.length > 0) {
        header_scrolling_div.addClass('scrolling_header_sorting_arrow_down');
      }
      else {
        table_header.addClass('sorting_arrow_down');
      }
      
    }
    else {
      if (header_scrolling_div.length > 0) {
        header_scrolling_div.addClass('scrolling_header_sorting_arrow_up');
      }
      else {
       table_header.addClass('sorting_arrow_up'); 
      }
    }

  }
  
}