function doCollectionButton(btn) {
  jq_btn         = $(btn)
  var btn_widget = jq_btn.parent();
  var btn_getter = btn_widget.find('.widget_getter').val();
  var collection = btn_widget.closest('.collection');
  var content    = collection.find('.collection_content');

  var toggle_view_all_listings = content.find('.toggle_view_all_listings');
  var show_possible_assoc = (toggle_view_all_listings.length > 0 && toggle_view_all_listings.is(':checked')) ? true : false;

  var filter_data       = get_filter_data(content, !show_possible_assoc);
  var search_all_filter = get_search_all_filter_data(content, !show_possible_assoc);

  // Display loading image
  content.find('.content_loading_image').html('<center id="content_loading_image"><img src="/images/ajax-loader.gif" /></center>');
  var collection_data = {
    'pretty_id': get_pretty_id(collection),
    // NOTE: data_classifier and data_id should use _parent_ methods, not root.
    //       Currently, it would break selection collections to alter this.
    'data_classifier':      get_parent_data_classifier(collection),
    'data_id':              get_parent_data_id(collection),
    'collection_getter':    get_data_getter(collection),
    'button_action':        btn_getter,
    'order_by_column':      get_table_sort_column(collection),
    'table_sort_direction': get_table_sort_direction(collection),
    'search_all_filter':    search_all_filter,
    'filter':               filter_data
  };
  var request = $.post('/collection_button/', collection_data)
                .done(function(response_string) {
                  response = JSON.parse(response_string);
                  // console.log(Object.getOwnPropertyNames(response));
                  content.find('.content_loading_image').empty(); // Remove loading image
                  if (response.type == 'download') {
                    // console.log(response);
                    
                    file_string = response.data;
                    // console.log(Object.getOwnPropertyNames(response));
                    // console.log('String: ' + string)
                    //Convert the Byte Data to BLOB object.
                    var blob = new Blob([file_string], { type: "application/octetstream" });
                    var fileName = response.filename || (collection_data.collection_getter + '_' + collection_data.button_action);
 
                    //Check the Browser type and download the File.
                    var isIE = false || !!document.documentMode;
                    if (isIE) {
                        window.navigator.msSaveBlob(blob, fileName);
                    } else {
                        var url = window.URL || window.webkitURL;
                        link = url.createObjectURL(blob);
                        var a = $("<a />");
                        a.attr("download", fileName);
                        a.attr("href", link);
                        $("body").append(a);
                        a[0].click();
                        $("body").remove(a);
                    }
                  }
                })
                .fail(function(data, responseText) {
                  content.find('.content_loading_image').empty(); // Remove loading image
                  btn_widget.append(data.responseText);
                });

  return request;
}
