function setup_clone_link() {
  // $('#clone_link').click(function() {
  //   make_clone_popup();
  //   // Cancel normal href navigation
  //   return false;
  // });
  
  $('#cloneDomainObjModal').on('show.bs.modal', function(event) {
    var root_data_classifier = get_root_data_classifier();
    var root_data_id = get_root_data_id();
    var root_data_getter = get_root_data_getter();
    var modal = $(this);
    var data = {
      data_classifier: root_data_classifier,
      data_id: root_data_id,
      data_getter: root_data_getter
    };
    
    var jqxhr = $.post('/clone_popup', data)
    .done(function(responseText) {
      modal.find('.cloning-information').html(responseText);
      //Handle any setup code for collections/organizers
      setup_collection_headers(modal);
      setup_organizer_headers(modal);
      populate_expanded_content(modal);
      setup_composer_getter_selector();
      setup_composer_getter();
    });
    
  });
  
  //Removes the information that was retrieved Via ajax so that the test selectors don't fail. -Gross
  $('#cloneDomainObjModal').on('hidden.bs.modal', function(event) {
    var modal = $(this);
    modal.find('.cloning-div').remove();
  });
  
  
  $('#confirmCloneDomainObj').click(function() {
    // Disable buttons right away
    $(this).prop('disabled', true);
    // No way to cancel in-progress clone currently
    $(this).parent().find("button:contains('Cancel')").prop('disabled', true);
    
    
    // Show loading message
    $(this).parent().append('<center id="content_loading_image">Cloning, please wait. &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/ajax-loader.gif" /></center>');
    
    var root_data_classifier = get_root_data_classifier();
    var root_data_id = get_root_data_id();
    var root_data_getter = get_root_data_getter();
    var composer_option_input = $('input[name=composer_option]:checked');
    var composer_getter_input = $('select#composer_getter');
    var composer_association_div = $('#composer_association_' + composer_getter_input.val());
    var new_composer_type_input = composer_association_div.find('select#new_composer_type');
    // Get the main organizer form
    form = $('form.organizer_form');
    // Make a hidden clone_domain_object field
    form.append("<input type='hidden' name='clone_domain_object' value='true' />");
    // Append necessary data to form
    if (composer_getter_input.length > 0) {
      form.append("<input type='hidden' name='composer_getter' value='"+composer_getter_input.val()+"' />");
      form.append("<input type='hidden' name='composer_option' value='"+composer_option_input.val()+"' />");
      form.append("<input type='hidden' name='new_composer_type' value='"+new_composer_type_input.val()+"' />");
    }
    form.submit();
  });
  
}

// function make_clone_popup() {
//   var root_data_classifier = get_root_data_classifier();
//   var root_data_id = get_root_data_id();
//   var root_data_getter = get_root_data_getter();
//
//   $.ajax({
//     type: 'POST',
//     url: '/clone_popup',
//     data: {
//       data_classifier: root_data_classifier,
//       data_id: root_data_id,
//       data_getter: root_data_getter
//     },
//     success: function(responseText) {
//       display_clone_popup_from_response(responseText);
//       // Make the composer_getter selection re-select an option if changed
//       setup_composer_getter();
//     }
//   });
// }

function setup_composer_getter() {
  $('#composer_getter').change(function() {
    var getter = $(this).val();
    var composer_association_div = $('#composer_association_' + getter);
    var same_composer_radio = composer_association_div.find('#same_composer_option')
    if (same_composer_radio.length > 0) {
      same_composer_radio.prop('checked',true);
    } else {
      composer_association_div.find('#new_composer_option').prop('checked',true);
    }
  });
  // NOTE: This code may not be fully checked.
  $('.composer_association:not(.hidden) .new_composer_type').change(function() {
    $('.composer_association:not(.hidden) .new_composer_option').prop('checked',true);
  });
}

// // Take the AJAX response and display as a popup
// function display_clone_popup_from_response(response) {
//   // Attach response to body
//   $('body').append(response);
//
//   // Create buttons
//   var buttons = {};
//   buttons['Cancel'] = function() {
//     $('#clone_popup').remove();
//   };
//   buttons['Clone'] = function() {
//     // Disable buttons right away
//     $(".ui-dialog-buttonpane button:contains('Clone')").button("disable");
//     // No way to cancel in-progress clone currently
//     $(".ui-dialog-buttonpane button:contains('Cancel')").button("disable");
//
//     // Show loading message
//     $('.ui-dialog-buttonpane').append('<center id="content_loading_image">Cloning, please wait. &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/ajax-loader.gif" /></center>');
//
//     var root_data_classifier = get_root_data_classifier();
//     var root_data_id = get_root_data_id();
//     var root_data_getter = get_root_data_getter();
//     var composer_option_input = $('input[name=composer_option]:checked');
//     var composer_getter_input = $('select#composer_getter');
//     var composer_association_div = $('#composer_association_' + composer_getter_input.val());
//     var new_composer_type_input = composer_association_div.find('select#new_composer_type');
//     // Get the main organizer form
//     form = $('form.organizer_form');
//     // Make a hidden clone_domain_object field
//     form.append("<input type='hidden' name='clone_domain_object' value='true' />");
//     // Append necessary data to form
//     if (composer_getter_input.length > 0) {
//       form.append("<input type='hidden' name='composer_getter' value='"+composer_getter_input.val()+"' />");
//       form.append("<input type='hidden' name='composer_option' value='"+composer_option_input.val()+"' />");
//       form.append("<input type='hidden' name='new_composer_type' value='"+new_composer_type_input.val()+"' />");
//     }
//     form.submit();
//   };
//   var clone_popup = $('#clone_popup')
//   clone_popup.dialog({
//     modal: true,
//     resizable: true,
//     width: 1000,
//     buttons: buttons,
//     position: ['middle', 20],
//     open: function() {
//       //$(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove();
//     },
//     close: function() {
//       // I think this occurs when 'esc' is pressed instead of pressing 'Cancel', but I'm not sure -SD
//       $('#clone_popup').remove();
//     }
//   });
//
//   //Handle any setup code for collections/organizers
//
//   setup_collection_headers(clone_popup);
//
//   setup_organizer_headers(clone_popup);
//
//   populate_expanded_content(clone_popup);
//   //setup_save_and_go_links($(this));
//   //setup_discard_and_go_links($(this));
//   //setup_delete_confirmation_prompt_links($(this));
//
//   setup_composer_getter_selector();
// }

function setup_composer_getter_selector() {
  $('select#composer_getter').change(function() {
    var new_getter = $(this).val();
    // Set all composer associations to hidden
    $('.composer_association').hide();
    $('.composer_association').addClass('hidden');
    // Unhide the selected composer_association
    $('#composer_association_' + new_getter).show();
    $('#composer_association_' + new_getter).removeClass('hidden');
  });
}

