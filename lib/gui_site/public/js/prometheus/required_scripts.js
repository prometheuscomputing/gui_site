// EXAMPLE:
// require(["helper/util"], function(util) {
//     //This function is called when scripts/helper/util.js is loaded.
//     //If util.js calls define(), then this function is not fired until
//     //util's dependencies have loaded, and the util argument will hold
//     //the module value for "helper/util".
// });
// JQuery and JQueryUI
require(['/js/jq/jquery-1.10.2.min.js'], function(util) { });
require(['/js/jq/jquery-1.10.2.min.js'], function(util) { });
require(['/js/jq/jquery-migrate-1.2.1.min.js'], function(util) { });
require(['/js/jq/jquery-ui-1.10.3.custom.min.js'], function(util) { });
// Plugins
require(['/js/jq/jquery.layout.all-1.2.0/jquery.layout.min.js'], function(util) { });
require(['/js/jq/jquery.layout.all-1.2.0/jquery.ui.all.js'], function(util) { });
require(['/js/jq/dist/jstree.min.js'], function(util) { });
require(['/js/jq/jquery-ui-timepicker-addon.js'], function(util) { });
require(['/js/jq/jquery.corner-2.13.js'], function(util) { });
require(['/js/jq/jquery.mousewheel.js'], function(util) { });
require(['/js/js_utils.js'], function(util) { });
require(['/js/common.js'], function(util) { });
require(['/js/change_listeners.js'], function(util) { });
require(['/js/clone.js'], function(util) { });
require(['/js/document_association.js'], function(util) { });
require(['/js/load_functions.js'], function(util) { });
require(['/js/syntax_highlighting.js'], function(util) { });
require(['/js/timestamp_widget.js'], function(util) { });
require(['/js/multi_simple_widget.js'], function(util) { });
require(['/js/imgpreview.jquery.js'], function(util) { });
require(['/js/collection_position.js'], function(util) { });
require(['/js/collection.js'], function(util) { });
require(['/js/organizer.js'], function(util) { });
require(['/js/richtext.js'], function(util) { });
//
require(['/js/main.js'], function(util) { });
