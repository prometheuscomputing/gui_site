// function display_concurrency_dialog() {
//   // Will click the corresponding radio button when the text is clicked
//   $('#concurrencyWarningModal th.select_all').click(function() {
//     $('#concurrencyWarningModal input.select_all[for="' + $(this).attr('for') + '"]').click();
//   });
//   // Will select all the corresponding column radio buttons on click
//   $('#concurrencyWarningModal input.select_all').click(function() {
//     var whose_change = $(this).attr('for');
//     $('#concurrencyWarningModal input.concurrency_selector').each(function(index, input) {
//       $(input).removeAttr('checked');
//     });
//     $('#concurrencyWarningModal td[for="' + whose_change + '"] input.concurrency_selector').each(function(index, input) {
//       $(input).attr('checked', 'checked');
//     });
//   });
//   // Will clear out the select-all, top-level radio buttons on the attribute radio button clicks
//   $('#concurrencyWarningModal input.concurrency_selector').click(function() {
//     $('#concurrencyWarningModal input.select_all').each(function(index, input) {
//       $(input).removeAttr('checked');
//     });
//   });
//
//   if ($('#concurrencyWarningModal').length > 0) {
//     var buttons = {};
//     // Essentially reverts to the state before the submit / page save.
//     buttons['Cancel'] = function() {
//       $(this).remove();
//     };
//     // Reloads the page with the current values from the database.  All changes, even those that were non-conflicting, will be lost/abandoned.
//     buttons['Abandon Your Changes'] = function() {
//       window.location = window.location.pathname;
//     };
//     buttons['Merge'] = function() {
//       console.log('merge')
//       var form = $('form#root_view_form');
//       // TODO: Move merge logic here to helper function
//       $('#concurrencyWarningModal tr.content_row').each(function(index, row) {
//         var popup_widget = $(row);
//         console.log('Processing ' + popup_widget.attr('data_pretty_name'));
//         var timestamp    = popup_widget.find('.last_update').val();
//         var form_widget  = form.find('input.widget_name[value="' + popup_widget.attr('data_pretty_name') + '"]').parents('.widget').first();
//         // if the concurrency widget has a value for last_update then put that value in the object page widget for the same attribute
//         if (timestamp !== undefined && form_widget.find('.last_update').length) {// If the element has a last_update input
//           form_widget.find('.last_update').val(timestamp);
//         }
//         // The difference between info and data is that info gets it's own key into the widgets' structure. Is not stored under the data hash.
//         popup_widget.find('.widget_data, .widget_info').each(function(index, input) {
//           var input = $(input); // input may be a bit misleading, can be select or textarea
//           // Convoluted way of choosing the input that was selected/checked from among the choices given for that attribute
//           if (input.parents('tr').find('td[for="' + input.parents('td').attr('for') + '"]').find('.concurrency_selector').is(':checked')) {
//             console.log('Selected popup widget ' + input.attr('name'));
//             // find all of the corresponding inputs on the object page
//             form_widget.find('*[name="' + input.attr('name') + '"]').each(function(index, form_input) {
//               var form_input = $(form_input);
//               console.log('Found form widget ' + input.attr('name'));
//               // Handle checkboxes on the form page with values from the popup
//               if (form_input.is(':checkbox')) {
//                 if (input.attr('value') === 'true') {
//                   form_input.attr('checked', 'checked');
//                 } else {
//                   form_input.removeAttr('checked');
//                 }
//               } else { // Use 'value' for everything else, works with most input types (text, dropdowns)
//                 form_input.val(input.val());
//                 // If the input has an initial value, then override it with the most recently discovered change
//                 most_recent_change_value = popup_widget.find('td.changes_found').find('*[name="' + input.attr('name') + '"]').val();
//                 if (form_input.is("[initial_value]")) {
//                   form_input.attr('initial_value', most_recent_change_value);
//                 }
//                 // (ONLY IF) If widget has a seperate input for the initial field value, then replace it with the most recently discovered change - the 'changes_found'
//                 var form_initial_input = form_widget.find('*[name="' + input.attr('name') + '_initial"]');
//                 if (form_initial_input.length > 0) {
//                   form_initial_input.val(most_recent_change_value);
//                 }
//               }
//             });
//           }
//         });
//       });
//       $(this).remove();
//       form.append("<input type='hidden' name='save_and_stay' />");
//       form.submit();
//     };
//
//     $('#concurrencyWarningModal').dialog({
//       modal: true,
//       resizable: true,
//       width: 1050,
//       position: ['middle', 20],
//       buttons: buttons,
//       open: function() {
//         $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove();
//       },
//       close: function() {
//         $(this).parents(".ui-dialog:first").remove();
//       }
//     });
//     // Center the buttons -- TODO: figure out why CSS isn't working
//     $(".ui-dialog-buttonpane").css('text-align', 'center');
//     $(".ui-dialog-buttonpane button").css('float', 'none');
//
//     // Initially disable the 'Select Existing' button (until a row is marked)
//     $(".ui-dialog-buttonpane button:contains(' Existing')").button("disable");
//   }
// }

function display_concurrency_dialog() {
  var modal = $('#concurrencyWarningModal');
  
  if (modal.length > 0) {  
    // Will click the corresponding radio button when the text is clicked
    // modal.find('th.select_all').click(function() {
    //   modal.find('input.select_all[for="' + $(this).attr('for') + '"]').click();
    // });
    // Will select all the corresponding column radio buttons on click
    modal.find('input.select_all').click(function() {
      var whose_change = $(this).attr('for');
      modal.find('input.concurrency_selector').each(function(index, input) {
        // $(input).removeAttr('checked');
        $(input).prop('checked', false)
      });
      modal.find('td[for="' + whose_change + '"] input.concurrency_selector').each(function(index, input) {
        $(input).prop('checked', 'checked');
      });
    });
    // Will clear out the select-all, top-level radio buttons on the attribute radio button clicks
    modal.find('input.concurrency_selector').click(function() {
      modal.find('input.select_all').each(function(index, input) {
        // $(input).removeAttr('checked');
        $(input).prop('checked', false);
      });
    });
    
    //Cancel Button
    modal.find('#concurrencyWarningCancel').click(function() {
      modal.modal('dispose');
    });
    
    //Abandon Changes Button
    modal.find('#concurrencyWarningAbandonChanges').click(function() {
      window.location = window.location.pathname;
    });
    
    //Merge Button
    modal.find('#concurrencyWarningMergeChanges').click(function() {
      // console.log('merge')
      var form = $('form#root_view_form');
      // TODO: Move merge logic here to helper function
      modal.find('tr.content_row').each(function(index, row) {
        var popup_widget = $(row);
        console.log('Processing ' + popup_widget.attr('data_pretty_name'));
        var timestamp    = popup_widget.find('.last_update').val();
        var form_widget  = form.find('input.widget_name[value="' + popup_widget.attr('data_pretty_name') + '"]').parents('.widget').first();
        // if the concurrency widget has a value for last_update then put that value in the object page widget for the same attribute
        if (timestamp !== undefined && form_widget.find('.last_update').length) {// If the element has a last_update input
          form_widget.find('.last_update').val(timestamp);
        }
        // The difference between info and data is that info gets it's own key into the widgets' structure. Is not stored under the data hash.
        popup_widget.find('.widget_data, .widget_info').each(function(index, input) {
          var input = $(input); // input may be a bit misleading, can be select or textarea
          // Convoluted way of choosing the input that was selected/checked from among the choices given for that attribute
          if (input.parents('tr').find('td[for="' + input.parents('td').attr('for') + '"]').find('.concurrency_selector').is(':checked')) {
            console.log('Selected popup widget ' + input.attr('name'));
            // find all of the corresponding inputs on the object page
            form_widget.find('*[name="' + input.attr('name') + '"]').each(function(index, form_input) {
              var form_input = $(form_input);
              console.log('Found form widget ' + input.attr('name'));
              // Handle checkboxes on the form page with values from the popup
              if (form_input.is(':checkbox')) {
                if (input.attr('value') === 'true') {
                  form_input.attr('checked', 'checked');
                } else {
                  form_input.removeAttr('checked');
                }
              } else { // Use 'value' for everything else, works with most input types (text, dropdowns)
                form_input.val(input.val());
                // If the input has an initial value, then override it with the most recently discovered change
                most_recent_change_value = popup_widget.find('td.changes_found').find('*[name="' + input.attr('name') + '"]').val();
                if (form_input.is("[initial_value]")) {
                  form_input.attr('initial_value', most_recent_change_value);
                }
                // (ONLY IF) If widget has a seperate input for the initial field value, then replace it with the most recently discovered change - the 'changes_found'
                var form_initial_input = form_widget.find('*[name="' + input.attr('name') + '_initial"]');
                if (form_initial_input.length > 0) {
                  form_initial_input.val(most_recent_change_value);
                }
              }
            });
          }
        });
      });
      modal.modal('dispose');
      form.append("<input type='hidden' name='save_and_stay' />");
      form.submit();
    });
    modal.modal('show');
  }
  
}