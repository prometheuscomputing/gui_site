//This file will contain misc functions for setting up widgets that don't have to exist in their own js files
function setup_miscellaneous_widgets(container) {
  setup_numeric_slider_widgets(container);
}

function setup_numeric_slider_widgets(container) {
  // $( ".number_slider" ).slider();
  
  container.find(".number_slider").each(function() {
    $(this).slider({
      min: parseInt($(this).data("min")) || 0,
      max: parseInt($(this).data("max")) || 100,
      step: parseInt($(this).data("step")) || 1,
      value: parseInt($(this).data("initial-value")) || parseInt($(this).data("min")) || 0,
      create: function() {
        $(this).find('.number_slider_handle').text($(this).slider("value"));
      },
      slide: function(event, ui) {
        $(this).find('.number_slider_handle').text(ui.value);
        $(this).parent().prev('.widget_data').val(ui.value);
      },
      change: function(event, ui){
        $(this).find('.number_slider_handle').text(ui.value);
        $(this).parent().prev('.widget_data').val(ui.value);
        var number_slider_widget = $(this);
        var widget_data = number_slider_widget.parent().prev('.widget_data');
        var new_value = widget_data.val();
        // var new_value = number_slider_widget.slider('value');
        // widget_data.val(new_value);

        // Update all widgets with this widget_data with the new value
        $('.widget_data[name="' + widget_data.attr('name') + '"]').each(function(i, input_element) {
          // Update all other inputs representing the same datum
          if (!widget_data.is(input_element)) {
            $(input_element).val(new_value);
          }
        });

        // Update all pretty views
        $('.pretty_view[name="' + widget_data.attr('name') + '"]').each(function(i, widget_pretty_view) {
          $(widget_pretty_view).html(new_value);
        });
      }
    });
  });


}
