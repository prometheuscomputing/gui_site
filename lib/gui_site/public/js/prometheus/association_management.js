function enable_association_entry_count(widget) {
  assoc_entry_count = widget.find('.entry_count').first();
  unassoc_entry_count = widget.find('.unassociated_entry_count').first();
  unassoc_entry_count.css('display', 'none')
  assoc_entry_count.removeAttr('display')
}
function enable_unassociation_entry_count(widget) {
  assoc_entry_count = widget.find('.entry_count').first();
  unassoc_entry_count = widget.find('.unassociated_entry_count').first();
  assoc_entry_count.css('display', 'none')
  unassoc_entry_count.removeAttr('display')
}