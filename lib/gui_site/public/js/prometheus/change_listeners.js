function setup_change_listeners() {
  $('#confirmDiscardChanges').click(function(){
    var lastBreadcrumb = $('#breadcrumbs a').last();
    if ( lastBreadcrumb.length > 0 ) {
      //Click the last breadcrumb to preserve breadcrumb history
      $('#breadcrumbs a').last().click();
    } else {
      //If there is only 1 item in the breadcrumb list reload the current page.
      //TODO Check to see if reloading from cache is sufficient.  If it is not put true in this method instead of false
      location.reload(false);
    }
  });

  // NOTE: 'form .view_widget' will not trigger the current domain object's fields.
  // Those inputs are not nested under a 'form .view_widget', but rather a '.view_widget form'
  // * Even were that not the case, would still not cause a problem.

  // NOTE: These listeners listen for most inputs. Any inputs that need exceptions need to be given a class and excluded.

  // Listens for undeleting associations
  $('form .view_widget:not(.unsaved_changes)').on('click', 'button.undelete_selected', function() {
    var widget = $(this).parents('.view_widget').first();
    if (widget.find('input.widget_type').first().attr('value') === 'collection' ) {
      return collection_undelete_associations(widget);
    } else if (widget.find('input.widget_type').first().attr('value') === 'organizer' ) {
      return organizer_undelete_associations(widget);
    }
  });

  // Listens for adding associations
  $('form .view_widget:not(.unsaved_changes)').on('click', 'button.assoc_submit', function () {
    var widget = $(this).parents('.view_widget').first();
    displayWidgetChangesMessage(widget);
    if (widget.find('input.widget_type').first().attr('value') === 'collection' ) {
      return collection_add_associations(widget);
    } else if (widget.find('input.widget_type').first().attr('value') === 'organizer' ) {
      return organizer_add_associations(widget);
    }
  });
  
  // Listens for break associations
  $('form .view_widget:not(.unsaved_changes)').on('click', 'button.break_submit', function () {
    var widget = $(this).parents('.view_widget').first();
    displayWidgetChangesMessage(widget);
    if (widget.find('input.widget_type').first().attr('value') === 'collection' ) {
      return collection_break_associations(widget);
    } else if (widget.find('input.widget_type').first().attr('value') === 'organizer' ) {
      return organizer_break_associations(widget);
    }
  });

  // Cover changes to dropdown selections or datepicker
  $('form').on('change', '.view_widget:not(.unsaved_changes) select, .view_widget:not(.unsaved_changes) input.hasDatepicker', function () {
    // Don't run if it's a filter change... not the greatest implementation
    if ($(this).parents('.filter_inputs').length > 0 || $(this).parents('.pagination_control').length > 0) { return true; }
    displayWidgetChangesMessage($(this).parents('.view_widget').first());
  });
  
  // Catch the backspace and delete keys
  // Create Cukes that cover when these should and shouldn't appear. No need to be 100% comprehensive
  $('form').on('keyup', '.view_widget:not(.unsaved_changes) textarea, .view_widget:not(.unsaved_changes) input', function (e) {
    // Don't run if it's a filter change... not the greatest implementation
    if ($(this).parents('.filter_inputs').length > 0 || $(this).parents('.pagination_control').length > 0) { return true; }
    if (e.keyCode == 8 || e.keyCode == 46) {
      displayWidgetChangesMessage($(this).parents('.view_widget').first());
    }
  });
  
  // Keypress covers most keys, but misses backspace and delete
  $('form').on('keypress', '.view_widget:not(.unsaved_changes) textarea, .view_widget:not(.unsaved_changes) input', function (e) {
    // Don't run if it's a filter change... not the greatest implementation
    if ($(this).parents('.filter_inputs').length > 0 || $(this).parents('.pagination_control').length > 0) { return true; }
    displayWidgetChangesMessage($(this).parents('.view_widget').first());
  });

  // Positional data listeners need to be active on widgets, even if they already have unsaved changes
  $('form').on('keypress', '.view_widget input.position_field', function (e) {
    addUnsavedPositioningFlag($(this).parents('.view_widget').first());
    displayWidgetChangesMessage($(this).parents('.view_widget').first());
  });
  
  $('form').on('keypress', 'input.document_position_field', function (e) {
    addDocumentUnsavedPositioningFlag($(this).parents('.document_association').first());
    // displayWidgetChangesMessage($(this).parents('.document_association').first());
  });
}

function displayWidgetChangesMessage(widget) {
  if (!widget.hasClass('root') && !widget.hasClass('unsaved_changes')) {
    widget.addClass('unsaved_changes');
    widget.find('.unsaved_changes_message').css('visibility','visible').removeClass('d-none');
  }
}