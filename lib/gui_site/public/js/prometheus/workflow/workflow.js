function setup_table_plugin() {
  $("#workflow_item_table").DataTable({
    "pageLength" : 300,
    // "order": [[ 0, "desc" ]],
    // "aaSorting": [[0, "desc"]],
    "lengthMenu": [[150, 300, -1], [150, 300, "All"]],
    "columnDefs": [
        // {
        //   targets: [0],
        //   type: "natural",
        //   sType: "natural",
        //   bRegex: true,
        //   bSmart: false
        // },
        {
          "targets": [ 4 ],
          "visible": false,
          // "searchable": false,
        }
      ]
    // "aoColumnDefs": [
    //     { "type": 'natural', "aTargets": [ 0 ] },
    //     { "sType": 'natural', "aTargets": [ 0 ] },
    //     { "visible" : false, "aTargets": [ 7 ]}
    //   ]
  });
  $("#workflow_item_table").show();
}

function focus_data_table_search_input() {
  $('.dataTables_filter input').focus();
}

function filter_table_by_selected_role() {
  $("#workflow_item_table").DataTable().column(4).search(
    $('#select_role_filter').val(),
    false,
    true
  ).draw();
}

function setup_select_filter_by_role () {
  $('.select_role_filter').on("change", function(){
    filter_table_by_selected_role();
  });
}


//Create new workflow javascript
function setup_new_workflow_buttons() {
  $('.start_workflow').on('click', function() {
    display_workflow_list_container($(this));
  });
}

function display_workflow_list_container(button) {
  var container = $(button).parent();
  var workflow_list_container = $('#new_workflow_item_container');
  container.append(workflow_list_container);
  // workflow_list_container.css('visibility','visible');
  // workflow_list_container.toggle();
  workflow_list_container.fadeIn(300, function(){$(this).focus()});
  // workflow_list_container.focus();
}


function setup_workflow_list_container_blur() {
  $('#new_workflow_item_container').focusout(function() {
    // console.log('halp! im gettin blurred');
    // $(this).toggle();
    $(this).fadeOut(300);
  });
}

$(document).ready(function(){
  setup_table_plugin();
  setup_select_filter_by_role(); 
  focus_data_table_search_input();
  setup_new_workflow_buttons();
  setup_workflow_list_container_blur();
});