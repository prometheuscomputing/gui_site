// This is currently only used by the Gui_Builder_Profile::File deletion link
function setup_delete_confirmation_prompt_links(content_div) {
  setup_confirmation_prompt_links(content_div, '.delete_confirmation_prompt_link', 'Delete', 'Are you sure you want to delete this item?', function(link){
    // Remove the Stored file from the file widget
    $(link).parents('.file_widget_input').append("<span class='no_stored_file'>No Stored File</div>");
    parent_widget = $(link).parents('div.widget');
    // NOTE: The comment below isn't terribly relevant anymore because we no longer record the basis_readtime!  Leaving it here because I am not entirely sure what it meant in the first place.
    // // Dynamically deleting object on page. Have to empty out basis_readtime to avoid concurrency issue.
    // TODO: Check out how deletion works on homepage with regards to concurrency and missing basis_readtimes.
    // if (parent_widget.find('.basis_readtime').length) {
    //   parent_widget.find('.basis_readtime').val('');
    // } else {
    //   parent_widget.append("<input class='basis_readtime' type='hidden' value=''>");
    // }
    $(link).parents('.stored_file').remove();
  })
}

function convert_delete_button_to_undelete(button) {
  $(button).removeClass('delete_selected');
  $(button).addClass('undelete_selected');
  $(button).text('Undelete');
}

function convert_undelete_button_to_delete(button) {
  $(button).removeClass('undelete_selected');
  $(button).addClass('delete_selected');
  $(button).text('Delete');
}

// function setup_confirmation_prompt_links_old(content_div, link_class, action, message, success) {
//   content_div.find(link_class).click(function() {
//     var link = $(this);
//     var action_url = link.attr('href');
//
//     // Make the popup div
//     $('body').append(
//         "<div title='Confirm "+action+"' id='"+action+"ConfirmationPopup' class='dialog'>" +
//           "<div class='dialog_message' readonly='readonly' id='popupMessage'>"+message+"</div>" +
//         "</div>");
//
//     var buttons = {};
//     buttons['Cancel'] = function() {
//       $(this).remove();
//     };
//     buttons[action] = function() {
//       $(this).remove();
//       $.ajax({
//         type: 'POST',
//         url: action_url,
//         contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//         success: success(link)
//       }); //End AJAX call
//     }; //End Confirm function
//
//     $('#' + action + 'ConfirmationPopup').dialog({
//       modal: true,
//       resizable: false,
//       width: 500,
//       buttons: buttons,
//       position: ['middle', 20],
//       open: function() {
//         $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar-close").remove();
//       },
//       close: function() {
//         $(this).parents(".ui-dialog:first").remove();
//       }
//     });
//     return false; // Cancel the normal HREF navigation
//   }); //End click handler
// } //End setup_confirmation_prompt_links


//Setup A Generic Confirmation Popup Modal using information in link_class provided
function setup_confirmation_prompt_links(content_div, link_class, action, message, success) {
  content_div.find(link_class).click(function() {
    // console.log('did I click on the delete link?');
    
    var link = $(this);
    var action_url = link.attr('href');
    
    // Make the popup div
    // $('body').append(
    //     "<div title='Confirm "+action+"' id='"+action+"ConfirmationPopup' class='dialog'>" +
    //       "<div class='dialog_message' readonly='readonly' id='popupMessage'>"+message+"</div>" +
    //     "</div>");

    //Build the popup div
    //This is disgusting and could be done much better with ES6 template strings but that causes issues with testing unless it is transpiled to ES5 -TB
    var lowercase_action = action.charAt(0).toLowerCase() + action.slice(1);
    var modalID = lowercase_action + 'ConfirmationModal';
    var htmlArray = [];
    htmlArray.push( '<div class = "modal" tabindex="-1" role="dialog" aria-labelledby="genericConfirmationModalLabel" id="' + modalID + '">');
    htmlArray.push( '<div class = "modal-dialog" role="document">');
    htmlArray.push( '<div class = "modal-content">');
    htmlArray.push( '<div class="modal-header">');
    htmlArray.push( '<h5 class= "modal-title" id="genericConfirmationModalLabel">'+ action +'</h5>');
    htmlArray.push( '<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    htmlArray.push( '</div>');
    htmlArray.push( '<div class="modal-body">' + message + '</div>');
    htmlArray.push( '<div class="modal-footer"><button id="genericModalCloseButton" class="btn btn-secondary">Cancel</button><button id="genericModalConfirmButton" class="btn btn-primary">'+ action +'</button></div>');
    htmlArray.push( '</div>');
    htmlArray.push( '</div>');
    htmlArray.push( '</div>');
    
    // console.log(htmlArray.join(''));
    $('body').append(htmlArray.join(''));
    
    var modal = $('#'+modalID);
    
    //Setup close and cancel button click handlers
    modal.find('.close, #genericModalCloseButton').click(function(){
      modal.modal('hide');
      // modal.modal('dispose');
      return false;
    });
    
    //Setup confirm click handler
    modal.find("#genericModalConfirmButton").click(function(){    
      $.ajax({
        type: 'POST',
        url: action_url,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: success(link)
      });
      
      modal.modal('hide');
      // modal.modal('dispose');
      return false;
    });
    
    modal.modal('show');
    
    modal.on('hidden.bs.modal', function(e){
      $(this).modal('dispose');
      $(this).remove();
      // modal.modal('dispose');
      // modal.remove();
    });
    
    return false; // Cancel the normal HREF navigation
  }); //End click handler
} //End setup_confirmation_prompt_links


function setupDeleteSelectedModal(container) {
  
  //TODO Leftover function that is neccessary currently for pending deletion stuff.  I wrote the modal without knowing it needed all this stuff.  Go back and make the button open the modal -TB
  
  container.on('click', 'button.delete_selected', function(){
    var modal = $('#collectionDeleteSelectedModal');
    
    //Setup the modal display
    // var button = $(event.relatedTarget);
    var button = $(this);
    var aname = button.attr('name').replace(/delete_/, "") + '\\[\\]';
    var objectsDeleteArray = [];
    
    //Covers singular organizer objects or all selected collection objects
    var inputs = $('input:checked.deletable[name='+ aname + '], input:hidden.deletable[name='+ aname + ']').each(function(i){
      objectsDeleteArray.push($(this).val());
    });

    $.ajax({
      type: 'POST',
      url :'/del_selected',
      data: {"objectsToDelete" : objectsDeleteArray},
      success: function(responseHTML) {
        modal.find('.modal-body .items-to-delete').html(responseHTML);
        modal.modal('show');
      }
    });
    
    //Setup the modal confirm button
    $('#confirmCollectionDeleteSelected').on('click', function(event) {
      // var modal = $('#collectionDeleteSelectedModal');
    
      if ($('.root.view_widget').find('button.submission_button').length > 0) {
        var parentWidget = button.parents('.view_widget').first();
        delete_associations(parentWidget);
        modal.modal('hide');
      } else {
        modal.modal('hide');
        big_spinning_blocker_on();
        var objectsDeleteArray = [];  
        var listItems = $('#collectionObjectsDeleteList li').each(function(i){
          var domainObjData = {id: $(this).data('id'), classifier: $(this).data('classifier') };
          objectsDeleteArray.push(domainObjData);
        });  
  
        $.ajax({
          type: 'POST',
          url :'/confirmedDeleteObjects',
          data: {"objectsToDelete" : objectsDeleteArray},
          success: function(responseHTML) {
            //This is kind of a crappy way to handle this but since you don't know where else on the current page these objects or any of their compositions are used it is better to just reload
            window.location.href=window.location.href;
          }
        });
      }
    
    });
    
    
    return false;
  });
  
  // $('#collectionDeleteSelectedModal').on('show.bs.modal', function(event) {
  //   var button = $(event.relatedTarget);
  //   var modal = $(this);
  //   var aname = button.attr('name').replace(/delete_/, "") + '\\[\\]';
  //   var objectsDeleteArray = [];
  //   var inputs = $('input:checked.deletable[name='+ aname + ']').each(function(i){
  //     objectsDeleteArray.push($(this).val());
  //   });
  //
  //   $.ajax({
  //     type: 'POST',
  //     url :'/del_selected',
  //     data: {"objectsToDelete" : objectsDeleteArray},
  //     success: function(responseHTML) {
  //       modal.find('.modal-body .items-to-delete').html(responseHTML);
  //     }
  //   });
  //
  // });
  
  //Delayed Deletion of Items unless there is no way to submit later then delete immediately anyhow
  // $('#confirmDelayedCollectionDeleteSelected').on('click', function(event){
  //   var modal = $('#collectionDeleteSelectedModal');
  //   if ($('.root.view_widget').find('button.submission_button').length > 0) {
  //     delete_associations($(delete_input).parents('.view_widget').first());
  //     modal.modal('hide');
  //   } else {
  //     modal.modal('hide');
  //     big_spinning_blocker_on();
  //     var objectsDeleteArray = [];
  //     var listItems = $('#collectionObjectsDeleteList li').each(function(i){
  //       var domainObjData = {id: $(this).data('id'), classifier: $(this).data('classifier') };
  //       objectsDeleteArray.push(domainObjData);
  //     });
  //
  //     $.ajax({
  //       type: 'POST',
  //       url :'/confirmedDeleteObjects',
  //       data: {"objectsToDelete" : objectsDeleteArray},
  //       success: function(responseHTML) {
  //         //This is kind of a crappy way to handle this but since you don't know where else on the current page these objects or any of their compositions are used it is better to just reload
  //         window.location.href=window.location.href;
  //       }
  //     });
  //   }
  // });
  
  //Immediate Deletion of items
  //TODO Make this immediate deletion instead of how it used to be where it is always delayed no matter what if there is a submit button
  // $('#confirmCollectionDeleteSelected').on('click', function(event) {
  //   var modal = $('#collectionDeleteSelectedModal');
  //
  //   if ($('.root.view_widget').find('button.submission_button').length > 0) {
  //     delete_associations($(delete_input).parents('.view_widget').first());
  //     modal.modal('hide');
  //   } else {
  //     modal.modal('hide');
  //     big_spinning_blocker_on();
  //     var objectsDeleteArray = [];
  //     var listItems = $('#collectionObjectsDeleteList li').each(function(i){
  //       var domainObjData = {id: $(this).data('id'), classifier: $(this).data('classifier') };
  //       objectsDeleteArray.push(domainObjData);
  //     });
  //
  //     $.ajax({
  //       type: 'POST',
  //       url :'/confirmedDeleteObjects',
  //       data: {"objectsToDelete" : objectsDeleteArray},
  //       success: function(responseHTML) {
  //         //This is kind of a crappy way to handle this but since you don't know where else on the current page these objects or any of their compositions are used it is better to just reload
  //         window.location.href=window.location.href;
  //       }
  //     });
  //   }
  //
  // });
  
}

function delete_associations(widget) {
  displayWidgetChangesMessage(widget);
  if (widget.find('input.widget_type').first().attr('value') === 'collection' ) {
    return collection_delete_associations(widget);
  } else if (widget.find('input.widget_type').first().attr('value') === 'organizer' ) {
    return organizer_delete_associations(widget);
  }
}