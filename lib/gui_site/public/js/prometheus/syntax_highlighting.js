// Set up syntax highlighting
function setup_syntax_highlighting(container) {
  syntax_highlight = new SyntaxHighlight();
  syntax_highlight.init();
  
  //syntax_highlight.populate_all_select_boxes();
  var select_box;
  container.find('.widget.code textarea').each(function(i, ele){
    syntax_highlight.setup_textarea(ele);
    select_box = syntax_highlight.select_box_from_textarea(ele);
    syntax_highlight.add_listeners(select_box);
  });
  // $.each($('textarea.code'), function(i, ele) {
  //   syntax_highlight.setup_textarea(ele);
  //   select_box = syntax_highlight.select_box_from_textarea(ele);
  //   syntax_highlight.add_listeners(select_box);
  // });
}

codemirror_root = '/js/editors/codemirror';

SyntaxHighlight = (function() {});

SyntaxHighlight.prototype = {
  init: function() {
    //var code = '<div class="code_select"><select class="empty"></select></div>';
    //$('textarea.code').parent().prepend(code);
    //$('textarea.code').each(function(index, textarea) {
    //  var choice = this.select_box_from_textarea(textarea);
    //  console.log(choice);
    //});
    //TODO write these files to the dom using a promise instead of this synch ajax call
    //TODO screw that use the codemirror addon load mode and figure out how it works.
    load_js(this._code_mirror_js_file);
    load_js(this._code_mirror_load_js_file);
    load_js(this._code_mirror_meta_js_file);
    // load_js(codemirror_root + '/mode/xml/xml.js');
    
    
    
    // CodeMirror.modeURL = '../mode/%N/%N.js';
    CodeMirror.modeURL = '/js/editors/codemirror/mode/%N/%N.js';
    
  },

  // reference to the CodeMirror object for the CodeMirror textarea we generate
  code_mirror: null,
  select_box: null,

	select_box_from_textarea: function(textarea) {
    //return $(textarea).parent().find('.code_select > select:first')[0];
    // return $(textarea).parents('.simple_widget:first').prev().find('select:first')[0];
    
    return $(textarea).closest('.code.widget').find('select')[0];
    
	},

  textarea_from_select_box: function(select) {
    // return $(select).parents('.simple_widget:first').next().find('textarea.code:first')[0];
    
    return $(select).closest('.code.widget').find('textarea.editor')[0];
  },

  load_language: function(lang) {
    // only load the language if it hasn't already been loaded before.
    // Previously loaded languages should be entries in the loaded_languages
    // array
    if ($.inArray(lang, this._loaded_languages) !== -1) {
      var lang_js_file = this._languages[lang];
      load_js(lang_js_file);
      this._loaded_languages.push(lang);
    }
  },

  setup_textarea: function(ele, lang) {
    // set the language equal to the selected value in the select box
    if (!lang) {
      var select = this.select_box_from_textarea(ele);
      // lang = $(select).val();
      lang = select.value;
    }
    
    // if the language selected does not have a parser, we won't change
    // anything or load anything
    if (this._languages[lang] === undefined) {
      console.log('language ' + lang + ' not a choice; setting to dummy');
      var old_lang = lang;
      // lang = 'dummy';
      lang = 'XML'; //Default to XML for now if there is no language that exists
    }
    //TODO Figure out what this is doing and Why
    // get the code (text) from the active code textarea
    // if (this.code_mirror) {
    //   var code = this.code_mirror.getCode();
    // }

    this.load_language(lang);
    // var code_mirror_settings = $.extend(this._default_settings, this._languages[lang]);
    var code_mirror_settings = this._default_settings;

		// the content field will fill in the textarea that we create with the
		// code from the previous text area
    // if (code !== undefined) {
    //   code_mirror_settings.content = code;
    //   $(this.code_mirror.wrapping).remove();
    // }
    
    // console.log(code_mirror_settings);

    this.code_mirror = CodeMirror.fromTextArea(ele, code_mirror_settings);

    // console.log(this.code_mirror);
    
    // console.log(CodeMirror.modes);
    // console.log(CodeMirror.mimeModes);

    this.decorate();
    
    this.changeMode(lang);

    // var syntax_class = this._syntax_not_found_class;
    // var select = this.select_box_from_textarea(ele);
    // if (lang === 'dummy') {
    //   // print a notice that the selected language does not have syntax
    //   // highlighting
    //   if ($(select).next('.' + syntax_class).length === 0) {
    //     var html = "\
    //       <span class=" + syntax_class + ">\
    //         <img src='/images/hazard.png' />\
    //         <span style='display:none'>\
    //           Syntax highlighting not supported for this language.\
    //         </span>\
    //       </span>";
    //
    //     $(select).after(html);
    //
    //     $('.' + syntax_class).hover(function() {
    //       $(this).find('span:first').show();
    //     }, function() {
    //       $(this).find('span:first').hide();
    //     });
    //   }
    // } else {
    //   // remove the notice
    //   $(select).next('.' + syntax_class).remove();
    // }
  },

  decorate: function() {
    $(".CodeMirror-wrapping").css("border", "1px solid black");
  },
  
  changeMode: function(language) {
    if (language) {
      var extension;
      var mode;
      var spec;
      switch (language) {
        case 'Javascript':
          extension = 'js';
          break;
        case 'XML':
          extension = 'xml';
          break;
        case 'SQL':
          extension = 'sql';
          break;
        case 'Ruby':
          extension = 'rb';
          break;
        case 'HTML':
          extension = 'html';
          break;
        case 'JSON':
          extension = 'json';
          break;
        case 'CSS':
          extension = 'css'
          break;
      }
    
      var info = CodeMirror.findModeByExtension(extension);
      // var info = CodeMirror.findModeByMIME(extension);
      
      if (info) {
        mode = info.mode;
        spec = info.mime;
      } else {
        mode = spec = language;
      }
      
      this.code_mirror.setOption('mode', spec);
      CodeMirror.autoLoadMode(this.code_mirror, mode);
    }
  },

  //TODO This method is dumb you should know the textarea make this all self contained as part of the init code -TB
  add_listeners: function(ele) {
		var self = this;
    $(ele).change(function() {
      var lang = $(this).val();
      // var textarea = self.textarea_from_select_box(ele);
      // self.setup_textarea(textarea, lang);
      self.changeMode(lang);
    });
  },

  _languages: {
    'Ruby': {
      parserfile: ['../contrib/ruby/js/tokenizeruby.js', 
                   '../contrib/ruby/js/parseruby.js'],
      stylesheet: codemirror_root + '/contrib/ruby/css/rubycolors.css'
    }, 
    'Python': {
			parserfile: '../contrib/python/js/parsepython.js',
			stylesheet: codemirror_root + '/contrib/python/css/pythoncolors.css'
    }, 
    'Javascript': {
      parserfile: ['tokenizejavascript.js', 'parsejavascript.js'],
      stylesheet: codemirror_root + '/css/jscolors.css'
    }, 
    'Lua': {
      parserfile: "../contrib/lua/js/parselua.js",
      stylesheet: codemirror_root + '/contrib/lua/css/luacolors.css'
    },
    'SQL': {
      parserfile: "../contrib/sql/js/parsesql.js",
      stylesheet: codemirror_root + '/contrib/sql/css/sqlcolors.css'
    },
    'Groovy': {
      parserfile: "../contrib/groovy/js/parsegroovy.js",
      stylesheet: codemirror_root + '/contrib/sql/css/sqlcolors.css'
    },
    'dummy': {
      parserfile: '../mode/xml/xm.js',
      stylesheet: codemirror_root + '/css/dummycolors.css',
    }
  },

  // settings that are common to all CodeMirror objects
  _default_settings: {
    // path: codemirror_root + '/js/',
    indentUnit: 2,
    lineNumbers: true,
    // if textWrapping is true, it causes mega-slowness, at least if
    // lineNumbers is true as well
    textWrapping: false
  },

  _code_mirror_js_file: codemirror_root + '/lib/codemirror.js',
  _code_mirror_load_js_file: codemirror_root + '/addon/mode/loadmode.js',
  _code_mirror_meta_js_file: codemirror_root + '/mode/meta.js',

  _syntax_not_found_class: 'syntax_not_found',

  _loaded_languages: []
}
