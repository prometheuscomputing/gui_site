// function diffUsingJS(base, newtxt, base_text_label, new_text_label) {
//   base_text_label = base_text_label || 'Base Text';
//   new_text_label  = new_text_label || 'New Text';
//   var sm = new difflib.SequenceMatcher(base, newtxt);
//   var opcodes = sm.get_opcodes();
//   var diffoutputdiv = dojo.byId("diff_view");
//   while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
//   var contextSize = dojo.byId("diff_context_size").value;
//   contextSize = contextSize ? contextSize : null;
//   diffoutputdiv.appendChild(diffview.buildView({ 
//     baseTextLines:  base,
//     newTextLines:   newtxt,
//     opcodes:        opcodes,
//     baseTextName:   base_text_label,
//     newTextName:    new_text_label,
//     contextSize:    contextSize,
//     viewType:       dojo.byId("diff_inline").checked ? 1 : 0 
//   }));
//   //window.location = window.location.toString().split("#")[0] + "#diff";
// }

function diffUsingJS(base, newtxt, base_text_label, new_text_label) {
  base_text_label = base_text_label || 'Base Text';
  new_text_label  = new_text_label || 'New Text';
  
  var diffoutputdiv = $('#diff_view');
  diffoutputdiv.empty();
  
  var baseLines = difflib.stringAsLines(base);
  var newLines = difflib.stringAsLines(newtxt);
  var sm = new difflib.SequenceMatcher(baseLines, newLines);
  var opcodes = sm.get_opcodes();
  
  diffoutputdiv.append(diffview.buildView({ 
    baseTextLines:  baseLines,
    newTextLines:   newLines,
    opcodes:        opcodes,
    baseTextName:   base_text_label,
    newTextName:    new_text_label,
    contextSize:    0,
    viewType:       0 //side by side -- 1 is inline
  }));
  
  //while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
  //var contextSize = dojo.byId("diff_context_size").value;
  //contextSize = contextSize ? contextSize : null;
  // var result = diffview({ 
  //   baseTextLines:  base,
  //   newTextLines:   newtxt,
  //   baseTextName:   base_text_label,
  //   newTextName:    new_text_label,
  //   inline:         false
  // });
  //result = diffview({baseTextLines: base,newTextLines: newtxt,baseTextName: base_text_label,newTextName: new_text_label,contextSize: '',inline: true,tchar: '',tsize: 4});

  // WORKING PRETTY DIFF CALL
  // result = prettydiff({
  //   comments: "indent",
  //   conditional: false,
  //   content: false,
  //   context: "",
  //   csvchar: ", ",
  //   diff: newtxt,
  //   difflabel: new_text_label,
  //   force_indent: false,
  //   html: false,
  //   inchar: " ",
  //   indent: "",
  //   inlevel: 0,
  //   insize: 4,
  //   jsscope: false,
  //   lang: "text",
  //   mode: "diff",
  //   quote: false,
  //   semicolon: false,
  //   source: base,
  //   sourcelabel: base_text_label,
  //   style: "indent",
  //   topcoms: false,
  //   wrap: 100,
  //   //diffview: 'sidebyside'
  //   diffview: 'inline'
  // });
  //diffoutputdiv.append(result[0]);
}


function scrollToElement(ele) {
    $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
}

$ = jQuery;
$.fn.extend({
  enable: function() {
    return this.each(function() {
      $(this).removeAttr('disabled');
    });
  },
  disable: function() {
    return this.each(function() {
      $(this).attr('disabled', 'disabled');
    });
  },
  attribute_row: function() {
    return this.each(function() {
      $(this).hover(function() {
        $(this).addClass('hover');
      }, function() {
        $(this).removeClass('hover');
      });
    });
  },
  richtext: function() {
    return this.each(function() {
      $(this).click(function() {
        var attr = $(this).children('.attr').text();
        $('tr.bg-info').removeClass('bg-info');
        if (attr == change_tracker.active_attribute) {
          change_tracker.minimize_diff();
        } else {
          var old_text = $(this).find('td.old').text();
          var new_text = $(this).find('td.new').text();
          var old_text_label = $('table.changes .old_time').text();
          var new_text_label = $('table.changes .new_time').text();
          change_tracker.active_attribute = attr;
          $(this).addClass('bg-info');
          diffUsingJS(old_text, new_text, old_text_label, new_text_label);
          $('#diff_outline').removeClass('d-none');
          scrollToElement($('#diff_outline'));
        }
      });
    });
  },

  timestamp: function() {
    return this.each(function() {
      $(this).find('input').click(function() {
        var label_text = $(this).next().text();
        if ($(this).is(':checked')) {
          change_tracker.counter++;
        } else {
          change_tracker.counter--;
        }
        change_tracker.update_change_button();
      });
    });
  }
});

change_tracker = {
  counter: 0,
  active_attribute: null,
  update_counter: function() {
    this.counter = $('#timestamp_list input:checked').length;
  },
  update_change_button: function() {
    var count = change_tracker.counter;
    if (count == 2) {
      change_tracker.disable_unchecked_boxes();
      $('#view_change_btn').enable();
    } else if (count < 2) {
      $('#view_change_btn').disable();
      change_tracker.enable_boxes();
    } else {
      change_tracker.disable_unchecked_boxes();
    }
  },
  enable_boxes: function() {
    $('#timestamp_list').find('input:disabled').enable();
  },
  disable_unchecked_boxes: function() {
    $('#timestamp_list').find('input:checkbox:not(:checked)').disable();
  },
  get_checked_timestamps: function() {
    return $.map($('#timestamp_list input:checkbox:checked'), function(e) {
      return $(e).attr('id').substring(5);
    });
  },
  submit_change_button: function() {
    if (change_tracker.counter != 2) {
    } else {
      $('#diff_outline').addClass('d-none');
      $('#change_view').html('<center><img src="/images/ajax-loader.gif" /></center>');
      $.ajax({
        type: 'GET',
        url: '/change_tracker/timestamps',
        data: {
          'timestamps': change_tracker.get_checked_timestamps().join('.'),
          'package': $('#package').val(),
          'domain_obj_class': $('#domain_obj_class').val(),
          'domain_obj_id': $('#domain_obj_id').val()
        },
        dataType: 'html',
        success: function(data, status) {
          $('#change_view').html(data);
        }
      });
    }
  },
  minimize_diff: function() {
    $('tr.active_attribute').removeClass('active_attribute');
    $('#diff_outline').addClass('d-none')
    change_tracker.active_attribute = null;
  }
}
