function get_page_number(content_div) {
  var has_page_number = content_div.parents('.widget').first().find('input[name="page_number"]').length > 0;
  if (has_page_number) {
    return content_div.parents('.widget').first().find('input[name="page_number"]').val();
  } else {
    return 1;
  }
}

function set_page_number(content_div, value) {
  var has_page_number = content_div.parents('.widget').first().find('input[name="page_number"]').length > 0;
  if (has_page_number) {
    content_div.parents('.widget').first().find('input[name="page_number"]').val(value);
  }
}

function get_page_size(content_div) {
  var has_page_size = content_div.find('input[name="page_size"]').length > 0;
  if (has_page_size) {
    return content_div.find('input[name="page_size"]').val();
  }
  else {
    return 0;
  }
}

function set_page_size(content_div, new_page_size) {
  var has_page_size = content_div.find('input[name="page_size"]').length > 0;
  if (has_page_size) {
    content_div.find('input[name="page_size"]').val(new_page_size);
  }
  else {
    return 0;
  }
}

// function get_max_page_number(content_div) {
//   var has_page_number = content_div.parents('.widget').first().find('input[name="max_page_number"]').length > 0;
//   if (has_page_number) {
//     return content_div.parents('.widget').first().find('input[name="max_page_number"]').val();
//   }
//   else {
//     return null;
//   }
// }
