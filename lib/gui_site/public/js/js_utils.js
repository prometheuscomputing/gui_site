// source: http://javascript.crockford.com/prototypal.html
// usage: 
//   newObject = Object.create(oldObject);
if (typeof Object.create !== 'function') {
  Object.create = function (o) {
    function F() {}
    F.prototype = o;
    return new F();
  };
}
