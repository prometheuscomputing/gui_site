module Gui
  
  # Set default server options
  SERVER_DEFAULTS = { 
    :action          => :start,
    :connection_type => :port,
    :daemonize       => false,
    :log             => '<LOG_DIR>/app.log', # <LOG_DIR> will be replaced
    :pid             => '<DATA_DIR>/pid/<SPEC_NAME>.pid', # <SPEC_NAME> will be replaced
    :port            => '7000',
    :host            => '0.0.0.0',
    :process_tag     => '<SPEC_NAME>_<SERVER>',
    :server          => RUBY_PLATFORM.match?(/java/) ? :mizuno : :puma,
    :socket          => '/tmp/<SPEC_NAME>.sock', # <SPEC_NAME> will be replaced by the loaded spec_name
    :puma_ctl_port   => '9293'
  }
  
  # Set the default gui_builder options.
  # Warning: For tests, car_example has it's own defaults in car_example/setup.rb that are loaded first. Not the normal case.
  OPTION_DEFAULTS = {
    # Some options have no default value but are included here because only options whose keys are in this hash will be passed on to gui_builder
    :change_tracker_mode            => :enabled,
    :clear_view_home                => false,
    :clear_view_selection_page      => true,
    :clone                          => true, # Rename to 'enable_clone'?
    :color_coding                   => true, # deprecate this color option in favor of a 'per view-type' approach?
    :concise_errors                 => nil,
    :home_page                      => {:url => '/', :name => 'Home'},
    :show_advanced_view             => true,
    :custom_pages                   => [],
    :custom_page_root               => nil,
    :custom_spec                    => nil,
    :db_location                    => nil,
    :db_url                         => nil,
    :default_page_size              => 8,
    :min_page_size                  => 1,
    :max_page_size                  => 1000,
    :default_text_language          => :Markdown,
    :default_domain_object          => Gui::Home.new,
    :disable_breadcrumbs_for_views  => [],
    :disable_cloning_for_views      => [],
    :disable_external_resources     => false,
    :disable_history_for_views      => [],
    :disable_info_messages          => false,
    :disable_send_email             => false,
    :disable_search_all             => false,
    :docker                         => false,
    :email_alert_recipients         => {},
    :enabled_form_draft_saving      => false,
    :enable_breadcrumbs             => true,
    :enable_concurrency             => true,
    :enable_db_download             => false,
    :enable_infinite_scrolling      => false,
    :enable_registration_without_inv_code => false,
    :favicon                        => nil,
    :forced_language_value          => nil,
    :license_agreement_path         => nil,
    :local_authentication           => true,
    :login                          => true,
    :login_or_register_blurb_path   => nil,
    :mode                           => :live, # or :dev
    :model_extensions               => nil,
    :multivocabulary                => false,
    :object_history                 => true, # Rename to 'enable_object_history'?
    :policy_active                  => false,
    :policy_default                 => :write, # <:read|:write|:none>.  Base permissions for users.
    :pre_schema_model_extensions    => nil,
    :require_email_for_user         => false, # Require email makes the login page require an email be entered.
    :schema_extensions_dir          => nil,
    :secure_cookie                  => false,
    :show_text_languages            => false,
    :side_nav_bar                   => false,
    :spec_module                    => nil,
    :spec_name                      => nil, # This really should be called gem_name, and should be in the form "<PROJECT NAME>_generated"
    :spec_short_title               => nil,
    :spec_title                     => nil,
    :spec_url                       => nil,
    :ssl                            => false, # Also used in server startup, but included here so that gui_builder can check whether or not it is running under SSL.
    :ssl_cert_file                  => nil,
    :ssl_key_file                   => nil,
    :ssl_verify                     => nil,
    :string_html_scripts            => nil,
    :suppress_rack_log              => false,
    :testing_mode                   => false, # Testing mode is used for specific things in a test environment. e.g. Fake mail server during testing
    :text_languages                 => ['Markdown', 'LaTeX', 'Plain', 'HTML', 'Textile', 'Kramdown'],
    :top_nav_bar                    => true,
    :tree_view                      => {:suppress => true},
    :web_authentication             => false,
    :workflow_options               => {}
  }
  
  # Apparently these aren't used anymore.  The client code is commented out but not removed...
  # Parsed option keys. These options have variables that need to be substituted in.
  # PARSED_OPTIONS = [:socket, :pid, :log, :process_tag, :log_dir, :spec_title, :tempfile_dir, :data_dir]
  
  DEFAULTS = SERVER_DEFAULTS.merge(OPTION_DEFAULTS)
end