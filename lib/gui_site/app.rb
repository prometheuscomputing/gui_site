# This file contains your application, it requires dependencies and necessary
# parts of the application.
#
# It will be required from either `config.ru` or `start.rb`


#External JavaScript Gems that setup themselves for Ramaze
if !Gui.option(:disable_external_resources)
  require 'prometheus_mathjax'
end

#possibly always required given the new ckeditor text widget.
require 'prometheus_ckeditor'

require_relative 'server_config'

puts Rainbow("\nChangeTracker: ").cyan + 
  Rainbow(
    if ChangeTracker.disabled
      'disabled.'
    else 
      if ChangeTracker.extra_limited_mode
        'extra_limited.'
      else
        if ChangeTracker.limited_mode 
          'limited.'
        else
          'enabled'
        end
      end
    end
  ).magenta
puts Rainbow("Testing mode:  ").cyan + Rainbow(Gui.option(:testing_mode) ? 'ON' : 'OFF').magenta
puts Rainbow("DB location:   ").cyan + Rainbow("#{DB.opts[:uri]}\n").magenta


require 'gui_builder/model_extensions'
require_relative 'controller/init'

if Gui.policy_active?
  puts Rainbow('Policy Active').magenta
  Gui.create_policy_classes if Gui.policy_active?
else
  puts Rainbow('Policy Inactive').magenta
end

# Make sure that Ramaze knows where you are
Ramaze.options.roots.push(__dir__)
Ramaze.options.mode = Gui.option(:mode)
# This is at odds with settings in server_config. -SD
# Encoding.default_external = Encoding::ASCII_8BIT

#---------------------------------------------
# Here we map the exception type to the action
#---------------------------------------------
require_relative 'errors'
Rack::RouteExceptions.route(Gui::Error403, '/error_403')
Rack::RouteExceptions.route(Gui::Error404, '/error_404')
Rack::RouteExceptions.route(Gui::Error405, '/error_405')
Rack::RouteExceptions.route(Gui::Error500, '/error_500')
Rack::RouteExceptions.route(Gui::Error501, '/error_501')
Rack::RouteExceptions.route(Exception, '/error_500')
#---------------------------------------------

if $verbose
  puts "Required Ramaze version #{Ramaze::VERSION}"
  puts "Required Innate version #{Innate::VERSION}"
  puts "Required Rack version #{Rack::VERSION}"
  puts "mode = #{Ramaze.options.get(:mode)[:value]}"
end
system "open 'http://localhost:#{Gui.option(:port)}'" if Gui.option(:launch_localhost)