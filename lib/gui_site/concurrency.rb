require 'active_support/core_ext/string/inflections' # The titleize gem has a bug.  This require fixes the problem.
module Gui::Concurrency

  # FIXME This is effectively a Controller for the concurrency popup.  Maybe it should actually be in a Controller class????
  def concurrency_popup(root_domain_obj, error)
    locals = {}
    # locals[:concurrency_errors]   = error.concurrency_issues # not used in template so commented out
    locals[:concurrency_warnings] = extract_warnings(error)
    locals[:concurrency_message]  = error.message
    locals[:domain_obj]           = root_domain_obj
    locals[:field_comparison]     = build_concurrency_widget_templates(root_domain_obj, error.concurrency_issues)
    template_path = File.join(__dir__, 'view', 'concurrency_popup.haml')
    Haml::Engine.new(File.read(template_path), :filename => template_path).render(self, locals)
  end
  
  def concurrency_popup_errors_from_issues(per_object_concurrency_data = {})
    # TODO we currently have no system for handling concurrency issues with associations (or ordering ??) even though some of the infrastructure to do so is built.  When handling of those other types of errors is implemented then we will need to modify this.
    per_object_concurrency_data.select{ |_, issues| issues['attributes'] && issues['attributes'].any? }
  end
  
  private
  def render_concurrency_issue(domain_obj, getter, data)
    director_args = {obj:domain_obj, getter:getter, user:current_user, options:{:render_custom_templates => 'concurrency', :data => data}}
    Gui::Director.new(**director_args).render
  end

  def build_concurrency_widget_templates(root_domain_obj, concurrency_issues = {})
    builder = ''
    # The assumption is that, by the time you get here, each obj really does have issues that we need to deal with
    # As of July 13, 2016, the only issue_category should be 'attributes'.  More may be implemented someday.
    concurrency_issues.each do |obj, issue_categories|
      pretty_name = obj.class.name.demodulize
      builder << "<tr class='label_row'><td colspan='5'>"
      if obj == root_domain_obj
        builder << "<h3>#{pretty_name}</h3>"
      else
        # we might not have getter here...
        builder << "<h4>Associated #{pretty_name}</h4>"
      end
      builder << '</td></tr>'
      issue_categories.each do |category, issues|
        if category == 'warnings' # these may be in there but we should ignore them here. they should have been removed in #extract_warnings
          next
        end
        issues.each do |getter, issue_data|
          builder << render_concurrency_issue(obj, getter, issue_data)
        end
      end
    end
    builder
  end
  
  def extract_warnings(error)
    collected_warnings = []
    error.concurrency_issues.each do |_, issue_categories|
      issue_warnings = issue_categories.delete('warnings')
      if issue_warnings
        issue_warnings.each { |issue_warning| collected_warnings << issue_warning }
      end
    end
    collected_warnings
  end

end
