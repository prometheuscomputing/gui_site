require_relative 'form_parsing/apply_form_changes'
require_relative 'form_parsing/attribute'
require_relative 'form_parsing/associations'
require_relative 'form_parsing/enumeration'
require_relative 'form_parsing/multi_primitive'
class Gui::Controller
  class FormParser
  
    # Given params describing a domain object, and a cache of domain objects,
    # returns the domain obj either from cache or the database
    # This may not be just for assoc params but that is how it is currently being used.  There is already a Controller methods named domain_obj_from_params.  Having two methods with the same name and different signatures is no bueno, so I changed this one. - MF
    def domain_obj_from_assoc_params(klass, id, getter = nil, domain_obj_cache = {})
      if getter && !getter.empty?
        # If id is "new" then obj will be nil.  Are we assuming that if getter is non-nil that we will not be dealing with a new domain object?
        obj = klass.to_const[id.to_i]
        raise "Unable to find parent obj #{klass}[#{id}]" unless obj
        domain_obj = obj.send(getter)
        raise "Unable to find domain_obj #{klass}[#{id}]" unless domain_obj
        klass = domain_obj.class.to_s
        id = domain_obj.id.to_s if domain_obj.id
      end
      return nil if klass.nil? || id.nil? || klass == '' || id == ''
      if domain_obj_cache.include?([id, klass])
        domain_obj_cache[[id, klass]]
      else
        domain_obj = (id.is_a?(String) && id.match?(/^new/)) ? klass.to_const.new : klass.to_const[id.to_i]
        domain_obj_cache[[id, klass]] = domain_obj
      end
    end
  
  end
end