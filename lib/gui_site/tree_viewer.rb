module Gui
  #--------------------------Stuff for the Tree Viewer ------------------------------------
  class TreeViewTree < Gui::Controller

    def initialize(options = {})
      # NOTE: mandatory options to pass in are :start_with, :host, :protocol
      default_options = {:ignored_getters => [], :ignored_getters_by_class => {}, :ignored_classes => []}
      @options      = default_options.merge(options)
      @nodes        = []
      @root_node    = nil
      @current_node = nil
      @host         = options[:host]
      @protocol     = options[:protocol]
      @spec_cache   = {}
      build_full_tree(options[:start_with]) if options[:start_with]
      # puts "FINISHED building tree with #{@nodes.count} nodes"
    end

    def add_node(node)
      @nodes << node unless @nodes.include?(node)
       # @nodes[node.dom_obj.object_id] ||= node
    end

    def add_edge(parent, child)
      # TODO: maybe make this better someday.  Parent adds edge to itself....don't really like that at all.  Nodes shouldn't contain information about edges.  Probably alright for now...
      # p = @nodes[parent.dom_obj.object_id]
      # c = @nodes[child.dom_obj.object_id]
      # puts "ADD EDGE - parent: #{parent.dom_obj.class}; child: #{child.dom_obj.class}"
      # p.add_child(c)
      # This is the same thing but doesn't fail if the parent and child aren't already part of the tree.  Probably it should fail if parent and/or child isn't part of the tree.
      parent.add_child(child)
    end

    def get_ordered_child_data(node)
      # FIXME policy here
      # It would be faster if aggregations could leverage #obj_policy_scope but...that would require the aggregations method to be in the controller namespace and/or have ready access to Pundit and @current_user...and it definitely isn't and doesn't.  So, we get the objects and then run each one through authorize_object
      aggs = node.aggregations.to_a
      return aggs if aggs.empty?
      # FIXME what do we do if some objects aren't readable?  Probably just not show in tree...
      # aggs.select! do |pair|
      #   puts pair.inspect;puts
      #   pair
      # end
      aggs.reject! do |pair|
        getter = pair.first
        klass  = pair.last[:data][:class]
        @options[:ignored_getters].include?(getter) || @options[:ignored_classes].include?(klass) || (@options[:ignored_getters_by_class][klass] && @options[:ignored_getters_by_class][klass].include?(getter)) ||
          klass.to_const.method_defined?(:type_of_file)
      end
      return aggs if aggs.empty?
    
      # get the spec ordering from the cache or find it and then cache it
      klass           = node.dom_obj.class
      ordered_getters = @spec_cache[klass]
      unless ordered_getters
        spec = Gui.loaded_spec.retrieve_view(:Details, klass, :Organizer)
        ordered_getters = spec ? spec.content.map { |widget| widget&.getter&.to_sym }.compact : []
        # even if it is empty we still store it
        @spec_cache[klass] = ordered_getters
      end
      aggs.sort_by { |pair| ordered_getters.index(pair.first) }
    end

    def build_down_from(node)#, bottom_graph=TreeViewTree.new())
      add_node(node)
      # child_data = node.compositions
      child_data = get_ordered_child_data(node)
      child_data.each do |getter, info_hash|
        label      = (info_hash[:data][:display_name] || getter.to_s).downcase
        child_objs = info_hash[:value]
        child_objs.each do |child_obj|
          # puts "About to get child node for #{node.unique_name} -- #{child_obj.class}"
          if child_obj.is_a?(ORM_Instance)
            child_node = @nodes.find { |n| n.dom_obj == child_obj } || TreeViewNode.new(child_obj, label, node)
            child_node.parent ||= node 
            child_node.label ||= label
            add_node(child_node)
            add_edge(node, child_node)
            build_down_from(child_node)
          else # when in heck does this happen??
            raise "How did this happen?"
            child_node = TreeViewNode.new(child_obj, label, node) 
            add_node(child_node)
            add_edge(node, child_node)
          end
        end
      end
    end
  
    def get_root_obj(obj)
      stored_roots = Gui::Controller.session[:tree_view_roots]
      if stored_roots && stored_roots.any?
        puts "tree root: #{stored_roots.first.pretty_inspect}"
        return stored_roots.first
      end
      parent = obj.composer
      # FIXME policy here.  Not sure what best course of action here is if not authorized for parent.  Probably return obj as the root obj.
      authorize_object_read(parent)
      if parent
        get_root_obj(parent)
      else
        obj
      end
    end
  
    def build_full_tree(starting_obj)
      # @current_node ||= TreeViewNode.new(starting_obj)
      # add_node(@current_node)
      root_obj = get_root_obj(starting_obj)
      # @root_node = @nodes[root_obj.object_id] || TreeViewNode.new(root_obj)
      @root_node = TreeViewNode.new(root_obj)
      add_node(@root_node)
      ret = build_down_from(@root_node)
      @current_node ||= @nodes.find { |n| n.dom_obj == starting_obj }
      ret
    end

    def to_s
      node_string = ''
      @nodes.each_with_index do |node, i|
        node_string += "TreeViewNode #{i}: #{node}\n"
      end
      node_string
    end

    def traverse_down_html(start_node, html = '')
      children = start_node.children
      # puts "******** Start Node: #{start_node.tree_view_display_name}\n#{children.inspect}\n\n"
      html << '<ul>' unless children.empty?
      children.each do |child|
        child == @current_node ? html << '<li id="current_node">' : html << '<li>'
        # html << '<a href=' << child.to_href(@host, @protocol) << '>'
        html << '<a href=' << child.to_href(@host, @protocol) << child.classifier_string << child.id_string << '>'
        html << child.tree_view_display_name
        html << '</a>'
        traverse_down_html(child, html)
        html << '</li>'
      end
      html << '</ul>' unless children.empty?
      html
    end

    def to_html
      html = '<ul>'
      @current_node == @root_node ? html << '<li id="current_node">' : html << '<li>'
      # html << '<a href=' << @root_node.to_href(@host, @protocol) << '>'
      html << '<a href=' << @root_node.to_href(@host, @protocol) << @root_node.classifier_string << @root_node.id_string << '>'
      html << @root_node.tree_view_display_name
      html << '</a>'
      traverse_down_html(@root_node, html)
      html << '</li></ul>'
      html
    end
  
    # def [](object)
    #   # @nodes[object.class.to_s + ':' + object.id.to_s]
    #   @nodes[object.object_id]
    # end

  end

  class TreeViewNode
    attr_reader   :unique_name
    attr_reader   :children
    attr_accessor :parent
    attr_accessor :label
    attr_reader   :compositions, :composer
    attr_reader   :aggregations
    attr_reader   :tree_view_display_name
    attr_reader   :dom_obj
    MAX_NAME_LENGTH = Gui.option(:tree_view)[:max_name_length] || 100
  
    def initialize(object, label = nil, parent_object = nil)
      # if parent
      #   puts "Building#new node for a #{object.class} with parent #{parent.unique_name}"
      # else
      #   puts "Building new node for a #{object.class} without a parent"
      # end
      @dom_obj  = object
      @label    = label.to_s if label
      @parent   = parent_object
      @children = []
      # if it isn't a ORM_Instance it won't respond to these.  It should be a primitive in this case and therefore be a leaf that can only be reached by traversing downward and will thus have no need for these arrays to be populated.
      # @composer = object.respond_to?(:composer) ? object.composer : []
      # FIXME using composition_data instead of compositions
      # @compositions = object.respond_to?(:compositions_with_data) ? object.compositions_with_data : {}
      @aggregations = dom_obj.respond_to?(:immediate_aggregations_with_data) ? dom_obj.immediate_aggregations_with_data : {}
      @id           = dom_obj.respond_to?(:id) ? dom_obj.id : parent.dom_obj.id
      @classifier   = dom_obj.is_a?(ORM_Instance) ? dom_obj.class.to_s : parent.dom_obj.class.to_s
      set_unique_name
      raise unless @unique_name
      @tree_view_display_name = set_tree_view_display_name
      raise unless @tree_view_display_name
    end
    
    def unique_name_for(obj)
      return nil unless obj.is_a?(ORM_Instance)
      "#{obj.class}:#{obj.id}"
    end
  
    def set_unique_name
      return @unique_name if @unique_name
      @unique_name = unique_name_for(@dom_obj) if unique_name_for(@dom_obj)
      @unique_name ||= unique_name_for(@parent&.dom_obj) if unique_name_for(@parent&.dom_obj)
      raise unless @unique_name # FIXME raise better than this...
      @unique_name
    end

    def set_tree_view_display_name
      obj = self.dom_obj
      dn  = obj.display_name if obj.respond_to?(:display_name)
      dn ||= obj.name if obj.respond_to?(:name)
      if dn.nil? && self.dom_obj.is_a?(ORM_Instance)
        dn = self.dom_obj.class.to_s
      end
      dn ||= obj.to_s
      tree_view_options = Gui.option(:tree_view)
      show_labels = if tree_view_options.is_a? Hash
        Gui.option(:tree_view)[:show_roles]
      else
        false
      end
      dn = "#{self.label} - #{dn}" if show_labels && self.label
      dn = dn[0..MAX_NAME_LENGTH]
      dn = obj.class.name.demodulize if dn == ''
      dn
    end

    def add_child(child_node)
      # puts "  Adding child_node: #{child_node.dom_obj.class}--#{child_node.unique_name} to #{self.dom_obj.class}--#{self.unique_name}"
      @children << child_node
      child_node.parent = self
    end

    def to_href(host, protocol)
      # raise
      set_unique_name
      path = unique_name.gsub(/:+/, "/")
      path = [host,path].join("/")
      path = protocol + "//" + path
      # puts "to_href for #{self.dom_obj.class} -- #{self.unique_name} -- #{path}"
      # self.children.each do |c|; puts "     #{c.dom_obj.class} -- #{c.unique_name}";end
      # puts
      path
    end
  
    def classifier_string
      " data-classifier = '#{@classifier}'"
    end
  
    def id_string
      " data-id = '#{@id}'"
    end
  end
end
