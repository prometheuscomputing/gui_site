module Gui
  def self.current_url(domain_obj, args = {})
    create_url_from_object(domain_obj, args)
  end
  
  # FIXME will not work for AdHocPageObject
  def self.create_url_from_object(obj, args = {})
    url = ""
    if obj.nil? || obj.class == Gui::Home
      url = Gui.option(:home_page)[:url]
    else
      url = '/' + Gui.route_for(obj.class)
      id = (obj.id.nil? || (obj.id.is_a?(String) && obj.id.empty?)) ? 'new' : obj.id
      url += "/#{id}/"
    end
    
    params = []
    reduce_url_options(args, obj.class)
    args.each do |key, value|
      params << "#{key}=#{value}"
    end
    
    param_string = params.join("&")
    
    unless param_string.empty?
      url = url + "?" + param_string
    end
    return url
  end
  
  def self.create_image_url(richtext, filename)
    "#{richtext}/images/#{filename}"
  end
  
  # TODO: this should be rewritten as NavigationNode#to_url -SD
  def self.create_url_from_breadcrumb(node)
    return node.url if node.is_a?(Gui::UrlNavigationNode)
    url = '/'
    unless node.domain_obj_class ==  Gui.loaded_spec.default_classifier.to_s      
      url << "#{Gui.route_for(node.package)}/" if node.package && !node.package.empty? && node.package != 'Object'
      url << "#{Gui.route_for(node.domain_obj_class)}/" 
      url << "#{node.domain_obj_id}/"
    end
    
    options = node.view_hash
    self.reduce_url_options(options, node.domain_obj_class)
    
    if options.any?
      url << '?'
      param_strings = []
      options.each do |param, value|
        param_strings << "#{param}=#{value}"
      end
      url << param_strings.join('&')
    end
    url
  end
  
  def self.create_history_url_from_breadcrumb(node)
    url = '/change_tracker/history/'
    url << node.package + '/'
    url << node.domain_obj_class + '/'
    url << node.domain_obj_id + '/'
  end
  
  def self.create_clone_url_from_breadcrumb(node)
    url = '/clone_popup/'
    url << node.package + '/'
    url << node.domain_obj_class + '/'
    url << node.domain_obj_id + '/'
  end
  
  def self.create_breadcrumb_from_url(url, obj = nil)
    base_url, _separator, params_string = url.partition('?')
    base_url.slice!(/https?:\/\/.*?(?=\/)/)
    base_url_parts = base_url.split('/').reject(&:empty?)
    # TODO: make better recognition system for url parts. This is not entirely accurate
    package = 'Object'
    domain_obj_class = Gui.loaded_spec.default_classifier.to_s
    if base_url_parts.any?
      domain_obj_id = if base_url_parts.last == 'new' || base_url_parts.last =~ /\d+/
        base_url_parts.pop
      else #default behavior
        'new'
      end
      domain_obj_class = base_url_parts.join('::')
    end
    
    # If domain_obj_class is not valid, create a literal url navigation node instead
    klass = domain_obj_class.to_const rescue nil
    return Gui::UrlNavigationNode.new(base_url) unless klass
    
    params = {}
    if params_string
      params_string.split('&').each do |param_string|
        key, value = param_string.split('=')
        params[key] = value
      end
    end
    Gui::NavigationNode.new(package, domain_obj_class, domain_obj_id, obj, params)
  end
    
  
  def self.create_url_from_hash(type, id, args = {})
    url = "/"
    unless type == Gui.loaded_spec.default_classifier.to_s
      url += "#{Gui.route_for(type)}/"
      url += "#{id}/" if id
    end
    
    
    params = []
    reduce_url_options(args, type)
    args.each do |key, value|
      params << "#{key}=#{value}"
    end
    
    param_string = params.join("&")
    
    unless param_string.empty?
      url = url + "?" + param_string
    end
    
    return url
  end

  def self.reduce_url_options options, object_type
    options.delete('view-type') if options['view-type'] == Gui.loaded_spec.default_view_type
    options.delete('view-name') if options['view-name'] == Gui.loaded_spec.default_view_name
    options.delete('view-classifier') if options['view-classifier'].to_s == object_type.to_s
  end
end
