class Gui::Controller::FormParser

  def parse_delete_associations(_domain_obj, _getter, widget_info, _assoc_info, domain_obj_cache = {})
    delete_assocs = []
    concurrency_issues = {}
    delete_assoc_params = widget_info['pending_association_deletions']
    delete_assoc_params.each do |dap|
      assoc_obj   = domain_obj_from_assoc_params(dap['classifier'], dap['identifier'], nil, domain_obj_cache)
      through_obj = domain_obj_from_assoc_params(dap['through_classifier'], dap['through_identifier'], nil, domain_obj_cache)
      # Might be missing 'to' object (far object) and have only the join. Try to delete the far obj, else try the 'through' obj
      delete_obj = assoc_obj || through_obj
      delete_obj.destroy
      delete_assocs << assoc_obj
    end
    {:delete_assocs => delete_assocs, :concurrency_issues => concurrency_issues}
  end

  # Parse additions to associations from the form
  def parse_new_associations(domain_obj, getter, widget_info, _assoc_info, domain_obj_cache = {})
    new_assocs = []
    concurrent_changes = {}
    concurrency_issues = {}
    new_assoc_params = widget_info['pending_association_additions']
    new_assoc_objs = []
    new_assoc_params.each do |nap|
      next unless nap.keys.any?
      # Flag for skipping association to add. Used for pre-populated objects, as they are already added.
      next if nap['skip_association'] && nap['skip_association'] == 'true'
      new_obj = domain_obj_from_assoc_params(nap['classifier'], nap['identifier'], nil, domain_obj_cache)
      if new_obj.nil?
        concurrent_changes[nap] = {:error => 'deleted'} if Gui.option(:enable_concurrency)
      else
        new_assoc_objs << new_obj
      end
    end

    if concurrent_changes.any?
      concurrency_issues[getter] = {}
      concurrency_issues[getter][:action]     = 'adding_associations'
      concurrency_issues[getter][:collection] = concurrent_changes
      concurrency_issues[getter][:label]      = widget_info['label']
    end

    new_assoc_objs.each do |assoc_obj|
      domain_obj.send(getter + '_add', assoc_obj)
      new_assocs << assoc_obj
    end
    [new_assocs, concurrency_issues]
  end


  # Parse removals from associations from the form
  def parse_breaking_associations(domain_obj, getter, widget_info, assoc_info, domain_obj_cache = {})
    broken_assocs = []
    break_assoc_params = widget_info['pending_association_removals']
    if assoc_info[:type].match?(/to_many$/)
      break_assoc_params.each do |bap|
        # Will skip objects that were deleted concurrently
        assoc_obj   = domain_obj_from_assoc_params(bap['classifier'], bap['identifier'], nil, domain_obj_cache)
        through_obj = domain_obj_from_assoc_params(bap['through_classifier'], bap['through_identifier'], nil, domain_obj_cache)
        next unless assoc_obj || through_obj
        # TODO: Figure out why associations with a through object need to be treated differently. In ceramics, this caused a problem where the association remover hooks were not called.
        if assoc_info[:through]
          through_obj.destroy
        else
          domain_obj.send(getter + '_remove', assoc_obj)
        end
        broken_assocs << assoc_obj
      end
    else # to_one association here
      domain_obj.send(getter + '=', nil)
    end
    broken_assocs
  end

  # Parse reordering of collections from the form
  # Will only create a concurrency issue if the item being reordered has been deleted or has had accociation broken.
  def parse_ordering_change(domain_obj, getter, widget_info, assoc_info)
    reordered_objs = []
    position_getter = (assoc_info[:singular_opp_getter] || assoc_info[:opp_getter]).to_s + '_position'
    
    many_to_many = assoc_info[:type] == :many_to_many
  
    # Get new item/position mapping from structure
    order_associated_params = widget_info['order']
    
    return [[],{}] if order_associated_params.nil? || order_associated_params.empty?

    new_item_positions = {}
    order_associated_params.each do |oap|
      # Try to order the 'through' objects if they exist, else order the objects themselves.
      item = domain_obj_from_assoc_params(oap['through_classifier'], oap['through_identifier'])
      item = domain_obj_from_assoc_params(oap['classifier'], oap['identifier']) if item.nil?
      next unless item
      new_item_positions[item] = oap['position'].to_i - 1
    end

    # Get old item/position mappings
    # If only reassigning ID's that were submitted in the form (typical case), then
    # do not pull all position data from database
    # Otherwise, pull all positions for all items in the collection and reorder appropriately
    old_item_positions = {}
    new_positions = new_item_positions.values
    old_positions = new_item_positions.map { |item, _| item.send(position_getter) }
  
    simple_reorder = (new_positions.sort == old_positions.compact.sort)
    items = if simple_reorder # Only reordering currently submitted items (no need to pull all items from DB)
      new_item_positions.map { |item, _| item }
    else # Find (and possibly reorder) *all* current item positions in DB
      many_to_many ? domain_obj.send(getter.to_s + '_associations').map { |h| h[:through] } : domain_obj.send(getter)
    end
    # Define mapping of items to their old positions
    
    items.each { |item| old_item_positions[item] = item.send(position_getter) }
  
    if simple_reorder
      sorted_item_positions = new_item_positions
    else
      # Merge new item/position pairings with old pairings from DB
      new_item_positions = old_item_positions.merge(new_item_positions)
      # Sort items by new position into an array
      sorted_items = new_item_positions.sort_by { |_, position| position.nil? ? 1 : position }.map(&:first)
      sorted_item_positions = {}
      sorted_items.each_with_index { |item, index| sorted_item_positions[item] = index }
    end
    
    sorted_item_positions.each_with_index do |(item, sorted_position), index|
      old_position = old_item_positions[item]
      next if sorted_position == old_position
      item.send(position_getter + '=', sorted_position.nil? ? index : sorted_position)
      item.save
      reordered_objs << item
    end
    
    [reordered_objs, {}]
  end
  
end