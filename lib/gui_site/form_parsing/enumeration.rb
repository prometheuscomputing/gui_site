class Gui::Controller::FormParser

  def parse_enumeration_change(domain_obj, getter, enum_info, enum_assocs_info)
    # No need for policy here but still want to create a single portal for all domain_obj send TODO FIXME
    enum_types = domain_obj.send(getter + "_type")
    raise "Enumeration type for #{getter} can't be determined" if enum_types.empty?
    associated_literals = []
    enum_types.each { |et| associated_literals += et.where(:value => enum_info["data"]["value"]).all }
    # Note: Ignoring the possibility that the Enumeration Literal has been deleted while this addition is being processed.  Liklihood is low that this will happen.  TODO -- handle this corner case anyway when you have time.
    # TODO SSA setter handles string or obj
    # TODO pass around strings
    new_value = enum_assocs_info[getter.to_sym][:type] == :many_to_many ? associated_literals : associated_literals.first
    # widget = Gui::Widgets.get_widget(enum_info['widget'], enum_info['label'])
    # raise "Couldn't find widget for #{enum_info['widget']} (labeled: #{enum_info['label']})" unless widget
    # return [[],nil] if enum_info['data'].empty? # Exit out if not in widget_structure
    # is_changed = widget.check_attribute_concurrency(domain_obj, getter, new_value, enum_info["initial_data"]["value"], nil, nil)# , {delete: delete_attr, restore: restore_attr})
    # puts "The value for #{getter} has changed? #{!!is_changed}"
    # if is_changed
    # Policy here! FIXME
    domain_obj.send(getter.to_s + '=', new_value)
    [associated_literals, enum_info['label']]
    # else
    #   [[],nil]
    # end
  end
  
end
