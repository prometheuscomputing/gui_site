class Gui::Controller::FormParser
  IGNORED_WIDGETS = []

=begin This method is never used.  Leaving it here for reference.
  def process_pending_assoc_content(content_item, answer = nil)
    if content_item['pending_association_additions'] && !content_item['pending_association_additions'].empty?
      answer ||= {}
      answer[:pending_association_additions] ||= []
      answer[:pending_association_additions] += content_item['pending_association_additions']
    end
    if content_item['pending_association_removals'] && !content_item['pending_association_removals'].empty?
      answer ||= {}
      answer[:pending_association_removals] ||= []
      answer[:pending_association_removals] += content_item['pending_association_removals']
    end
    if content_item['pending_association_deletions'] && !content_item['pending_association_deletions'].empty?
      answer ||= {}
      answer[:pending_association_deletions] ||= []
      answer[:pending_association_deletions] += content_item['pending_association_deletions']
    end
    # Hmmmmm.  This should only be necessary if there are multiple widgets on the page that allow the manipulation of this association.  Do we need to handle that? Also, the above could just be  = instead of += if the assoc can only be manipulated by a single widget.  It gets complicated if there are multiple widgets because we have to way to know the order of the operations that occurred.  FIXME -- same is true for collections...what about doc_assocs?
    # answer[:pending_association_additions].uniq!
    # answer[:pending_association_removals].uniq!
    # answer[:pending_association_deletions].uniq!
    answer
  end
=end

  # We are creating new objects and adding and removing associations in here.  We are also collating all attribute changes and grouping them by changed object.
  # Params:
  #  new_objects - a hash with obj_key keys of objects newly created by applying form changes. It does not seem to include all newly created objects (such as association classes).
  #  inherited_getter - In some cases (contexts), a nil getter is specified, and the getter is inherited from the containing widget
  def organize_content(content_array, domain_obj, domain_obj_key, data, new_objects, destroyed_objects = [], inherited_getter = nil, through_obj = nil, through_obj_key = nil)
    content_array.each do |content_item|
      # Define the 'parent' of this content_item
      # Switch on 'association_class_widget' to determine whether the parent context is domain_obj or through_obj

      is_assoc_class = content_item['association_class_widget']
      parent_obj = is_assoc_class ? through_obj : domain_obj
      unless parent_obj
        puts "!" * 44
        puts "Warning: Found nil parent_obj for content_item: #{content_item.inspect}"
        puts "No changes for this content will be parsed."
        puts "!" * 44
        next
      end
      parent_obj_key = is_assoc_class ? through_obj_key : domain_obj_key
      # Define the 'content_obj' (and possibly 'content_through_obj') that this content_item represents
      content_obj_id = content_item['data_id']
      content_obj_classifier = content_item['data_classifier']
      content_through_obj_id = content_item['through_id']
      content_through_obj_classifier = content_item['through_classifier']
      # Define the content_getter. This is how the content_obj relates to the parent_obj.
      # When the content_item's getter is not specified (or empty) then the parent's getter is used.
      # This occurs for contexts without specified getters, where the context is being used as a container only (for example to apply a CSS style).
      # TODO: see if this can be avoided
      content_getter = content_item['getter'] if content_item['getter'] && !content_item['getter'].empty?
      
      # Does this content_item itself have content (view vs simple widgets)
      if content_item['content']
        # For style widgets we allow a 'nil' getter to be specified, meaning that the style inherits the parent's context
        # We can in future open this up to allow all types of widgets to specifiy a nil getter and thus inherit the parent context,
        # provided we develop a mechanism for specifying which widgets are allowed to do this (without trusting the form data here) and under what conditions it happens
        # For form parsing, this means we need to set the getter, obj, obj_key, through_obj, through_obj_key to the parent's
        if content_getter.nil? && (content_item['widget'] == 'context' || content_item['widget'] == 'section')
          content_getter = inherited_getter
          content_obj = parent_obj
          content_obj_key = parent_obj_key
          new_obj_created = false
          content_through_obj = through_obj
          content_through_obj_key = through_obj_key
        else
          content_getter ||= inherited_getter # if content_item['widget'] == 'context'
          # Define content_obj, associated key, and new creation boolean from form strings
          content_obj, content_obj_key, new_obj_created = domain_obj_from_form_strings(content_obj_id, content_obj_classifier, new_objects)
          # Define content_through_obj, associated key, and new creation boolean from form strings
          content_through_obj, content_through_obj_key, _ = domain_obj_from_form_strings(content_through_obj_id, content_through_obj_classifier, new_objects)
        end
        
        # TODO: We need to define the expected behavior when the parent_obj is destroyed (or removed?) and there are additions/creations within its context.
        # Currently, we continue to process them unless the parent_obj was destroyed, and the addition is a composition.
        next if new_obj_created && destroyed_objects.include?(parent_obj) && parent_obj.class.compositions.map { |info| info[:getter].to_s }.include?(content_getter)
        
        # Add content_obj to parent_obj if add_association is specified, or this is a context with a getter and an object was created for the context (auto_create)
        # if content_item['add_association'] || ((content_item['widget'] == 'context' ) && new_obj_created && content_getter)
        if content_item['add_association'] || (new_obj_created && content_getter)
          raise "Error: no getter for relationship between #{parent_obj.class} and #{content_obj.class} through #{content_through_obj.class} (inherited_getter: #{inherited_getter.inspect})" unless content_getter
          add_content_obj_to_parent_obj(parent_obj, content_getter, content_obj, content_through_obj)
        end
        case content_item['widget']
        when 'context'
          organize_content(content_item['content'], content_obj, content_obj_key, data, new_objects, destroyed_objects, content_getter || inherited_getter, content_through_obj, content_through_obj_key)
        when 'organizer'
          data[parent_obj_key] ||= {:domain_obj => parent_obj, :attributes => {}}
          data[parent_obj_key][:attributes][content_getter] = content_item.reject { |k,_| k == 'content' }
          organize_content(content_item['content'], content_obj, content_obj_key, data, new_objects, destroyed_objects, content_getter || inherited_getter, content_through_obj, content_through_obj_key)
        when 'collection'
          data[parent_obj_key] ||= {:domain_obj => parent_obj, :attributes => {}}
          data[parent_obj_key][:attributes][content_getter] = content_item
        else
          message = "There is currently no way to handle content found in this:\n"
          PP.pp(content_item, message)
          raise message
        end
      else # it has no content so it should be a leaf widget, which probably means it is a attribute expressed in a SimpleWidget
        next unless content_item['mutable']
        next if IGNORED_WIDGETS.include?(content_item['widget'])
        next unless content_getter && !content_getter.empty? # examples of when this happens: html widget, an image widget that is pulling the image from a url
        data[parent_obj_key] ||= {:domain_obj => parent_obj, :attributes => {}}
        next if data[parent_obj_key][:attributes][content_getter] # skip it if we've seen it before (as might happen on a clearview page)
        data[parent_obj_key][:attributes][content_getter] = content_item
      end
    end
    {:data => data, :destroyed_objects => destroyed_objects}
  end
  
  def domain_obj_from_form_strings(obj_id, obj_classifier, new_objects = {})
    newly_created = false 
    if positive_integer?(obj_id) # then it is a valid id for an existing object
      obj = obj_classifier.to_const[obj_id.to_i]
      obj_key = "#{obj_classifier}_#{obj_id}"
    elsif obj_id && !obj_id.to_s.empty?
      # there could be many things here but right now they only thing implemented is that it is a uid placeholder for an as yet uncreated object
      # new_objects is a hash where the keys are the new_object placeholder uids from the page and the values are the newly created objects
      obj_key = "#{obj_classifier}_#{obj_id}"
      obj = new_objects[obj_key]
      unless obj
        obj = obj_classifier.to_const.new
        obj.save
        newly_created = true
        # TODO: obj_id should never be 'new', but should always have a unique identifier (e.g. a uuid or 'new_1')
        # Note: skipping this step when obj_id == 'new' could cause extra objects to be created in some cases.
        new_objects[obj_key] = obj unless obj_id == 'new'
      end
    else
      # No content_obj defined, so use parent_obj as context
      obj = nil
      obj_key = nil
    end
    [obj, obj_key, newly_created]
  end

  # Apply attribute and association changes from the form
  # Arguments:
  # * domain_obj        -  The root domain object upon which the page was based.  This may
  #                        not be the only domain object for which changes will be applied.
  # * page_state        -  provides the structure of the widget data available in params.
  # * domain_obj_cache  -  A cache of (previously modified) domain objects.
  #                        Using this prevents previous unsaved modifications from being overwritten
  #                        by a change specified via the form.
  def apply_form_changes(domain_obj, page_state, domain_obj_cache = {})
    page_content = page_state.first['content'] # Ignore all but the first top level
    root_obj_key = "#{domain_obj.class}_#{page_state.first['data_id']}"
    # add the root obj to the new_objects hash if the root domain_obj is new
    new_objects    = domain_obj.new? ? {root_obj_key => domain_obj} : {}
    sorted_content = organize_content(page_content, domain_obj, root_obj_key, {}, new_objects)
    # TODO this is in here just to try it out.  Really, the browser shouldn't even be submitting unchanged values.  It's real value is in making things more readable for debugging.  See the method definition for #get_changes_from_content.
    content_with_changes = get_changes_from_content(sorted_content)
    domain_objects_with_changes = content_with_changes[:data].values # results in an array of hashes,eachwith two keys, :domain_obj and :attributes
    # domain_objects_with_changes = sorted_changes[:data].values # results in an array of hashes, each with two keys, :domain_obj and :attributes
    domain_objects_with_changes = domain_objects_with_changes.reject do |item|
      sorted_content[:destroyed_objects].include?(item[:domain_obj])
    end
    # Can't do this *yet* because the widgets first need to provide the proper values that the models can use to validate against
    # domain_objs_to_validate = []
    # domain_objects_with_changes.each { |item| domain_objs_to_validate << {:domain_obj => item[:domain_obj], :changes =>get_cast_values(item[:domain_obj], item[:attributes])} } # FIXME clean up unused stuff here after working it out
    # problems = []
    # domain_objs_to_validate.each do |item|
    #   validation_result = item[:domain_obj].class.validate_changes(item[:changes])
    #   problems << validation_result if validation_result[:invalid] && validation_result[:invalid].any?
    # end
    result = []
    domain_objects_with_changes.each do |item|
      changes = apply_changes_to(item[:domain_obj], item[:attributes], domain_obj_cache)
      result << {:domain_obj => item[:domain_obj], :changes => changes}
    end
    result
  end

  # This method probably doesn't make things any faster or more efficient.  It really just results in more readable output for debugging.  Hence the fact that we are normally just returning the changes without any processing.
  def get_changes_from_content(changes)
    # changes[:data].each_value do |data| 
    #   data[:attributes] = data[:attributes].reject do |_, attr_data|
    #     # attributes
    #     (attr_data["data"] == attr_data["initial_data"]) ||
    #     # associations
    #     (attr_data["data"] == {"value"=>nil} && attr_data["initial_data"] == {}) && !has_pending_assocs?(attr_data) ||
    #     # files
    #     (attr_data["data"] == {"last_update"=>"", "filename"=>nil} && attr_data["initial_data"] == {"last_update"=>"", "filename"=>""})
    #   end
    # end
    changes
  end
  
  def has_pending_assocs?(data)
    (data['pending_association_additions'] && data['pending_association_additions'].any?) ||
    (data['pending_association_removals']  && data['pending_association_removals'].any?)  ||
    (data['pending_association_deletions'] && data['pending_association_deletions'].any?)
  end
  private :has_pending_assocs?
  
=begin leaving this in here for possibly finishing and using later  -- MF 11-16-2016
  def get_cast_values(domain_obj, changes)
    attrs         = domain_obj.class.attributes
    assocs        = domain_obj.class.associations
    enums         = assocs.select { |k,v| v[:enumeration] }
    # Note that we aren't removing the enums from the rest of the associations.  This means that we need to check if a change is to an enum property before we move on to checking to see whether it is an association.
    attr_getters  = attrs.map { |k,v| v[:getter] }
    assoc_getters = assocs.map { |k,v| v[:getter] }
    enum_getters  = enums.map { |k,v| v[:getter] }
    result = {}
    changes.each do |getter, data|
      if attr_getters.include? getter.to_sym

        # Apply changes to domain_obj, and return any errors and/or updated fields
        result[getter.to_sym] = formatted_current_value(data)
      end
    end
    # pp result if result.any?
    result
      # # Enumerations need special handling because they are associations that are treated like attributes on the page.
      # elsif enum_getters.include? getter.to_sym
      #
      #   # this assumes that concurrency checking is not concerned with anything that the user for didn't actively change on the page.  perhaps we should warn if there is a concurrent change?  we already warn for files.
      #   next if data["data"] == data["initial_data"]
      #   result = get_cast_value_for_enumeration_change(domain_obj, getter, data, enums)
      #
      # elsif assoc_getters.include? getter.to_sym
      #   assoc_info = assocs.find { |k,v| v[:getter] == getter.to_sym }
      #   assoc_info = assoc_info.last # it better be here, else how did we enter this block?
      #
      #   # If it came from advanced view it will be a hash.  If not, it came from clearview, which isn't currently using pending_associations
      #   if data.is_a? Hash
      #     # Parse breaking associations
      #     if data['pending_association_removals'] && data['pending_association_removals'].any?
      #       temp_assocs = parse_breaking_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
      #       broken_assocs += temp_assocs if temp_assocs.any?
      #     end
      #
      #     # Parse new associations
      #     if data['pending_association_additions'] && data['pending_association_additions'].any?
      #       temp_assocs, temp_concurrents = parse_new_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
      #       new_assocs += temp_assocs if temp_assocs.any?
      #       # Use the following returned concurrencies hash when we have only ajax submitting the association changes
      #       # concurrency_issues['new_assocs'] ||= {}
      #       # concurrency_issues['new_assocs'].merge!(temp_concurrents) if temp_concurrents.any?
      #     end
      #
      #     # Parse deletions
      #     if data['pending_association_deletions'] && data['pending_association_deletions'].any?
      #       result = parse_delete_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
      #       # NOTE: nothing is done here with result[:concurrency_issues]
      #       deleted_assocs += result[:delete_assocs] if result[:delete_assocs] && result[:delete_assocs].any?
      #     end
      #
      #     # Parse ordering changes - Must be run every time for ordered collections (e.g. when objects are deleted, added, or broken).
      #     if data['widget'] == 'collection' && assoc_info[:ordered]
      #       temp_assocs, temp_concurrents = parse_ordering_change(domain_obj, getter, data, assoc_info)
      #       reordered_assocs << temp_assocs if temp_assocs.any?
      #     end
      #   end
      # else
      #   pp data
      #   raise "No getter #{getter} for #{domain_obj}"
      # end
    # end

    # domain_obj.save if domain_obj # Save any attribute changes
    # return {
    #   :updated_fields     => updated_fields,
    #   :new_assocs         => new_assocs,
    #   :broken_assocs      => broken_assocs,
    #   :errors             => errors,
    #   :concurrency_issues => concurrency_issues,
    #   :deleted_assocs     => deleted_assocs,
    #   :uniqueness_errors  => uniqueness_errors
    # }
  end
=end
  
  def apply_changes_to(domain_obj, changes, domain_obj_cache)
    attrs         = domain_obj.class.attributes
    assocs        = domain_obj.class.associations.reject { |_,v| v[:primitive] }
    prim_assocs   = domain_obj.class.associations.select { |_,v| v[:primitive] }
    enums         = assocs.select { |_,v| v[:enumeration] }
    
    # Note that we aren't removing the enums from the rest of the associations.  This means that we need to check if a change is to an enum property before we move on to checking to see whether it is an association.
    attr_getters  = attrs.map { |_,v| v[:getter] }
    assoc_getters = assocs.map { |_,v| v[:getter] }
    prim_assoc_getters = prim_assocs.map { |_,v| v[:getter] }
    enum_getters  = enums.map { |_,v| v[:getter] }
    domain_obj_cache[[domain_obj.id.to_s, domain_obj.class.to_s]] = domain_obj
    domain_obj.save # Why?

    updated_fields     = []
    new_assocs         = []
    broken_assocs      = []
    deleted_assocs     = []
    reordered_assocs   = []
    uniqueness_errors  = []
    errors             = []
    concurrency_issues = {}
    warnings           = []
    changes.each do |getter, data|
      if attr_getters.include?(getter.to_sym)
        # Apply changes to domain_obj, and return any errors and/or updated fields
        attr_result       = parse_attribute_change(domain_obj, getter, data)
        errors            += attr_result[:errors]            if attr_result[:errors]
        updated_fields    += attr_result[:updated]           if attr_result[:updated]
        uniqueness_errors += attr_result[:uniqueness_errors] if attr_result[:uniqueness_errors]
        if attr_result[:concurrency_issues]
          concurrency_issues['attributes'] ||= {}
          concurrency_issues['attributes'].merge!(attr_result[:concurrency_issues])
        end
        if attr_result[:concurrency_warnings]
          concurrency_issues['warnings'] ||= []
          concurrency_issues['warnings'] += attr_result[:concurrency_warnings]
        end
        if attr_result[:concurrency_messages]
          concurrency_issues['messages'] ||= []
          concurrency_issues['messages'] += attr_result[:concurrency_messages]
        end
        if attr_result[:concurrency_warnings]
          warnings << attr_result[:concurrency_warnings]
        end
        # + whatever other warnings we come up with

      # Enumerations need special handling because they are associations that are treated like attributes on the page.
      elsif enum_getters.include?(getter.to_sym)

        # this assumes that concurrency checking is not concerned with anything that the user for didn't actively change on the page.  perhaps we should warn if there is a concurrent change?  we already warn for files.
        next if data['data'] == data['initial_data']

        # Apply changes to domain_obj, and return any errors and/or updated fields
        enum_assocs, updated_field = parse_enumeration_change(domain_obj, getter, data, enums)
        new_assocs += enum_assocs
        updated_fields << updated_field if updated_field
        
      elsif prim_assoc_getters.include?(getter.to_sym)
        next if data['data'] == data['initial_data']
        # TODO create reasonable widgets for attributes that are arrays (i.e. sets) of primitive values.  Here we have just a quick and dirty implementation to be able to handle arrays of primitives using the existing widgets (as of August 2019).  If a property is typed as a primitive type and has a multiplicity greater than one then the underlying persistance implementation is a to-many association to the primitive type.  We aren't really going to treat it totally like other associations at this point though.  At this point, we aren't supporting any pending associations for these things (but hey, neither does clearview...)
        new_primitive_assocs, updated_field = parse_multiprimitive_change(domain_obj, getter, data)#, prim_assocs[getter.to_sym])
        new_assocs += new_primitive_assocs
        updated_fields << updated_field if updated_field
        
      elsif assoc_getters.include?(getter.to_sym)
        assoc_info = assocs.values.find { |v| v[:getter] == getter.to_sym }
        # assoc_info = assoc_info.last # it better be here, else how did we enter this block?
          
        # If it came from advanced view it will be a hash.  If not, it came from clearview, which isn't currently using pending_associations
        if data.is_a?(Hash)
          
          # Parse breaking associations
          if data['pending_association_removals'] && data['pending_association_removals'].any?
            temp_assocs = parse_breaking_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
            broken_assocs += temp_assocs if temp_assocs.any?
          end

          # Parse new associations
          if data['pending_association_additions'] && data['pending_association_additions'].any?
            temp_assocs, _ = parse_new_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
            new_assocs += temp_assocs if temp_assocs.any?
            # Use the following returned concurrencies hash when we have only ajax submitting the association changes
            # concurrency_issues['new_assocs'] ||= {}
            # concurrency_issues['new_assocs'].merge!(temp_concurrents) if temp_concurrents.any?
          end

          # Parse deletions
          if data['pending_association_deletions'] && data['pending_association_deletions'].any?
            result = parse_delete_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
            # NOTE: nothing is done here with result[:concurrency_issues]
            deleted_assocs += result[:delete_assocs] if result[:delete_assocs] && result[:delete_assocs].any?
          end

          # Parse ordering changes - Must be run every time for ordered collections (e.g. when objects are deleted, added, or broken).
          if data['widget'] == 'collection' && assoc_info[:ordered]
            temp_assocs, _ = parse_ordering_change(domain_obj, getter, data, assoc_info)
            reordered_assocs << temp_assocs if temp_assocs.any?
          end
        end
      else
        STDERR.puts data.pretty_inspect
        puts "assocs: #{assoc_getters.sort}"
        puts "attrs: #{attr_getters.sort}"
        puts "primitive associations (i.e. multi-primitives): #{prim_assoc_getters.sort}" if prim_assoc_getters.any?
        STDERR.puts assocs.pretty_inspect
        raise "No getter '#{getter}' for #{domain_obj.class}"
      end
    end
    # FIXME policy here!
    domain_obj.save if domain_obj # Save any attribute changes
    return {
      :updated_fields     => updated_fields,
      :new_assocs         => new_assocs,
      :broken_assocs      => broken_assocs,
      :errors             => errors,
      :concurrency_issues => concurrency_issues,
      :deleted_assocs     => deleted_assocs,
      :uniqueness_errors  => uniqueness_errors,
      :warnings           => warnings.flatten.compact.uniq
    }
  end
  
  # def cast_wigdet_values_for(changes)
  #   puts_blue "Cast Changes"
  #   pp changes
  #   cast_values = []
  #   changes.each { |obj_with_changes| cast_values << {:domain_obj => obj_with_changes[:domain_obj], :attributes => get_cast_values(obj_with_changes[:domain_obj], obj_with_changes[:attributes])} }
  #   puts_blue "*************************"
  #   pp cast_values
  #   puts_blue "*************************"
  #   cast_values
  # end

  def attribute_info_for(getter, klass)
    getter_path = getter.split('.')
    attribute_getter = getter_path.pop
    return nil unless attribute_getter
    getter_path.each do |g|
      info = klass.associations[g.to_sym]
      klass = info[:class].to_const
    end
    klass.attributes[attribute_getter.to_sym]
  end

  def remove_obj_from_cache!(domain_obj, domain_obj_cache = {})
    domain_obj_cache[[domain_obj.id.to_s, domain_obj.class.to_s]] = nil
  end

  private
  def positive_integer? input
    val = Integer(input)
    val > 0
  rescue ArgumentError, TypeError
    false
  end
  
  # Given a parent_obj, a getter, a content_obj, and potentially a through_obj, adds the content_obj to the parent_obj (using the through_obj if applicable)
  # Adds newly created through objects to the new_objects hash
  def add_content_obj_to_parent_obj(parent_obj, getter, content_obj, through_obj = nil)
    property = parent_obj.class.properties[getter.to_sym]
    # For alias associations, use the base property information
    # This could potentially cause unexpected behavior, but there seems to be an issue using :singular_opp_getter otherwise
    # TODO: Fix problem with alias association info instead of using base property
    property = parent_obj.class.properties[property[:alias_of]] if property[:alias_of]
    raise "No such property #{getter} defined for #{parent_obj.class}. Could not add #{content_obj.inspect}" unless property
    case property[:type]
    when :one_to_one, :many_to_one
      if property[:through]
        raise "A through_obj must be passed for the #{parent_obj.class}.#{getter} association" unless through_obj
        parent_setter = "#{property[:singular_opp_getter]}="
        content_setter = "#{property[:getter]}="
        through_obj.send(parent_setter, parent_obj)
        through_obj.send(content_setter, content_obj)
      else
        parent_obj.send("#{getter}=", content_obj)
      end
      # parent_obj.send("#{getter}=", content_obj)
    when :one_to_many, :many_to_many
      if property[:through]
        raise "A through_obj must be passed for the #{parent_obj.class}.#{getter} association" unless through_obj
        parent_setter = "#{property[:singular_opp_getter]}="
        content_setter = "#{property[:singular_getter]}="
        through_obj.send(parent_setter, parent_obj)
        through_obj.send(content_setter, content_obj)
      else
        parent_obj.send("#{getter}_add", content_obj)
      end
    end
  end
end
