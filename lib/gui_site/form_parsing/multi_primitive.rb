class Gui::Controller::FormParser
  def parse_multiprimitive_change(domain_obj, getter, data)
    types = domain_obj.send(getter + "_type")
    raise "Primitive type for #{getter} can't be determined" if types.empty?
    raw_data_string = data['data']['value']
    widget_type = data['widget']
    parsed_values = case widget_type
    when 'integer'
      raw_values = raw_data_string.split(/\s/)
      raw_values.each do |rv|
        raise "Values for #{getter} must be integers.  User submitted the following values: #{raw_values.inspect}" unless rv =~ /^\d+$/
      end
      raw_values.map(&:to_i)
    when 'string'
      regex = /(")([^"\\]*(?:\\.[^"\\]*)*)\1/ # adapted from answer by ridgerunner found at https://stackoverflow.com/questions/5695240/php-regex-to-ignore-escaped-quotes-within-quotes
      # regex = /(["'])([^"\\]*(?:\\.[^"\\]*)*)\1/ # this one may work with either single or double quotes but it is not thorougly tested
      raw_data_string.scan(regex).map { |s| s[1].sub(/\\/) { '' } }
    else
      raise "Can't parse multi-primitives from an #{widget_type} widget."
    end
    domain_obj.send(getter.to_s + '=', parsed_values)
    [parsed_values, data['label']]
  end
end
