module Gui
  class Controller
    class FormParser
      # Parse form changes to an attribute
      # This work was moved into gui_director.  That same should probably be done for the other #parse_ methods.
      def parse_attribute_change(domain_obj, getter, widget_info)
        AttributeParser.new(domain_obj, getter, widget_info).parse
      end
  
      # TODO Strip away some or all of the information about the widget.  We really should not need it after this.
      def formatted_current_value(widget_info)
        widget_class = Gui::Widgets.get_widget_class(widget_info['widget'])
        answer = {:widget_data => widget_info} # get rid of me when you are sure you can safely do so
        answer[:submitted] = widget_class.format_submitted_data(widget_info['data'])['value']
        answer[:initial]   = widget_class.format_submitted_data(widget_info['initial_data'])['value']
        answer
      end
    end

    class AttributeParser
      attr_reader :domain_obj, :getter, :widget_info, :widget_class, :current_value
      def initialize(domain_obj, getter, widget_info)
        @domain_obj    = domain_obj # this is the domain object that we are parsing attributes for
        @getter        = getter
        @current_value = domain_obj.send(getter.to_sym) # Policy here? Not strictly necessary since this is never a user-facing value
        @widget_info   = widget_info
        @widget_class  = Gui::Widgets.get_widget_class(widget_info['widget'])
        raise "Couldn't find widget type for #{widget_info['widget']} (labeled: #{widget_info['label']})" unless widget_class
      end
      
      def parse
        # widget_info.keys: ["label", "widget", "type", "data", "initial_data", "initial_data_in_params", "id", "delete"]

        # Return if there is nothing about this attribute in the widget_structure
        return {} unless widget_info['data']&.any?

        opts = {
          :delete      => widget_info['delete'] == true, 
          :widget_info => widget_info, 
          :last_update => widget_info['last_update'], 
          :domain_obj  => domain_obj, 
          :getter      => getter,
          :label       => widget_info['label']
        }
        
        answer = {}
        
        answer.merge!(check_for_concurrent_changes(opts)) if Gui.option(:enable_concurrency)
        # puts Rainbow("#{domain_obj.class}.#{getter}\n#{answer.pretty_inspect}").magenta unless answer.empty?
        
        if opts[:delete]
          puts Rainbow("Delete #{domain_obj.class}.#{getter}").red if opts[:delete]
          set_attribute(nil) 
          return answer
        end
        
        submitted_data = format_submitted_data(widget_info['data'])

        # Exit out if no data was in params or if has been restored already
        empty_data = widget_class.empty_data?(submitted_data)
        if empty_data
          # puts Rainbow("#{domain_obj.class}.#{getter} had empty_data!").orange
          return answer
        else
          # FIXME? I think the only widget that can possibly indicate that it has empty data is File.  Should the others be able to?
          # puts Rainbow("#{domain_obj.class}.#{getter} had data --> #{submitted_data}").green
        end

        # Now we check to see if the value has changed and, if so, try to set it.
        unless answer[:errors] || answer[:concurrency_issues]
          begin
            potential_new_value = widget_class.cast_value_for_setting(submitted_data, opts)
          rescue Gui::WidgetValueError => e
            answer[:errors] ||= []
            answer[:errors] << e.message
            puts Rainbow("#{domain_obj.class}.#{getter} had errors!").orange
            return answer
          end
        
          # Check to see if the field has changed or not
          # Field hasn't changed if the new value is an empty string, it's how html treats nil inputs
          formatted_current_value = Gui::Utils.remove_carriage_returns(Gui::Utils.trim_leading_and_trailing_newlines(current_value))
          # puts Rainbow("#{domain_obj.class}.#{getter} - fcv:#{formatted_current_value.inspect}; pnv:#{potential_new_value.inspect}").green
          unless (formatted_current_value == nil && potential_new_value == '') || formatted_current_value.eql?(potential_new_value)
            begin
              # puts Rainbow("CHANGE #{domain_obj.class}.#{getter}").orange
              set_attribute(potential_new_value)
              answer[:updated] ||= []
              answer[:updated] << widget_info['label']
            rescue SpecificAssociations::NonUniqueWarning, SpecificAssociations::NonUniqueError => e
              # Ignore for now. Will raise error later if no concurrencies are found.
              answer[:uniqueness_errors] ||= []
              answer[:uniqueness_errors] << e
            end
          end
        else
          # TODO did someone intend to do something here??
          if answer[:concurrency_issues]
          end
          if answer[:errors]
          end
        end
                
        # TODO come up with something better than shoving the concurrency_warnings into the errors (suspect it is done this way because we know how to render :errors but not :concurrency_warnings)
        if answer[:concurrency_warnings]
          answer[:errors] ||=[]
          answer[:errors] += answer[:concurrency_warnings]
        end
        # puts 'return ' + Rainbow("#{domain_obj.class}.#{getter} --> #{answer}").orange unless answer.empty?
        answer
      end

      def check_for_concurrent_changes(opts)
        widget_class.check_concurrency(domain_obj:domain_obj, getter:getter, current_value:current_value, widget_info:widget_info, opts:opts)
      end
    
      def format_submitted_data(data)
        answer = {}
        data.each do |key, submitted_value|
          if v = format_submitted_value(submitted_value)
            answer[key] = v
          end
        end
        answer
      end
      
      def format_submitted_value(value)
        return nil unless value
        return value unless value.is_a?(String)
        # Encoding conversion only necessary when comparying variables to values in database. Maybe don't hardcode?
        Gui::Utils.remove_carriage_returns(Gui::Utils.trim_leading_and_trailing_newlines(value.force_encoding('utf-8')))
      end
  
      def set_attribute(new_value)
        begin
          # FIXME? Exception driven code.  Not ideal?
          # Policy here?
          domain_obj.send(getter + '=', new_value)
        rescue SpecificAssociations::NonUniqueWarning => e
          raise e
        rescue StandardError => e
          puts "A #{e.class} occurred while passing through AttributeParser#set_attribute:  domain object was a #{domain_obj.class.name}, getter => #{getter.inspect}. new_value => #{new_value.inspect}.  The error message was: #{e.message}"
          raise e
        end
      # FIXME Policy here? Or should we check a whole lot earlier than this? Depends on whether we want to implement business rules that actually restrict what values may be set based on policy or if we just want to enforce read / write
        domain_obj.save
      end

    end # inner class AttributeParser
  end # class Controller
end # module Gui