
require 'optparse'
require_relative 'launcher'

module Gui
  class CliParser
    attr_accessor :parsed_values
    
    def initialize
      @parsed_values = {}
    end
    
    def default_parser
      parser = OptionParser.new do |opts|
        # NOTE: do not specify a '-g' option. It is reserved for specifying an alternate Gui location
        opts.banner = "Usage: gui_builder [OPTIONS] spec_name [spec_title] [spec_module]"

        opts.on('--start', 'Starts the server. This is the default action.') do
          self.parsed_values[:action] = :start
        end

        opts.on('--stop', 'Stop the server.') do
          self.parsed_values[:action] = :stop
        end

        opts.on('--[no-]daemonize', 
                "Daemonize the process. default: #{Gui::DEFAULTS[:daemonize]}") do |v|
          self.parsed_values[:daemonize] = v
        end
        
        opts.on("-r", "--rackup MODEL_EXTENSIONS_FILE", "Use a custom rackup file") do |v|
          self.parsed_values[:rackup_file] = File.expand_path(v)
        end

        opts.on('--[no-]login', 
                "Require login (or not). default: #{Gui::DEFAULTS[:login]}") do |v|
          self.parsed_values[:login] = v
        end
        
        opts.on('--host ADDRESS', String, "Sets the host address to use e.g. 0.0.0.0") do |v|
          self.parsed_values[:connection_type] = 'port'
          self.parsed_values[:host] = v
        end
        
        opts.on('-p', '--port PORT', String, 
                "Port to use. Implies '--connection port' if set. (default: #{Gui::DEFAULTS[:port]})") do |v|
          self.parsed_values[:connection_type] = 'port'
          self.parsed_values[:port] = v
        end

        opts.on('-c', '--connection CONNECTION_TYPE', String, 
                "Connection type ('port' or 'socket') (default: #{Gui::DEFAULTS[:connection_type]})") do |v|
          self.parsed_values[:connection_type] = v
        end
        
        opts.on('-s', '--socket SOCKET', String, 
                "Socket to use. Implies '--connection socket' if set. (default: #{(Gui::DEFAULTS[:socket] || '/tmp/<spec_name>.sock')})") do |v|
          self.parsed_values[:connection_type] = 'socket'
          self.parsed_values[:socket] = v
        end
  
        opts.on('-m', '--mode MODE', String, 
                "Mode (default: #{Gui::DEFAULTS[:mode]})") do |v|
          self.parsed_values[:mode] = v.to_sym
        end
        
        opts.on('--testing MODE', String, 
                "Testing Mode (default: #{Gui::DEFAULTS[:testing]})") do |v|
          self.parsed_values[:testing_mode] = v
        end

        opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
          self.parsed_values[:verbose] = v
        end

        opts.on("-u", "--custom-spec SPEC_FILE", "Use a custom spec file or directory") do |v|
          self.parsed_values[:custom_spec] = File.expand_path(v)
        end

        opts.on("-e", "--model-extensions MODEL_EXTENSIONS_FILE", "Use a model extensions file") do |v|
          self.parsed_values[:model_extensions] = File.expand_path(v)
        end
        
        opts.on('-h', '--schema-extensions-dir SCHEMA_EXTENSIONS_DIR', 'Specify a Sequel Migrations dir to apply for schema extensions') do |v|
          self.parsed_values[:schema_extensions_dir] = File.expand_path(v)
        end
        
        opts.on("-b", "--database DB_LOCATION", "Specify a custom DB location") do |v|
          self.parsed_values[:db_location] = v && !v.empty? ? File.expand_path(v) : v
        end
        
        opts.on("--no-local-auth",
                "Disable local authentication") do
          self.parsed_values[:local_authentication] = false
        end
        
        opts.on("--web-auth",
                "Disable local authentication") do
          self.parsed_values[:web_authentication] = true
        end

        opts.on("--ssl",
                "Enable SSL") do
          self.parsed_values[:ssl] = true
        end

        opts.on("--ssl-key-file PATH", String,
                "Specify SSL key file") do |key_file|
          self.parsed_values[:ssl_key_file] = key_file
        end

        opts.on("--ssl-cert-file PATH", String,
                "Specify SSL certificate file") do |cert_file|
          self.parsed_values[:ssl_cert_file] = cert_file
        end

        opts.on("--ssl-verify",
                "Enable SSL verification") do
          self.parsed_values[:ssl_verify] = true
        end
        
        opts.on("--secure-cookie") do
          self.parsed_values[:secure_cookie] = true
        end
        
        opts.on("--concise_errors") do
          self.parsed_values[:concise_errors] = true
        end
        
        opts.on("--pid PID", String, "Location of PID file") do |pid|
          # Without File.expand_path(...) relative paths would be interpreted as relative to the Ramaze directory within gui_builder
          # Using File.expand_path(...), the paths are now relative to the user's current directory
          self.parsed_values[:pid] = File.expand_path(pid)
        end
        
        opts.on("--log LOG", String, "Location of Log file") do |log|
          # Without File.expand_path(...) relative paths would be interpreted as relative to the Ramaze directory within gui_builder
          # Using File.expand_path(...), the paths are now relative to the user's current directory
          self.parsed_values[:log] = File.expand_path(log)
        end
        
        opts.on("--log-dir LOG_DIR", String, "Location of Log directory") do |log_dir|
          # Without File.expand_path(...) relative paths would be interpreted as relative to the Ramaze directory within gui_builder
          # Using File.expand_path(...), the paths are now relative to the user's current directory
          self.parsed_values[:log_dir] = File.expand_path(log_dir)
        end
        
        opts.on("--[no-]docker", "Indicate that this application is intended to be launched within a Docker container") do |v|
          self.parsed_values[:docker] = v 
        end
        
        opts.on("--server SERVER", String, "Server type. One of thin, mizuno, or rackup (rackup is only for use by $EXTRA_ARGV)") do |server|
          self.parsed_values[:server] = server.to_sym
        end
        
        opts.on("--launch-localhost") do
          self.parsed_values[:launch_localhost] = true
        end
        
        opts.on("--help") do
          puts opts
          exit
        end
      end
      parser
    end
    
    
    # def self.parse_options
    #   cli_parser_instance = self.new
    #   
    #   cli_parser_instance.default_parser.parse!
    #   
    #   # Check for bad option combinations
    #   raise "Error: either local or web authentication must be enabled." if (parsed_values[:local_authentication] == false) && (parsed_values[:web_authentication] == false)
    #   raise "Error: Must also specify both --ssl-key-file and --ssl-cert-file if --ssl is specified" if parsed_values[:ssl] && !(parsed_values[:ssl_key_file] && parsed_values[:ssl_cert_file])
    #   
    #   cli_parser_instance.parsed_values
    # end
  end
end