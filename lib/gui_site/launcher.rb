require 'rainbow'
require 'fileutils'
require 'lodepath'
require 'gui_director'
require 'gui_spec'
require_relative 'cli_parser'
require_relative 'default_options'

WEB_ROOT = __dir__
module Gui
  # Provide shortcut "Gui.run" that calls "Gui::Launcher.execute"
  def self.run(application_module, options = {})
    Gui::Launcher.execute(application_module, {:launch_dir => launch_dir(caller)}.merge(options))
  end
  
  # if this was launched from a file in a bin directory, return the directory that contained the bin directory
  def self.launch_dir(lines)
    line = lines.find { |l| l =~ /\/bin\// }
    dir = line.slice(/.*(?=\/bin)/) if line
    dir
  end

  class Launcher
    # Start the server based on options
    # The options parameter is any options that are specified in addition to those found
    #    in the meta_info.rb's DEFAULT_APP_OPTIONS hash.
    def self.execute(application_module, options)
      # $EXTRA_ARGV is used
      #   * To control if we send #run! to Thin or #start! to Mizuno (see below)
      #   * To pass in arguments other than those to the server, when config.ru is used
      # Some kind of switch is necessary because we do not launch the server if the application is launched by 'rackup' 
      # (or something similar that uses a config.ru other than the sibling to this file).
      # That's the case for Heroku (and similar) deployment.
      # When we launch via config.ru, we still need a way to pass arguments into the app: $EXTRA_ARGV does that.
      # Note that $EXTRA_ARGV is *NOT* used to pass arguments to the webserver (Thin, Mizuno).
      # To pass argments to the webserver when config.ru is used to launch,
      #   * Use something like "#\ -s <server>" (where <server> is, for exampe, thin) as the first line of config.ru
      #   * Append to that first line -O NAME[=VALUE] to specify arguments to <server>
      #     You can find applicable arguments by executing 'rackup -s <server> -h'  (they vary from one server to another)
      # Regretably, there seems to be no way to compute arguments to server when you use config.ru...
      # but you probably don't need those computations anyway if you are using config.ru.
      if $EXTRA_ARGV
        puts "Warning: $EXTRA_ARGV used together with ARGV. Original ARGV: #{ARGV.inspect}" unless ARGV.empty?
        $EXTRA_ARGV.each { |extra| ARGV << extra }
      end
      
      cli_parser = Gui::CliParser.new
      opt_parser = cli_parser.default_parser

      opt_parser.parse!
      options[:port] = ENV['GUI_SITE_PORT'] if ENV['GUI_SITE_PORT']
      options = options.merge(cli_parser.parsed_values)
      options = application_module::DEFAULT_APP_OPTIONS.merge(options)
        
      # Merge default options with user defined settings
      options = DEFAULTS.merge(options)
      
      add_project_extensions(application_module, options)
      
      morph_spec_options!(options)
      
      # Copy spec_name as spec_title unless it is user-defined
      options[:spec_title] = options[:spec_name] unless options[:spec_title]
      Gui.unevaluated_options = options
      Gui.application_modules << application_module
      
      # Please see "About $EXTRA_ARGV" in runner.rb.
      # NOTE: this is deprecated in favor of :server => :setup_only option. -SD
      if $EXTRA_ARGV && (options[:server] != :setup_only && options[:server] != :rackup)
        puts "Deprecation warning: Specifying $EXTRA_ARGV will no longer prevent server startup. \n" +
             "                     Please specify :server => :setup_only (or :rackup) in order to prevent server from launching."
        return
      end
      show_options if Gui.option(:mode) == :dev || ENV['TESTING']
      case options[:server]
      when :thin
        require 'thin'
        # Thin arg --chdir points to gui_site dir which looks in for config.ru
        Thin::Runner.new(thin_args(options)).run!
      when :mizuno
        require 'mizuno'
        require 'mizuno/runner'
        ARGV.replace mizuno_args(options)
        Mizuno::Runner.start!
      when :puma
        require 'puma'
        require 'puma/cli'
        require 'puma/control_cli'
        execute_puma(options)
      when :setup_only, :rackup
        # Do not launch, so tests can directly interact with Ramaze
      else
        raise "Cannot launch unknown server: #{server}"
      end
    end
    
    # This is a collection of changes to the options that used to occur in server_config
    # TODO: remove this method in favor of more explicit parameters in the first place. <-- I'm not sure this is entirely possible (or that more explicit parameters are desireable) given the logic expressed here (MF).
    def self.morph_spec_options!(options)
      options[:home_page] = options[:custom_home_page] if options[:custom_home_page]
      options[:spec_name] = options[:spec_name].downcase.sub(/\.rb$/, '')
      if options[:tree_view] == true
        options[:tree_view] = {}  # maintain backwards compatibility and warn
        puts Rainbow("\nThe :tree_view option accepts a hash now.  This hash can contain additional directives.  Please replace ':tree_view => true' with ':tree_view => {}'\n").red
      end
      unless options[:log_dir]
        # This is the convention now...
        options[:log_dir] = File.join(options[:launch_dir], 'log') if options[:launch_dir]
      end
    end
    
    # Automatically adds model_extensions and custom_spec from project.  Replaces (and improves upon) functionality from Foundation.
    def self.add_project_extensions(application_module, options)
      unless options[:custom_spec]
        cs = LodePath.find(::File.join(application_module::GEM_NAME, 'custom_spec.rb'), :file, :lax) ||
             LodePath.find(::File.join(application_module::GEM_NAME, 'custom_spec'), :directory, :lax)
        options[:custom_spec] = cs if cs
      end
      unless options[:model_extensions]
        me = LodePath.find(::File.join(application_module::GEM_NAME, 'model_extensions.rb'), :file, :lax)
        options[:model_extensions] = me if me && ::File.exist?(me)
      end
    end    
    
    def self.show_options
      o = Gui.options
      nilo, seto = o.partition { |k,v| v.nil? || (v.respond_to?(:empty?) && v.empty?) }
      seto.sort_by! { |k,v| k.to_s }
      nilo.sort_by! { |k,v| k.to_s }
      longest_key = o.keys.map(&:to_s).max_by(&:length).length
      puts
      puts Rainbow('Valued Options:').magenta
      seto.each { |k,v| printf("    :%-#{longest_key}s %s %s\n", k, '=>', v.inspect) }
      puts Rainbow('Unset Options:').magenta
      nilo.each { |k,v| printf("    :%-#{longest_key}s %s %s\n", k, '=>', v.inspect) }
      puts
    end

    # Get the arguments for Thin based on options
    def self.thin_args(options)
      args = []
      case Gui.option(:action)
      when :start
        args << 'start'
        args += ['-t', '600'] # Set timeout to 10 minutes
        if options[:daemonize]
          args += ['--log', Gui.option(:log)] # Logging is only applicable when daemonizing
          args += ['--tag', Gui.option(:process_tag)]
          args << '-d'
        end
      when :stop
        args += ['--timeout', '5']
      end
      if options[:ssl]
        args << '--ssl'
        args += ['--ssl-key-file', Gui.option(:ssl_key_file)] if Gui.option(:ssl_key_file)
        args += ['--ssl-cert-file', Gui.option(:ssl_cert_file)] if Gui.option(:ssl_cert_file)
        args << '--ssl-verify' if options[:ssl_verify]
      end
      args += ['--chdir', WEB_ROOT]
      args += ['--pid', Gui.option(:pid)]
      # args += ['--threaded'] # Experimental thin option. Greatly improves performance.
      args += ['-R', options[:rackup_file]] if options[:rackup_file]
      args += connection_args(options)
      args
    end
    
    def self.execute_puma(options)
      args = []
      control_url_args   = ['--control-url', "tcp://127.0.0.1:#{Gui.option(:puma_ctl_port)}"]
      control_token_args = ['--control-token', 'puma_ctl_token']
      case options[:action]
      when :start
        # Puma has no built-in timeout
        if options[:daemonize]
          # No ability to log or tag process?
          # args += ['--log', Gui.option(:log)] # Logging is only applicable when daemonizing
          # args += ['--tag', Gui.option(:process_tag)]
          args << '-d' if Puma::Const::VERSION[0].to_i < 5 # Puma v5 removes daemonization option
          args.concat(control_url_args)
          args.concat(control_token_args)
        end
        args.concat(['--dir', WEB_ROOT])
        # Puma will not automatically create the directory containing the pidfile, so create it manually
        FileUtils.mkdir_p(File.dirname(Gui.option(:pid)))
        args.concat(['--pidfile', Gui.option(:pid)])
        args.concat(['--threads', '16']) # 16 is the default. Increase as needed.

        if options[:connection_type].to_sym == :port
          protocol       = options[:ssl] ? 'ssl' : 'tcp'
          ssl_params     = "?key=#{Gui.option(:ssl_key_file)}&cert=#{Gui.option(:ssl_cert_file)}" if options[:ssl]
          connection_url = "#{protocol}://#{Gui.option(:host)}:#{options[:port]}#{ssl_params}"
        elsif options[:connection_type].to_sym == :socket && !options[:ssl]
          # Check for nil socket and replace with socket named by spec_name
          socket         = Gui.option(:socket) || "/tmp/#{spec_module.downcase}.sock"
          connection_url = 'unix://' + socket
        else
          raise "Unsupported puma configuration: #{options.inspect}"
        end

        #Setup puma logging with an output log file
        #TODO split the log between console output and file output??
        if Gui.option(:mode) == :dev
          # args.concat(['--redirect-stdout', Gui.option(:log)])
          # args.concat(['--redirect-stderr', Gui.option(:log)])
          # args.concat(['--redirect-append'])
        else
          args.concat(['--redirect-stdout', Gui.option(:log)])
          args.concat(['--redirect-stderr', Gui.option(:log)])
          args.concat(['--redirect-append'])
        end
        
        args.concat(['--bind', connection_url])
        args.concat(['--debug'])
        # args += [options[:rackup_file]] if options[:rackup_file]
        rackup_file = File.expand_path(File.join(__dir__, 'config.ru'))
        raise "No rackup file at #{rackup_file}" unless File.exist?(rackup_file)
        args.concat([rackup_file])
        Puma::CLI.new(args).run
      when :stop
        args = control_url_args
        args += control_token_args
        args << 'stop'
        Puma::ControlCLI.new(args).run
      when :restart # Unused at present, but here to document pumactl usage
        args = control_url_args
        args += control_token_args
        args << 'restart'
        Puma::ControlCLI.new(args).run
      end
    end
    
    # Get the arguments for Mizuno based on options
    def self.mizuno_args(options)
      args = []
      if options[:daemonize]
        case options[:action]
        when :start
          args << '--start'
          # Currently no way to specify timeout on CLI
        when :stop
          args << '--stop'
        when :status
          args << '--status'
        end
      else
        raise "A non-daemonized mizuno instance may only be started. No other actions are allowed" if options[:action] != :start
      end
      # Change :socket to :port since mizuno doesn't do sockets
      # TODO: shouldn't be modifying options here. Should raise instead.
      if options[:connection_type] == :socket
        puts "Sockets are not supported by mizuno. Will use port #{options[:port]} instead."
        options[:connection_type] = :port
      end
      args += ['--root', WEB_ROOT]
      args += ['--pidfile', Gui.option(:pid)]
      args += connection_args(options)
      args
    end
    
    # Return connection arguments for use by webserver
    # Used by both thin and mizuno
    def self.connection_args(options)
      args = []
      case options[:connection_type].to_sym
      when :port
        args += ['--port', options[:port]]
      when :socket
        # Check for nil socket and replace with socket named by spec_name
        socket = Gui.option(:socket) || "/tmp/#{spec_module.downcase}.sock"
        args += ['--socket', socket]
      else
        raise "Invalid connection type specified. Received '#{options[:connection_type]}' when expecting 'port' or 'socket'"
      end
      args
    end
  end
end
