opts = Gui.unevaluated_options

  # Set encoding defaults
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# Set up constants based on gui builder options
puts "Starting with mode: #{Gui.option(:mode).inspect}"

# Get gui_director to initialize the model specified in opts
Gui.configure_director

CUSTOM_PAGES = opts[:custom_pages]
CUSTOM_PAGE_ROOT = opts[:custom_page_root]

# ENABLE_DB_MUTEX = true
# DB_MUTEX = Mutex.new if ENABLE_DB_MUTEX
WEB_AUTHENTICATION = opts[:web_authentication]
LOCAL_AUTHENTICATION = opts[:local_authentication]
raise "Invalid authentication configuration. No methods are allowed." unless WEB_AUTHENTICATION || LOCAL_AUTHENTICATION
