# Define a subclass of Ramaze::Controller holding your defaults for all
# controllers

$DEBUG = false
Ramaze::Log.loggers = [] if $quiet

require 'json'
require_relative '../request_safe_params_patch'
require_relative '../breadcrumbs'
require_relative '../form_parser'
require_relative '../concurrency'
require_relative '../tree_viewer'
require_relative '../emailer'

module Gui
  VIEW_DIR = File.join(__dir__, '../view')
end # Gui
require_relative 'controller'
require_relative 'main'
require_dir(relative('main'))

# Here go your requires for subclasses of Controller:
require_relative 'domain_controllers'
Gui.define_domain_package_controllers
# Gui.define_domain_controllers

# Push additional custom page root onto Ramaze roots if provided
Ramaze.options.roots.push(CUSTOM_PAGE_ROOT) if CUSTOM_PAGE_ROOT && !Ramaze.options.roots.include?(CUSTOM_PAGE_ROOT)

# recursively get all controllers in the CUSTOM_PAGE_ROOT/controller<s> folder and then require them
if Gui.option(:load_all_controllers)
  Dir.glob("#{CUSTOM_PAGE_ROOT}/controller*/**/*.rb").each { |controller| require controller }
end
# CUSTOM_PAGES holds information about only those custom pages which are explicitly specified in meta_info.rb.  The purpose of explicitly specifying them in meta_info.rb would be that you want to capture meta_information about them (i.e. you need more than just to require them)
CUSTOM_PAGES.each do |page_name, page_info|
  # Push per-custom-page root if provided
  page_root = page_info[:root] || CUSTOM_PAGE_ROOT
  Ramaze.options.roots.push(page_root) unless Ramaze.options.roots.include?(page_root)
  page_controller = page_info[:controller] || File.join(page_root, 'controller', page_name.to_s)
  require page_controller
end

require_relative 'change_tracker' if defined? ChangeTracker

require_relative 'users'
require_relative 'enumerations'
require_relative 'workflow'
require_relative 'policy'



