detail_view(Gui::EnumerationsManagementPage) do
  Gui::EnumerationsManagementPage::ENUMS_DATA.map do |datum|
    getter = datum[:getter]
    klass  = datum[:class].to_const
    view_ref(:Summary, getter, klass, :label => klass.to_title)
  end
end
