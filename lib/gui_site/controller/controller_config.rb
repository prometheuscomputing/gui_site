require 'digest'
require 'digest/bubblebabble'
require 'ruby-prof' if ENV['PERFORMANCE_PROFILE']
module Gui
  class Controller < WebFrameworkController
    # map <tt>'/'</tt>
    
    # Do not use a layout on AJAX requests
    layout(Gui.option(:layout) || 'default') {!request.xhr?}
  
    # Duplicates the 'engine :Haml' command, but also sets the charset
    provide :html, :engine => :Haml, :type => 'text/html; charset=utf-8'
  
    # Set up user authentication for all pages
    puts "About to set up user helper; This will hang if the Gui_Builder_Profile::User does not exist..." if $verbose
    helper :user
    # Override that the default class for users is User
    trait :user_model => Gui_Builder_Profile::User
    puts "Done setting up user helper" if $verbose
  
    # Allow server to send files
    helper :send_file
  
    # Block everything unless logged in
    helper :aspect
  
    # TODO: Have a unified way of blocking pages that the user must be logged in to see
    #       currently, there is this 'names_of_forward_facing_actions' which is bad because
    #       it doesn't mark which controllers, and the controller-wide public_page? query,
    #       which applies to all actions on a controller.
    #
    #       Maybe there could be a Controller#public_page?(action_name) query. or maybe
    #       Controller#accessable(action_name, user) to allow levels of control.
    #
    # TODO: watch for possible security hole if action name is the same as domain object name
    names_of_forward_facing_actions = %w(login register forgot_password reset_password verify_email error_500 error_403 error_404 error_405)
  
    before_all do
      start_profiler if ENV['PERFORMANCE_PROFILE']
            
      if $verbose
        puts "*************************** Starting page load for: ***************************"
        puts "* url: #{self.request.fullpath}"
        puts "* Action: #{self.class}##{self.action.name}"
        puts "*" * 80
      end
    
      if Gui.option(:secure_cookie)
        puts "Using secure cookie transmission" if $verbose
        Ramaze::Session.options[:secure] = true
      else
        puts "_NOT_ using secure cookie transmission (i.e. https not required)" if $verbose
      end
      Ramaze::Session.options[:ttl] = false
      # Prevent call to #logged_in? from forcing a cookie reload when it checks for a user in the request.env
      # This is a hack, and a better solution should be found.
      request.env[Ramaze::Helper::UserHelper::RAMAZE_HELPER_USER] = session[:user] if session[:user]
    
      if Gui.option(:login)
        # If there are no registered users and if we are not already on the
        # GbUsersController#register method, redirect us there
        unless logged_in?
          puts "Not logged in" if $verbose
          # Redirect to login unless page is forward facing
          unless names_of_forward_facing_actions.include?(self.action.name) || (respond_to?(:public_page?) && public_page?)
            session[:requested_before_login] = self.request.fullpath
            user_count = Gui_Builder_Profile::User.count
            if LOCAL_AUTHENTICATION && (user_count == 0) && !(self.action.instance.is_a?(GbUsersController) && self.action.method == 'register')
              puts "#{self.class.name}: User is not logged in. No users are registered. Action is not already 'register'. Redirecting to /gb_users/register" if $verbose
              raw_redirect '/gb_users/register'
            end
            puts "#{self.class.name}: User is not logged in. Attempted to access internally-facing '#{self.action.name}'. Redirecting to /gb_users/login" if $verbose
            raw_redirect '/gb_users/login'
          end
        end
      end
    
      # session[:roles] and session[:perspectives] are setup by GbUsersController#login so set default only if non-existent
      session[:roles]        ||= []
      session[:perspectives] ||= []
    
      # Put the user in the session so it can be found again on the next request
      session[:user]     = user
      session[:username] = user.login || 'Guest'
      user_has_name = (user.first_name && !user.first_name.strip.empty?) || (user.last_name && !user.last_name.strip.empty?)
      session[:full_name] = user_has_name ? "#{user.first_name} #{user.last_name}" : session[:username]
    
      # Initialize flash message vars
      flash[:error_messages] = flash[:error_messages] || []
      flash[:info_messages]  ||= []

      # Set breadcrumbs based off input from page, or create a new empty path
      if request.safe_param('breadcrumb-0')
        # Get a sorted list of breadcrumb URLs from the params
        breadcrumb_urls = safe_params.select { |k, _| k =~ /^breadcrumb-[0-9]+$/ }.sort.map { |e| url_decode(e[1]) }
        flash[:current_breadcrumbs] = Gui::NavigationPath.new(breadcrumb_urls)
      elsif flash[:prev_breadcrumbs]
        flash[:current_breadcrumbs] = flash[:prev_breadcrumbs]
      else
        flash[:current_breadcrumbs] = Gui::NavigationPath.new
      end
      
      GuiDirector.clear_change_tracker
    end # before_all
    
    after_all do
      _after_all
    end
    
    # Internal content for after_all hook
    # Specified independently from after_all, so that it can be manually invoked via #respond_with_hooks
    def _after_all
      # Stop profiler and print results to files
      stop_profiler if ENV['PERFORMANCE_PROFILE']
    end

  end # Controller
end # Gui
