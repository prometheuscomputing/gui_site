require 'uri'
require 'pp'
require 'json'
require 'mail'
# Require extensions to GbUsersController
require_dir(relative('users'))

#require 'breadcrumbs'

module Gui
  class GbUsersController < Gui::Controller
    map '/gb_users/'

    layout(Gui.option(:layout) || 'default') { !request.xhr? }
    set_layout (Gui.option(:login_layout)    || 'login_layout')    => [:login]
    set_layout (Gui.option(:register_layout) || 'register_layout') => [:register]
  
    def index  
      login # which will in turn redirect to default_url if logged_in?
    end
    
    #### Helper methods used across mutliple functionalities ####
    def refresh_session_roles(some_user = user)
      session[:roles]    = some_user.roles
      session[:is_admin] = some_user.admin?
    end
  
    def refresh_session_perspectives(some_user = user)
      current_perspectives = some_user.perspectives || [Gui_Builder_Profile::Perspective.default_perspective]
      session[:perspectives] = current_perspectives
      # Set up session[:substitutions] if multivocabulary is enabled
      session[:substitutions] = session[:perspectives].map { |psptv| psptv.substitutions }.flatten if Gui.option(:multivocabulary)
    end
    
    def refresh_permissions(some_user = user)
      session[:organization] = some_user.organizations.first unless session[:is_admin]
    end
    
    private
    
    def delete_roles(role_ids, all_roles)
      deleted_roles = delete_objects_from_set(role_ids, all_roles)
      flash[:info_messages] << "Deleted Role#{deleted_roles.length > 1 ? 's' : ''}: &nbsp; #{deleted_roles.map(&:name).join(', ')}"
      deleted_roles
    end
  
    # Deletes users based on an array of user ids (as strings)
    # all_users is the set of possible users to be deleted
    def delete_users(user_ids, all_users)
      # Don't allow current user to be deleted
      user_ids = user_ids.reject { |uid| uid == user.id.to_s }
      deleted_users = delete_objects_from_set(user_ids, all_users)
      flash[:info_messages] << "Deleted User#{deleted_users.length > 1 ? 's' : ''}: &nbsp; #{deleted_users.map { |u| u[:login] }.join(', ')}"
      deleted_users
    end
  
    def add_user(password_requirements, email_requirements, roles, user_categories = [])
      username, password, email = safe_params['new_login'], safe_params['new_password'], safe_params['new_email'] 
      begin
        new_user = Gui_Builder_Profile::User.register(:login => username, :password => password, :email => email, :password_requirements => password_requirements, :email_requirements => email_requirements, :is_admin_panel => true,  :roles => roles, :user_categories => user_categories)
        if new_user
          flash[:info_messages] << "User '#{new_user.login}' Created"
        end
        # Must commit here to be able to catch User error
        GuiDirector.commit_change_tracker
      rescue Gui::InvalidLogin => e
        flash[:error_messages] << e.message.nl2br
      end
      # Start change tracker again
      GuiDirector.start_change_tracker unless GuiDirector.change_tracker_is_started?
      new_user
    end
  
    # Deletes objects based on an array of id strings
    # set is the set of possible objects to be deleted
    def delete_objects_from_set(id_strings_to_delete, set)
      # Kick out non-admins
      raise Gui::Error403.new("I'm sorry.  You may not delete objects via administration panel without administrative privileges.") unless session[:is_admin]
      objs_to_delete = set.select { |obj| id_strings_to_delete.include?(obj.id.to_s) }
      deleted_objs = []
      objs_to_delete.each do |obj_to_delete|
        deleted_objs << obj_to_delete
        obj_to_delete.destroy
      end
      deleted_objs
    end
  
    def convert_file_to_haml(file, locals = {})
      haml_output = nil
      if file.nil?
        haml_output = ''
      elsif file.match?(/.pdf$/)
        raise "INVALID FILE: PDF RENDERING SUPPORT IS NOT YET IMPLEMENTED"
      elsif file.match?(/.latex$/)
        raise "INVALID FILE: LATEX RENDERING SUPPORT IS NOT YET IMPLEMENTED"
      elsif file.match?(/.haml$/)
        haml_output = Haml::Engine.new(File.read(file), :filename => file).render(self, locals)
      else
        raise "INVALID #{File.basename(file)} FILE: Unknown format detected."
      end
      haml_output
    end

    def set_user_roles(some_user, new_role_ids, old_roles = nil, all_roles = Gui_Builder_Profile::UserRole.all, is_current_user = false)
      new_roles = set_object_collection_from_ids('roles', some_user, new_role_ids, old_roles, all_roles)
      refresh_session_roles(some_user) if is_current_user
      new_roles
    end
  
    def set_user_perspectives(some_user, new_perspective_ids, old_perspectives = nil, all_perspectives = Gui_Builder_Profile::Perspective.all, is_current_user = false)
      new_perspectives = set_object_collection_from_ids('perspectives', some_user, new_perspective_ids, old_perspectives, all_perspectives)
      refresh_session_perspectives(some_user) if is_current_user
      new_perspectives
    end
  
    # Set an object's collection from an array of ids
    def set_object_collection_from_ids(collection_name, object, new_value_ids, old_values = nil, all_values = nil)
      new_values = all_values.select { |v| new_value_ids.include?(v.id.to_s) }
      set_object_collection(collection_name, object, new_values, old_values)
    end
  
    # Set a domain object's collection to the given values using the least changes possible (if any are needed)
    def set_object_collection(collection_name, object, new_values, old_values = nil)
      old_values ||= object.send(collection_name)
      add_values = new_values - old_values
      add_values.each { |v| raise "Tried to add nil to collection" if v.nil?; object.send(collection_name + '_add', v) }
      remove_values = old_values - new_values
      remove_values.each { |v| object.send(collection_name + '_remove', v) }
      new_values
    end
  
    def add_regexp_to_email_requirements(project_options)
      regexp_string   = safe_params['email_requirement_regexp']
      description     = safe_params['email_requirement_description']
      failure_message = safe_params['email_requirement_failure_message']
      email_requirement = build_regexp_from_params(regexp_string, description, failure_message)
      project_options.add_email_requirement(email_requirement)
    end
  
    def add_regexp_to_password_requirements(project_options)
      regexp_string   = safe_params['password_requirement_regexp']
      description     = safe_params['password_requirement_description']
      failure_message = safe_params['password_requirement_failure_message']
      password_requirement = build_regexp_from_params(regexp_string, description, failure_message)
      project_options.add_password_requirement(password_requirement)
    end
  
    def build_regexp_from_params(regexp_string, description, failure_message)
      parsed_description = description.empty? ? nil : description
      parsed_failure_message = failure_message.empty? ? nil : failure_message
      regexp_cond = Gui_Builder_Profile::RegularExpressionCondition.create(:description => parsed_description, :failure_message => parsed_failure_message)
      # Assigned separately to use custom setter.
      regexp_cond.regular_expression = regexp_string
      regexp_cond.save
      regexp_cond
    end
    
  end # GbUsersController
end # Gui
