module Gui
  class MainController < Gui::Controller

    private
    
    # Returns a domain object from the passed data
    # data_classifier: the Class of the domain object
    # data_id: the ID of the domain object, 'new' or 'new_X', or a constructor ('new_<constructor_name>')
    # data_getter: if passed, is called on the domain object from the first two parameters, and the result is returned instead.
    def domain_obj_from_data(data_classifier, data_id, data_getter = nil)
      if data_classifier.is_a?(Gui::AdHocPageObject) # includes Home
        authorize_creation(data_classifier)
        return data_classifier.new
      end
      
      # The assumption is that if we got to this point, then data_classifier is a class from our domain model
      raise Gui::Error500 if data_classifier.abstract? # Domain object class cannot be abstract
      obj = case data_id
            when "new", /^new_(\d+)$/, /^new_(.*)_for/ # matches new, new_id, and new_thing_for_classifier that auto_create uses
              authorize_creation(data_classifier)
              data_classifier.singleton? ? data_classifier.instance : data_classifier.new
            when /^new_(.*)$/
              authorize_creation(data_classifier)
              constructor_name = $1
              unless data_classifier.respond_to?(:constructors) && data_classifier.constructors.key?(constructor_name.to_sym)
                raise Gui::Error500.new("Invalid constructor: #{constructor_name.inspect}")
              end
              data_classifier.send("new_#{constructor_name}")
            else
              object_in_policy_scope(data_classifier, data_id)
            end
    
      # FIXME data_getter should be for either a file or a RichText only!  Check here or upstream?
      return obj if data_getter.nil? || data_getter == 'nil' || data_getter.empty?
      
      assoc_info = data_classifier.associations[data_getter.to_sym]
      attr_info  = data_classifier.attributes[data_getter.to_sym]
      raise Gui::Error500.new("No such getter: #{data_getter.inspect} for #{data_classifier}") unless assoc_info || attr_info
      raise Gui::Error500.new("Not a to-one association: #{data_getter.inspect}") if assoc_info && !assoc_info[:type].to_s =~ /to_one$/
      # FIXME do we need a policy check here? At this point, this is either a File or RichText
      # We are not yet implementing attribute level policy...but we will...maybe...
      obj.send(data_getter)
    end
    
    # Returns a domain object from the strings in the request parameters
    def domain_obj_from_params(use_getter = true)
      data_classifier = safe_params['data_classifier']
      return Gui.loaded_spec.default_domain_object unless data_classifier
      data_id = safe_params['data_id']
      data_id = 'new' if data_id.nil? || data_id.empty?
      data_getter = safe_params['data_getter'] if use_getter
      domain_obj_from_strings(nil, data_classifier, data_id, data_getter)
    end
  
    # Returns a through domain object from the strings in the request parameters
    def domain_through_obj_from_params(use_getter = true)
      data_classifier = safe_params['through_classifier']
      return Gui.loaded_spec.default_domain_object unless data_classifier
      data_id = safe_params['through_id']
      data_getter = safe_params['data_getter'] if use_getter
      domain_obj_from_strings(nil, data_classifier, data_id, data_getter)
    end
  
    # Returns a domain object parsed from the given strings
    # NOTE: the package_str input here is the Ramaze formatted package
    def domain_obj_from_strings(package_str, data_classifier_str, data_id_str, data_getter = nil)
      if package_str
        pkg = package_str.to_camelcase.to_const
        raise "Can't get namespace from #{package_str} using String#to_const" unless pkg
      end
      data_classifier = data_classifier_str.to_const(pkg || Object)
      data_id = (data_id_str =~ /^new/) ? data_id_str : data_id_str.to_i
      domain_obj_from_data(data_classifier, data_id, data_getter)
    end
  
  end
end