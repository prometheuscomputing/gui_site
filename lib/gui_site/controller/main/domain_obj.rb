module Gui
  class DomainController < MainController
  end
  
  class ObjController < MainController
    class << self; attr_accessor :model; end # better if read only but must change metaprogramming of value for that
    attr_reader :data_id, :data_getter, :is_creation, :query
    def initialize
      rq = request.env['REQUEST_PATH'].split('/').reject(&:empty?)
      data_id = rq.shift
      @is_creation = true if data_id =~ /^new/

      # "rack.request.query_hash"=>{"view-name"=>"Details", "view-type"=>"Organizer"}}
      @query = request.env['rack.request.query_hash'] || {}
      super
    end
  end
  
  class DomainObjController < ObjController
    def initialize
      super
      raise Gui::Error500.new("Unexpected request: #{request.env['REQUEST_PATH']}#{data_getter}.  /#{data_id} should be an integer or 'new'.") unless data_id =~ /\d+/ || is_creation
    end
  end
  
  class ObjController < MainController
    def index(*args)
      puts "Args: #{args}"

      
      # obviously you need to FIXME
      data_operation, operation_arg = nil
  
      # TODO parse this do get obj.  Forget about namespace / domain_controllers / etc...
      # rq = request.env['REQUEST_PATH'].split('/').reject(&:empty?)
      # puts Rainbow("REQUEST_PATH - #{rq}").coral
      
      # _name_parts = rq.take_while { |x| x !~ /new|\d+/ }
      # puts Rainbow("_name_parts - #{_name_parts}").coral
      # _name_parts.size.times { rq.shift }
      # _name = _name_parts.join('::')
      # puts Rainbow("_name - #{_name}").coral
  
      # uri = request.env['REQUEST_URI']
      # pi = request.env['PATH_INFO']
      # puts 'REQUEST_URI: ' + Rainbow(uri).orange
      # puts 'PATH_INFO: ' + Rainbow(pi).green
  
      # Set readtime to now so we know when the user loads a gui page. Save to gui page.
      # TODO so, shouldn't this happen as close to serving the page as possible, instead of early on in the process?
      GuiDirector.start_change_tracker

      # Resolve the data classifier. If not resolved, error_404
      # klass = resolve_classifier_in_namespace(klass)

      # data_id = rq.shift
      # data_getter = rq.shift
      #
      # # Convert data_id to int unless it indicates a new object creation
      # is_creation = !!(data_id =~ /^new/)
      # data_id = is_creation ? data_id : data_id.to_i
        
      # policy
      authorize_creation(klass) if is_creation

      # Get the domain object from the given parameters
      # puts Rainbow("#{klass.name}[#{data_id}]").yellow
      
      domain_obj = domain_obj_from_data(klass, data_id, data_getter)
      puts Rainbow(domain_obj.inspect).magenta
      # FIXME what is the use case for sending data_getter?  assuming it is for file downloads and such but never was intended to go to object pages.

      # policy
      # Will also need to do this for collection controller....
      # authorize(domain_obj, :show?) unless is_creation

      @rendered_view = object_page(domain_obj, data_getter, data_operation, operation_arg)
    end
        
    private

    def klass
      self.class.model
    end

    def object_page(domain_obj, data_getter, data_operation, operation_arg)
      Gui.log_info {"** Creating object page for #{request.url} **"}
      # Keep params in logs? This is how most sites work, but then again, they don't have the same quantity
  
      # Get a sanitized version of the passed parameters
      logged_params = "** PARAMS: **\n#{safe_params.inspect}\n\n"
      Gui.log_info { logged_params }
  
      # Ignore uniqueness warnings if continue_anyway was specified
      ignore_uniqueness_warnings = (safe_params['continue_anyway'] == 'true')
  
      # Set view information variables from params or defaults
      # FIXME why use safe_params here when we have the info in @query?
      raise if safe_params['view-name'] != @query['view-name']
      raise if safe_params['view-type'] != @query['view-type']
      view_name = (safe_params['view-name'] || Gui.loaded_spec.default_view_name).to_sym
      view_type = (safe_params['view-type'] || Gui.loaded_spec.default_view_type).to_sym
  
      # Set the view_classifier from parameter, or default to domain object class
      # FIXME can we avoid resolve_classifier_in_namespace?  We can if view-classifier is always the same as klass
      # vc = safe_params['view-classifier'].to_s
      # puts "view-classifier: #{vc} for #{klass.name}"
      # # raise unless klass.name == vc
      view_classifier = resolve_classifier_in_namespace(safe_params['view-classifier'], false) || domain_obj.class
      
      # puts Rainbow("vc: #{view_classifier} <---> #{view_classifier1}")
      
      # FIXME data_getter is only for files.  Files should be handled by a file controller.
      if data_getter
        prop = klass.properties[data_getter.to_sym]
        unless prop[:class] == "Gui_Builder_Profile::File"
          puts Rainbow(prop.pretty_inspect).green
          pp request # debug
          raise "NOT A FILE!" unless prop[:class] == "Gui_Builder_Profile::RichText"
        end
      end
    
      # Set association information.
      # This is typically used in the creation of a new object, and sets up an initial association
      assoc_classifier = safe_params['assoc-classifier'].to_const if safe_params['assoc-classifier']
      assoc_id = safe_params['assoc-id'].match?(/^new/) ? safe_params['assoc-id'] : safe_params['assoc-id'].to_i if safe_params['assoc-id']
      assoc_getter = safe_params['assoc-getter']
      # Set up assoc_obj if we have enough information
      # This is the object that is to be associated to the domain_obj via the assoc_getter
      assoc_obj = domain_obj_from_data(assoc_classifier, assoc_id) if assoc_classifier && assoc_id && assoc_getter
  
      # If domain obj was not found, error_404
      raise Gui::Error500.new("Invalid domain object specified: #{request.env['REQUEST_PATH']}#{data_getter}") if domain_obj.nil?
      # Prevent non-admins from viewing or modifying restricted classes
      # FIXME use policy here instead of admin_restricted_classes
      raise Gui::Error403.new("I'm sorry, you don't have permission to access a #{klass}") if !session[:is_admin] && Gui.admin_restricted_classes.include?(klass)
      raise Gui::Error403.new("I'm sorry, you don't have permission to access a #{view_classifier}") if !session[:is_admin] && Gui.admin_restricted_classes.include?(view_classifier)
      raise Gui::Error403.new("I'm sorry, you don't have permission to access a #{assoc_classifier}") if assoc_obj && !session[:is_admin] && Gui.admin_restricted_classes.include?(assoc_classifier)
      
      # Get the page state from parameters
      page_state = domain_obj.is_a?(GuiSpec::AdHocPageObject) ? [] : build_page_state
      # pp safe_params;puts '**************************************'
      # if page_state && page_state.any?
      #   puts Rainbow("page_state").magenta; pp page_state;puts
      # end
  
      # Handle 'image_action' and 'action_download' operations
      # TODO: make 'image_action' and 'action_download' params into data operations
      # Respond immediately if image_action is specified
      image_action(safe_params['image_action'], domain_obj, page_state) if safe_params['image_action']
      # Respond immediately if action_download is specified
      action_download(safe_params['action_download'], domain_obj, page_state) if safe_params['action_download']
      # Handle data operation if present. All data operations respond immediately
      process_data_operation(data_operation, operation_arg, domain_obj, page_state) if data_operation && !data_operation.empty?
  
      # Reset breadcrumbs if there are none or we are navigating to home page
      reset_breadcrumbs if (klass == Gui.loaded_spec.default_classifier) || flash[:current_breadcrumbs].nil?
  
      flash[:assoc_obj] = assoc_obj if assoc_obj
  
      flash[:creation] = is_creation
  
      previous_page_url = breadcrumbs_previous_page || Gui.option(:home_page)[:url]
      # We don't know that the current url is the home page or that the root obj of the page is a Gui::Home.  We might be on a custom page instead of the home page.  The home page as well as all custom pages and pages like enumerations management are based on a "fake" GuiSpec::AdHocPageObject domain object.
      if domain_obj && (domain_obj.class < ORM_Instance)
        current_page_url = current_url(domain_obj)
      else
        current_page_url = safe_params["from_url"] || Gui.option(:home_page)[:url]
      end
      add_breadcrumb(current_page_url)
  
      if request.post? && is_processable_domain_obj?(domain_obj)
        
        # FIXME seems like we need policy here for create / update / delete
        
        # Since we will be redirecting to another gui page, we need to save the breadcrumbs from the POST
        # They will be restored when the next page loads (via before_all)
        flash[:prev_breadcrumbs] = flash[:current_breadcrumbs]
    
        # ------- Begin Discard Request Handlers -------
        # Discard changes and go to the given URL
        discard_and_go(domain_obj) if safe_params['discard_and_go']
        # Discard changes and select existing domain obj (in response to uniqueness error/warning) and redirect to selected item.
        select_existing(safe_params['select_existing'], safe_params['selected_obj'], assoc_obj, assoc_getter) if safe_params['select_existing']
    
        # ------- Parse Changes to Domain Obj -------
        # Parse changes made to domain_obj from the form data.
        # Handle assoc_obj (object association assigned via URL)
        parse_domain_obj_changes_from_state(domain_obj, assoc_obj, assoc_getter, page_state, ignore_uniqueness_warnings) unless domain_obj.is_a?(GuiSpec::AdHocPageObject)
        # This is used to display the message "Successfully created a X"
        # TODO - maybe replace domain_obj.class.to_title with domain_obj.title or something?
        flash[:created_new_object] = {:class_name => domain_obj.class.to_title} if safe_params['is_creation'] == 'true'
    
        # ------- Begin Save Request Handlers -------
        # Process action_buttons and redirect to current page if clicked
        pressed_action_button = get_pressed_action_button(page_state)
        process_action_button(pressed_action_button, domain_obj, assoc_obj, page_state) if pressed_action_button
        # Clone domain object and redirect to clone
        clone_domain_object(domain_obj) if safe_params['clone_domain_object']
    
        # Save changes and close the current browser tab (via JS)
        # save_and_close if safe_params['save_and_close']
    
        # Save changes and redirect to the specified URL
        save_and_go(domain_obj) if safe_params['go_to_url']
    
        # Save changes and redirect to another creation page
        save_and_create_another(domain_obj.class) if safe_params['save_and_continue']
    
        #Advance The Worklow and Go to the next Page
        advance_workflow(domain_obj) if safe_params['advance_workflow']
    
        # Save changes and redirect to the previous page in breadcrumbs
        save_and_leave(domain_obj, previous_page_url) if safe_params['save_and_leave']
    
        # Save changes and redirect to the current page
        # This occurs if "Save" is clicked, or if "Add Associations" or "Break Associations" is clicked
        save_and_stay(domain_obj) if safe_params['save_and_stay'] || safe_params.keys.any? { |k| k =~ /^break_assoc_for_/ || k =~ /^add_assoc_for_/}
    
        # Raise error, since one of the above request handlers should have redirected us somewhere else.
        raise "Gui method failed to process request with params:\n#{safe_params.pretty_inspect}"
      end # POST handling
  
      
      # FIXME this belongs in a FileController.  Need to route to a FileController if data_getter is for a file?
      # If the domain object is a File, it should be sent for download
      send_file_stream(domain_obj.filename, domain_obj.data, domain_obj.mime_type) if view_classifier == Gui_Builder_Profile::File
  
      # Handle assoc_obj (object association assigned via URL)
      associate_domain_obj_to_assoc_obj(domain_obj, assoc_obj, assoc_getter)
  
      # Load the page state from the page post, so we can have the values for the get request
      page_state = flash[:continue_edit] ? flash[:page_state] : nil
      flash.delete(:page_state)
      # TODO: fix this... It's impossible to tell what came from the flash this way and also not everything in flash will be needed as a local -Sam
      rendering_locals = flash.combined
      
      # TODO FIXME remove this after done with analysis.  We're checking for situations where the domain_obj is something other than the model that this is a controller for OR if the view classifier is for a class other than the model that this is a controller for.  In the first case, we should have shifted to a controller for the other type of domain object instead of continuing to handle it here.  In the second case...well, figure out how it ended up that way (hint: maybe it shouldn't have)
      if !domain_obj.is_a?(Gui_Builder_Profile::File) && domain_obj.class != klass
        puts Rainbow("Domain Obj != data_classifier! #{domain_obj.class.name}[#{data_id}] -- dc:#{klass}, view_name:#{view_name}, view_classifier:#{view_classifier}, view_type:#{view_type}").orange
      end
      if !domain_obj.is_a?(Gui_Builder_Profile::File) && klass != view_classifier
        puts Rainbow("View Classifier != Model -- #{domain_obj.class.name}[#{data_id}] -- dc:#{klass}, view_name:#{view_name}, view_classifier:#{view_classifier}, view_type:#{view_type}").orange
      end

      # FIXME can we use @query for the view stuff?
      render_object(domain_obj, view_name, view_classifier, view_type, rendering_locals, page_state)
    end
    
    # Render the given object
    def render_object(obj, view_name = :Details, view_classifier = nil, view_type = :Organizer, additional_locals = {}, page_state = nil)
      # puts "Rendering a #{obj.class} #{view_name} #{view_type}"
      raise "#{obj.class} is not compatible with view #{view_classifier}" if view_classifier && !obj.is_a?(view_classifier)
      # Add some information to the locals hash
      # Mark if this is a new object being created
      locals = {}
      locals[:spec_title]   = Gui.option(:spec_title)
      locals[:url] = Gui.create_url_from_hash(klass, data_id,
                                              "view-name"       => view_name,
                                              "view-classifier" => view_classifier,
                                              "view-type"       => view_type)
      # Add any additional locals
      locals.merge! additional_locals
      locals[:page_state] = page_state if page_state
      locals[:page_title] = nil#Gui.option(:spec_title)
      # OK, we are using locals as the options hash that gets passed to render
      # output = Gui::Director.new.render(obj, nil, view_type, view_name, view_classifier, locals)
      director_args = {obj:obj, view_type:view_type, view_name:view_name, view_classifier:view_classifier, options:locals}
      
      
      output = Gui::Director.new(**director_args).render # We get back all of the HTML that results from rendering a root vertex
      # render_view is defined in the Innate gem
      render_view(:object_wrapper, :output => output, :breadcrumbs => current_breadcrumbs, :locals => locals) do |action|
        # This causes the rendering to be treated as a layout (object_wrapper is similar to a second layout).
        # This prevents the before_all hook from running when render_view is called.
        action.options[:is_layout] = true
      end
    end
  
    # Whether or not the domain object can be processed for changes
    def is_processable_domain_obj?(domain_obj)
      domain_obj.is_a?(ORM_Instance) || domain_obj.is_a?(GuiSpec::AdHocPageObject)
    end
    
    def process_data_operation(data_operation, operation_arg, domain_obj, page_state)
      case data_operation
      when 'images'
        view_image(domain_obj, operation_arg, page_state)
      when 'delete_image'
        delete_image(domain_obj, operation_arg, page_state)
      # when 'delete'
      #   delete_domain_obj(domain_obj, klass, data_id, data_getter, page_state)
      else
        raise Gui::Error500.new("Operation not recognized: #{data_operation.inspect}")
      end
    end
    
    def select_existing(select_type, selected_obj_string, assoc_obj, assoc_getter)
      # TODO: do not split on '___', but instead have separate fields for class and id -SD
      selected_obj_class, selected_data_id = selected_obj_string.split('___')
      selected_obj = selected_obj_class.to_const[selected_data_id]
      raise "Error: Unable to retrieve selected object with class: #{selected_obj_class} and id: #{selected_data_id}" unless selected_obj
    
      # Discard pending changes
      GuiDirector.clear_change_tracker
    
      # Handle association changes depending on select type
      case select_type
      when 'discard_and_select' # Discard the pending item creation and select an existing item instead
        raise "Error: No assoc_obj to associate with existing item." unless assoc_obj
        # Handle assoc_obj (object association assigned via URL)
        associate_domain_obj_to_assoc_obj(selected_obj, assoc_obj, assoc_getter)
      when 'merge' # Merge the current item with an existing item
        raise "Currently unsupported -- pending implementation of merge feature"
        # raw_redirect merge_domain_objs(selected_obj, domain_obj)
      when 'discard_and_view' # Discard the pending item creation and view an existing item
      else
        raise "Unknown value for select_existing parameter. Received: #{select_type.inspect}"
      end
    
      GuiDirector.commit_change_tracker
    
      selected_obj_url = current_url(selected_obj, true)
      puts "MainController: gui: select_existing chosen. object _not_ saved. Redirecting to page: #{selected_obj_url}" if $verbose
      raw_redirect selected_obj_url
    end
 

  end
end
