module Gui
  class MainController < Gui::Controller
    # TODO: This is copied from report_generator.rb, put it in a shared location.
    def send_file_stream(name, file_data, content_type = nil, content_disposition = nil)
      content_type ||= Rack::Mime.mime_type(File.extname(name))
      content_disposition ||= 'attachment'
      header = {}
      header['Content-Length'] = Rack::Utils.bytesize(file_data).to_s
      header['Content-Type'] = content_type
      header['Content-Disposition'] = "#{content_disposition}; filename = \"#{name}\""
      respond_with_hooks file_data, 200, header
    end
    private :send_file_stream
    
    def trigger_action_download(url)
      locals = {:url => url}
      template_path = File.join(VIEW_DIR, 'action_download.haml')
      # return download_html
      Haml::Engine.new(File.read(template_path), :filename => template_path).render(self, locals)
    end

    # called from #object_page in gui.rb...and apparently nowhere else.
    def action_download(action, domain_obj, page_state)
      # Verify action is a valid getter.
      raise "Invalid getter: #{action} for #{domain_obj.class}" unless domain_obj.class.attributes.key?(action.to_sym)
    
      file_info = handle_errors_for(page_state, domain_obj) { domain_obj.gb_send_for_action_download(action) }
      if file_info && file_info[:content]
        send_file_stream(file_info[:filename], file_info[:content], file_info[:mime_type])
      else
        raw_redirect current_url(domain_obj)
        # respond 'Error: A problem was encountered while trying to provide the file.'
      end
    end

    # FIXME appears to never be used.  Make it go away?
    def image_action(action, domain_obj, page_state)
      raise "Who called #image_action method?"
      # Verify action is a valid getter.
      raise "Invalid getter: #{action} for #{domain_obj.class}" unless domain_obj.class.attributes.key?(action.to_sym)
    
      image = handle_errors_for(page_state, domain_obj) { domain_obj.gb_send_complex(action) }
      if image
        send_file_stream("#{action}.png", image, 'image/png', 'inline')
      else
        raw_redirect current_url(domain_obj)
        #Todo:  Need to respond with error image
        # respond 'Error: A problem was encountered while trying to provide an image.'
      end
    end
  end
end
