module Gui
  class MainController < Gui::Controller
    # Populates collection lists on the current page via ajax
    def populate_collection
      # collection_report
      error_404("Can't populate collection if not using AJAX.") unless request.xhr?
      GuiDirector.start_change_tracker
  
      domain_obj = domain_obj_from_params(false)
  
      render_options = {}
      render_options[:ajax_request] = true
      render_options[:pretty_id]    = safe_params['pretty_id']

      order_options = {}
      order_options[:order_by_column] = safe_params['order_by_column'].underscore.to_sym if safe_params['order_by_column']
      order_options[:sort_direction]  = safe_params['table_sort_direction'] if safe_params['table_sort_direction']
      render_options[:order_options]  = order_options
  
      # Retrieve previous and current filter. If different from each other, then reset pagination.
      current_filter = process_filter(safe_params['filter'])
      # Previous filter was stored in a single input, rather than duplicate every filter input. (substitutes \"=>\" with \":\") to match current param
      param_previous_filter = safe_params['previous_filter'] && !safe_params['previous_filter'].empty? ? JSON.parse(safe_params['previous_filter'].gsub('\"=>\"', '\":\"')) : nil
      previous_filter       = process_filter(param_previous_filter)
  
      search_all_filter = safe_params['search_all_filter'] || {}

      current_search_all_value = search_all_filter && search_all_filter["search_all_value"] || ""
  
      previous_search_all_filter = safe_params['previous_search_all_filter'] || ""
  
      render_options[:collection_size]  = safe_params['collection_size'].to_i
      render_options[:page_size]        = safe_params['page_size'].to_i
      render_options[:page_index]       = safe_params['page_index'].to_i
  
      # Reset page index if filters changed.
      filters_changed = safe_params['previous_filter'] && 
                        current_filter != previous_filter && 
                        !(current_filter == {} && previous_filter == nil) && 
                        current_search_all_value.empty?
      filters_changed ||= previous_search_all_filter.empty? && !current_search_all_value.empty?
      filters_changed ||= previous_search_all_filter != current_search_all_value
      render_options[:page_index] = 0 if filters_changed
  
      render_options[:filter]                        = current_filter
      render_options[:search_all_filter]             = (search_all_filter['search_all_value'] && search_all_filter['search_all_value'] != "") ? search_all_filter : {}
      render_options[:show_filter_row]               = (safe_params['show_filter_row'] == 'y') # || filter.any?)
      render_options[:show_search_all_filter_row]    = (safe_params['show_search_all_filter_row'] == 'y')
      render_options[:show_possible_assoc]           = safe_params['show_possible_assoc'] == 'y'
      render_options[:unassociated_only]             = safe_params['unassociated_only'] == 'y'
      render_options[:selection]                     = safe_params['selection'] == 'y'
      # Standalone means that this widget is not tied to a parent object, and is not expected to be rendered from an object page
      # standalone collections don't render links with '.save_and_go_link', and don't show "Save &" portion of "Save & Create New..." label
      render_options[:standalone]                    = safe_params['standalone'] == 'y'
      # When rendering a selection, set standalone to true
      render_options[:standalone]                    = true if render_options[:selection]
      render_options[:selection_param_name]          = safe_params['selection_param_name'] if safe_params['selection_param_name'] && !safe_params['selection_param_name'].empty?
      render_options[:selection_create]              = safe_params['selection_create'] == 'y'
      render_options[:object_view_name]              = safe_params['object_view_name'] if safe_params['object_view_name']
      render_options[:object_view_type]              = safe_params['object_view_type'] if safe_params['object_view_type']
      # TODO: looks like there are two different systems for returning pending association information
      #       from the page. The following 'params' method and the 'page_state' method.
      render_options[:pending_association_additions] = safe_params['pending_association_additions'].values if safe_params['pending_association_additions']
      render_options[:pending_association_removals]  = safe_params['pending_association_removals'].values  if safe_params['pending_association_removals']
      render_options[:pending_association_deletions] = safe_params['pending_association_deletions'].values if safe_params['pending_association_deletions']
  
      render_options[:disabled]                      = safe_params['disabled']           == 'true'
      render_options[:creation_disabled]             = safe_params['creation_disabled']  == 'true'
      render_options[:deletion_disabled]             = safe_params['deletion_disabled']  == 'true'
      render_options[:removal_disabled]              = safe_params['removal_disabled']   == 'true'
      render_options[:addition_disabled]             = safe_params['addition_disabled']  == 'true'
      render_options[:traversal_disabled]            = safe_params['traversal_disabled'] == 'true'

      getter          = safe_params['data_getter'].to_sym if safe_params['data_getter'] && !safe_params['data_getter'].empty?
      view_type       = safe_params['view_type'].to_sym if safe_params['view_type']
      view_name       = safe_params['view_name'].to_sym if safe_params['view_name']
      view_classifier = safe_params['view_classifier'].to_sym if safe_params['view_classifier']
      respond_with_hooks(render_collection(domain_obj, getter, view_type, view_name, view_classifier, render_options))
    end
    
    # This is initially implemented to provide a file download as a result of clicking a button in a collection.  Perhaps more can be done?
    # FIXME write cukes (if you can).  There are currently no tests for this!
    def collection_button
      raise Gui::Error404.new("#collection_button must be called using AJAX.") unless request.xhr?
      domain_obj    = domain_obj_from_params(false)
      button_method = safe_params['button_action']
      saf = safe_params.dig('search_all_filter', 'search_all_value')&.[](0) ? safe_params['search_all_filter'] : {}
      opts = {
        :collection_getter => safe_params['collection_getter'].to_sym,
        :advanced_filter   => process_filter(safe_params['filter']),
        # check to see if search_all_filter value is empty or not
        :simple_filter     => format_search_all_filter(saf),
        :page_params       => safe_params
      }
      
      # Generic signature of DomainObj (or AdHocPageObject) methods is to simply send current_user and opts hash and let that method sort out what it needs from it.
      # TODO find a better solution than sending current_user to the domain_obj.  The reason we are doing this is to allow the domain_obj to call policy methods...which may be undesirable.
      raise Gui::Error501.new("#{domain_obj.class}##{button_method} has not been implemented!") unless domain_obj.respond_to?(button_method.to_sym) # technically this should be a 501 but we don't have handling for that implemented yet
      
      # THIS MUST RETURN A JSON FORMATTED STRING that includes the keys 'type' and 'data'.  See collection_button.js for a better understanding of what you should return
      domain_obj.send(button_method.to_sym, current_user, opts)
    end

    private
    def render_collection(domain_obj, getter, view_type, view_name, view_classifier, options = {})
      filter            = options[:filter] || {}
      order             = options[:order_options] || {} 
      search_all_filter = options[:search_all_filter] || {}

      order             = format_order(order)
      simple_filter     = format_filter(filter)
      search_all_filter = format_search_all_filter(search_all_filter)

      options[:filter]  = filter
      options[:order]   = order
  
      options[:search_all_filter] = search_all_filter
      
      options[:simple_filter] = format_filter(filter)        
      
      director_args = {obj:domain_obj, getter:getter, user:current_user, view_type:view_type, view_name:view_name, view_classifier:view_classifier, options:options}
      
      Gui::Director.new(**director_args).render(:collection)
    end
    
    def render_collection_from_domain_obj(domain_obj, assoc_info, options = {})
      director_args = {obj:domain_obj, getter:assoc_info[:getter], user:current_user, view_type: :Collection, view_name: :Summary, options:options}
      Gui::Director.new(**director_args).render
    end
    
    # The JS sent a hash of hashes for the filter so that information about different filtering modes could be conveyed to the server.
    # Building the page requires a simple hash that just has the name of the field and value
    def format_filter filter
      return_hash = {}
      filter.each do |_filter_type, filter_set|
        filter_set.each do |field, value|
          return_hash[field.to_s] = value
        end
      end
      return_hash
    end
  
    def format_search_all_filter search_all_filter
      # return {}
      return {} if search_all_filter.nil? || search_all_filter.empty?
      search_all_value = search_all_filter["search_all_value"]
      formatted_search_all_hash = {}
      # puts search_all_filter
      columns = search_all_filter["columns"] || {}
      columns_array = columns.map { |i| i[1] }
      # columns = search_all_filter["columns"] || {}
      return {} if columns_array.nil? || columns_array.empty? || search_all_value.nil? || search_all_value == ""
    
      #TODO Write parser for the search value to incorporate ANDs and ORs
      columns_hash = {}
    
      possible_object_types = search_all_filter["possible_types"] || []
      # puts possible_object_types
    
      #This builds a hash for easier access and cleaner interface for parsing.
      #It also elimiates Multiples and Literals from being included in the filter for now
      #TODO Create a way to allow searching of multiples or literals
      columns_array.each do |pair|
        columns_hash[pair.first.to_sym] = {:column_name => pair.first.to_sym, :search_type => pair.last.to_sym} if pair[1]
      end
    
      # puts columns_hash.inspect
      matched_object_types = possible_object_types.select { |type_string| type_string =~ /#{search_all_value}/i }
    
      # puts matched_object_types
    
      formatted_search_all_hash[:search_all_value] = search_all_value
      formatted_search_all_hash[:columns] = columns_hash
      formatted_search_all_hash[:object_type] = matched_object_types.first
      # puts formatted_search_all_hash

      # column_names = columns_array.map(&:first)
      # puts column_names
      # puts columns_array
    
      # sql = People::Person.filter(or_query)
      # puts sql.sql
      # puts sql.all.inspect
    

      # puts or_query.inspect
      formatted_search_all_hash
    end
  
    def format_order(order)
      return_hash = {}
      if order && !order.empty?
        return_hash[:column] = order[:order_by_column] if order[:order_by_column]
        return_hash[:column] = 'type' if order[:order_by_column] == :sub_type
        return_hash[:descending_order] = order[:sort_direction] if order[:sort_direction] && order[:sort_direction] != ''
      end
      return_hash
    end
    
    # Processes filters for collections
    def process_filter(input_filter = {})
      if input_filter
        output_filter = {}
        input_filter.each do |field, value|
          output_filter[field.to_sym] = value if field && !field.empty? && value && !value.empty?
        end
        output_filter
      end
    end
  end
end
