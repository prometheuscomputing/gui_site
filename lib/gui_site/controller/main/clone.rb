module Gui
  class MainController < Gui::Controller
    def clone_popup
      @domain_obj = domain_obj_from_params
      @compositions = @domain_obj.compositions
      @composer_associations = @domain_obj.class.composers
      @composer_associations_types = @composer_associations.map do |ca|
        @domain_obj.send(ca[:getter].to_s + '_type').reject(&:abstract?)
      end
      @composition_associations = @domain_obj.class.compositions
      # Get the domain_object's composer & composer association info if one exists
      @current_composer = @domain_obj.composer
      @current_composer_association = @domain_obj.composer_association
      render_options = {
        :unassociated_only => true,
        :expanded => true,
        :hide_collection_header => true,
        :selection => true,
        :selection_param_name => 'composer_option'}
      @composer_selections = {}
      @composer_associations.each do |ca|
        @composer_selections[ca[:getter]] = render_collection_from_domain_obj(@domain_obj, ca, render_options)
      end
    end

    def clone_domain_object(domain_obj)
      GuiDirector.start_change_tracker unless GuiDirector.change_tracker_is_started?
      composer_option     = safe_params['composer_option']
      composer_getter_str = safe_params['composer_getter']
      composer_getter     = composer_getter_str.to_sym if composer_getter_str && !composer_getter_str.empty?
  
      if composer_getter # composer_getter is nil here if user selects 'None'
        composer_association = domain_obj.class.associations[composer_getter]
        case composer_option
        when 'same'
          composer = domain_obj.composer
        when 'new'
          composer_class = safe_params['new_composer_type'].to_const #composer_association[:class].to_const
          composer = composer_class.create
        else # Is a specific composer
          composer_class_str, composer_id_str = composer_option.split('___')
          composer = domain_obj_from_strings(nil, composer_class_str, composer_id_str)
        end
      else
        composer = nil
      end
  
      clone = domain_obj.deep_clone(:ignore_composer => true)
      if composer
        # NOTE: I don't think to_many composer associations are meaningful. Should probably disallow this. -SD
        to_many = (composer_association[:type].to_s =~ /to_many$/)
        composer_setter = composer_getter.to_s + (to_many ? '_add' : '=')
        clone.send(composer_setter, composer)
      end
      clone.save
      GuiDirector.commit_change_tracker
  
      # Must set flash[:info_messages] so that the message isn't expired on next page
      flash[:info_messages] = ["Successfully cloned #{domain_obj.class.to_title}. Now viewing clone."]
      clone_url = Gui.create_url_from_object(clone, current_options(false))
      raw_redirect clone_url
    end
  end
end
