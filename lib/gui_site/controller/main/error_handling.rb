require 'digest'
require 'digest/bubblebabble'

module Gui
  class MainController < Gui::Controller
    
    def handle_errors_for(page_state, domain_obj, assoc_obj = nil, options = {}, &block)
      # Prevent uniqueness warnings from triggering a popup
      SpecificAssociations.disable_uniqueness_warnings if options[:ignore_uniqueness_warnings]
    
      begin
        ret = yield
      rescue Gui::PageContentError => e
        flash[:page_content_error] = e.message
        GuiDirector.clear_change_tracker
      rescue Gui::WidgetValueError => e
        flash[:page_content_error] = e.message
        GuiDirector.clear_change_tracker
      rescue Gui::PageContentError => pce
        flash[:page_content_error] = pce.message
        GuiDirector.clear_change_tracker
      rescue Gui::FileMissingError => fme
        flash[:file_missing_error] = fme.message
        GuiDirector.clear_change_tracker
      rescue Gui::LatexError => le
        puts "rescuing LatexError in handle_errors_for"
        flash[:latex_error] = le.message
        GuiDirector.clear_change_tracker
      rescue Gui::ImageError => ie
        puts "rescuing ImageError in handle_errors_for"
        flash[:image_error] = ie.message
        GuiDirector.clear_change_tracker
      rescue BusinessRules::RuleError => re
        flash[:broken_rule] = re.message
        GuiDirector.clear_change_tracker
      rescue SpecificAssociations::NonUniqueWarning, SpecificAssociations::NonUniqueError => e
        # Putting these in flash, but they will be later transferred into 'locals'
        # TODO: eventually remove all dependence on params in favor of page_state
      
        # Parse through attributes and copy any file uploads to a tempdir
        copy_uploads_to_tempdir!(page_state)
        flash[:params]        = safe_params
        flash[:page_state]    = page_state
      
        flash[:continue_edit] = true

        # Set uniqueness error type based on captured exception type
        flash[:uniqueness_error_type] = e.is_a?(SpecificAssociations::NonUniqueWarning) ? :warning : :error
        # set uniqueness failures (a failure is a violation of a uniqueness constraint; typically there will only be one, but multiple constraints can be applied on a column)
        # The triggering column for each failure is failure.column
        # All related columns to a failure are stored in failure.columns
        # See SSA -> lib/sequel_specific_associations/model/uniqueness_constraints.rb for more info on failure
        flash[:uniqueness_failures] = e.failures
        # set uniqueness message (short description of failure(s))
        flash[:uniqueness_message]  = e.failure_message

        # Set up variables needed to render popup
        flash[:has_assoc_obj]    = !assoc_obj.nil?
        # render uniquness popup content and store in flash[:uniqueness_popup]
        flash[:uniqueness_popup] = self.uniqueness_popup
        puts Rainbow("After calling uniqueness_popup!").cadetblue
        # Clear the changes from the change tracker (will not lose the users entered data since their input will be used to repopulate inputs on the rendered page)
        # clear_change_tracker
        GuiDirector.clear_change_tracker
      
        current_url_with_options = current_url(domain_obj, true)
        puts "MainController: gui: Uniqueness warning. Redirecting to current_page: #{current_url_with_options}" if $verbose
        # Reload the current page
        raw_redirect current_url_with_options
      rescue Gui::ConcurrencyError => e
        copy_uploads_to_tempdir!(page_state)
        flash[:params]            = safe_params # Does not work for adding, reordering, or breaking associations!
        flash[:page_state]        = page_state
        # Still a bad way to send popup to be rendered (using the flash), but that's how we're doing it.
        # FIXME it appears that we are only able to handle concurrency issues on the primary domain object.  What if there is one on something else (e.g. from inner organizer or clearview)?
        flash[:concurrency_popup] = concurrency_popup(domain_obj, e) 
        flash[:continue_edit]     = true
        GuiDirector.clear_change_tracker
        current_url_with_options  = current_url(domain_obj, true)
        puts "MainController: gui: ConcurrencyError. Redirecting to current_page: #{current_url_with_options}" if $verbose
        raw_redirect current_url_with_options
      end # changes block

      # Reenable uniqueness warnings for this thread if disabled above
      SpecificAssociations.enable_uniqueness_warnings if options[:ignore_uniqueness_warnings]
      ret
    end

    # primary_domain_obj is the domain_obj on which the page was based
    # originally we were treating all of the error messages as if they were from the primary_domain_obj, which really wasn't correct.  Exactly how to handle errors related to secondary domain objects is still somewhat undecided.  TODO implement a nice way to handle them.
    def add_errors_to_flash(errors, primary_domain_obj)
      flash[:error_messages] = flash[:error_messages] || []
      errors.each do |obj, errors_for_obj|
        next unless errors_for_obj.any?
        if obj == primary_domain_obj # can/should we use #is? here?
          errors_for_obj.each { |error| flash[:error_messages] << error }
        else
          errors_for_obj.each { |error| flash[:error_messages] << "In related #{obj.class.name.demodulize}: #{error}"}
        end
      end
      flash[:error_messages].flatten.uniq
    end
  end
end