module Gui
  class AdHocPageController < Gui::MainController
    layout(Gui.option(:layout) || 'default') { !request.xhr? }
    DEFAULT_PAGE_SIZE = Gui.option(:default_page_size)
    MAX_PAGE_SIZE     = Gui.option(:max_page_size)

    def self.data_classifier; @data_classifier; end
    def self.data_classifier=(dc); @data_classifier = dc; end
    
    private
    # Render the given object
    def render_object(additional_locals = {})
      data_classifier = self.class.data_classifier
      # Add some information to the locals hash
      locals = {:spec_title => Gui.option(:spec_title)}
      locals.merge!(additional_locals)
      # using locals as the options hash that gets passed to render
      director_args = {obj:data_classifier.new, user:current_user, view_type: :Organizer, view_name: :Details, view_classifier:data_classifier, options:locals}
      output = Gui::Director.new(**director_args).render # We get back all of the HTML that results from rendering a root vertex
      # render_view is defined in the Innate gem
      render_view(:object_wrapper, :output => output, :breadcrumbs => current_breadcrumbs, :locals => locals) do |action|
        # This causes the rendering to be treated as a layout (object_wrapper is similar to a second layout).
        # This prevents the before_all hook from running when render_view is called.
        action.options[:is_layout] = true
      end
    end  
  end
end
