module Gui
  class Controller
    def populate_jstree(host, protocol)
      error_404("Can't ceate the TreeView if not using AJAX.") unless request.xhr?
      data_classifier = safe_params['data_classifier']
      data_id         = safe_params['data_id']
      data_classifier = resolve_classifier_in_namespace(data_classifier)
      domain_obj      = domain_obj_from_data(data_classifier, data_id)
      # starting_node = Gui::TreeViewer::TreeViewNode.new(domain_obj)
      # TODO we need to be able to pass in more options to tree view and these options need to be specified in the application project
      opts = {:start_with => domain_obj, :host => host, :protocol => protocol}
      tree_view_options = Gui.option(:tree_view) || {}
      tree = Gui::TreeViewTree.new(opts.merge(tree_view_options))
      html = tree.to_html
      respond_with_hooks({:html => html, :options => tree_view_options}.to_json)
    end
  
    # FIXME policy here .. check through this
    def render_node_from_treeview
      breadcrumb_urls = safe_params['breadcrumbs']
      flash[:current_breadcrumbs] = Gui::NavigationPath.new(breadcrumb_urls)
    
      #Code for getting the data id and classifier from url passed as link if neccessary
      # regexp = /(?:https?:\/\/(?:.+?\/)?(?::[0-9]+)?)(.*)$/
      # match = regexp.match link
      # obj_link = match[1]
      # parts = obj_link.split "/"
      # data_id = parts.pop
      # data_classifier = parts.join('::')
      data_id         = safe_params['data_id']
      data_classifier = safe_params['data_classifier']
      data_classifier = resolve_classifier_in_namespace(data_classifier)
      domain_obj      = domain_obj_from_data(data_classifier, data_id)
      page_state      = build_page_state
      add_breadcrumb(current_url(domain_obj))
      flash[:prev_breadcrumbs] = flash[:current_breadcrumbs]
      view_name = :Details
      respond_with_hooks render_object(domain_obj, data_id, data_classifier, view_name, page_state)
      # respond render_object(domain_obj, data_id, data_classifier)
      # respond gui(data_classifier, data_id)
      # render_object(obj, data_id, data_classifier, view_name = :Details, view_classifier = nil, view_type = :Organizer, simple_layout = true, page_state = nil)
    end 
  end
end