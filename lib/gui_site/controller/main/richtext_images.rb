module Gui
  class MainController < Gui::Controller
    private
    # Object image manipulation methods
    # Returns a file stream of the image file with the given filename from the 'images' collection
    def view_image(domain_obj, image_filename, page_state)
      raise Gui::Error500 unless domain_obj.is_a?(Gui_Builder_Profile::RichText) # Cannot access images for object other than RichText
      handle_errors_for(page_state, domain_obj) {
        _, image = richtext_image(domain_obj, image_filename)
        send_file_stream image.filename, image.data, image.mime_type
      }
    end
    # Deletes the image file with the given filename from the 'images' collection
    def delete_image(domain_obj, image_filename, page_state)
      raise Gui::Error500 unless domain_obj.is_a?(Gui_Builder_Profile::RichText) # Cannot delete images for object other than RichText
      handle_errors_for(page_state, domain_obj) {
        GuiDirector.start_change_tracker unless GuiDirector.change_tracker_is_started?
        rti, image = richtext_image(domain_obj, image_filename)
        image.destroy
        rti.destroy
        GuiDirector.commit_change_tracker
        respond_with_hooks "Deleted image."
      }
    end
    # Returns the File and RichTextImage associated with the given filename from the 'images' collection
    def richtext_image(domain_obj, image_filename)
      raise Gui::Error500 unless image_filename # No image filename specified 
      # TODO: this is inefficiently getting all images from the DB. Fix it.
      images = domain_obj.images.map { |i| [i, i.image] }
      rti, image = images.select { |_, i| i.filename == image_filename }.first
      unless image
        if request.xhr?
          respond_with_hooks("Image not found with filename: #{image_filename.inspect}", 404) unless image
        else
          raise Gui::Error404.new("Image not found with filename: #{image_filename.inspect}")
        end
      end
      [rti, image]
    end
  end
end
