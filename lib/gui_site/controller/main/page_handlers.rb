module Gui
  class MainController < Gui::Controller
    
    # These page handlers define what to do with the user's changes to the submitted page, and
    # redirect to another page
    # ------------ Define various page handlers here ------------
    # Discard pending changes and go to the specified url
    def discard_and_go(domain_obj)
      GuiDirector.clear_change_tracker
      url = insert_url_assoc_id(request.safe_relative_url_param('go_to_url'), domain_obj)
      puts "MainController: gui: discard_and_go chosen. object not saved. Redirecting to new page: #{url}" if $verbose
      raw_redirect url
    end
    def insert_url_assoc_id(url, domain_obj)
      return url unless domain_obj.id
      url.sub('assoc-id=&', "assoc-id=#{domain_obj.id}&")
    end
    # def save_and_close
    #   GuiDirector.commit_change_tracker
    #   respond '<html><head></head><body onload="window.close();"></body></html>'
    # end
    def save_and_go(domain_obj)
      GuiDirector.commit_change_tracker
      # Get URL from params and insert domain obj's id into "assoc-id=" param
      url = insert_url_assoc_id(request.safe_relative_url_param('go_to_url'), domain_obj)
      add_breadcrumb(current_url(domain_obj))
      raw_redirect url
    end
    def save_and_create_another(domain_obj_class)
      GuiDirector.commit_change_tracker
      # Remove last page from breadcrumbs (so we don't stack breadcrumbs when we reload the same page)
      remove_last_breadcrumb
      raw_redirect(Gui.create_url_from_hash(domain_obj_class, 'new', current_options(nil, true)))
    end
    def save_and_leave(domain_obj, previous_url)
      GuiDirector.commit_change_tracker
      add_breadcrumb(current_url(domain_obj))
      remove_last_breadcrumb
      raw_redirect(previous_url)
    end
    def save_and_stay(domain_obj)
      GuiDirector.commit_change_tracker
      current_url = current_url(domain_obj)
      add_breadcrumb(current_url)
      raw_redirect(current_url)
    end
    def advance_workflow(domain_obj)
      GuiDirector.commit_change_tracker
      user_roles = user.roles.map(&:name)
      puts "user roles is #{user_roles}"
      event_name = nil
      domain_obj.handle_state_transition(event_name, user_roles)
      url = domain_obj.get_state_page_url
      raw_redirect(url)
    end
    # ------------ End page handler definitions ------------
  end
end