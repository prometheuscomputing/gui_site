module Gui
  class MainController < Gui::Controller
    # Handles deleting a domain object or objects from collection via the "Delete" button
    def del_selected
      error_404("del_selected must be called via AJAX") unless request.xhr?
      # FIXME policy here !!!!
      @objects_to_delete = {}
      obj_data_list = safe_params['objectsToDelete']
      obj_data_list.each do |obj_data|
        klass, id = obj_data.split('___')
        domain_obj = klass.to_const[id.to_i]
        raise "Could not find domain object in del_selected" unless domain_obj
        @objects_to_delete[domain_obj] = domain_obj.compositions(false).reject do |obj|
          klass = obj.class
          klass.primitive? || klass.enumeration? || obj.is_a?(Gui_Builder_Profile::BinaryData)
        end
        @objects_to_delete
      end
    end
    
    def confirmedDeleteObjects
      error_404("del_selected must be called via AJAX") unless request.xhr?
      obj_data_list = safe_params['objectsToDelete']
      objects_to_delete = []
      
      obj_data_list.values.each do |obj_hash|
        data_id = obj_hash['id']
        data_classifier = obj_hash['classifier']
        domain_obj = data_classifier.to_const[data_id.to_i]
        raise "Could not find domain object in del_selected" unless domain_obj
        objects_to_delete << domain_obj
      end 
      
      GuiDirector.start_change_tracker unless GuiDirector.change_tracker_is_started?
      # FIXME policy here !
      objects_to_delete.each(&:destroy)
      GuiDirector.commit_change_tracker
      respond_with_hooks 'Delete Processed!'
      
    end
  end
end
