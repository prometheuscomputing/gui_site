module Gui
  class MainController < Gui::Controller
    def get_pressed_action_button(page_state)
      buttons = get_action_buttons(page_state)
      pressed_buttons = buttons.select { |b| safe_params.key?(b['action_button']) }
      raise "More than one action button was pressed." if pressed_buttons.count > 1
      pressed_buttons.first
    end
  
    def get_action_buttons(page_state)
      buttons = []
      page_state.each do |w|
        case w['type']
        when 'simple'
          buttons << w if w['widget'] == 'button'
        when 'view'
          buttons += get_action_buttons(w['content'])
        end
      end
      buttons
    end
  
    def process_action_button(button, domain_obj, assoc_obj, page_state)
      action = button['getter']
      # Ensure that the button action is a valid attribute getter
      # TODO: This won't necessarily be an attribute getter. Need a new 'operations' hash
      raise "Invalid getter: #{action} for #{domain_obj.class}" unless domain_obj.class.attributes.key?(action.to_sym)
    
      handle_errors_for(page_state, domain_obj, assoc_obj) {
        # NOTE: temporarily disabled button_widget validation actions
        # TODO: shouldn't this be a part of the model instead? -SD
        # if button_widget.validation_action
        #   valid = domain_obj.gb_send_for_button_action(button_widget.validation_action)
        #   raw_redirect current_url(domain_obj) unless valid
        # end
       
        case button['display_result'].to_sym
        when :none
          # Activate button action -- Should have this switch on option :accessor/:code
          domain_obj.gb_send_for_button_action(action)
          # Do nothing and let page load continue as normal
        when :popup
          result = domain_obj.gb_send_for_button_action(action)
          # Display results in a popup
          flash[:action_popup] = self.action_popup(button['label'], result)
        when :file
          # Need to first save page, then on reload, download resulting pdf
          flash[:trigger_action_download] = self.trigger_action_download("?action_download=#{action}")
          #self.action_file(button['label'], result)
        when :web_page
          result = domain_obj.gb_send_for_button_action(action)
          # Interpret results as entire page -- untested
          respond_with_hooks result
        else
          raise "Unknown display_result type #{button['display_result']}"
        end
        domain_obj.save
      }
      GuiDirector.commit_change_tracker
      # Add url to breadcrumbs.
      current_url = current_url(domain_obj)
      add_breadcrumb(current_url)
      raw_redirect current_url
    end
  end
end
  