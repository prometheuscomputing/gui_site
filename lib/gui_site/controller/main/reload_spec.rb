module Gui
  class MainController < Gui::Controller
    def load_spec
      Gui.load_specs
      respond_with_hooks "true"
    end
  end
end
