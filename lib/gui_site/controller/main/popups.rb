require 'base64'
module Gui
  class MainController < Gui::Controller  
    
    def render_association_from_domain_obj(domain_obj, assoc_info, options = {}, view_classifier = nil)
      director_args = {obj:domain_obj, getter:assoc_info[:getter], user:current_user, view_name:options.delete(:view_name), view_classifier:view_classifier, options:options}
      Gui::Director.new(**director_args).render
    end
    
    def uniqueness_popup
      conflicts = []
      flash[:uniqueness_failures].each { |f| conflicts += f.conflicts }
      conflicts.uniq!
      if conflicts.size > 1
        raise "Multiple uniqueness conflicts received.  It is impossible to build a Vertex from an array.\n#{conflicts.inspect}"
      end
      locals = {}
      locals[:conflicting_obj_content] = self.render_uniqueness_conflict(conflicts.first)
      locals[:is_warning] = (flash[:uniqueness_error_type] == :warning)
      template_path = File.join(VIEW_DIR, 'uniqueness_popup.haml')
      Haml::Engine.new(File.read(template_path), :filename => template_path).render(self, locals)
    end
    
    def render_uniqueness_conflict(domain_obj)
      # TODO if need be, it would probably be possible to do this once for each domain obj and then join the resulting HTML.  Question though is whether the rest of the system is ready to handle multiple selection widgets in a single uniqueness popup?
      options = {
        :controller_type     => 'CollectionContent',
        :show_filter_row     => false,
        :show_possible_assoc => false
      }    
      Gui::Director.new(obj:domain_obj, user:current_user, options:options).render(:selection)
    end
  
    def image_upload_popup
      getter      = safe_params['getter']
      @pretty_id  = safe_params['pretty_id']
      domain_obj  = domain_obj_from_params
      through_obj = domain_through_obj_from_params if safe_params['through_id']
      invalid_domain_object_getter = domain_obj.class.attributes.key?(getter.to_sym)
      invalid_through_getter = (through_obj && through_obj.class.attributes.key?(getter.to_sym))
      respond_with_hooks "Invalid getter" unless invalid_domain_object_getter || invalid_through_getter
      @richtext = invalid_through_getter ? through_obj.send(getter) : domain_obj.send(getter)
      @richtext_getter = getter
      @object_url = Gui.create_url_from_object(domain_obj) + '/' + @richtext_getter
      @uploaded_images = []
      @uploaded_images = @richtext.images.map { |rti| i = rti.image; [i, ::Base64.strict_encode64(i.data)] } if @richtext
    end
 
    def action_popup(title, action_result)
      locals = {}
      locals[:title] = title
      locals[:action_result] = action_result
      template_path = File.join(VIEW_DIR, 'action_popup.haml')
      # return popup_html
      Haml::Engine.new(File.read(template_path), :filename => template_path).render(self, locals)
    end
  end
end