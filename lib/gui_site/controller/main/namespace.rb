module Gui
  class MainController < Gui::Controller
    # Note common/constants provides Module#namespace.  This overrides that (unfortunately) and also does something different.  It is concerned with the namespace of the domain model (or package) that this is a controller for.  This is really kinda crappy and maybe there is a better way...  FIXME ?
  
    # Define namespace class attribute for this controller
    class << self; attr_accessor :namespace; end
    # Initialize namespace to Object for MainController
    # This will be overridden by subclasses of MainController
    self.namespace = Object
      
    # Make class-level namespace available at instance level
    def namespace
      self.class.namespace
    end
  
    # Resolve the passed classifier. If not resolved, error_404
    def resolve_classifier_in_namespace(classifier, required = true)
      raise unless classifier
      classifier = classifier.to_const(namespace) unless classifier.is_a?(Class)
      raise unless classifier.is_a?(Class)
      classifier
    rescue NameError, RuntimeError
      # TODO: give this a better error when classifier is nil or something other than a class
      if required
        # puts; puts "  ** PROBLEM! ** "*6
        # puts e.backtrace
        # puts e.message
        error_message = "Unable to resolve classifier #{classifier} in namespace #{namespace}.  Requested path was #{request.env['REQUEST_PATH']}"
        puts error_message
        raw_redirect(default_url) if flash[:from_login]
        raise Gui::Error404.new(error_message)
      end
    end
  end
end
