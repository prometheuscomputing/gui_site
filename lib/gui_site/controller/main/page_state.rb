module Gui
  class MainController < Gui::Controller
    # Create the page state from the combination of the widget_structure and params
    def build_page_state
      return if safe_params['widget_structure'].nil?
      structure = JSON.parse(safe_params['widget_structure'])
      # puts "PARAMS"; pp safe_params.reject { |k,v| k == 'widget_structure' }
      add_params_to_structure!(structure)
      structure
    end

    # Recursively insert parameters into structure (replacing previous 'parameter name' value)
    def add_params_to_structure!(structure)
      return unless structure
      structure.each do |widget_info|
        # if widget_info["label"] == 'Photo'
        # # if widget_info["type"] == 'simple'
        #   puts;puts "#{widget_info["label"]} before";pp widget_info.select { |k,v| ['data', 'initial_data', 'initial_data_in_params'].include? k }
        # end
        widget_info['data'].each do |name, param_name|
          # Assign the actual parameter to the data hash
          # Note that '[]' is stripped from the end of the param name to match the same behavior by the browser
          # when it submits parameters.
          widget_info['data'][name] = safe_params[param_name.sub(/\[\]$/, '')]
        end
        # Overrides the normal initial_data values if the data was stored in a parameter, if any.
        # Necessary for values that require special formatting, such as textarea's
        widget_info['initial_data_in_params'].each do |name, param_name|
          widget_info['initial_data'][name] = safe_params[param_name.sub(/\[\]$/, '')]
        end
        # if widget_info["label"] == 'Photo'
        # # if widget_info["type"] == 'simple'
        #   puts "#{widget_info["label"]} after";pp widget_info.select { |k,v| ['data', 'initial_data', 'initial_data_in_params'].include? k }
        # end
        add_params_to_structure!(widget_info['content'])
      end
      structure
    end
  end
end
