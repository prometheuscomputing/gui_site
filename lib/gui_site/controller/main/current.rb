module Gui
  class MainController < Gui::Controller
  
    def current_url(domain_obj, with_assoc_info = false)
      Gui.current_url(domain_obj, current_options(domain_obj, with_assoc_info))
    end
  
    def current_options(domain_obj = nil, with_assoc_info = false)
      view_hash = {}
      
      view_hash['view-name']       = safe_params['view-name'] if safe_params['view-name']&.[](0)
      
      view_hash['view-classifier'] = safe_params['view-classifier'] if safe_params['view-classifier']&.[](0) && (domain_obj.nil? || (safe_params['view-classifier'] != domain_obj.class.to_s))
      
      view_hash['view-type']       = safe_params['view-type'] if safe_params['view-type'] && !safe_params['view-type'].empty?
      
      if with_assoc_info && safe_params['assoc-classifier'] && safe_params['assoc-id'] && safe_params['assoc-getter']
        view_hash['assoc-classifier'] = safe_params['assoc-classifier']
        view_hash['assoc-id']         = safe_params['assoc-id']
        view_hash['assoc-getter']     = safe_params['assoc-getter']
      end
      
      view_hash
    end
  end
end
