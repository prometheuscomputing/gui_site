module Gui
  class MainController < Gui::Controller
    def parse_domain_obj_changes_from_state(domain_obj, assoc_obj, assoc_getter, page_state, ignore_uniqueness_warnings)
      handle_errors_for(page_state, domain_obj, assoc_obj, :ignore_uniqueness_warnings => ignore_uniqueness_warnings) {
        # TODO: merge #associate_domain_obj_to_assoc_obj with #apply_form_changes
        associate_domain_obj_to_assoc_obj(domain_obj, assoc_obj, assoc_getter)
        domain_obj_cache = assoc_obj ? {[assoc_obj.id.to_s, assoc_obj.class.to_s] => assoc_obj} : {}
        results = FormParser.new.apply_form_changes(domain_obj, page_state, domain_obj_cache)
        updated_fields     = {}
        new_assocs         = {}
        broken_assocs      = {}
        deleted_assocs     = {}
        errors             = {}
        uniqueness_errors  = {}
        concurrency_issues = {}
        warnings           = []
        results.each do |result_hash|
          changed_obj = result_hash[:domain_obj]
          result      = result_hash[:changes]
          updated_fields[changed_obj]     = result[:updated_fields] if result[:updated_fields] && result[:updated_fields].any?
          new_assocs[changed_obj]         = result[:new_assocs] if result[:new_assocs] && result[:new_assocs].any?
          broken_assocs[changed_obj]      = result[:broken_assocs] if result[:broken_assocs] && result[:broken_assocs].any?
          deleted_assocs[changed_obj]     = result[:deleted_assocs] if result[:deleted_assocs] && result[:deleted_assocs].any?
          errors[changed_obj]             = result[:errors] if result[:errors] && result[:errors].any?
          uniqueness_errors[changed_obj]  = result[:uniqueness_errors] if result[:uniqueness_errors] && result[:uniqueness_errors].any?
          concurrency_issues[changed_obj] = result[:concurrency_issues] if result[:concurrency_issues] && result[:concurrency_issues].any?
          warnings = result[:warnings]
        end
              
        # We need to wait until all form parsing has finished before raising any concurrency errors
        concurrency_errors = concurrency_popup_errors_from_issues(concurrency_issues) # not every issue is an actionable error...for now
        if concurrency_errors.any?
          updated_fields = []# We needed to clear updated fields, as they won't actually be updated if any concurrencies exist
          raise Gui::ConcurrencyError.new(concurrency_errors)
        end

        if uniqueness_errors.any?
          # Will only raise if Gui::ConcurrencyError has not been raised. Ordering in the code here is important.
          # Raising only the first object encountered, it's the way it was working before. Could modify later to raise all at once.
          # puts "UNIQUENESS ERRORS:"
          # pp uniqueness_errors
          raise uniqueness_errors.values.flatten.first
        end
      
        flash[:edited_object]        = "Updated the fields: #{updated_fields.values.flatten.join(', ')}." if updated_fields.any?
        flash[:new_associations]     = new_assocs     #if new_assocs.any?
        flash[:broken_associations]  = broken_assocs  #if broken_assocs.any?
        flash[:deleted_associations] = deleted_assocs #if deleted_assocs.any?
        flash[:warnings]             = warnings
        add_errors_to_flash(errors, domain_obj)
      }
    end
  
    private
    # Associate assoc_obj to domain_object (possibly indirectly through a chained getter)
    def associate_domain_obj_to_assoc_obj(domain_obj, assoc_obj, assoc_getter)
      if assoc_obj
        # # Ignoring complex (chained) assoc_getters for now.
        # assoc = domain_obj.class.associations.select { |name, info| info[:opp_getter] == assoc_getter }
        # domain_obj.send(assoc[:getter] + '_add', assoc_obj) rescue SpecificAssociations::FailedSubsetConstraint
      
        #puts "Associating #{assoc_obj.inspect} to \n#{domain_obj.inspect}"
        # Save the domain obj in order to give it an ID (Currently SCT is not smart enough to do this for you)
        domain_obj.save
        getter_path = assoc_getter.split('.')
        last_getter_method = getter_path.pop # Last getter must be handled separately
        # assoc_obj_association_info = domain_obj.class.associations.values.select { |i| i[:opp_getter].to_s == final_getter_method }.first

        # Need to traverse association path from object to be associated (assoc_obj) to last object in getter chain before domain object (This
        #    is the object to which the domain object needs to be associated)
        # If the getter is a simple getter, then the following loop will not execute
        current_obj = assoc_obj
        getter_path.each do |getter|
          info = current_obj.class.associations[getter.to_sym]
          unless info
            raise "Could not find information for association '#{getter}' in associations info for #{current_obj.class}: #{current_obj.class.associations}\n" +
                "While traversing the assoc_getter: #{assoc_getter} from #{assoc_obj.inspect}"
          end
          unless info[:type].to_s =~ /to_one$/ || info[:type].to_s =~ 'associates'
            raise "Bad traversal path using the assoc_getter: #{assoc_getter} from #{assoc_obj.inspect}. Association #{getter} is not to_one and cannot be traversed."
          end
          current_obj = current_obj.send(getter)
          if current_obj.nil?
            raise "Error when traversing path: #{assoc_getter} from #{assoc_obj.inspect}. Association #{getter} does not have a value."
          end
        end

        # We now have all the information we need to associate the last_assoc_obj to the domain object
        last_assoc_obj = current_obj
        last_assoc_info = last_assoc_obj.class.associations[last_getter_method.to_sym]
        through_obj_getter = last_assoc_info[:opp_getter].to_s.singularize
        begin
          if last_assoc_info[:type].to_s =~ /to_one$/ || last_assoc_info[:type].to_s == 'associates'
            # TODO: make this work so that the association can be viewed before the page is saved.
            #       Problem: there is no add_<prefix>_through method
            if last_assoc_info[:through] && domain_obj.is_a?(last_assoc_info[:through].to_const(self.class.namespace))
              domain_obj.send("#{through_obj_getter}=", last_assoc_obj)
            else
              last_assoc_obj.send("#{last_getter_method}=", domain_obj)
            end
          else # to_many
            if last_assoc_info[:through] && domain_obj.is_a?(last_assoc_info[:through].to_const(self.class.namespace))
              domain_obj.send("#{through_obj_getter}=", last_assoc_obj)
            else # Object being associated is not an Association Class
              last_assoc_obj.send("#{last_getter_method}_add", domain_obj)
            end
          end
        rescue SpecificAssociations::FailedSubsetConstraint => e
          e
          # Do nothing
          # TODO: find a way to report this failure to the user.
        end
      end
    end
  end
end
