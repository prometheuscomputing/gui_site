module Gui
  class MainController < Gui::Controller

    private
    
    # Copy any file uploads to a temporary directory so they won't get cleaned up too soon.
    # This method is invoked in the case that a validation error was raised, and the file can't
    # immediately be saved to the database (and we don't want to force the user to re-upload).
    def copy_uploads_to_tempdir!(page_state)
      file_widgets = get_file_widgets(page_state)
      file_widgets.each { |fw| move_upload_to_tempfile(fw) }
    end
  
    # Given the form_info structure, retrieve all 'file' widget parameter names.
    def get_file_widgets(page_state)
      file_widgets = []
      page_state.each do |info|
        if info['widget'] == 'file'
          file_widgets << info
        elsif info['widget'] == 'organizer'
          file_widgets += get_file_widgets(info['content']) if info['content']
        end
      end
      file_widgets
    end
  
    # Move a Rack Multipart Tempfiles to a new file (to prevent being cleaned up too soon)
    def move_upload_to_tempfile(file_widget)
      # TODO: File's form data is incorrectly stored in 'filename'
      file_info = file_widget['data']['filename']
      return unless file_info
      tempfile = file_info[:tempfile]

      return unless File.exist?(tempfile)
      new_file = File.join(Gui.option(:tempfile_dir), file_info[:filename])
      # TODO: write cleanup for TEMPFILE_DIR. Currently these files will persist forever.
      #       Since this only occurs on validation error when uploading a file, it's a rare enough case
      #       to ignore for now. -SD
      FileUtils.mv(file_info[:tempfile], new_file)
      file_info[:tempfile] = File.new(new_file)
    end
  end
end
