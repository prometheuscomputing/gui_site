module Gui
  class MainController < Gui::Controller
    # Parse markdown preview
    def markdown_preview
      error_404("markdown_preview must be called via AJAX") unless request.xhr?
      @markdown = request.safe_param('data')
    end
  
    def kramdown_preview
      error_404("kramdown_preview must be called via AJAX") unless request.xhr?
      @kramdown = request.safe_param('data')
    end
  
    # Parse textile preview
    def textile_preview
      @textile = request.safe_param('data')
    end

    def latex_preview render_to = 'html'
      if request.post?
        case render_to
        when 'html'
          return nil
        when 'pdf'
          result_pdf = Gui::Latex.render_data_to_pdf(request.safe_param('data'), :add_preamble => true)
          flash[:message] = "Compiling of LaTex file #{result_pdf.nil? ? 'had errors' : 'was successful'}"
          send_file_stream('output.pdf', result_pdf) unless result_pdf.nil?
        end
      else
        return "No information given to process"
      end
    end
  end
end
