module Gui
  class GbUsersController < Gui::Controller
    # admin control panel
    def user_admin
      raise Gui::Error403.new("I'm sorry, you may not access the organization administration panel without administrative privileges.") unless session[:is_admin]
          
      @all_users = Gui_Builder_Profile::User.all
      @all_users_with_roles = {}
      @all_users.each { |u| @all_users_with_roles[u] = u.roles }
      @roles                 = Gui_Builder_Profile::UserRole.all
      @invitations           = Gui_Builder_Profile::InvitationCode.all
      @project_options       = Gui_Builder_Profile::ProjectOptions.ct_instance
      @email_requirements    = @project_options.email_requirements
      @password_requirements = @project_options.password_requirements
    
      if request.post?
        # Start change tracker
        GuiDirector.start_change_tracker
      
        # Handle Users
        if safe_params['users_to_del']
          deleted_users = delete_users(safe_params['users_to_del'], @all_users)
          # Remove deleted users from @all_users
          @all_users -= deleted_users
          @all_users_with_roles.reject! { |user, _| deleted_users.include?(user) }
        end
        unless safe_params['new_login'].empty?
          new_user = add_user(@password_requirements, @email_requirements, [])
          if new_user
            @all_users << new_user
            @all_users_with_roles[new_user] = new_user.roles
          end
        end
      
        # Handle User/Role assignments
        # users_roles is a hash of arrays where
        # the key is the user_id and the value is an array of role_ids
        if safe_params['users_roles']
          modified_users = []
          @all_users_with_roles.each do |this_user, old_roles|
            new_role_ids = safe_params['users_roles'][this_user.id.to_s]
            next unless new_role_ids
            is_current_user = (this_user.pk == current_user.pk)
            # Get rid of the empty placeholder value that is needed in the event all roles are removed
            new_role_ids.shift
            # Handle role changes
            # NOTE: if any roles are hidden, this causes them to get unset on form submission. -SD
            new_roles = set_user_roles(this_user, new_role_ids, old_roles, @roles, is_current_user)
            modified_users << this_user unless Set.new(old_roles) == Set.new(new_roles)
            old_roles.replace(new_roles)
          end
          flash[:info_messages] << "Changed roles for user#{modified_users.length > 1 ? 's' : ''}: &nbsp; #{modified_users.collect(&:login).join(', ')}" if modified_users.any?
        end
      
        # Handle Email Requirements
        if safe_params['email_requirements_to_del']
          email_requirements_to_del = @email_requirements.select { |er| safe_params['email_requirements_to_del'].include?(er.id.to_s) }
          email_requirements_to_del.each(&:destroy)
          flash[:info_messages] << "Deleted Email Requirement#{email_requirements_to_del.length > 1 ? 's' : ''}: &nbsp; #{email_requirements_to_del.collect(&:regular_expression).join(', ')}"
          # Remove deleted email_requirements from @email_requirements
          @email_requirements -= email_requirements_to_del
        end
        if safe_params['email_requirement_regexp'].to_s.strip[0] # if not empty
          begin
            email_requirement = add_regexp_to_email_requirements(@project_options)
            flash[:info_messages] << "Email Requirement '#{email_requirement.regular_expression}' created"
            # Add new requirement to @email_requirements
            @email_requirements << email_requirement
          rescue ArgumentError # ArgumentError is raised by String#to_regexp if the resulting regular expression is malformed
            flash[:error_messages] << "Invalid Email Requirement: '#{safe_params['email_requirement']}'. Must be a valid regular expression."
          end
        end
      
        # Handle Invitation Codes
        if safe_params['invitations_to_del']
          invitations_to_del = @invitations.select { |i| safe_params['invitations_to_del'].include?(i.id.to_s) }
          invitations_to_del.each do |i|
            # Remove deleted invitation from @invitations
            @invitations.delete(i)
            i.destroy
          end
          flash[:info_messages] << "Deleted Invitation Code#{invitations_to_del.length > 1 ? 's' : ''}: &nbsp; #{invitations_to_del.collect(&:code).join(', ')}"
        end
        if safe_params['invitation_code_code'].to_s.strip[0]
          invitation_code = add_invitation_code
          flash[:info_messages] << "Invitation Code '#{invitation_code.code}' created"
          # Add new requirement to @email_requirements
          @invitations << invitation_code
        end
        
        GuiDirector.commit_change_tracker
      end
    end
    def add_invitation_code(code = nil, expires = nil, uses_remainin = nil)
      code          ||= safe_params['invitation_code_code']
      expires       ||= safe_params['invitation_code_expires']
      uses_remainin ||= safe_params['invitation_code_uses_remaining']
      Gui_Builder_Profile::InvitationCode.create(:code => code, :expires => expires, :uses_remaining => uses_remaining)
    end
  end
end