module Gui
  class GbUsersController < Gui::Controller
    # TODO this should be in a helper as well
    def send_verify_email(user=nil)
      @user = user
      email_subject = "Verify Email Account"
      # This is sent to the template to be used as the url path for the reset password link
      # This assumes assume the website url is https or localhost:7000 if the application is in testing mode.
      @site_url = Gui.option(:testing_mode) ? "http://localhost:7000" : "https://#{request.host}"
      email_body = convert_file_to_haml(File.join(VIEW_DIR, 'gb_users/verify_email_template.haml'))
      Gui::Emailer.send_email user.email, email_body, email_subject
    end
    
    # Verify a user's email address via an external link
    # The user is not assumed to be logged in
    def verify_email(id=nil, token=nil)
      # There is no user logged in so administrator privileges are required to find the user
      @user = Gui_Builder_Profile::User.find(:email_confirmation_token => token, :id => id)
      if @user
        GuiDirector.start_change_tracker
        @user.verify_email
        # There is no user logged in so administrator privileges are required to modify the user
        GuiDirector.commit_change_tracker
      
        # Hackish way to update user for the current session if there is a user logged in
        if user  
          user.email_verified = true
        end
      else
        raise Gui::Error500 # "No user associated with this information"
      end
    
    end
  
    def forgot_password
      if request.post?
        @email = safe_params['email']
        if @email && @email != ''
          # There is no user logged in, so administrator privileges are required to find the user
          @user = Gui_Builder_Profile::User.find(:email => @email)
        end
        if @user
          @email_response_message = "An email with password recovery instructions has been sent to the user associated with the email: " + @email
        
          # Uses the global option to determine if the application is being ran in testing mode to avoid email spamming
          # The email for forgot password will not be sent unless the user's email has been verified
          verified_test = Gui.option(:testing_mode) ? @user : @user && @user.email_verified
          if verified_test
            GuiDirector.start_change_tracker
            @user.generate_password_reset_info
            # There is no user logged in, so administrator privileges are required to reset the password
            GuiDirector.commit_change_tracker
          
            # The url that is sent in the email will default to localhost:7000 if the application is in testing mode
            # The url in the email will be set using the global option website_url
            @site_url = Gui.option(:testing_mode) ? "http://localhost:7000" : "https://#{request.host}"
            email_subject = "Account Password Reset"
            email_body = convert_file_to_haml(File.join(VIEW_DIR, 'gb_users/forgot_password_email_template.haml'))
            Gui::Emailer.send_email @email, email_body, email_subject
          else
            # TODO can we get the e-mail address of the site admin and provide it here?
            @user_error = "Please contact the site administrator for assistance."
          end
        else
          @user_error = "Email address not found.  Please enter the email associated with your user account."
        end
      end
    end
  
    def reset_password(token = nil)
      raise Gui::Error500 unless token # "Can't reset password without a working password reset link." 
      @user = Gui_Builder_Profile::User.find(:password_reset_token => token)
      if request.post?
        if @user
          # By default the time limit on the password reset link is 1 week from the time it is sent
          time_limit = @user.password_reset_time_limit
          if time_limit >= Time.now
            GuiDirector.start_change_tracker
            new_password = safe_params['new_password']
            reenter_new_password = safe_params['reenter_new_password']
            errors = Gui_Builder_Profile::User.validate_passwords(new_password, reenter_new_password)
            if errors.any?
              GuiDirector.clear_change_tracker
              @user_error = errors.join("<br />")
            else
              @user.reset_password(new_password)
              GuiDirector.commit_change_tracker
              @password_reset_successful_message = true
            end
          else
            # raise Gui::Error500 # "Can't reset password link has expired"
            @user_error = "The Reset Password Link has expired please retry forgot password again."
          end
        else
          # raise Gui::Error500 # "Password Reset token not associated with any users"
          @user_error = "This Password Reset link is not associated with any users."
        end
      else
        @user_error = session[:password_errors].join("<br />") if session[:password_errors]
      end
    end
  
    # This page is shown to the user directly after logging in, if their current password is invalidated by new rules.
    def change_password
      raise Gui::Error403.new("I'm sorry, you may not change your password right now.") unless session[:allow_password_change] #"Can't change password since allow_password_change was not set."
      if request.post?
        # Skip password change for now if 'Skip' button was clicked
        if safe_params['skip']
          requested_before_login = session[:requested_before_login]
          session.delete(:requested_before_login)
          session.delete(:password_errors)
          session.delete(:allow_password_change)
          raw_redirect requested_before_login || '/'
        end
      
        GuiDirector.start_change_tracker
        new_password         = safe_params['new_password']
        reenter_new_password = safe_params['reenter_new_password']
        errors = Gui_Builder_Profile::User.validate_passwords(new_password, reenter_new_password)
      
        if errors.any?
          GuiDirector.clear_change_tracker
          @user_error = errors.join("<br />")
        else
          user.change_password(new_password)
          GuiDirector.commit_change_tracker
          requested_before_login = session[:requested_before_login]
          session.delete(:requested_before_login)
          session.delete(:password_errors)
          session.delete(:allow_password_change)
          raw_redirect requested_before_login || '/'
        end
      else
        @user_error = session[:password_errors].join("<br />")
      end
    end
    
  end
end
