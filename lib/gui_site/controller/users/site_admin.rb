module Gui
  class GbUsersController < Gui::Controller
    def db_download
      raise Gui::Error405 unless Gui.option(:enable_db_download)
      raise Gui::Error403.new("I'm sorry, you may not download the database without administrative privileges.") unless session[:is_admin]
      db_data = File.read(Gui.option(:db_location))
      send_file_stream('SQLite.db', db_data, 'application/x-sqlite3')      
    end
    
    def send_file_stream(name, file_data, content_type = nil, content_disposition = nil)
      content_type ||= Rack::Mime.mime_type(File.extname(name))
      content_disposition ||= 'attachment'
      header = {}
      header['Content-Length'] = Rack::Utils.bytesize(file_data).to_s
      header['Content-Type'] = content_type
      header['Content-Disposition'] = "#{content_disposition}; filename = #{name}"
      respond_with_hooks file_data, 200, header
    end
    
    def site_admin
      # Kick out non-site-admins
      raise Gui::Error403.new("I'm sorry, you may not access the site admininistration panel without administrative privileges.") unless session[:is_admin]
      @admins         = Gui_Builder_Profile::UserRole.admin.users
      @project_opts   = Gui_Builder_Profile::ProjectOptions.ct_instance
      @passwd_rules   = @project_opts.password_requirements
      @perspectives   = Gui_Builder_Profile::Perspective.all
      @roles          = Gui_Builder_Profile::UserRole.all
      
      # TODO: split out individual operations as new functions (delete_users, add_user, etc)
      if request.post?
        # Start change tracker
        GuiDirector.start_change_tracker
        params = request.safe_params
        # Handle Users
        if params['site_admins_to_del']
          deleted_users = delete_users(params['site_admins_to_del'], @admins)
          # Remove deleted users from @admins
          @admins -= deleted_users
        end
        unless params['new_login'].empty?
          ucs = []
          # NOTE: using @passwd_rules here means that a newly created password requirement will not take effect here.
          new_admin = add_user(@passwd_rules, [], [Gui_Builder_Profile::UserRole.admin], ucs)
          if new_admin
            @admins << new_admin
          end
        end
        if params['roles_to_del']
          deleted_roles = delete_organizations(params['roles_to_del'], @roles)
          @roles -= deleted_roles
        end
        
        # Handle Roles
        if params['roles_to_del']
          deleted_roles = delete_roles(params['roles_to_del'], @roles)
          @roles -= deleted_roles
          # @all_users_with_roles.each { |u,roles| roles -= deleted_roles }
        end
        unless params['role_name'].empty?
          new_role = add_role(params['role_name'])
          @roles << new_role if new_role
        end
        
        # Handle Password Requirements
        if params['password_requirements_to_del']
          password_requirements_to_del = @passwd_rules.select { |pr| params['password_requirements_to_del'].include?(pr.id.to_s) }
          password_requirements_to_del.each(&:destroy)
          flash[:info_messages] << "Deleted Password Requirement#{password_requirements_to_del.length > 1 ? 's' : ''}: &nbsp; #{password_requirements_to_del.join(', ')}"
          # Remove deleted password_requirements from @passwd_rules
          @passwd_rules -= password_requirements_to_del
        end
        unless params['password_requirement_regexp'].nil? || params['password_requirement_regexp'].empty?
          begin
            password_requirement = add_regexp_to_password_requirements(@project_opts)
            flash[:info_messages] << "Password Requirement '#{password_requirement.regular_expression}' created"
            # Add new requirement to @passwd_rules
            @passwd_rules << password_requirement
          rescue ArgumentError # ArgumentError is raised by String#to_regexp if the resulting regular expression is malformed
            flash[:error_messages] << "Invalid Password Requirement: '#{params['password_requirement']}'. Must be a regular expression, including slashes and any options."
          end
        end
        
        # Handle Perspectives
        if params['perspectives_to_del']
          perspectives_to_del = @perspectives.select { |psptv| params['perspectives_to_del'].include?(psptv.id.to_s) }
          perspectives_to_del.each(&:destroy)
          flash[:info_messages] << "Deleted Perspective#{perspectives_to_del.length > 1 ? 's' : ''}: &nbsp; #{perspectives_to_del.map(&:name).join(', ')}"
          # Remove deleted perspectives from @perspectives
          @perspectives -= perspectives_to_del
        end
        unless params['perspective_name'] == ''
          if !(Gui_Builder_Profile::User.valid_perspective? params['perspective_name'])
            flash[:error_messages] << "Invalid Perspective. Must be alpha-numeric."
          elsif !(Gui_Builder_Profile::User.valid_perspective? params['perspective_base'])
            flash[:error_messages] << "Invalid Perspective Base. Must be alpha-numeric."
          else
            perspective_base = params['perspective_base'].to_i > 0 ? params['perspective_base'].to_i : nil
            @new_perspective = Gui_Builder_Profile::Perspective.new_perspective(params['perspective_name'], perspective_base)
            flash[:info_messages] << "Perspective '#{@new_perspective.name}' created"
            # Add new perspective to @perspectives
            @perspectives << @new_perspective
          end
        end
        GuiDirector.commit_change_tracker
      end
    end
    # Deletes organizations based on an array of organization ids (as strings)
    # all_orgs is the set of possible organizations to be deleted
    def delete_organizations(org_ids, all_orgs)
      deleted_orgs = delete_objects_from_set(org_ids, all_orgs)
      flash[:info_messages] << "Deleted Organization#{deleted_orgs.length > 1 ? 's' : ''}: &nbsp; #{deleted_orgs.map(&:name).join(', ')}"
      deleted_orgs
    end
    private :delete_organizations
    
    def add_role(name)
      # Kick out non-admins
      raise Gui::Error403.new("I'm sorry, you may not access site administration panel without administrative privileges.") unless session[:is_admin]
      unless Gui_Builder_Profile::User.valid_role?(name)
        flash[:error_messages] << "Invalid Role name: #{name}"
        return nil
      end
      new_role = Gui_Builder_Profile::UserRole.create(:name => name)
      flash[:info_messages] << "Role '#{new_role.name}' created" if new_role
      new_role
    end
    private :add_role  
  end
end