module Gui
  class GbUsersController < Gui::Controller
    
    def preferences
      @perspectives = Gui_Builder_Profile::Perspective.all
      @notices = []
      @errors = []
      unless Gui.option(:disable_send_email)
        if !current_user.email_verified
          @notices << "This account hasn't verified its email account yet.  Do this by visiting the Confirm Email sent to your account when it was created or by reentering a new email address below."
        end
      end
      @use_accessibility = current_user.use_accessibility?
      @first_name        = current_user.first_name
      @last_name         = current_user.last_name
      
      if request.post?
        GuiDirector.start_change_tracker
        params = request.safe_params
      
        # Handle password change
        if params['old_password'] && !params['old_password'].empty?
          pwd_errors = []
          pwd_errors << "Incorrect old password entered" unless user.right_password?(params['old_password'])
          pwd_errors += Gui_Builder_Profile::User.validate_passwords(params['new_password'], params['reenter_new_password'])
        
          if pwd_errors.any?
            @errors << pwd_errors.join('<br />')
          else
            current_user.change_password(params['new_password'])
            @notices << "Successfully changed password"
          end
        end
      
        if params['email'] && !params['email'].empty?
          #TODO maybe validate email
          email_errors = Gui_Builder_Profile::User.validate_email(params['email'], [], current_user.email)
          if email_errors.any?
            @errors << email_errors.join('<br />')
          else
            current_user.change_email(params['email'])
            send_verify_email(current_user)
            @notices << "Successfully changed email.  Please verify this email by following the instructions sent to #{params['email']}."
          end
        end
        
        if @first_name.nil? && params['first_name'].empty?
          # Do nothing unless we want to require them to have a name
        elsif  @first_name != params['first_name']
          @first_name = current_user.first_name = params['first_name']
          @notices << "First name is now #{current_user.first_name}."
        end
        
        if @last_name.nil? && params['last_name'].empty?
          # Do nothing unless we want to require them to have a name
        elsif  @last_name != params['last_name']
          @last_name = current_user.last_name = params['last_name']
          @notices << "Last name is now #{current_user.last_name}."
        end
      
        # Handle perspectives
        @user_perspectives = set_user_perspectives(current_user, params['perspectives'] || [], old_perspectives = nil, @perspectives, true)
        # Handle accessibility preference
        use_accessibility_param = !!params['use_accessibility']
        if use_accessibility_param != @use_accessibility
          current_user.use_accessibility = use_accessibility_param
          current_user.save
          @use_accessibility = use_accessibility_param
          @notices << "Successfully updated accessibility preference"
        end
        GuiDirector.commit_change_tracker
      else
        @user_perspectives = current_user.perspectives
      end
    end
  
  end
end