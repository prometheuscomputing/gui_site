module Gui
  class GbUsersController < Gui::Controller
    layout(Gui.option(:layout) || 'default') {!request.xhr?}

    def login(is_login = false)
      raw_redirect(default_url) if logged_in?
      # FIXME why set instance vars here when the values are going into locals?
      @spec_title = Gui.option(:spec_short_title) || Gui.option(:spec_title)
      locals = {:spec_title => @spec_title, :spec_url => Gui.option(:spec_url)}
      @login_or_register_blurb = convert_file_to_haml(Gui.option(:login_or_register_blurb_path) || File.join(VIEW_DIR, 'gb_users/default_login_or_register_blurb.haml'), locals)
      if request.post?
        params    = request.safe_params.symbolize_keys
        @username = params[:login]
        # Make sure this was a request to log in
        if params[:request_type] == 'Login' || is_login == true
          begin
            GuiDirector.start_change_tracker
            login    = params[:login]
            password = params[:password]
            creds = {'login' => login, 'password' => password}
            print "Logging in as #{login.inspect}... " if $verbose
            if user_login(creds)            
              # Set the roles for this session to the user's current roles
              refresh_session_roles#(current_user)
              refresh_session_perspectives#(current_user)
              requested_before_login = session[:requested_before_login] || default_url
              puts Rainbow(requested_before_login).green
              
              # Check that current password complies with requirements
              password_errors = Gui_Builder_Profile::User.validate_password(creds['password'])
              if password_errors.any?
                session[:password_errors] = password_errors
                session[:allow_password_change] = true
                raw_redirect '/gb_users/change_password'
              else # No issues with password
                session.delete(:requested_before_login)
                GuiDirector.commit_change_tracker
                puts "GbUsersController: login: successful login. Redirecting to: #{requested_before_login}" if $verbose
                flash[:from_login] = true
                raw_redirect requested_before_login
              end
            else
              puts "Failure" if $verbose
              # TODO: remove raise/rescue
              raise Gui::InvalidLogin, "Invalid username and password"
            end
          rescue Gui::InvalidLogin => e
            @user_error = e.message.nl2br
          end

        elsif params[:request_type] == 'Log Out'
          #raw_redirect GbUsersController.r(:logout)
          puts "GbUsersController: login: Received Log Out request. Redirecting to: /gb_users/logout" if $verbose
          raw_redirect '/gb_users/logout'
        end
      end
    end

    # TODO: Need to find a way to test out CarExample with different settings (ex: default vs custom license aggreement file)
    def register
      raw_redirect(default_url) if logged_in?
      @first_user = Gui_Builder_Profile::User.count == 0
      @spec_title = Gui.option(:spec_short_title) || Gui.option(:spec_title)
      locals = {:spec_title => @spec_title, :spec_url => Gui.option(:spec_url)}
      @license_agreement = convert_file_to_haml(Gui.option(:license_agreement_path) || File.join(VIEW_DIR, 'gb_users/default_license_agreement.haml'), locals)
      @login_or_register_blurb = convert_file_to_haml(Gui.option(:login_or_register_blurb_path) || File.join(VIEW_DIR, 'gb_users/default_login_or_register_blurb.haml'), locals)

      if request.post?
        # Ensure roles are set up
        GuiDirector.start_change_tracker 
        Gui_Builder_Profile::UserRole.setup_roles
        GuiDirector.commit_change_tracker

        GuiDirector.start_change_tracker
        params = request.safe_params
        @first_name = params['first_name']
        @last_name = params['last_name']
        @username = params['login']
        @invitation_code = params['invitation']
        begin
          @new_account = Gui_Builder_Profile::User.register(
            :login => params['login'], 
            :password => params['password'], 
            :reenter_password => params['reenter_password'],
            :code => params['invitation'] || '',
            :email => params['email'],
            :first_name => @first_name,
            :last_name => @last_name,
            :first_user => @first_user)
          # Note that #register commits and restarts change tracker
          GuiDirector.commit_change_tracker
          puts "Registration Success" if $verbose
          # NOTE: There seems to be a race condition here.
          #       Any slow down in the database causes this to fail with a "550 Message rejected" error
          #       Sleeping here does not mitigate the problem, so it must be a deadlock or misconfiguration that occurs earlier.
          send_verify_email(@new_account)
          login(true)
        rescue Gui::InvalidLogin => e
          GuiDirector.clear_change_tracker
          @new_account = nil
          @user_error = e.message.nl2br
        end
      end
    end
    
    def logout
      user_logout
      # NOTE: user_logout appears to fail to reset the session and sid (the first time it is run?).
      # Manually clearing session and resetting the sid here to compensate. -SD
      session.clear
      session.resid!
    end
  
  end
end
