module Gui
  class GbUsersController < Gui::Controller

    def edit_perspective
      params = request.safe_params
      @perspective = Gui_Builder_Profile::Perspective[params['perspective'].to_i]
      @perspective_substitutions = @perspective.substitutions
      @p_name = @perspective.name
      if request.post?
        GuiDirector.start_change_tracker
        unless params['master_word'] == ''
          if !(Gui_Builder_Profile::User.valid_word? params['master_word'])
            flash[:error_messages] << "Invalid Master Word. Must be alpha-numeric."
          elsif !(Gui_Builder_Profile::User.valid_word? params['replacement_word'])
            flash[:error_messages] << "Invalid Replacement Word. Must be alpha-numeric."
          else
            # If there is an existing substitution with the same master_word, use that.
            existing = @perspective_substitutions.select { |s| s.master_word == params['master_word'] }.first
            if existing
              @new_substitution = existing
            else
              @new_substitution = Gui_Builder_Profile::MultivocabularySubstitution.create
              # Set the master word
              @new_substitution.master_word = params['master_word']
              @perspective.add_substitution(@new_substitution)
              # Add new substitution to @perspective_substitutions
              @perspective_substitutions << @new_substitution
            end
            # Set the replacement word
            @new_substitution.replacement_word = params['replacement_word']
            @new_substitution.save
            flash[:info_messages] << "Substitution of '#{@new_substitution.replacement_word}' for '#{@new_substitution.master_word}' added."
          end
        end
    
        if params['subs_to_del']
          subs_to_del = @perspective_substitutions.select { |s| params['subs_to_del'].include?(s.id.to_s) }
          subs_to_del.each(&:destroy)
          flash[:info_messages] << "Deleted Substitution#{subs_to_del.length > 1 ? 's' : ''}: &nbsp; #{subs_to_del.map { |s| '"' + s.replacement_word + '"' + ' for ' + '"' + s.master_word + '"' }.join(', ')}"
          # Remove deleted substitutions from @perspective_substitutions
          @perspective_substitutions -= subs_to_del
        end
    
        # Set up session[:substitutions] if multivocabulary is enabled
        # TODO: This will only update for the current_user. Need a mechanism to update for all users.
        #       As-is, this means other users' substitutions will not be updated until logout/login.
        session[:substitutions] = session[:perspectives].map(&:substitutions).flatten if Gui.option(:multivocabulary)
        GuiDirector.commit_change_tracker
      end
    end
    
  end
end