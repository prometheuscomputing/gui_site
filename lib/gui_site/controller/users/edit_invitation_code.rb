module Gui
class GbUsersController < Gui::Controller

    def edit_invitation_code
      params = request.safe_params
      id = params['invitation_code']
      if id == "new"
        @expires = nil
        @uses_remaining = nil
        @code = nil
        @roles_granted = []
      elsif id.match?(/^\d+$/)
        @invitation_code  = Gui_Builder_Profile::InvitationCode[params['invitation_code'].to_i]
        @expires = format_date @invitation_code.expires
        @uses_remaining = @invitation_code.uses_remaining
        @code = @invitation_code.code
        @roles_granted = @invitation_code.roles_granted
      else
        # FIXME Somebody manually put in a bad param in the url
        # TODO: use correct error system instead of raising. Still better than letting this case fall through! -SD
        raise "Cannot edit invitation code: #{id.inspect}"
      end

      @roles = Gui_Builder_Profile::UserRole.all
  
      if request.post?
        flash[:info_messages]   = []
        flash[:error_messages]  = []
        GuiDirector.start_change_tracker
        if id == "new"
          @invitation_code  = Gui_Builder_Profile::InvitationCode.new
        end
        
        if params['invitation_code_code'].empty?
          flash[:error_messages] << "The 'Code' value may not be empty!"
          
        elsif @invitation_code.code != params['invitation_code_code']
          @invitation_code.code = params['invitation_code_code']
          flash[:info_messages] << "The invitation code is now #{@invitation_code.code}."
        end
        
        if @invitation_code.uses_remaining.nil? && params['invitation_code_uses_remaining'].empty?
          # Do nothing
        elsif @invitation_code.uses_remaining.to_s != params['invitation_code_uses_remaining']
          new_value = params['invitation_code_uses_remaining']
          if new_value.nil? || new_value.strip.empty?
            @invitation_code.uses_remaining = nil
            flash[:info_messages] << "The invitation code has an unlimited number of uses."
          elsif new_value.strip.match?(/^\d+$/)
            @invitation_code.uses_remaining = new_value.to_i
            flash[:info_messages] << "The invitation code may now be used #{new_value.to_i} times."
          else
            flash[:error_messages] << "The number of remaining uses must be entered as a positive integer value."
          end
        end
        
        if @invitation_code.expires.nil? && params['invitation_code_expires'].empty?
          # Do nothing
        elsif @expires != params['invitation_code_expires']
          new_value = params['invitation_code_expires']
          if new_value.nil? || new_value.empty?
            @invitation_code.expires = nil
            @expires = nil
            flash[:info_messages] << "The invitation code does not expire."
          else
            begin
              expiration_date = Date.parse(params['invitation_code_expires'])
              @invitation_code.expires = expiration_date
              today = Date.today
              delta = (expiration_date - today).to_i
              @expires = format_date expiration_date
              if delta > 0
                flash[:info_messages] << "The invitation expires in #{delta} days."
              elsif delta == 0
                flash[:info_messages] << "The invitation expires today."
              else
                flash[:error_messages] << "The invitation expired #{delta * -1} days ago."
              end
            rescue StandardError => e
              puts e.message; puts e.backtrace
              flash[:error_messages] << "The submitted time could not be parsed.  Please use the calendar widget."
            end
          end
        end
        
        # Handle Roles
        params['invitation_code_roles'] ||= []
        params_role_ids = params['invitation_code_roles'].map(&:to_i)
        existing_role_ids = @invitation_code.roles_granted.map(&:id)
        new_role_ids = params_role_ids - existing_role_ids
        removed_role_ids = existing_role_ids - params_role_ids
        new_role_ids.each do |role_id|
          role = Gui_Builder_Profile::UserRole[role_id]
          @invitation_code.add_to_roles_granted role
          flash[:info_messages] << "The invitation now grants the #{role.name} role."
        end
        removed_role_ids.each do |role_id|
          role = Gui_Builder_Profile::UserRole[role_id]
          @invitation_code.remove_from_roles_granted role
          flash[:info_messages] << "The invitation no longer grants the #{role.name} role."
        end
        if flash[:error_messages].any?
          flash[:info_messages].clear
        else
          @invitation_code.save
          GuiDirector.commit_change_tracker
          # render_full("edit_invitation_code?invitation_code=#{@invitation_code.id}")
          raw_redirect("?invitation_code=#{@invitation_code.id}")
        end
      end
    end
    
    def format_date date
      date ? date.strftime("%B %-d, %Y") : nil
    end
  end
end