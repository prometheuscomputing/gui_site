module Gui
class GbUsersController < Gui::Controller

    def edit_regex
      params = request.safe_params
      pp params
      obj           = Gui_Builder_Profile::RegularExpressionCondition[params['regex'].to_i]
      @regex        = obj.regular_expression
      @description  = obj.description
      @message      = obj.failure_message
  
      if request.post?
        # flash[:info_messages]   = []
        # flash[:error_messages]  = []
        GuiDirector.start_change_tracker
        
        if params['regex_regex'].empty?
          flash[:error_messages] << "The 'Regular Expression' value may not be empty!"
        elsif @regex != params['regex_regex']
          obj.regular_expression = params['regex_regex']
          flash[:info_messages] << "The regular expression is now #{obj.regular_expression}."
        end
        
        if @description.nil? && params['regex_description'].empty?
          # Do nothing
        elsif @description != params['regex_description']
          obj.description = params['regex_description']
          flash[:info_messages] << "The description is now #{obj.description}."
        end
        
        if @message.nil? && params['regex_message'].empty?
          # Do nothing
        elsif @message != params['regex_message']
          obj.failure_message = params['regex_message']
          flash[:info_messages] << "The failure message is now #{obj.failure_message}."
        end
        obj.save
        GuiDirector.commit_change_tracker
      end
    end
    
    def format_date date
      date ? date.strftime("%B %-d, %Y") : nil
    end
  end
end