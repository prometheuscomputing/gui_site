=begin
TODO
- implement role based access
  - policies namespaced by role?
- lock down data_getter
- all the count methods that are used in the view have to go through scope as well
- implement special role for 'not logged in'...in which case current_user is nil
- find all places where controller is engaged in db operations
- handle collections
=end

module Gui
  class Controller < WebFrameworkController
    include Gui::PolicyMethods
    # attr_accessor :action_name # used by Pundit?  May not need...
    
    # Pundit needs #current_user
    def current_user
      # TODO use session[:user] or use request.env['ramaze.helper.user'] ?
      user #request.env['ramaze.helper.user'] || session[:user]
    end    
  end
end
