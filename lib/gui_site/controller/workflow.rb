module Gui
  class WorkflowController < Gui::Controller
    map '/workflow/'

    def public_page?
      false
    end

    #Pages

    def index
  
      #This will get all of the workflow classes items might need to be tweaked for performance reasons if the table gets too large
      #If strict role enforcement is on then only the items the currently logged in user can access are retrieved  
      @workflow_classes = Gui.option(:workflow_options)[:workflow_classes]
      
      if Gui.option(:workflow_options)[:strict_role_enforcement]
        role_names = user.roles.map { |role| [role.name.to_sym, role.name.to_s] }.flatten.compact
        # Code to select workflow items for a role exists in Application! Need to fix. -SD
        @all_workflow_list = @workflow_classes.map { |workflow_class| workflow_class.to_const.select_workflow_items_by_role role_names }.flatten
      else
        @all_workflow_list = @workflow_classes.map { |workflow_class| workflow_class.to_const.select.all }.flatten
      end  
    end
    
    # Summary note for SD's review of workflow component.
    # Much of the code for the workflow component is actually located in car_example.
    # In order to make the workflow component work generally, it will need to be relocated.
    # The best solution would be to create a Sequel workflow adapter project, and separate the gui_site related code from
    # the code required to access workflow information at the model level.
    #
    # For reference, the following methods are used in gui_site, but located in car_example:
    # From controller/worflowl.rb (this file)
    # get_state_roles
    # available_events
    # get_state_page_url
    # handle_state_transition(event_name, user_roles)
    # select_workflow_items_by_role(role_names)
    #
    # From workflow/index.haml
    # workflow_identifier
    # readable_state_name
    # readable_next_action
    
    #NOTE: Application-specific code does not belong here -SD
    #Test of custom workflow controller and ui
    def inspected_vehicle
      workflow_item_id = safe_params['workflow-item-id']
      workflow_class   = safe_params['workflow-item-classifier']
      @car = workflow_class.to_const[workflow_item_id] # <--- WTF?? FIXME
    end
    
    #Actions

    def start_a_new_workflow
      new_workflow_item = create_new_workflow_item
      redirect_to_first_workflow_page new_workflow_item
    end

    #TODO This could be done with more dynamically than a page refresh
    def advance_workflow_item
      workflow_item_id = safe_params['workflow-item-id']
      workflow_class   = safe_params['workflow-item-classifier'].to_const  
      event_name       = safe_params['event-name']
      
      role_enforcement = Gui.option(:workflow_options)[:strict_role_enforcement]
      user_roles = user.roles.map(&:name) if role_enforcement
      
      workflow_item = workflow_class[workflow_item_id]
      # state transition code seems to be located in application! Will have to rewrite. -SD
      workflow_item.handle_state_transition(event_name, user_roles)
      url = '/workflow'
      raw_redirect url
    end

    private

    def create_new_workflow_item
      #TODO make this work with the correct workflow item
      #TODO make this work with a selection of possible multiple workflow item types
      workflow_class = safe_params['workflow-item-classifier'].to_const
      ChangeTracker.start
      # new_workflow_item = Automotive::Car.new
      new_workflow_item = workflow_class.new
      new_workflow_item.save
      ChangeTracker.commit
      new_workflow_item.refresh
      new_workflow_item
    end

    def redirect_to_first_workflow_page workflow_object
      # URL generating code located in application! Not good. -SD
      url = workflow_object.get_state_page_url
      raw_redirect url
    end
    
    def display_available_actions workflow_object
      # code to retrieve available events located in application!!! Why? -SD
      available_events = workflow_object.available_events
      html_structure = ""
     
      #A bunch of unstructured event links
      html_structure << "<ul class='available-action-list'>"
      base_url = "/workflow/advance_workflow_item?workflow-item-id=#{workflow_object.id}&workflow-item-classifier=#{workflow_object.class}"
      available_events.each do |event_name, events|
        #Guess from the first event of all events with the same name
        event = events.first
        event_action_name = event.meta[:next_action_display_name] || event.name.to_s.titleize
        event_param = "&event-name=#{event_name}"
        html_structure << "<li><a href=#{base_url}#{event_param}>#{event_action_name}</a></li>"
      end
      html_structure << "</ul>"
      
      #TODO this would be a link or something else
      # if available_actions.count > 1
      #   html_structure = ""
      # else
      #   html_structure = "<a href = '#{workflow_object.get_state_page_url}'> #{workflow_object.readable_next_action} </a>"
      # end
      
      #{workflow_item.readable_next_action}
      
      #Some other UI structure if there is more than 1 currently a select box
      html_structure
    end
    
    def workflow_role_selection_box
      user_roles = user.roles
      role_names = user_roles.map(&:name)
      html_structure = "<select class='select_role_filter' id='select_role_filter' name='role_selection'>"
      html_structure << "<option value =''>All Available Roles</option>"
      role_names.each do |role_name|
        html_structure << "<option value='#{role_name}'>#{role_name.to_s.titleize}</option>"
      end
      html_structure << "</select>"
    end
    
    def workflow_role_data_attributes workflow_object
      # code to get state and role relationships is in application??? There's no way it can be reused if it's there. -SD
      roles = workflow_object.get_state_roles
      data_array = ""
      roles.each do |role|
        data_array << "#{role} "
      end
      data_array
    end
    
    def create_new_workflow_button
      if @workflow_classes.count > 1
        html_structure = "<button id='start_workflow_button' class='start_workflow'> Start A New Workflow</button>"
      else
        url = "/workflow/start_a_new_workflow?workflow-item-classifier=#{@workflow_classes.first}"
        html_structure = "<div class='start_new_workflow_single'><a href=#{url}>Start A New Workflow</a></div>"
      end
      html_structure
    end
    
    def create_new_workflow_list
      workflow_classes = Gui.option(:workflow_options)[:workflow_classes]
      workflow_list = "<ul class='workflow_item_list'>"
      workflow_classes.each do |workflow_class|

        workflow_list << "<a href='/workflow/start_a_new_workflow?workflow-item-classifier=#{workflow_class}'>"
        workflow_list << "<li class='hover' data-class=#{workflow_class}>#{workflow_class.to_const.to_title}</li>"
        workflow_list << "</a>"
      end
      workflow_list << "</ul>"
      html_structure = "<div id='new_workflow_item_container' class='workflow_item_container' tabindex='-1'>"
      html_structure << workflow_list
      html_structure << "</div>"
    end

    #Generic Way to return error messages can be changed if using ajax updating
    def workflow_error_message_text
      Ramaze::Current.session.flash[:workflow_error_message]
    end
    
  end
end