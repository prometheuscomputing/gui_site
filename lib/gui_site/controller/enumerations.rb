module Gui
  class EnumerationsManagementPage < Gui::AdHocPageObject
    domain_classes = Gui.option(:spec_module).to_const.top_level_modules.map { |m| m.to_const.classes(:no_imports => true) }.flatten.select { |c| c < ORM_Instance }
    enums      = domain_classes.select { |c| c.enumeration? }
    ENUMS_DATA = enums.map { |ec| Gui::EnumerationsManagementPage.add_methods(ec, {}) }
    GuiSpec.from_dsl_file(relative('enumerations_spec.rb'), {:spec => Gui.loaded_spec})
  end
  
  class GbEnumerationsController < Gui::AdHocPageController
    map '/gb_enumerations/'
    self.data_classifier = Gui::EnumerationsManagementPage
    def enumerations
      # Add Policy here
      return render_object(:page_title => 'Enumerations Management') if session[:is_admin]
      raise Gui::Error403.new("I'm sorry, you may not access the enumeration management page without administrative privileges.")
    end
  end # GbEnumerationsController
  
end # Gui
