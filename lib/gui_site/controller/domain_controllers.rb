module Gui
  def self.define_domain_controllers
    package_names = Gui.loaded_spec.covered_packages # Can we replace this by just doing ORM_Instance children? or something else?  Would like to get rid of #covered_packages.  Maybe at least extract to config in meta_info.rb.
    package_names << 'Gui_Builder_Profile' unless package_names.include?('Gui_Builder_Profile')
    packages = package_names.map { |pn| Object.const_get(pn) }
    define_domain_module_controllers(packages, proc { |mod| mod.is_a?(Class) })
  end
  
  # TODO remove if we decide not to do this
  def self.define_domain_package_controllers
    package_names = Gui.loaded_spec.covered_packages
    package_names << 'Gui_Builder_Profile' unless package_names.include?('Gui_Builder_Profile')
    packages = package_names.map { |pn| Object.const_get(pn) }
    define_domain_module_controllers(packages, proc { |mod| !mod.is_a?(Class) })
    # package_names.each do |pkg_name|
    #   pkg_cntrlr      = Class.new(Gui::MainController)
    #   pkg_cntrlr_name = pkg_name.gsub('::', '_') + "Controller"
    #   # TODO figure out why we are told that Gui_Builder_ProfileController is already defined here.  Gui_Builder_Profile is getting run through here twice.  It would just be nice to know how and why that is happening.
    #   Object.const_set(pkg_cntrlr_name, pkg_cntrlr)
    #   pkg_cntrlr.namespace = pkg_name.to_const
    #   url                  = Gui.route_for(pkg_name)
    #   pkg_cntrlr.map("/#{url}")
    # end
  end
  
  # In Ruby, Classes are Modules....
  # TODO should this be limited to only those things that are part of the domain?  e.g. remove things like ChangeTracker::UnitOfWork, ORM_Instance::Associations::ManyToOneAssociationReflection, and the Pundit related classes?  What about join table classes?  What about abstract classes?  What about classes that are supposed to be interfaces?
  # @test must be a proc that allows you to determine whether or not a controller is defined or not.  If the proc returns a truthy result then a controller will be created.
  def self.define_domain_module_controllers(modules, test = nil)
    test ||= proc {true}
    modules.each do |mod|
      case mod
      when Class
        # note that this doesn't create a controller for the class.  It only creates one for it's children.  The idea here is we're passing in something like ORM_Instance.  If you need to create a controller for an explicitly specified class, use #define_domain_module_controller
        mod.children.each do |child|
          define_domain_module_controller(child) if test.call(child)
        end
      when Module
        define_domain_module_controller(mod) if test.call(mod)
        mod.modules.each { |sub_mod| define_domain_module_controller(sub_mod) if test.call(sub_mod) }
        
        mod.classes.each do |klass|
          define_domain_module_controller(klass) if test.call(klass)
        end
      else
        raise "Cannot create a controller for #{mod.inspect} because it isn't an instance of Module."
      end
    end
  end

  def self.define_domain_module_controller(mod)
    superclass = mod.is_a?(Class) ? Gui::DomainObjController : Gui::DomainController
    name = mod.name.gsub('::', '_') + "Controller"
    return if Object.const_defined?(name) # do not create the same controller twice
    controller = Class.new(superclass)
    Object.const_set(name, controller)
    # FIXME this is patched up to keep working while still fiddling around.  Clean up after finished fiddling.
    controller.model = mod if controller.respond_to?(:model)
    controller.namespace = mod
    route = '/' + Gui.route_for(mod)
    # puts Rainbow("#{name} --> #{route}").cadetblue if Gui.option(:mode) == :dev
    controller.map(route)
  end
end
