# require 'uri'
# require 'pp'
# require 'json'
# require 'sequel'

module Gui
class ChangeTrackerController < Gui::Controller
  map '/change_tracker/'
  
  layout(Gui.option(:layout) || 'default') { !request.xhr? }
  
  # TODO: remove these -SD
  attr_accessor :domain_obj,
                :old_time,
                :new_time,
                :breadcrumbs

  def history(package_str, domain_obj_class_str, domain_obj_id_str)
    object_logger Logger.new(File.join(Gui.option(:log_dir), "change_tracker.log"))
    object_logger.log_warn!
    object_logger.datetime_format = '%r'
    log_info {"-" * 60}
    
    if request.safe_param('discard_and_go')
      flash[:prev_breadcrumbs] = flash[:current_breadcrumbs]
      go_to_url = request.safe_relative_url_param('go_to_url')
      puts "ChangeTrackerController: history: discard_and_go chosen. object not saved. Redirecting to new page: #{go_to_url}" if $verbose
      raw_redirect go_to_url
    end
    
    @package_str = package_str
    @domain_obj_class_str = domain_obj_class_str
    @domain_obj_id_str = domain_obj_id_str
    self.domain_obj = domain_obj_from_info(@package_str, @domain_obj_class_str, @domain_obj_id_str)
    
    add_breadcrumb_node(Gui::NavigationNode.from_domain_obj(self.domain_obj)) if current_breadcrumbs.empty?
    
    # Gracefully handle the case where there is no domain object
    if self.domain_obj.nil?
      puts "ChangeTrackerController: No domain obj found. Returning 404 error."
      raise Gui::Error500
    end
  end
  
  def timestamps
    unless request.xhr?
      puts "ChangeTrackerController: timestamps was called directly (not from ajax). Returning 404 error."
      #raw_redirect index
      error_404
    end
    @package_str = request.params['package']
    @domain_obj_class_str = request.params['domain_obj_class']
    @domain_obj_id_str = request.params['domain_obj_id']
    self.domain_obj = domain_obj_from_info(@package_str, @domain_obj_class_str, @domain_obj_id_str)
    
    # Parse timestamps
    timestamps = request.safe_param('timestamps').split('.').map do |time|
      if time == 'of_creation'
        nil
      else
        sec, usec = time.split(':')
        Time.at(sec.to_i, usec.to_i)
      end
    end.compact.sort
    
    if timestamps.length == 1
      # Only new_time was given
      self.old_time = nil
      self.new_time = timestamps[0]
    else
      self.old_time = timestamps[0]
      self.new_time = timestamps[1]
    end
  end
  
  def _format_time time
    time.strftime '%c'
  end
  
  private
  
  def domain_obj_from_info(package_str, domain_obj_class_str, domain_obj_id_str, domain_obj_getter = nil)
    #raise 'Bad package specified' unless package_str && !package_str.empty?
    if package_str && !package_str.empty?
      namespace = package_str.to_camelcase.to_const # this should raise anyway if there is no constant
      raise "Can't get namespace from #{package_str} -- #{package_str.to_camelcase} using String#to_const" unless namespace
    else
      namespace = Object
    end
    raise "Bad domain_obj_class specified: '#{domain_obj_class_str}'" unless domain_obj_class_str && !domain_obj_class_str.empty?
    domain_obj_class = domain_obj_class_str.to_const(namespace)
    
    if domain_obj_class == Gui.loaded_spec.default_classifier
      base_domain_obj = Gui.loaded_spec.default_domain_object
    else
      if domain_obj_id_str && !domain_obj_id_str.empty?
        domain_obj_id = domain_obj_id_str.to_i
        base_domain_obj = domain_obj_class[domain_obj_id]
      else # No domain obj id (new obj)
        # Associations are added to a fake object that is (hopefully) never saved.
        # The associations are then applied to the correct object when the form is submitted
        base_domain_obj = domain_obj_class.new
      end
    end
    
    # If data_getter is specified, then return the value of that association (provided that it is a to-one association)
    if domain_obj_getter && !domain_obj_getter.empty?
      getter = domain_obj_getter.to_sym
      getter_assoc_info = domain_obj_class.associations[getter]
      raise "Invalid getter '#{domain_obj_getter}' specified for class #{domain_obj_class}" unless getter_assoc_info
      raise "Specified getter '#{domain_obj_getter}' has multiplicity and can't be used as a data getter" unless getter_assoc_info[:type].to_s =~ /to_one$/
      domain_obj = base_domain_obj.send(getter)
    else
      domain_obj = base_domain_obj
    end
    # Patching possibility of URL manipulation to view restricted classes by non-admins
    # FIXME use policy here instead of admin_restricted_classes
    raise Gui::Error403 if !session[:is_admin] && Gui.admin_restricted_classes.include?(domain_obj.class)
    domain_obj
  end
  
  # Returns an array of all the attribute changes on the current domain object
  # b/w timestamps
  # The array is in the following format:
  #     attribute_changes[n] = { :attribute => attr_name, :old_value => val, :new_value => val, 
  #                                       :richtext => bool_val }
  #
  # :attribute is the name of the attribute
  # :old_value is the value of the attribute at the older timestamp
  # :new_value is the value of the attribute at the newer timestamp
  # :richtext is a boolean stating whether these values are richtext, therefore
  # =>  should be shown in the diff viewer
  def attributes
    unless self.new_time.is_a?(Time)
      raise "There must be 2 timestamps to get the changes"
    end

    changes = []
    # Get simple attribute changes
    history = self.domain_obj.ct_history
    # Currently the page is handing us nil as the old_time if it needs the oldest available state
    # Convert this to a timestamp (if creation was tracked)
    self.old_time ||= versions_info.first[:version]
    old_obj = history.version_at(self.old_time)
    raise "Got nil object for old_obj at time: #{self.old_time}" unless old_obj
    new_obj = history.version_at(self.new_time)
    raise "Got nil object for new_obj at time: #{self.new_time}" unless new_obj
    properties_to_show(self.domain_obj).each do |column, info|
      # Not showing association changes for now
      next unless info[:attribute]
      # Filter out unchanged columns (and columns that have changed from nil to '' and vice versa)
      next if old_obj[column].to_s == new_obj[column].to_s
      changes << {
        :attribute => info[:display_name] || info[:getter].to_title,
        :old_value => old_obj[column],
        :new_value => new_obj[column],
        :richtext => info[:complex] && info[:class] == "Gui_Builder_Profile::RichText"}
    end
    
    changes
  end
  
  # Returns an array of version information (timestamps & labels) corresponding to the available versions of
  # the current domain object.  The timestamps are objects of Time
  def versions_info
    if self.domain_obj.respond_to?(:ct_history)
      versions_info = []
      changes = self.domain_obj.ct_history.changes
      uows = self.domain_obj.ct_history.units_of_work
      # NOTE: sometimes the Lifecycle change is not the first change, and this is calculated incorrectly.
      # tracked_at_creation = changes.first.is_a?(ChangeTracker::ChangeLifecycle)
      # If creation wasn't tracked add in a fake version that can be compared against.
      versions_info << {:version => Time.now, :columns_modified => []} unless uows.any?
      properties = properties_to_show(self.domain_obj)
      uows.each do |uow, _|
        info = {:version => uow.completion_time, :author => uow.author}
        version_changes = changes.select { |c| c[:completion_time] == uow.completion_time }
        attribute_changes, _ = version_changes.partition { |c| c.is_a?(ChangeTracker::ChangeAttribute) }
        association_changes, _ = version_changes.partition { |c| c.is_a?(ChangeTracker::ChangeAssociation) }
        modified_properties = []
        attribute_changes.each do |c|
          attr_info = properties[c.attribute_name.to_sym]
          next unless attr_info # Skip any attributes not included in filtered list of properties
          next if c.old_value.to_s == c.new_value.to_s # Skip changes where the change is from nil to ''
          # Newer models will have :display_name, convert getter otherwise.
          # String#to_title is defined in SSA -- I think. Need to unify usage of title methods
          # As it stands, there are relatively similar 'title' methods in modelDriven_builder, magicdraw_extensions, gui_director, gui_site, and SSA -SD
          modified_properties << attr_info[:display_name] || attr_info[:getter].to_title
        end
        association_changes.each do |c|
          assoc_info = properties[c.assoc_info_for(self.domain_obj)[:getter]]
          next unless assoc_info # Skip any associations not included in filtered list of properties
          modified_properties << assoc_info[:display_name] || assoc_info[:getter].to_title
        end
        next unless modified_properties.any?
        info[:columns_modified] = modified_properties.uniq
        versions_info << info
      end
      versions_info
    else
      raise "Invalid domain object #{self.domain_obj}"
    end
  end
  
  # When the object was created
  # Returns the Time when the object was created
  def time_created
    self.domain_obj.ct_history.versions.first
  end
    
  def attributes_to_show(domain_obj)
    domain_obj.class.attributes.select do |name, info|
      (name != :id) &&
      (name.to_s !~ /_id$/) &&
      (name.to_s !~ /_class$/) &&
      (!info[:complex] || info[:class] == "Gui_Builder_Profile::RichText")
    end
  end
  
  # TODO: This information should be model-driven. A complex type should identify which properties are important for comparison.
  def columns_to_show(domain_obj)
    columns_info = {}
    attributes_to_show(domain_obj).each do |_, info|
      c = if info[:complex]
        if info[:class] == "Gui_Builder_Profile::RichText"
          (info[:column].to_s + '_content').to_sym
        elsif info[:class] == "Gui_Builder_Profile::File"
          (info[:column].to_s + '_filename').to_sym
        else # Unknown complex type.
          info[:column]
        end
      else
        info[:column]
      end
      columns_info[c] = info
    end
    columns_info
  end
  
  def properties_to_show(domain_obj)
    props = {}
    all_properties = domain_obj.class.properties
    all_properties.each do |name, info|
      # Skip associations that are for complex attributes
      next if info[:for_complex_attribute]
      if info[:complex]
        case info[:class]
        when "Gui_Builder_Profile::RichText"
          getter = (name.to_s + '_content').to_sym
          props[getter] = info
        when "Gui_Builder_Profile::File"
          getter = (name.to_s + '_filename').to_sym
          props[getter] = info
        else
          # Unknown complex attribute.
          props[name] = info
        end
      else
        props[name] = info
      end
    end
    props
  end
end # ChangeTrackerController
end # Gui
