module Gui
  class MainController < Gui::Controller
    # Include concurrency module
    include Gui::Concurrency
  
    map '/'
  
    # Set it up where the subclasses of this class can get to the views.
    map_views(".")
  
    #set_layout_except 'default' => [:upload, :file_list, :markdown_preview]
    layout(Gui.option(:layout) || 'default') do |path|
      !(request.xhr? || [:upload, :file_list, :populate_collection, :collection_button].include?(path.to_sym))
    end
    engine :Haml

    # Ramaze::Route['/'] = '/gui' # This way doesn't allow parameters to be passed <--- Well, then use a regex or proc...duh. See Ramaze documentation
    trait :separate_default_action => true # Means that the default action can't be reached by its action name. This means that the name of the action won't be taken as a parameter.
    trait :default_action_name => "gui"
  
    # Set public_page? to false for this controller
    # This means that login is required for actions within this controller
    def public_page?; false; end
  end # MainController
end # Gui
