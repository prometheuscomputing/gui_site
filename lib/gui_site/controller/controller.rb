require 'ruby-prof' if ENV['PERFORMANCE_PROFILE']
require_relative 'controller_config'
module Gui
  class Controller < WebFrameworkController
    # [GuiServerConfiguration] configuration settings for the server
    attr_accessor :config
    
    def safe_params
      @safe_params ||= request.safe_params
    end
    
    def default_url
      Gui.option(:home_page)[:url]
    end
    
    # FIXME If there is a list of pages that should not show a homepage link then it should be something can be configured instead of just this hard-coded list.
    def homeless_urls
      [default_url, '/gb_users/login', '/gb_users/register']
    end
    
    # Run hooks that normally occur after an action completes since respond skips those
    # Currently only running the after_all hook
    def respond_with_hooks(body, status = 200, header = {})
      _after_all
      respond(body, status, header)
    end
    
    def start_profiler
      profiler = RubyProf::Profile.new(:include_threads => [Thread.current])
      puts "Starting profile for #{self.request.fullpath} in Thread: #{Thread.current.object_id} for request: #{self.request.object_id}"
      Thread.current[:request_profilers] ||= {}
      Thread.current[:request_profilers][self.request.object_id] = profiler
      Thread.current[:request_profilers][self.request.object_id.to_s + 'time'] = Time.now
      profiler.start
    end
    
    def stop_profiler
      tmp_dir = File.expand_path(File.join(__dir__, '../../../tmp'))
      if ENV['PERFORMANCE_PROFILE']
        time_taken = Time.now - Thread.current[:request_profilers][self.request.object_id.to_s + 'time']
        print "Stopping profile for #{self.request.fullpath} in Thread: #{Thread.current.object_id} for request: #{self.request.object_id} (#{time_taken})..."
        output_prefix = self.request.fullpath.tr('/', '_')  + '-'
        output_prefix += safe_params['data_classifier'] + '-' if safe_params['data_classifier']
        output_prefix += safe_params['data_id'] + '-'         if safe_params['data_id']
        output_prefix += safe_params['data_getter'] + '-'     if safe_params['data_getter']
        existing_files = Dir["#{tmp_dir}/#{output_prefix}[0-9]*"]
        if existing_files.any?
          highest_num = existing_files.map { |f| f.match(/^.*\-(\d+)$/)[1].to_i }.max
          next_num = highest_num + 1
        else
          next_num = 1
        end
        output_dir_name = output_prefix + next_num.to_s
        output_dir = File.join(tmp_dir, output_dir_name)
        FileUtils.mkdir_p output_dir
        # Print time_taken to filename in output dir
        FileUtils.touch File.join(output_dir, time_taken.to_s + '.txt')
        print " ... "
        result = Thread.current[:request_profilers][self.request.object_id].stop
        puts " stopped."
        Thread.current[:request_profilers].delete(self.request.object_id)
        print "Printing results for #{self.request.fullpath} in Thread: #{Thread.current.object_id} ..."
        # Flat text printer
        # printer = RubyProf::FlatPrinter.new(result)
        # File.open File.join(output_dir, "flat.txt"), 'w' do |file|
        #   printer.print(file, :min_percent => 0.1)
        # end

        # Save a callgrind trace
        # You can load this up in kcachegrind or a compatible tool.
        printer = RubyProf::CallTreePrinter.new(result)
        calltree_dir = File.join(output_dir, 'calltree')
        FileUtils.mkdir_p(calltree_dir)
        printer.print(:path => calltree_dir, :profile => '')
    
        printer = RubyProf::GraphHtmlPrinter.new(result)
        File.open File.join(output_dir, "graph.html"), 'w' do |file|
          printer.print(file)
        end

        printer = RubyProf::CallStackPrinter.new(result)
        File.open File.join(output_dir, "callstack.html"), 'w' do |file|
          printer.print(file)
        end
        
        puts " printed."
      end
    end
        
  end # Controller
end # Gui
