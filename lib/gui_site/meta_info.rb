# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module GuiSite
  # For more information about meta_info.rb, please see project Foundation, lib/Foundation/meta_info.rb
  
  # Required
  GEM_NAME = 'gui_site'
  VERSION  = '8.2.0'
  AUTHORS  = ["Samuel Dana", "Ben Dana", "Michael Faughn"]
  SUMMARY  = %q{Starts a webserver based on a given model/view, adding user accounts and other features.}
  HOMEPAGE = 'https://gitlab.com/prometheuscomputing/gui_site'
  # Optional
  EMAILS      = ["s.dana@prometheuscomputing.com", "m.faughn@prometheuscomputing.com"]
  DESCRIPTION = SUMMARY

  # Required
  LANGUAGE    = :ruby
  LANGUAGE_VERSION = ['>= 2.7']
  RUNTIME_VERSIONS = { :mri => ['>= 2.7'] }

  DEPENDENCIES_RUBY = {
    'lodepath'                => '~> 0.1',
    'puma'                    => '~> 5.2',
    'tilt'                    => '~> 2.0',
    'blankslate'              => '~> 3.1',
    'rake'                    => '~> 0.8.7',
    'ramaze'                  => '>= 2015.10.28', 
    'common'                  => '~> 1.11',
    'indentation'             => '~> 0.1',
    'gui_director'            => '~> 7.0',
    'html_gui_builder'        => '~> 8.0',
    'workflow'                => '~> 1.2',
    'workflow_sequel_adapter' => '~> 0.0',
    'prometheus_mathjax'      => '~> 0.0',
    'prometheus_ckeditor'     => '~> 4.14',
    'mail'                    => '~> 2.6'
  }
  DEPENDENCIES_MRI = {
  }
  DEPENDENCIES_JRUBY = {
    'mizuno' => '',
    'json_pure' => '',
    #'jdbc-sqlite3' => '',
  }

  # Previous development dependencies were for testing, so they have been moved into car_example
  DEVELOPMENT_DEPENDENCIES_RUBY = {
  }
end