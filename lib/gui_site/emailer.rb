module Gui
  module Emailer
    def send_email
      Gui::Emailer.send_email(email_address, body, subject)
    end
    
    def self.send_email(email_address, body, subject)
      return nil if email_address.nil? || email_address.empty? || Gui.option(:disable_send_email)
      mail = Mail::Message.new
      # mail.delivery_method :smtp, {address: 'localhost', port: 1025, authentication: 'plain'}
    
      # TODO Check authentication types and determine if plain is acceptable
      # The mail will be sent using a gmail mail server and the account no_reply@prometheuscomputing.com
      options = {:address        => Gui.option(:emailer_address)  || "smtp.gmail.com",
                 :port           => Gui.option(:emailer_port)     || 587,
                 :domain         => Gui.option(:emailer_domain)   || "prometheuscomputing.com",
                 :user_name      => Gui.option(:emailer_sender)   || "no_reply@prometheuscomputing.com",
                 :password       => Gui.option(:emailer_password) || "kip-did-aw-ayt-7d-g)",
                 :authentication => Gui.option(:emailer_auth)     || 'plain',
                 :enable_starttls_auto => true }
    
    
      # Testing Code for email so that the Cucumber tests don't spam the real server
      # Requires mailcatcher Ruby gem for Cucumber tests
      test_smtp_port = MAILCATCHER_SMTP_PORT if defined?(MAILCATCHER_SMTP_PORT)
      test_options = { :address => 'localhost', :port => test_smtp_port, :authentication => 'plain' }
      testing_mode = Gui.option(:testing_mode)
      testing_mode ? mail.delivery_method(:smtp, test_options) : mail.delivery_method(:smtp, options)
      # Basic email parameters before it is sent using the mail gem
      mail.to           = email_address
      mail.body         = body
      mail.subject      = subject
      mail.content_type = "text/html; charset=UTF-8"
      mail.from         = "no_reply@prometheuscomputing.com"
      begin
        mail.deliver
      rescue StandardError => e
        puts "Error sending email: #{e}"
      end
    end
  end
end