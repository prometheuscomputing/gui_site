module Ramaze
  module View
    ##
    # View adapter that allows views to use Haml, for more information see the
    # following website: http://haml-lang.com/
    #
    module Haml
      def self.call(action, string)
        options = action.options

        if haml_options = action.instance.ancestral_trait[:haml_options]
          options = options.merge(haml_options)
        end

        action.options[:filename] = (action.view || '(haml)')
        # PATCH IS HERE
        # Remove these options because they are deprecated and Temple doesn't want to see them anymore.
        haml = View.compile(string) { |s| ::Haml::Engine.new(s, options.reject { |k,_| [:is_layout, :needs_method, :content_type].include?(k) }) }
        html = haml.to_html(action.instance, action.variables)

        return html, 'text/html'
      end
    end # Haml
  end # View
end # Ramaze

innate_action_class = Innate.const_get(Innate.constants.find { |c| c == :Action })
innate_action_class.define_method(:wrap_in_layout) do |&block|  
  # This method originally used a deprecated (Ruby 2.x), then illegal (Ruby 3.x) usage of &Proc.new
  layout ? dup.render_in_layout(&block) : block&.call
end
