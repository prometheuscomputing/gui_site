#!/usr/bin/env rackup
#
# config.ru for ramaze apps
# use thin >= 1.0.0
# thin start -R config.ru
#
# rackup is a useful tool for running Rack applications, which uses the
# Rack::Builder DSL to configure middleware and build up applications easily.
#
# rackup automatically figures out the environment it is run in, and runs your
# application as FastCGI, CGI, or standalone with Mongrel or WEBrick -- all from
# the same configuration.
#
# Do not set the adapter.handler in here, it will be ignored.
# You can choose the adapter like `ramaze start -s mongrel` or set it in the
# 'start.rb' and use `ruby start.rb` instead.

# Fix newer versions of rack for ruby 1.9.1
if RUBY_VERSION == '1.9.1'
  module URI
    WFKV_ = nil # This dummy value will be replaced by rack when ramaze is required
  end
  require 'rack/backports/uri/common_18'
end

require 'rainbow'
require 'common/object_logger'
require 'ramaze'
require_relative 'ramaze_patch'
require 'gui_director'
require 'html_gui_builder'

puts Rainbow(Gui.option(:mode).inspect).green 

# =============================
# = Set up Ramaze application =
# =============================
require_relative 'app'

# 'start' Ramaze. Doesn't actually start until 'run Ramaze' is called.
Ramaze.start(:root => Ramaze.options.roots, :adapter => :puma, :started => true)

# middleware hack
suppress_rack_log = proc {
  module Rack
    class CommonLogger
      def call(env)
        # do nothing
        @app.call(env)
      end
    end
  end
}

# =========================
# = Initialize Rack stack =
# =========================
if $suppress_output
  $verbose = false
  $quiet = true # Turns off all Ramaze logging if true
  $DEBUG = false
  $debug = false
  suppress_rack_log.call
else
  $verbose = true
  $quiet = false # Turns off all Ramaze logging if true
end
suppress_rack_log.call if Gui.option(:suppress_rack_log)
# Catch requests for things in the public folder before they go to RuoteKit
# RuoteKit (or the Sinatra framework it uses) strips the extension off the requests,
# which keeps them from being served properly.
#use Rack::Static, :urls => ['/css', '/js', '/favicon.ico', '/editor', '/images'], :root => ["#{app_root}/public"]

run Ramaze
