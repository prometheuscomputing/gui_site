module Gui
  # FIXME for all of these error methods / haml.  Stop setting @var and use locals instead!!
  class Controller < WebFrameworkController
    def error_403
      @message = flash[:error403]
      response.status = 403
    end
    
    # FIXME this method is often called directly.  Instead, an Error404 should be raised and let Rack pick it up and direct us to here.
    def error_404(message = nil)
      @url = request.env[Rack::RouteExceptions::PATH_INFO]
      @message = message
      response.status = 404
    end
    
    def error_405
      @message = flash[:error405]
      response.status = 405
    end
  
    # This is called when an unexpected stack trace occurs. This most often occurs in the case of a malformed url anyway,
    # and is more informative to the user than the default "Internal Server Errror"
    def error_500
      path  = request.env[Rack::RouteExceptions::PATH_INFO]
      error = request.env[Rack::RouteExceptions::EXCEPTION]
      @key  = get_error_key(error.backtrace)
      @message  = "\nRef: #{@key}\n"
      @message << "At: #{Time.now}\n"
      @message << "URL: #{path}\n"
      @message << "User ID: #{Ramaze::Current.session[:user].id}\n"
      @message << "From: #{request.env['REMOTE_ADDR']}\n"
      @message << "--------------------------------------------------------\n"
      unless Gui.unevaluated_options[:concise_errors] && Gui.option(:mode) == :dev
        unless request.params&.empty?
          @message << "Params:\n"
          parameters = request.params.dup
          parameters.delete('password')
          parameters.delete('reenter_password')
          @message << "#{parameters.pretty_inspect}\n"
          @message << "--------------------------------------------------------\n"
        end
      end
      @message << flash[:error500] + "\n" if flash[:error500]
      @message << error.message + "\n"
      error.backtrace.each_with_index do |line, i|
        @message << sprintf("%2d: %s\n", i, line)
      end
      log_error @message
      if request.xhr?
        template_path = File.join(__dir__, 'view/ajax_error.haml')
        html = Haml::Engine.new(File.read(template_path), :filename => template_path).render(self, :key => @key)
        puts html; puts
        respond_with_hooks(html, 500)
      end
      raw_redirect(default_url) if false #was_logging_in(error.backtrace) # <--- that method never did anything, always returned nil no matter what.  Leaving this here because maybe there was some unfinished intention and good reason.  Think about this later.  TODO
      response.status = 500
    end
  
    # "Not Implemented" Error
    # TODO have this do something different than 500 error
    def error_501
      flash[:error500] = flash[:error501]
      error_500
    end
    
    # This method consumes an array that is the lines of a stack trace, collects the lines from the top of the stack until it reaches code that is not from Prometheus' code base, joins them into a string, and returns a string that should give us a way to identify the error in the log and that the user can reference when discussing the matter with developers / support. Note: if the first lines are not from Prometheus code it will attempt to find some that are.  This isn't likely to be perfect, just a best effort.
    def get_error_key(trace)
      str = ''
      begun    = false
      finished = false
      trace.each do |line|
        if line.match?(/gui_|sequel_/)
          begun = true
          str << line
        else
          finished = true if begun
        end
        break if finished
      end
      str = trace.join if str.empty? # we failed to find what we were looking for so use the entire stack trace
      Digest::SHA256.bubblebabble(str)[0..10]
    end
    private :get_error_key
  end
  
  class Error403 < StandardError
    def initialize(msg = nil)
      Ramaze::Current.session.flash[:error403] = msg || "I'm sorry, you don't have permission to do that."
    end
  end
  class Error404 < StandardError
    def initialize(msg = nil)
      Ramaze::Current.session.flash[:error404] = msg if msg # Currently any msg isn't passed to the browser
    end
  end
  class Error405 < StandardError
    def initialize(msg = nil)
      Ramaze::Current.session.flash[:error405] = msg  || "I'm sorry, that action isn't possible."
    end
  end
  class Error500 < StandardError
    def initialize(msg = nil)
      Ramaze::Current.session.flash[:error500] = msg if msg
    end
  end
  class Error501 < StandardError
    def initialize(msg = nil)
      Ramaze::Current.session.flash[:error501] = msg if msg
    end
  end
end
